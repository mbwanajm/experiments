	var fbAppId = '1670558763159469';

	// This check is just here to make sure you set your app ID. You don't
	// need to use it in production. 
	// if (fbAppId === '1670558763159469') {
	// alert('Darproperty Facebook Connect');
	// }

	/*
	 * This is boilerplate code that is used to initialize
	 * the Facebook JS SDK.  You would normally set your
	 * App ID in this code.
	 */
	window.fbAsyncInit = function() {
		FB.init({
			appId : '1670558763159469', // App ID
			status : true, // check login status
			cookie : true, // enable cookies to allow the
			// server to access the session
			xfbml : true, // parse page for xfbml or html5
			// social plugins like login button below
			version : 'v2.5', // Specify an API version
		});

		// Put additional init code here
	};

	// Load the SDK Asynchronously
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) {
			return;
		}
		js = d.createElement(s);
		js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));