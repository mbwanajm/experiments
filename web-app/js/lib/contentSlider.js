/**
 * Created by DAR PROPERTY on 3/1/2016.
 */
$(document).ready(function() {
    $("#content-slider").lightSlider({
        loop:true,
        item:2,
        speed:500,
        keyPress:true
    });
    $(".featured-slider").lightSlider({
        loop:true,
        item:3,
        speed:500,
        keyPress:true
    });
    $("#latest-slider").lightSlider({
        loop:true,
        item:3,
        speed:500,
        keyPress:true

    });
    $('#image-gallery1').lightSlider({
        gallery:true,
        item:1,
        thumbItem:9,
        slideMargin: 0,
        speed:500,
        auto:true,
        loop:true,
        onSliderLoad: function() {
            $('#image-gallery1').removeClass('cS-hidden');
        }
    });
});
