#
# Callback to be called when ajax region reload is complete
#
updateRegions = (regions, textStatus, xhr) ->
   content = '<option value="any">select region</option>'
   regions.sort()
   for region in regions
      content += "<option value='#{region}'>#{region}</option>"

   $('#property_region').html content

#
# Register for on change event on region selector
#
$ ->
   $('#property_country').change ->
      $.getJSON '/country/getRegionsForCountry', {'name': $(this).val()}, updateRegions

