#
# Callback to be called when ajax region reload is complete
#
updateAreas = (areas, textStatus, xhr) ->
  content = '<option value="any">select area</option>'
  areas.sort()
  for area in areas
    content += "<option value='#{area}'>#{area}</option>"

  $('#property_areas').html content

#
# Register for on change event on region selector
#
$ ->
  $('#property_region').change ->
    $.getJSON '/region/getAreasForRegion', {'name': $(this).val()}, updateAreas

