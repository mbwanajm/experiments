photoCount = 0 # keeps track of the photos
latestPhoto = '' #contains id of the element of last added photo

# generates html content that renders an upload file component
getPhotoHtml = ->
  photoCount += 1
  latestPhoto = "photo#{photoCount}"
  """
  <div class='control-group'>
    <label class='control-label'>photo #{photoCount}</label>
    <div class='controls'>
      <input name='#{latestPhoto}' id='#{latestPhoto}' type='file'/>
      </div>
  </div>
  """
$ ->
  # append a new file upload element
  $('#property_add_photo').click ->
    $('#property_extra_photos').append getPhotoHtml()

    # animate the showing of the new photo
    $("#{latestPhoto}")
    .hide()
    .fadeIn(800)

  # Hide room numbers when type is plot or warehouse
  $('#property_type').change ->
    if(this.value is 'plot' or this.value is 'warehouse')
      $('.property_number_of_rooms').fadeOut()
    else
      $('.property_number_of_rooms').fadeIn()


