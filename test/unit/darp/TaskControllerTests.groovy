package darp



import grails.test.mixin.*

import com.mbwanajm.darproperty.TaskController

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(TaskController)
class TaskControllerTests {

    void testSomething() {
        fail "Implement me"
    }
}
