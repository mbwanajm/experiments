package com.mbwanajm

import com.mbwanajm.darproperty.UserRoleController
import grails.test.mixin.*

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(UserRoleController)
class UserRoleControllerSpec {

    void testSomething() {
        fail "Implement me"
    }
}
