package com.mbwanajm.darproperty



import org.junit.*
import grails.test.mixin.*

@TestFor(Featured_propertyController)
@Mock(Featured_property)
class Featured_propertyControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/featured_property/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.featured_propertyInstanceList.size() == 0
        assert model.featured_propertyInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.featured_propertyInstance != null
    }

    void testSave() {
        controller.save()

        assert model.featured_propertyInstance != null
        assert view == '/featured_property/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/featured_property/show/1'
        assert controller.flash.message != null
        assert Featured_property.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/featured_property/list'

        populateValidParams(params)
        def featured_property = new Featured_property(params)

        assert featured_property.save() != null

        params.id = featured_property.id

        def model = controller.show()

        assert model.featured_propertyInstance == featured_property
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/featured_property/list'

        populateValidParams(params)
        def featured_property = new Featured_property(params)

        assert featured_property.save() != null

        params.id = featured_property.id

        def model = controller.edit()

        assert model.featured_propertyInstance == featured_property
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/featured_property/list'

        response.reset()

        populateValidParams(params)
        def featured_property = new Featured_property(params)

        assert featured_property.save() != null

        // test invalid parameters in update
        params.id = featured_property.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/featured_property/edit"
        assert model.featured_propertyInstance != null

        featured_property.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/featured_property/show/$featured_property.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        featured_property.clearErrors()

        populateValidParams(params)
        params.id = featured_property.id
        params.version = -1
        controller.update()

        assert view == "/featured_property/edit"
        assert model.featured_propertyInstance != null
        assert model.featured_propertyInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/featured_property/list'

        response.reset()

        populateValidParams(params)
        def featured_property = new Featured_property(params)

        assert featured_property.save() != null
        assert Featured_property.count() == 1

        params.id = featured_property.id

        controller.delete()

        assert Featured_property.count() == 0
        assert Featured_property.get(featured_property.id) == null
        assert response.redirectedUrl == '/featured_property/list'
    }
}
