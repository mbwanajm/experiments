package com.mbwanajm.darproperty



import org.junit.*
import grails.test.mixin.*

@TestFor(References_tableController)
@Mock(References_table)
class References_tableControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/references_table/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.references_tableInstanceList.size() == 0
        assert model.references_tableInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.references_tableInstance != null
    }

    void testSave() {
        controller.save()

        assert model.references_tableInstance != null
        assert view == '/references_table/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/references_table/show/1'
        assert controller.flash.message != null
        assert References_table.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/references_table/list'

        populateValidParams(params)
        def references_table = new References_table(params)

        assert references_table.save() != null

        params.id = references_table.id

        def model = controller.show()

        assert model.references_tableInstance == references_table
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/references_table/list'

        populateValidParams(params)
        def references_table = new References_table(params)

        assert references_table.save() != null

        params.id = references_table.id

        def model = controller.edit()

        assert model.references_tableInstance == references_table
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/references_table/list'

        response.reset()

        populateValidParams(params)
        def references_table = new References_table(params)

        assert references_table.save() != null

        // test invalid parameters in update
        params.id = references_table.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/references_table/edit"
        assert model.references_tableInstance != null

        references_table.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/references_table/show/$references_table.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        references_table.clearErrors()

        populateValidParams(params)
        params.id = references_table.id
        params.version = -1
        controller.update()

        assert view == "/references_table/edit"
        assert model.references_tableInstance != null
        assert model.references_tableInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/references_table/list'

        response.reset()

        populateValidParams(params)
        def references_table = new References_table(params)

        assert references_table.save() != null
        assert References_table.count() == 1

        params.id = references_table.id

        controller.delete()

        assert References_table.count() == 0
        assert References_table.get(references_table.id) == null
        assert response.redirectedUrl == '/references_table/list'
    }
}
