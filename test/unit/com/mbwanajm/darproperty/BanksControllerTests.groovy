package com.mbwanajm.darproperty



import org.junit.*
import grails.test.mixin.*

@TestFor(BanksController)
@Mock(Banks)
class BanksControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/banks/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.banksInstanceList.size() == 0
        assert model.banksInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.banksInstance != null
    }

    void testSave() {
        controller.save()

        assert model.banksInstance != null
        assert view == '/banks/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/banks/show/1'
        assert controller.flash.message != null
        assert Banks.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/banks/list'

        populateValidParams(params)
        def banks = new Banks(params)

        assert banks.save() != null

        params.id = banks.id

        def model = controller.show()

        assert model.banksInstance == banks
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/banks/list'

        populateValidParams(params)
        def banks = new Banks(params)

        assert banks.save() != null

        params.id = banks.id

        def model = controller.edit()

        assert model.banksInstance == banks
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/banks/list'

        response.reset()

        populateValidParams(params)
        def banks = new Banks(params)

        assert banks.save() != null

        // test invalid parameters in update
        params.id = banks.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/banks/edit"
        assert model.banksInstance != null

        banks.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/banks/show/$banks.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        banks.clearErrors()

        populateValidParams(params)
        params.id = banks.id
        params.version = -1
        controller.update()

        assert view == "/banks/edit"
        assert model.banksInstance != null
        assert model.banksInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/banks/list'

        response.reset()

        populateValidParams(params)
        def banks = new Banks(params)

        assert banks.save() != null
        assert Banks.count() == 1

        params.id = banks.id

        controller.delete()

        assert Banks.count() == 0
        assert Banks.get(banks.id) == null
        assert response.redirectedUrl == '/banks/list'
    }
}
