package com.mbwanajm.darproperty



import org.junit.*
import grails.test.mixin.*

@TestFor(MonthlyIssueController)
@Mock(MonthlyIssue)
class MonthlyIssueControllerTests {


    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/monthlyIssue/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.monthlyIssueInstanceList.size() == 0
        assert model.monthlyIssueInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.monthlyIssueInstance != null
    }

    void testSave() {
        controller.save()

        assert model.monthlyIssueInstance != null
        assert view == '/monthlyIssue/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/monthlyIssue/show/1'
        assert controller.flash.message != null
        assert MonthlyIssue.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/monthlyIssue/list'


        populateValidParams(params)
        def monthlyIssue = new MonthlyIssue(params)

        assert monthlyIssue.save() != null

        params.id = monthlyIssue.id

        def model = controller.show()

        assert model.monthlyIssueInstance == monthlyIssue
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/monthlyIssue/list'


        populateValidParams(params)
        def monthlyIssue = new MonthlyIssue(params)

        assert monthlyIssue.save() != null

        params.id = monthlyIssue.id

        def model = controller.edit()

        assert model.monthlyIssueInstance == monthlyIssue
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/monthlyIssue/list'

        response.reset()


        populateValidParams(params)
        def monthlyIssue = new MonthlyIssue(params)

        assert monthlyIssue.save() != null

        // test invalid parameters in update
        params.id = monthlyIssue.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/monthlyIssue/edit"
        assert model.monthlyIssueInstance != null

        monthlyIssue.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/monthlyIssue/show/$monthlyIssue.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        monthlyIssue.clearErrors()

        populateValidParams(params)
        params.id = monthlyIssue.id
        params.version = -1
        controller.update()

        assert view == "/monthlyIssue/edit"
        assert model.monthlyIssueInstance != null
        assert model.monthlyIssueInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/monthlyIssue/list'

        response.reset()

        populateValidParams(params)
        def monthlyIssue = new MonthlyIssue(params)

        assert monthlyIssue.save() != null
        assert MonthlyIssue.count() == 1

        params.id = monthlyIssue.id

        controller.delete()

        assert MonthlyIssue.count() == 0
        assert MonthlyIssue.get(monthlyIssue.id) == null
        assert response.redirectedUrl == '/monthlyIssue/list'
    }
}
