package com.mbwanajm.darproperty



import org.junit.*
import grails.test.mixin.*

@TestFor(ContactsController)
@Mock(Contacts)
class ContactsControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/contacts/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.contactsInstanceList.size() == 0
        assert model.contactsInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.contactsInstance != null
    }

    void testSave() {
        controller.save()

        assert model.contactsInstance != null
        assert view == '/contacts/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/contacts/show/1'
        assert controller.flash.message != null
        assert Contacts.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/contacts/list'

        populateValidParams(params)
        def contacts = new Contacts(params)

        assert contacts.save() != null

        params.id = contacts.id

        def model = controller.show()

        assert model.contactsInstance == contacts
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/contacts/list'

        populateValidParams(params)
        def contacts = new Contacts(params)

        assert contacts.save() != null

        params.id = contacts.id

        def model = controller.edit()

        assert model.contactsInstance == contacts
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/contacts/list'

        response.reset()

        populateValidParams(params)
        def contacts = new Contacts(params)

        assert contacts.save() != null

        // test invalid parameters in update
        params.id = contacts.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/contacts/edit"
        assert model.contactsInstance != null

        contacts.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/contacts/show/$contacts.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        contacts.clearErrors()

        populateValidParams(params)
        params.id = contacts.id
        params.version = -1
        controller.update()

        assert view == "/contacts/edit"
        assert model.contactsInstance != null
        assert model.contactsInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/contacts/list'

        response.reset()

        populateValidParams(params)
        def contacts = new Contacts(params)

        assert contacts.save() != null
        assert Contacts.count() == 1

        params.id = contacts.id

        controller.delete()

        assert Contacts.count() == 0
        assert Contacts.get(contacts.id) == null
        assert response.redirectedUrl == '/contacts/list'
    }
}
