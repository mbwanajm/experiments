package com.mbwanajm.darproperty



import org.junit.*
import grails.test.mixin.*

@TestFor(InterestratesController)
@Mock(Interestrates)
class InterestratesControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/interestrates/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.interestratesInstanceList.size() == 0
        assert model.interestratesInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.interestratesInstance != null
    }

    void testSave() {
        controller.save()

        assert model.interestratesInstance != null
        assert view == '/interestrates/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/interestrates/show/1'
        assert controller.flash.message != null
        assert Interestrates.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/interestrates/list'

        populateValidParams(params)
        def interestrates = new Interestrates(params)

        assert interestrates.save() != null

        params.id = interestrates.id

        def model = controller.show()

        assert model.interestratesInstance == interestrates
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/interestrates/list'

        populateValidParams(params)
        def interestrates = new Interestrates(params)

        assert interestrates.save() != null

        params.id = interestrates.id

        def model = controller.edit()

        assert model.interestratesInstance == interestrates
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/interestrates/list'

        response.reset()

        populateValidParams(params)
        def interestrates = new Interestrates(params)

        assert interestrates.save() != null

        // test invalid parameters in update
        params.id = interestrates.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/interestrates/edit"
        assert model.interestratesInstance != null

        interestrates.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/interestrates/show/$interestrates.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        interestrates.clearErrors()

        populateValidParams(params)
        params.id = interestrates.id
        params.version = -1
        controller.update()

        assert view == "/interestrates/edit"
        assert model.interestratesInstance != null
        assert model.interestratesInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/interestrates/list'

        response.reset()

        populateValidParams(params)
        def interestrates = new Interestrates(params)

        assert interestrates.save() != null
        assert Interestrates.count() == 1

        params.id = interestrates.id

        controller.delete()

        assert Interestrates.count() == 0
        assert Interestrates.get(interestrates.id) == null
        assert response.redirectedUrl == '/interestrates/list'
    }
}
