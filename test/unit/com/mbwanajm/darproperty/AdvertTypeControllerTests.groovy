package com.mbwanajm.darproperty



import org.junit.*
import grails.test.mixin.*

@TestFor(AdvertTypeController)
@Mock(AdvertType)
class AdvertTypeControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/advertType/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.advertTypeInstanceList.size() == 0
        assert model.advertTypeInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.advertTypeInstance != null
    }

    void testSave() {
        controller.save()

        assert model.advertTypeInstance != null
        assert view == '/advertType/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/advertType/show/1'
        assert controller.flash.message != null
        assert AdvertType.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/advertType/list'

        populateValidParams(params)
        def advertType = new AdvertType(params)

        assert advertType.save() != null

        params.id = advertType.id

        def model = controller.show()

        assert model.advertTypeInstance == advertType
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/advertType/list'

        populateValidParams(params)
        def advertType = new AdvertType(params)

        assert advertType.save() != null

        params.id = advertType.id

        def model = controller.edit()

        assert model.advertTypeInstance == advertType
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/advertType/list'

        response.reset()

        populateValidParams(params)
        def advertType = new AdvertType(params)

        assert advertType.save() != null

        // test invalid parameters in update
        params.id = advertType.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/advertType/edit"
        assert model.advertTypeInstance != null

        advertType.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/advertType/show/$advertType.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        advertType.clearErrors()

        populateValidParams(params)
        params.id = advertType.id
        params.version = -1
        controller.update()

        assert view == "/advertType/edit"
        assert model.advertTypeInstance != null
        assert model.advertTypeInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/advertType/list'

        response.reset()

        populateValidParams(params)
        def advertType = new AdvertType(params)

        assert advertType.save() != null
        assert AdvertType.count() == 1

        params.id = advertType.id

        controller.delete()

        assert AdvertType.count() == 0
        assert AdvertType.get(advertType.id) == null
        assert response.redirectedUrl == '/advertType/list'
    }
}
