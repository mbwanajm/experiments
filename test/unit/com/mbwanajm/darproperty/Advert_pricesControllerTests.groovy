package com.mbwanajm.darproperty



import org.junit.*
import grails.test.mixin.*

@TestFor(Advert_pricesController)
@Mock(Advert_prices)
class Advert_pricesControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/advert_prices/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.advert_pricesInstanceList.size() == 0
        assert model.advert_pricesInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.advert_pricesInstance != null
    }

    void testSave() {
        controller.save()

        assert model.advert_pricesInstance != null
        assert view == '/advert_prices/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/advert_prices/show/1'
        assert controller.flash.message != null
        assert Advert_prices.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/advert_prices/list'

        populateValidParams(params)
        def advert_prices = new Advert_prices(params)

        assert advert_prices.save() != null

        params.id = advert_prices.id

        def model = controller.show()

        assert model.advert_pricesInstance == advert_prices
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/advert_prices/list'

        populateValidParams(params)
        def advert_prices = new Advert_prices(params)

        assert advert_prices.save() != null

        params.id = advert_prices.id

        def model = controller.edit()

        assert model.advert_pricesInstance == advert_prices
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/advert_prices/list'

        response.reset()

        populateValidParams(params)
        def advert_prices = new Advert_prices(params)

        assert advert_prices.save() != null

        // test invalid parameters in update
        params.id = advert_prices.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/advert_prices/edit"
        assert model.advert_pricesInstance != null

        advert_prices.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/advert_prices/show/$advert_prices.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        advert_prices.clearErrors()

        populateValidParams(params)
        params.id = advert_prices.id
        params.version = -1
        controller.update()

        assert view == "/advert_prices/edit"
        assert model.advert_pricesInstance != null
        assert model.advert_pricesInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/advert_prices/list'

        response.reset()

        populateValidParams(params)
        def advert_prices = new Advert_prices(params)

        assert advert_prices.save() != null
        assert Advert_prices.count() == 1

        params.id = advert_prices.id

        controller.delete()

        assert Advert_prices.count() == 0
        assert Advert_prices.get(advert_prices.id) == null
        assert response.redirectedUrl == '/advert_prices/list'
    }
}
