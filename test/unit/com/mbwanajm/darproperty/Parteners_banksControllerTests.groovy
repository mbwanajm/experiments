package com.mbwanajm.darproperty



import org.junit.*
import grails.test.mixin.*

@TestFor(Parteners_banksController)
@Mock(Parteners_banks)
class Parteners_banksControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/parteners_banks/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.parteners_banksInstanceList.size() == 0
        assert model.parteners_banksInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.parteners_banksInstance != null
    }

    void testSave() {
        controller.save()

        assert model.parteners_banksInstance != null
        assert view == '/parteners_banks/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/parteners_banks/show/1'
        assert controller.flash.message != null
        assert Parteners_banks.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/parteners_banks/list'

        populateValidParams(params)
        def parteners_banks = new Parteners_banks(params)

        assert parteners_banks.save() != null

        params.id = parteners_banks.id

        def model = controller.show()

        assert model.parteners_banksInstance == parteners_banks
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/parteners_banks/list'

        populateValidParams(params)
        def parteners_banks = new Parteners_banks(params)

        assert parteners_banks.save() != null

        params.id = parteners_banks.id

        def model = controller.edit()

        assert model.parteners_banksInstance == parteners_banks
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/parteners_banks/list'

        response.reset()

        populateValidParams(params)
        def parteners_banks = new Parteners_banks(params)

        assert parteners_banks.save() != null

        // test invalid parameters in update
        params.id = parteners_banks.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/parteners_banks/edit"
        assert model.parteners_banksInstance != null

        parteners_banks.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/parteners_banks/show/$parteners_banks.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        parteners_banks.clearErrors()

        populateValidParams(params)
        params.id = parteners_banks.id
        params.version = -1
        controller.update()

        assert view == "/parteners_banks/edit"
        assert model.parteners_banksInstance != null
        assert model.parteners_banksInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/parteners_banks/list'

        response.reset()

        populateValidParams(params)
        def parteners_banks = new Parteners_banks(params)

        assert parteners_banks.save() != null
        assert Parteners_banks.count() == 1

        params.id = parteners_banks.id

        controller.delete()

        assert Parteners_banks.count() == 0
        assert Parteners_banks.get(parteners_banks.id) == null
        assert response.redirectedUrl == '/parteners_banks/list'
    }
}
