package com.mbwanajm.darproperty



import org.junit.*
import grails.test.mixin.*

@TestFor(ReceiptsController)
@Mock(Receipts)
class ReceiptsControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/receipts/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.receiptsInstanceList.size() == 0
        assert model.receiptsInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.receiptsInstance != null
    }

    void testSave() {
        controller.save()

        assert model.receiptsInstance != null
        assert view == '/receipts/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/receipts/show/1'
        assert controller.flash.message != null
        assert Receipts.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/receipts/list'

        populateValidParams(params)
        def receipts = new Receipts(params)

        assert receipts.save() != null

        params.id = receipts.id

        def model = controller.show()

        assert model.receiptsInstance == receipts
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/receipts/list'

        populateValidParams(params)
        def receipts = new Receipts(params)

        assert receipts.save() != null

        params.id = receipts.id

        def model = controller.edit()

        assert model.receiptsInstance == receipts
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/receipts/list'

        response.reset()

        populateValidParams(params)
        def receipts = new Receipts(params)

        assert receipts.save() != null

        // test invalid parameters in update
        params.id = receipts.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/receipts/edit"
        assert model.receiptsInstance != null

        receipts.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/receipts/show/$receipts.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        receipts.clearErrors()

        populateValidParams(params)
        params.id = receipts.id
        params.version = -1
        controller.update()

        assert view == "/receipts/edit"
        assert model.receiptsInstance != null
        assert model.receiptsInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/receipts/list'

        response.reset()

        populateValidParams(params)
        def receipts = new Receipts(params)

        assert receipts.save() != null
        assert Receipts.count() == 1

        params.id = receipts.id

        controller.delete()

        assert Receipts.count() == 0
        assert Receipts.get(receipts.id) == null
        assert response.redirectedUrl == '/receipts/list'
    }
}
