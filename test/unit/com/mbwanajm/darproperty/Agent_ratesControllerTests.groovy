package com.mbwanajm.darproperty



import org.junit.*
import grails.test.mixin.*

@TestFor(Agent_ratesController)
@Mock(Agent_rates)
class Agent_ratesControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/agent_rates/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.agent_ratesInstanceList.size() == 0
        assert model.agent_ratesInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.agent_ratesInstance != null
    }

    void testSave() {
        controller.save()

        assert model.agent_ratesInstance != null
        assert view == '/agent_rates/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/agent_rates/show/1'
        assert controller.flash.message != null
        assert Agent_rates.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/agent_rates/list'

        populateValidParams(params)
        def agent_rates = new Agent_rates(params)

        assert agent_rates.save() != null

        params.id = agent_rates.id

        def model = controller.show()

        assert model.agent_ratesInstance == agent_rates
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/agent_rates/list'

        populateValidParams(params)
        def agent_rates = new Agent_rates(params)

        assert agent_rates.save() != null

        params.id = agent_rates.id

        def model = controller.edit()

        assert model.agent_ratesInstance == agent_rates
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/agent_rates/list'

        response.reset()

        populateValidParams(params)
        def agent_rates = new Agent_rates(params)

        assert agent_rates.save() != null

        // test invalid parameters in update
        params.id = agent_rates.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/agent_rates/edit"
        assert model.agent_ratesInstance != null

        agent_rates.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/agent_rates/show/$agent_rates.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        agent_rates.clearErrors()

        populateValidParams(params)
        params.id = agent_rates.id
        params.version = -1
        controller.update()

        assert view == "/agent_rates/edit"
        assert model.agent_ratesInstance != null
        assert model.agent_ratesInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/agent_rates/list'

        response.reset()

        populateValidParams(params)
        def agent_rates = new Agent_rates(params)

        assert agent_rates.save() != null
        assert Agent_rates.count() == 1

        params.id = agent_rates.id

        controller.delete()

        assert Agent_rates.count() == 0
        assert Agent_rates.get(agent_rates.id) == null
        assert response.redirectedUrl == '/agent_rates/list'
    }
}
