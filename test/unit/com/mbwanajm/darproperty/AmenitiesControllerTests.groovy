package com.mbwanajm.darproperty



import org.junit.*
import grails.test.mixin.*

@TestFor(AmenitiesController)
@Mock(Amenities)
class AmenitiesControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/amenities/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.amenitiesInstanceList.size() == 0
        assert model.amenitiesInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.amenitiesInstance != null
    }

    void testSave() {
        controller.save()

        assert model.amenitiesInstance != null
        assert view == '/amenities/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/amenities/show/1'
        assert controller.flash.message != null
        assert Amenities.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/amenities/list'

        populateValidParams(params)
        def amenities = new Amenities(params)

        assert amenities.save() != null

        params.id = amenities.id

        def model = controller.show()

        assert model.amenitiesInstance == amenities
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/amenities/list'

        populateValidParams(params)
        def amenities = new Amenities(params)

        assert amenities.save() != null

        params.id = amenities.id

        def model = controller.edit()

        assert model.amenitiesInstance == amenities
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/amenities/list'

        response.reset()

        populateValidParams(params)
        def amenities = new Amenities(params)

        assert amenities.save() != null

        // test invalid parameters in update
        params.id = amenities.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/amenities/edit"
        assert model.amenitiesInstance != null

        amenities.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/amenities/show/$amenities.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        amenities.clearErrors()

        populateValidParams(params)
        params.id = amenities.id
        params.version = -1
        controller.update()

        assert view == "/amenities/edit"
        assert model.amenitiesInstance != null
        assert model.amenitiesInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/amenities/list'

        response.reset()

        populateValidParams(params)
        def amenities = new Amenities(params)

        assert amenities.save() != null
        assert Amenities.count() == 1

        params.id = amenities.id

        controller.delete()

        assert Amenities.count() == 0
        assert Amenities.get(amenities.id) == null
        assert response.redirectedUrl == '/amenities/list'
    }
}
