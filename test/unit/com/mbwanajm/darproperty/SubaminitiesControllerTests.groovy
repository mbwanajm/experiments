package com.mbwanajm.darproperty



import org.junit.*
import grails.test.mixin.*

@TestFor(SubaminitiesController)
@Mock(Subaminities)
class SubaminitiesControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/subaminities/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.subaminitiesInstanceList.size() == 0
        assert model.subaminitiesInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.subaminitiesInstance != null
    }

    void testSave() {
        controller.save()

        assert model.subaminitiesInstance != null
        assert view == '/subaminities/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/subaminities/show/1'
        assert controller.flash.message != null
        assert Subaminities.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/subaminities/list'

        populateValidParams(params)
        def subaminities = new Subaminities(params)

        assert subaminities.save() != null

        params.id = subaminities.id

        def model = controller.show()

        assert model.subaminitiesInstance == subaminities
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/subaminities/list'

        populateValidParams(params)
        def subaminities = new Subaminities(params)

        assert subaminities.save() != null

        params.id = subaminities.id

        def model = controller.edit()

        assert model.subaminitiesInstance == subaminities
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/subaminities/list'

        response.reset()

        populateValidParams(params)
        def subaminities = new Subaminities(params)

        assert subaminities.save() != null

        // test invalid parameters in update
        params.id = subaminities.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/subaminities/edit"
        assert model.subaminitiesInstance != null

        subaminities.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/subaminities/show/$subaminities.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        subaminities.clearErrors()

        populateValidParams(params)
        params.id = subaminities.id
        params.version = -1
        controller.update()

        assert view == "/subaminities/edit"
        assert model.subaminitiesInstance != null
        assert model.subaminitiesInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/subaminities/list'

        response.reset()

        populateValidParams(params)
        def subaminities = new Subaminities(params)

        assert subaminities.save() != null
        assert Subaminities.count() == 1

        params.id = subaminities.id

        controller.delete()

        assert Subaminities.count() == 0
        assert Subaminities.get(subaminities.id) == null
        assert response.redirectedUrl == '/subaminities/list'
    }
}
