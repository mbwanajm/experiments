<html>
<head>
    <title>Cant fullfill your request</title>
    <meta name="layout" content="main"/>
</head>

<body>
<div class="container">
    <div class="row">
    <div class='col-sm-12'>
        <h2>Oops something Went Wrong</h2>

        <p class="alert alert-info">
            Something went wrong while processing your request, our team of programmers
            is working on fixing it right now. please wait a bit and try again later
        </p>
        <br>
        <g:link controller="contactUs" class="btn btn-warning" params="${[msg: "I am experiencing a problem in the website page/ while accessing .................."]}"> <i class="fa fa-envelope-square"></i> Report this problem </g:link>
    </div>
</div>
</div>

</body>

</html>

