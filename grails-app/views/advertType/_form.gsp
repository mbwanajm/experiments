<%@ page import="com.mbwanajm.darproperty.AdvertType" %>



<div class="fieldcontain ${hasErrors(bean: advertTypeInstance, field: 'name', 'error')} required">
    <label for="name">
        <g:message code="advertType.name.label" default="Name"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="name" required="" value="${advertTypeInstance?.name}"/>
</div>

