
<%@ page import="com.mbwanajm.darproperty.Featured_property" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'featured_property.label', default: 'Featured_property')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-featured_property" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-featured_property" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list featured_property">
			
				<g:if test="${featured_propertyInstance?.receipts}">
				<li class="fieldcontain">
					<span id="receipts-label" class="property-label"><g:message code="featured_property.receipts.label" default="Receipts" /></span>
					
						<span class="property-value" aria-labelledby="receipts-label"><g:link controller="receipts" action="show" id="${featured_propertyInstance?.receipts?.id}">${featured_propertyInstance?.receipts?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${featured_propertyInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="featured_property.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${featured_propertyInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${featured_propertyInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="featured_property.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${featured_propertyInstance}" field="description"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${featured_propertyInstance?.country}">
				<li class="fieldcontain">
					<span id="country-label" class="property-label"><g:message code="featured_property.country.label" default="Country" /></span>
					
						<span class="property-value" aria-labelledby="country-label"><g:link controller="country" action="show" id="${featured_propertyInstance?.country?.id}">${featured_propertyInstance?.country?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${featured_propertyInstance?.region}">
				<li class="fieldcontain">
					<span id="region-label" class="property-label"><g:message code="featured_property.region.label" default="Region" /></span>
					
						<span class="property-value" aria-labelledby="region-label"><g:fieldValue bean="${featured_propertyInstance}" field="region"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${featured_propertyInstance?.area}">
				<li class="fieldcontain">
					<span id="area-label" class="property-label"><g:message code="featured_property.area.label" default="Area" /></span>
					
						<span class="property-value" aria-labelledby="area-label"><g:fieldValue bean="${featured_propertyInstance}" field="area"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${featured_propertyInstance?.dateAdded}">
				<li class="fieldcontain">
					<span id="dateAdded-label" class="property-label"><g:message code="featured_property.dateAdded.label" default="Date Added" /></span>
					
						<span class="property-value" aria-labelledby="dateAdded-label"><g:formatDate date="${featured_propertyInstance?.dateAdded}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${featured_propertyInstance?.location}">
				<li class="fieldcontain">
					<span id="location-label" class="property-label"><g:message code="featured_property.location.label" default="Location" /></span>
					
						<span class="property-value" aria-labelledby="location-label"><g:fieldValue bean="${featured_propertyInstance}" field="location"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${featured_propertyInstance?.expireDate}">
				<li class="fieldcontain">
					<span id="expireDate-label" class="property-label"><g:message code="featured_property.expireDate.label" default="Expire Date" /></span>
					
						<span class="property-value" aria-labelledby="expireDate-label"><g:formatDate date="${featured_propertyInstance?.expireDate}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${featured_propertyInstance?.price}">
				<li class="fieldcontain">
					<span id="price-label" class="property-label"><g:message code="featured_property.price.label" default="Price" /></span>
					
						<span class="property-value" aria-labelledby="price-label"><g:fieldValue bean="${featured_propertyInstance}" field="price"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${featured_propertyInstance?.currency}">
				<li class="fieldcontain">
					<span id="currency-label" class="property-label"><g:message code="featured_property.currency.label" default="Currency" /></span>
					
						<span class="property-value" aria-labelledby="currency-label"><g:fieldValue bean="${featured_propertyInstance}" field="currency"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${featured_propertyInstance?.descriptionSW}">
				<li class="fieldcontain">
					<span id="descriptionSW-label" class="property-label"><g:message code="featured_property.descriptionSW.label" default="Description SW" /></span>
					
						<span class="property-value" aria-labelledby="descriptionSW-label"><g:fieldValue bean="${featured_propertyInstance}" field="descriptionSW"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${featured_propertyInstance?.photos}">
				<li class="fieldcontain">
					<span id="photos-label" class="property-label"><g:message code="featured_property.photos.label" default="Photos" /></span>
					
						<g:each in="${featured_propertyInstance.photos}" var="p">
						<span class="property-value" aria-labelledby="photos-label"><g:link controller="photo" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${featured_propertyInstance?.saleType}">
				<li class="fieldcontain">
					<span id="saleType-label" class="property-label"><g:message code="featured_property.saleType.label" default="Sale Type" /></span>
					
						<span class="property-value" aria-labelledby="saleType-label"><g:link controller="saleType" action="show" id="${featured_propertyInstance?.saleType?.id}">${featured_propertyInstance?.saleType?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${featured_propertyInstance?.size}">
				<li class="fieldcontain">
					<span id="size-label" class="property-label"><g:message code="featured_property.size.label" default="Size" /></span>
					
						<span class="property-value" aria-labelledby="size-label"><g:fieldValue bean="${featured_propertyInstance}" field="size"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${featured_propertyInstance?.tags}">
				<li class="fieldcontain">
					<span id="tags-label" class="property-label"><g:message code="featured_property.tags.label" default="Tags" /></span>
					
						<g:each in="${featured_propertyInstance.tags}" var="t">
						<span class="property-value" aria-labelledby="tags-label"><g:link controller="tag" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${featured_propertyInstance?.type}">
				<li class="fieldcontain">
					<span id="type-label" class="property-label"><g:message code="featured_property.type.label" default="Type" /></span>
					
						<span class="property-value" aria-labelledby="type-label"><g:link controller="type" action="show" id="${featured_propertyInstance?.type?.id}">${featured_propertyInstance?.type?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${featured_propertyInstance?.id}" />
					<g:link class="edit" action="edit" id="${featured_propertyInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
