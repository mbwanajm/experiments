<%@ page import="com.mbwanajm.darproperty.Featured_property" %>



<div class="fieldcontain ${hasErrors(bean: featured_propertyInstance, field: 'receipts', 'error')} required">
	<label for="receipts">
		<g:message code="featured_property.receipts.label" default="Receipts" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="receipts" name="receipts.id" from="${com.mbwanajm.darproperty.Receipts.list()}" optionKey="id" required="" value="${featured_propertyInstance?.receipts?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: featured_propertyInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="featured_property.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${featured_propertyInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: featured_propertyInstance, field: 'description', 'error')} required">
	<label for="description">
		<g:message code="featured_property.description.label" default="Description" />
		<span class="required-indicator">*</span>
	</label>
	<g:textArea name="description" cols="40" rows="5" maxlength="500" required="" value="${featured_propertyInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: featured_propertyInstance, field: 'country', 'error')} required">
	<label for="country">
		<g:message code="featured_property.country.label" default="Country" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="country" name="country.id" from="${com.mbwanajm.darproperty.Country.list()}" optionKey="id" required="" value="${featured_propertyInstance?.country?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: featured_propertyInstance, field: 'region', 'error')} required">
	<label for="region">
		<g:message code="featured_property.region.label" default="Region" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="region" required="" value="${featured_propertyInstance?.region}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: featured_propertyInstance, field: 'area', 'error')} required">
	<label for="area">
		<g:message code="featured_property.area.label" default="Area" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="area" required="" value="${featured_propertyInstance?.area}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: featured_propertyInstance, field: 'dateAdded', 'error')} ">
	<label for="dateAdded">
		<g:message code="featured_property.dateAdded.label" default="Date Added" />
		
	</label>
	<g:datePicker name="dateAdded" precision="day"  value="${featured_propertyInstance?.dateAdded}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: featured_propertyInstance, field: 'location', 'error')} ">
	<label for="location">
		<g:message code="featured_property.location.label" default="Location" />
		
	</label>
	<g:textField name="location" value="${featured_propertyInstance?.location}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: featured_propertyInstance, field: 'expireDate', 'error')} ">
	<label for="expireDate">
		<g:message code="featured_property.expireDate.label" default="Expire Date" />
		
	</label>
	<g:datePicker name="expireDate" precision="day"  value="${featured_propertyInstance?.expireDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: featured_propertyInstance, field: 'price', 'error')} required">
	<label for="price">
		<g:message code="featured_property.price.label" default="Price" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="price" value="${fieldValue(bean: featured_propertyInstance, field: 'price')}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: featured_propertyInstance, field: 'currency', 'error')} required">
	<label for="currency">
		<g:message code="featured_property.currency.label" default="Currency" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="currency" required="" value="${featured_propertyInstance?.currency}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: featured_propertyInstance, field: 'descriptionSW', 'error')} ">
	<label for="descriptionSW">
		<g:message code="featured_property.descriptionSW.label" default="Description SW" />
		
	</label>
	<g:textField name="descriptionSW" value="${featured_propertyInstance?.descriptionSW}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: featured_propertyInstance, field: 'photos', 'error')} ">
	<label for="photos">
		<g:message code="featured_property.photos.label" default="Photos" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${featured_propertyInstance?.photos?}" var="p">
    <li><g:link controller="photo" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="photo" action="create" params="['featured_property.id': featured_propertyInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'photo.label', default: 'Photo')])}</g:link>
</li>
</ul>

</div>

<div class="fieldcontain ${hasErrors(bean: featured_propertyInstance, field: 'saleType', 'error')} required">
	<label for="saleType">
		<g:message code="featured_property.saleType.label" default="Sale Type" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="saleType" name="saleType.id" from="${com.mbwanajm.darproperty.SaleType.list()}" optionKey="id" required="" value="${featured_propertyInstance?.saleType?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: featured_propertyInstance, field: 'size', 'error')} required">
	<label for="size">
		<g:message code="featured_property.size.label" default="Size" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="size" value="${fieldValue(bean: featured_propertyInstance, field: 'size')}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: featured_propertyInstance, field: 'tags', 'error')} ">
	<label for="tags">
		<g:message code="featured_property.tags.label" default="Tags" />
		
	</label>
	<g:select name="tags" from="${com.mbwanajm.darproperty.Tag.list()}" multiple="multiple" optionKey="id" size="5" value="${featured_propertyInstance?.tags*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: featured_propertyInstance, field: 'type', 'error')} required">
	<label for="type">
		<g:message code="featured_property.type.label" default="Type" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="type" name="type.id" from="${com.mbwanajm.darproperty.Type.list()}" optionKey="id" required="" value="${featured_propertyInstance?.type?.id}" class="many-to-one"/>
</div>

