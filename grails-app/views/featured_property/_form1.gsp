
<%@ page import="com.mbwanajm.darproperty.*" %>
<%@page import="tz.co.darproperty.*"%>

<g:javascript src="darproperty/CountryLoader.js"/>
<g:javascript src="darproperty/RegionLoader.js"/>
<g:javascript src="darproperty/AddProperty.js"/>
%{--<% --}%

%{--DatesManager dt = new DatesManager();--}%
%{--dt.generateCurrentDate();--}%
%{--String current_date = dt.getFullDate();--}%

%{--Featured fpt = new Featured();--}%
%{--fpt.generatePname();--}%
%{--String pname = fpt.getPname();--}%

%{--ReferencesTable ref = new ReferencesTable();--}%
%{--ref.setRefNo(refno);--}%
%{--ref.generateExpiredate();--}%
%{--String expired = ref.getExpireDate();--}%
 %{--%>--}%



<div class=" row ${hasErrors(bean: featured_propertyInstance, field: 'name', 'error')} required">
	<label class="control-label col-sm-3" for="name">
		<g:message code="featured_property.name.label" default="Title" />
		<span class="required-indicator">*<span class="hd4">(You can change it)</span></span>
	</label>
	<div class="col-sm-5 ">
	<g:textField name="name" class="form-control" placeholder="Any of your choice" style="text-align:center;" required="" value="${pname}"/>
</div>
</div>
<br><br>

<div class=" row ${hasErrors(bean: featured_propertyInstance, field: 'type', 'error')} required">
	<label class="control-label col-sm-3" for="type">
		<g:message code="featured_property.type.label" default="Type" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-5  select_control">
	<g:select id="type" name="type" class="form-control" from="${com.mbwanajm.darproperty.Type.list()}" optionKey="id" required="" value="${featured_propertyInstance?.type?.id}" class="many-to-one"/>
</div>
</div>
<br><br>
<div class=" row ${hasErrors(bean: featured_propertyInstance, field: 'saleType', 'error')} required">
	<label class="control-label col-sm-3" for="saleType">
		<g:message code="featured_property.saleType.label" default="Sale Type" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-5  select_control">
	<g:select id="saleType" name="saleType" from="${com.mbwanajm.darproperty.SaleType.list()}" class="form-control" optionKey="id" required="" value="${featured_propertyInstance?.saleType?.id}" class="many-to-one" />
</div>
</div>
<br><br>

<div class=" row ${hasErrors(bean: featured_propertyInstance, field: 'country', 'error')} required">
	<label class="control-label col-sm-3" for="country">
		<g:message code="featured_property.country.label" default="Country" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-5  select_control ">
	<dp:i18Select
            id="property_country"
            name='country'
            optionPrefix=""
            options="${Country.findAll().collect { it.name }}"
            showAnyOption="true"
            noSelectText="property.country"
            noSelectValue="any"
            />
                </div>
                </div>
<br><br>
<div class=" row ${hasErrors(bean: featured_propertyInstance, field: 'region', 'error')} required">
	<label class="control-label col-sm-3" for="region">
		<g:message code="featured_property.region.label" default="Region" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-5  select_control">
	<dp:i18Select
            id='property_region'
            name="region"
            optionPrefix=""
            options="${Region.findAll().collect { it.name }}"
            showAnyOption="true"
            noSelectText="property.region"
            noSelectValue="any"
            />
            </div>
</div>
<br><br>
<div class=" row ${hasErrors(bean: featured_propertyInstance, field: 'area', 'error')} required">
	<label class="control-label col-sm-3" for="area">
		<g:message code="featured_property.area.label" default="Area" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-5  select_control">
	<dp:i18Select
            id='property_areas'
           
            name="area"
            optionPrefix=""
            options="${Region.findByName("Dar es salaam").areas.sort()}"
            showAnyOption="true"
            noSelectText="property.area"
            noSelectValue="any"
            class="select_control"/>
            </div>
</div>
<br><br>
<div class=" row ${hasErrors(bean: featured_propertyInstance, field: 'location', 'error')} ">
	<label class="control-label col-sm-3" for="location">
		<g:message code="featured_property.location.label" default="Location" />
		
	</label>
	<div class="col-sm-7  ">
	<g:textArea name="location"  cols="40" rows="5" maxlength="500" required="" value=" Eg: Mikocheni B near Darproperty "/>
	
</div>
</div>
<br><br>

<div class=" row ${hasErrors(bean: featured_propertyInstance, field: 'currency', 'error')} required">
	<label class="control-label col-sm-3" for="currency">
		<g:message code="featured_property.currency.label" default="Currency" />
		<span class="required-indicator">*<span class="hd4">(For price bellow)</span></span>
	</label>
	<div class="col-sm-5  ">
	<g:select name="currency" from="${["USD", "TZS", "RWF", "KES"]}"
                                                value="" class="form-control" class="form-control"/>
</div>
</div>
<br><br>

<div class=" row ${hasErrors(bean: featured_propertyInstance, field: 'price', 'error')} required">
	<label class="control-label col-sm-3" for="price">
		<g:message code="featured_property.price.label" default="Price" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-5  ">
	<g:field type="number" name="price" placeholder="Eg. 2000000" value="${fieldValue(bean: featured_propertyInstance, field: 'price')}" required="" class="form-control"/>
</div>
</div>
<br><br>

<div class=" row ${hasErrors(bean: featured_propertyInstance, field: 'description', 'error')} required">
	<label class="control-label col-sm-3" for="description">
		<g:message code="featured_property.description.label" default="Description in English" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-7  ">
	<g:textArea name="description"  cols="40" rows="5" maxlength="500" required="" value="${featured_propertyInstance?.description}"/>
</div>
</div>
<br><br>

<div class=" row ${hasErrors(bean: featured_propertyInstance, field: 'descriptionSW', 'error')} ">
	<label class="control-label col-sm-3" for="descriptionSW">
		<g:message code="featured_property.descriptionSW.label" default="Description in Swahili" />
		<span class="hd4">(Maelezo)</span>
		
	</label>
	<div class="col-sm-7  ">
	<g:textArea name="descriptionSW"  cols="40" rows="5" maxlength="500" required="" value="Maelezo kwa kiswahili:"/>
</div>
</div>
<br><br>

<div class=" row ${hasErrors(bean: featured_propertyInstance, field: 'size', 'error')} required">
	<label class="control-label col-sm-3" for="size">
		<g:message code="featured_property.size.label" default="Size" />
		<span class="required-indicator">*<span class="hd4">(Sqm)</span></span>
	</label>
	<div class="col-sm-5  ">
	<g:field type="number"  name="size" placeholder="Eg. 1232" value="${fieldValue(bean: featured_propertyInstance, field: 'size')}" required="" class="form-control"/>
	</div>
</div>
<br><br>

<div class=" row ${hasErrors(bean: featured_propertyInstance, field: 'dateAdded', 'error')} ">
	<label class="control-label col-sm-3" for="dateAdded">
		<g:message code="featured_property.dateAdded.label" default="Date Added (Today)" />
		
	</label>
	<div class="col-sm-5" >
	<g:textField name="dateAdded" value="${current_date}" readonly="readonly" style="text-align:center;" class="form-control hd3"/>
	</div>
</div>
<br><br>

<div class=" row ${hasErrors(bean: featured_propertyInstance, field: 'expireDate', 'error')} ">
	<label class="control-label col-sm-3" for="expireDate">
		<g:message code="featured_property.expireDate.label" default="Expire Date Will be" />
		
	</label>
	<div class="col-sm-5">
	<g:textField name="expireDate" value="${expired}" readonly="readonly" style="text-align:center;" class="form-control hd2"/>
	
</div>
</div>
<br><br>

  <div class='row'>
            <label class="control-label col-sm-3 ">
            <span class="glyphicon glyphicon-ok"></span> if available:</label>
                <div class="col-sm-7  ">
               
                    <g:each in="${Tag.findAll()}" var="tag">
                    <div class=" col-sm-4 hd2" >
                    <label for="${tag.name}" class="control-label col-sm-5">${tag.name}</label>  
                    <div class="col-sm-5"> <input type="checkbox" name="${tag.name}" value="${tag.id}" class="form-control"></div>
                        
                        </div>
                    </g:each>
                    
                </div>
            </div>
        
