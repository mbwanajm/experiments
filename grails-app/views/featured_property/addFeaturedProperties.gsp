<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
<meta name="layout" content="main"/>
<title>ADD ADVERT | STEP 2</title>
</head>
<body>

<!-- RECEIVE & PROCESS -->
<% 
String refno = "";
String fname = "";
String mname = "";
String lname = "";
String company = "";
String phone = "";
String email = "";
String tel = "";
if(request.getParameter("step1")!=null){
  refno = request.getParameter("refno");
  fname = request.getParameter("firstName");
  mname = request.getParameter("middleName");
  lname = request.getParameter("lastName");
  company = request.getParameter("companyName");
  phone = request.getParameter("phone");
  email = request.getParameter("email");
  tel = request.getParameter("tel");
	}
 %>
 <!-- IF STEP 1 SUBMITTED -->
<g:if test="${request.getParameter("step1")!=null }">
   <div class="container" >
  	<div class="row">
  
  <!--  PANEL  -->
  <div class="panel panel-default">
  <div class="panel-heading">
  	<h4 class="modal-title mod_txt" align="center" >ADDING ADVERT INFORMATION</h4>
  </div>
  <div class="panel-body">
        <g:form role = "form" action= "addFeaturedPhotos" controller="featured_property" class="form-verticle">
        <g:hiddenField name="refno" value="${refno }"/>
        <g:hiddenField name="fname" value="${fname }"/>
        <g:hiddenField name="mname" value="${mname }"/>
        <g:hiddenField name="lname" value="${lname }"/>
        <g:hiddenField name="company" value="${company }"/>
        <g:hiddenField name="phone" value="${phone }"/>
        <g:hiddenField name="email" value="${email }"/>
        <g:hiddenField name="tel" value="${tel }"/>
  		<g:render template="form1" model="[refno:refno]"></g:render>
  		 <div class="form-group">
	<div class=" col-sm-8 offset">
		
	</div>
	<div class="col-sm-4 " align="left">
	<hr>
	<button class="btn btn-primary" name="step2" > Proceed >> </button>
	</div>
    </div>
  		</g:form>	
  </div>
  <div class="panel-footer">
   <p class="hd4" align="left"> <a href="">Terms & Conditions</a> | <a href="">Help</a> | <a href="">Contact Us</a></p>
  </div>
  </div>
  
  
  </div>
  </div>
  </g:if>
   <!-- ELSE BACK HOME -->
<g:if test="${request.getParameter("step1")==null}">
<% 
			
			  response.sendRedirect("../home/");
			  
			  
			  %>
</g:if>
</body>
</html>