<%@ page import="com.mbwanajm.darproperty.Country; com.mbwanajm.darproperty.Tag; com.mbwanajm.darproperty.Region; com.mbwanajm.darproperty.SaleType; com.mbwanajm.darproperty.Type" %>

<html>
<head>
    <title>ADD ADVERT | STEP 3</title>
    <meta name="layout" content="main"/>
    <g:javascript src="darproperty/CountryLoader.js"/>
    <g:javascript src="darproperty/RegionLoader.js"/>
    <g:javascript src="darproperty/AddProperty.js"/>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>

<body>
<g:if test="${request.getParameter("step2") != null}">

    <%
        String refno = "";
        String fname = "";
        String mname = "";
        String lname = "";
        String company = "";
        String phone = "";
        String email = "";
        String tel = "";

        String pname = "";
        String type = "";
        String saletype = "";
        String country = "";
        String region = "";
        String area = "";
        String location = "";
        String currency = "";
        double price = 0.0;
        String description_en = "";
        String description_sw = "";
        double psize = 0.0;
        String added = "";
        String expired = "";

        int no_of_tags = 0;
        int counter = 0;

        no_of_tags = ames.numberOfTagsData();
        String[] tag_names = ames.fetchTagNames();
        String[] tags = new String[no_of_tags];



        try {

            for (int i = 0; i < no_of_tags; i++) {


                if (request.getParameter("" + tag_names[i] + "") != null) {
                    tags[counter] = request.getParameter("" + tag_names[i] + "");

                    counter++;
                }


            }

            refno = request.getParameter("refno");
            fname = request.getParameter("fname");
            mname = request.getParameter("mname");
            lname = request.getParameter("lname");
            company = request.getParameter("company");
            phone = request.getParameter("phone");
            email = request.getParameter("email");
            tel = request.getParameter("tel");

            pname = request.getParameter("name");
            type = request.getParameter("type");
            saletype = request.getParameter("saleType");
            country = request.getParameter("country");
            region = request.getParameter("region");
            area = request.getParameter("area");
            location = request.getParameter("location");
            currency = request.getParameter("currency");
            price = Double.parseDouble(request.getParameter("price"));
            description_en = request.getParameter("description");
            description_sw = request.getParameter("descriptionSW");
            psize = Double.parseDouble(request.getParameter("size"));
            added = request.getParameter("dateAdded");
            expired = request.getParameter("expireDate");


        } catch (Exception e) {
            out.print(e);
        }


    %>


    <!-- IF STEP 2 SUBMITTED -->

    <div class="container">

        <div class='row'>
            <g:form method="post" action="saveProperty" enctype="multipart/form-data">
                <g:hiddenField name="refno" value="${refno}"/>
                <g:hiddenField name="fname" value="${fname}"/>
                <g:hiddenField name="mname" value="${mname}"/>
                <g:hiddenField name="lname" value="${lname}"/>
                <g:hiddenField name="company" value="${company}"/>
                <g:hiddenField name="phone" value="${phone}"/>
                <g:hiddenField name="email" value="${email}"/>
                <g:hiddenField name="tel" value="${tel}"/>

                <g:hiddenField name="name" value="${pname}"/>
                <g:hiddenField name="type" value="${type}"/>
                <g:hiddenField name="saleType" value="${saletype}"/>
                <g:hiddenField name="country" value="${country}"/>
                <g:hiddenField name="region" value="${region}"/>
                <g:hiddenField name="area" value="${area}"/>
                <g:hiddenField name="location" value="${location}"/>
                <g:hiddenField name="currency" value="${currency}"/>
                <g:hiddenField name="price" value="${price}"/>
                <g:hiddenField name="description" value="${description_en}"/>
                <g:hiddenField name="descriptionSW" value="${description_sw}"/>
                <g:hiddenField name="size" value="${psize}"/>
                <g:hiddenField name="dateAdded" value="${added}"/>
                <g:hiddenField name="dateExpired" value="${expired}"/>
                <g:hiddenField name="selectedTags" value="${counter}"/>
                <%
                    for (int i = 0; i < counter; i++) {

                        out.print("<input type='hidden' name='" + tags[i] + "' value='" + ames.getTagName("" + tags[i]) + "'>");

                    }
                %>


                <div class='control-group'>
                    <div class="col-sm-5 offset"></div>

                    <div class="property_Photo col-sm-7">
                        <fieldset>
                            <legend>Property Photos:</legend>


                            <div class='control-group'>
                                <div class="row" align="center">
                                    <div id="loading3">
                                        <div class="form-group" align="center">
                                            <img src="${resource(dir: 'bootstrap/imgs/', file: '3.gif')}"/>
                                            <span class="hd3">Saving...</span>
                                            <!-- loading image -->

                                        </div>

                                    </div>
                                </div>

                                <div class="row">
                                    <label class='control-label col-sm-5'>Main Photo:</label>

                                    <div class=" required">
                                        <div class='controls col-sm-7 hd2'><input name="photo1" id="photo1" type="file"
                                                                                  required="required"/></div>
                                    </div>
                                </div>
                                <hr class="style-two">

                                <div class="row">
                                    <label class='control-label col-sm-5'>Other Photo:</label>

                                    <div class='controls col-sm-7 hd4'><input name="photo2" type="file"/></div>
                                </div>

                                <div class="row">
                                    <label class='control-label col-sm-5'>Other Photo:</label>

                                    <div class='controls col-sm-7 hd4'><input name="photo3" type="file"/></div>
                                </div>

                                <div class="row">
                                    <label class='control-label col-sm-5'>Other Photo:</label>

                                    <div class='controls col-sm-7 hd4'><input name="photo4" type="file"/></div>
                                </div>

                                <div class="row">
                                    <label class='control-label col-sm-5'>Other Photo:</label>

                                    <div class='controls col-sm-7 hd4'><input name="photo5" type="file"/></div>
                                </div>
                            </div>
                            <br>

                            <div class="row">
                                <div class="col-sm-5 offset"></div>

                                <div class="col-sm-7"><g:submitButton class="btn btn-success" name="save"/></div>

                            </div>
                            <br>
                            <legend></legend>
                        </fieldset>
                    </div>
                </div>

            </g:form>
        </div>

    </div>
</g:if>
<!-- ELSE BACK HOME  -->
<g:if test="${request.getParameter("step2") == null}">
    <%

        response.sendRedirect("../home/");


    %>
</g:if>
<br>
<br>
</body>

</html>