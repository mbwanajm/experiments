
<%@page import="tz.co.darproperty.*"%>
<%@ page import="com.mbwanajm.darproperty.Region_areas" %>

<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'region_areas.label', default: 'Region_areas')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="container" align="left">
			<div class="row">
			<div class="col-sm-5">
			
			<a href="#list-region_areas" class="skip" tabindex="-1" title="<g:message code="default.link.skip.label" default="Skip to content&hellip;"/>">
			
		<span class="glyphicon glyphicon-refresh"></span></a>
	<li class="btn btn-default">	<g:link controller ="config" action=""><span class="glyphicon glyphicon-wrench"></span> </g:link></li>
			</div>
			<div class="col-sm-6" align="right"><a href="#areaadding" class="btn btn-default"><span class="glyphicon glyphicon-pushpin"></span> Add Areas</a></div>
			</div>
	

	
		<div id="list-region_areas" class="content scaffold-list" role="main">
			<h1><span class="glyphicon glyphicon-list"></span> <g:message code="default.list.label" args="[entityName]" /> </h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table class="table">
				<thead>
					<tr>
					
						<g:sortableColumn property="areas_string" title="${message(code: 'region_areas.areas_string.label', default: 'Areasstring')}" />
					
						<g:sortableColumn property="point" title="${message(code: 'region_areas.point.label', default: 'Point')}" />
					
						<g:sortableColumn property="region_id" title="${message(code: 'region_areas.region_id.label', default: 'Regionid')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${region_areasInstanceList}" status="i" var="region_areasInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td class="hd2"><g:link action="show" id="${region_areasInstance.id}">${fieldValue(bean: region_areasInstance, field: "areas_string")}</g:link></td>
					
						<td class="hd2">${fieldValue(bean: region_areasInstance, field: "point")}</td>
					
						<td class="hd2">${fieldValue(bean: region_areasInstance, field: "region_id")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${region_areasInstanceTotal}" />
			</div>
		</div>
		</div>

	<br>
	</body>
</html>
