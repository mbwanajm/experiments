<%@ page import="com.mbwanajm.darproperty.Region_areas" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'region_areas.label', default: 'Region_areas')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="container">
			<div class="row">
			<div class="col-sm-5">
			
			<a href="#edit-region_areas" class="skip" tabindex="-1" title="<g:message code="default.link.skip.label" default="Skip to content&hellip;"/>">
		<span class="glyphicon glyphicon-refresh"></span></a>
	
			</div>
			
			</div>
	
	<br>
		<div class="nav" role="navigation">
			<ul><li class="btn btn-default">	<g:link controller ="config" action=""><span class="glyphicon glyphicon-wrench"></span> </g:link></li>
			<li class="btn btn-default"><span class="glyphicon glyphicon-th-list"></span>  
				<g:link class="list hd2" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		
		
		<div id="edit-region_areas" class="content scaffold-edit" role="main">
			<h1><g:message code="default.edit.label" args="[entityName]" />  <span class="glyphicon glyphicon-pencil"></span></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${region_areasInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${region_areasInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<div class="row">
			<div class="col-sm-12">
			<g:form method="post" >
				<g:hiddenField name="id" value="${region_areasInstance?.id}" />
				<g:hiddenField name="version" value="${region_areasInstance?.version}" />
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons">
					<g:actionSubmit class="btn btn-primary" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
					<!--<g:actionSubmit class="disabled" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" formnovalidate="" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />-->
				</fieldset>
			</g:form>
			</div>
			</div>
		</div>
		</div>
		<br>
		<br>
	</body>
</html>
