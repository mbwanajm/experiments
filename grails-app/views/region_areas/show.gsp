
<%@ page import="com.mbwanajm.darproperty.Region_areas" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'region_areas.label', default: 'Region_areas')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
<div class="container">
			<div class="row">
			<div class="col-sm-5">
			
			<a href="#edit-region_areas" class="skip" tabindex="-1" title="<g:message code="default.link.skip.label" default="Skip to content&hellip;"/>">
		<span class="glyphicon glyphicon-refresh"></span></a>
	
			</div>
			
			</div>
	
	<br>
		<div class="nav" role="navigation">
			<ul>
				<li class="btn btn-default">	<g:link controller ="config" action=""><span class="glyphicon glyphicon-wrench"></span> </g:link></li>
			<li class="btn btn-default"><span class="glyphicon glyphicon-th-list"></span>  
				<g:link class="list hd2" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		
		
		<div id="show-region_areas" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list region_areas">
			
				<g:if test="${region_areasInstance?.areas_string}">
				<li class="fieldcontain">
					<span id="areas_string-label" class="property-label"><g:message code="region_areas.areas_string.label" default="Areasstring" /></span>
					
						<span class="property-value" aria-labelledby="areas_string-label"><g:fieldValue bean="${region_areasInstance}" field="areas_string"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${region_areasInstance?.point}">
				<li class="fieldcontain">
					<span id="point-label" class="property-label"><g:message code="region_areas.point.label" default="Point" /></span>
					
						<span class="property-value" aria-labelledby="point-label"><g:fieldValue bean="${region_areasInstance}" field="point"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${region_areasInstance?.region_id}">
				<li class="fieldcontain">
					<span id="region_id-label" class="property-label"><g:message code="region_areas.region_id.label" default="Regionid" /></span>
					
						<span class="property-value" aria-labelledby="region_id-label"><g:fieldValue bean="${region_areasInstance}" field="region_id"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<div class="row">
			<div class="col-sm-12">
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${region_areasInstance?.id}" />
					<g:link class="btn btn-warning" action="edit" id="${region_areasInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<!--<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />-->
				</fieldset>
			</g:form>
			</div>
			</div>
		</div>
		</div>
		<br>
		<br>
	</body>
</html>
