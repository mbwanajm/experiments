<%@ page import="com.mbwanajm.darproperty.Region_areas" %>


<div class="container" align="center">
<div class="panel panel-default">
 <div class="panel-header"></div>
  <div class="panel-body">
  
  <div class="fieldcontain ${hasErrors(bean: region_areasInstance, field: 'areas_string', 'error')} ">
  <div class="col-sm-3" align="right">
	<label for="areas_string">
		<g:message code="region_areas.areas_string.label" default="Areasstring" />
		
	</label>
	</div>
	<div class="col-sm-9" align="left">
	<g:textField name="areas_string" value="${region_areasInstance?.areas_string}" class="form-control"/>
	</div>
</div>

<div class="fieldcontain ${hasErrors(bean: region_areasInstance, field: 'point', 'error')} required">
 <div class="col-sm-3" align="right">
	<label for="point">
		<g:message code="region_areas.point.label" default="Point" />
		<span class="required-indicator">*</span>
	</label>
	</div>
	<div class="col-sm-9" align="left">
	<g:field name="point" value="${fieldValue(bean: region_areasInstance, field: 'point')}" required="" class="form-control"/>
	</div>
</div>

<div class="fieldcontain ${hasErrors(bean: region_areasInstance, field: 'region_id', 'error')} required">
<div class="col-sm-3" align="right">
	<label for="region_id">
		<g:message code="region_areas.region_id.label" default="Regionid" />
		<span class="required-indicator">*</span>
	</label>
	</div>
	<div class="col-sm-9" align="left">
	<g:field name="region_id" type="number" value="${region_areasInstance.region_id}" required="" readonly="" class="form-control" />
	</div>
</div>
  
  </div>
  </div>
  </div>


