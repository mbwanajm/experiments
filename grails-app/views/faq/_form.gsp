<%@ page import="com.mbwanajm.darproperty.Faq" %>


<div class="container" align="center">
<div class="panel panel-default">
 <div class="panel-header"></div>
  <div class="panel-body">
  
<div class="fieldcontain ${hasErrors(bean: faqInstance, field: 'question', 'error')} ">
<div class="col-sm-3" align="right">
	<label for="question">
		<g:message code="faq.question.label" default="Question" />
		
	</label>
	</div>
	<div class="col-sm-9" align="left">
	<g:textField name="question" value="${faqInstance?.question}" class="form-control"/>
	</div>
</div>

<div class="fieldcontain ${hasErrors(bean: faqInstance, field: 'answer', 'error')} ">
<div class="col-sm-3" align="right">
	<label for="answer">
		<g:message code="faq.answer.label" default="Answer" />
		
	</label>
	</div>
	<div class="col-sm-9" align="left">
	<g:textArea name="answer" id='wysiwyg'  cols="40" rows="5" maxlength="1000" value="${faqInstance?.answer}" class="form-control"/>
	</div>
	
</div>
</div>
</div>
</div>

