
<%@ page import="com.mbwanajm.darproperty.Faq" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'faq.label', default: 'Faq')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
	<div class="container" align="left">
			<div class="row">
			<div class="col-sm-5">
			
			<a href="#list-faq" class="skip" tabindex="-1" title="<g:message code="default.link.skip.label" default="Skip to content&hellip;"/>">
		<span class="glyphicon glyphicon-refresh"></span></a>
	
			</div>
			
			</div>
			
			
		
		<div class="nav" role="navigation">
			<ul>
			<li class="btn btn-default">	<g:link controller ="config" action=""><span class="glyphicon glyphicon-wrench"></span> </g:link></li>
				<li  class="btn btn-default"> <span class="glyphicon glyphicon-plus"></span>
				<g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-faq" class="content scaffold-list" role="main">
			<h1><span class="glyphicon glyphicon-th-list"></span> <g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table class="table">
				<thead>
					<tr>
					
						<g:sortableColumn property="question" title="${message(code: 'faq.question.label', default: 'Question')}" />
					
						<g:sortableColumn property="answer" title="${message(code: 'faq.answer.label', default: 'Answer')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${faqInstanceList}" status="i" var="faqInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td class="hd2"><g:link action="show" id="${faqInstance.id}">${fieldValue(bean: faqInstance, field: "question")}</g:link></td>
					
						<td class="hd2">${fieldValue(bean: faqInstance, field: "answer")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${faqInstanceTotal}" />
			</div>
		</div>
		</div>
			<br>
	</body>
</html>
