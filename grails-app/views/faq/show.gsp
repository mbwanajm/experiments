
<%@ page import="com.mbwanajm.darproperty.Faq" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'faq.label', default: 'Faq')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
	<div class="container">
		
	
			<div class="row">
			<div class="col-sm-5">
			
			<a href="#show-faq" class="skip" tabindex="-1" title="<g:message code="default.link.skip.label" default="Skip to content&hellip;"/>">
		<span class="glyphicon glyphicon-refresh"></span></a>
	
			</div>
			
			</div>
	
	<br>
	
		
		<div class="nav" role="navigation">
			<ul>
				
				<li class="btn btn-default">	<g:link controller ="config" action=""><span class="glyphicon glyphicon-wrench"></span> </g:link></li>
				<li class="btn btn-default"><span class="glyphicon glyphicon-th-list"></span>
				<g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li  class="btn btn-default"> <span class="glyphicon glyphicon-plus"></span>
				<g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-faq" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list faq">
			
				<g:if test="${faqInstance?.question}">
				<li class="fieldcontain">
					<span id="question-label" class="property-label"><g:message code="faq.question.label" default="Question" /></span>
					
						<span class="property-value" aria-labelledby="question-label"><g:fieldValue bean="${faqInstance}" field="question"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${faqInstance?.answer}">
				<li class="fieldcontain">
					<span id="answer-label" class="property-label"><g:message code="faq.answer.label" default="Answer" /></span>
					
						<span class="property-value" aria-labelledby="answer-label"><g:fieldValue bean="${faqInstance}" field="answer"/></span>
					
				</li>
				</g:if>
			
			</ol>
			
			<div class="row">
			<div class="col-sm-12">
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${faqInstance?.id}" />
					<g:link  class="btn btn-warning" action="edit" id="${faqInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="btn btn-danger" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
			</div>
			</div>
		</div>
		</div>
		<br>
	</body>
</html>
