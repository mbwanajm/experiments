<head>
    <meta name='layout' content='main'/>
    <title><g:message code="springSecurity.denied.title"/></title>
</head>

<body>
<div class='row'>
    <div class='col-sm-12' align="center">
        <g:link controller="config" ><i class="fa fa-wrench text-muted"></i> Config</g:link>
        <div class='alert alert-warning'><g:message code="springSecurity.denied.message"/></div>

    </div>
</div>
</body>
