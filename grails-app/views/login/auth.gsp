<html>
<head>
    <meta name='layout' content='main'/>
    <title>DarProperty>><g:message code="springSecurity.login.title"/></title>
    <style type='text/css' media='screen'>
    #login {
        margin: 15px 0px;
        padding: 0px;
        text-align: center;
        font-family: Quicksand_Regular, tunga, arial;
        font-size: 11pt;
        color: #fff;
    }

    #login .inner {
        width: 340px;
        padding-bottom: 6px;
        margin: 60px auto;
        text-align: left;
        border: 1px solid #aab;
        background-color: #000;
        /* -moz-box-shadow: 2px 2px 2px #eee;
         -webkit-box-shadow: 2px 2px 2px #eee;
         -khtml-box-shadow: 2px 2px 2px #eee;
         box-shadow: 2px 2px 2px #eee;
         box-shadow:0px 2px 8px;*/
        border-radius: 5px;
    }

    #login .inner .fheader {
        border-radius: 5px;
        padding: 18px 26px 14px 26px;
        background-color: #090909;
        margin: 0px 0 14px 0;
        color: #ed1c24;
        font-size: 18px;
        font-weight: bold;
        text-align: center;
        border-bottom: 0px solid #ed1c24;
    }

    #login .inner .cssform p {
        clear: left;
        margin: 0;
        padding: 4px 0 3px 0;
        padding-left: 105px;
        margin-bottom: 20px;
        height: 1%;
    }

    #login .inner .cssform input[type='text'] {
        width: 120px;
        padding: 5px;
    }

    #login .inner .cssform label {
        font-weight: bold;
        float: left;
        text-align: right;
        margin-left: -105px;
        width: 110px;
        padding-top: 3px;
        padding-right: 10px;
    }

    #login #remember_me_holder {
        padding-left: 120px;
    }

    #login #submit {
        margin-left: 15px;
    }

    #login #remember_me_holder label {
        float: none;
        margin-left: 0;
        text-align: left;
        width: 200px
    }

    #login .inner .login_message {
        padding: 6px 25px 20px 25px;
        color: #c33;
    }

    #login .inner .text_ {
        width: 120px;
    }

    #login .inner .chk {
        height: 12px;
    }
    </style>
</head>

<body>

<div id='login'>
    <div class='inner'>
        <div class='fheader'><g:message code="springSecurity.login.header"/></div>
        <hr class="style-one">
        <g:if test='${flash.message}'>
            <div class='alert alert-danger'>${flash.message}</div>
        </g:if>

        <form action='${postUrl}' method='POST' id='loginForm' class='form-horizontal' autocomplete='off' role='form'>
            <div class="form-group" align="center">

                <label class="col-sm-offset-1 col-sm-3" for='username'><g:message
                        code="springSecurity.login.username.label"/>:</label>

                <div class="col-sm-6">
                    <input type='text' name='j_username' id='username' autocomplete="off" class="form-control"
                           required="required"/>
                </div>

            </div>

            <div class="form-group">
                <label class="col-sm-offset-1 col-sm-3" for='password'><g:message
                        code="springSecurity.login.password.label"/>:</label>

                <div class="col-sm-6">
                    <input type='password' name='j_password' id='password' class="form-control"
                           required="required"/>
                </div>
            </div>

            <p id="remember_me_holder">
                <input type='checkbox' class='chk' name='${rememberMeParameter}' id='remember_me'
                       <g:if test='${hasCookie}'>checked='checked'</g:if>/>
                <label for='remember_me'><g:message code="springSecurity.login.remember.me.label"/></label>
            </p>

            <div class="form-group ">
                <div class="col-sm-offset-6 col-sm-4 ">
                    <input type='submit' id="submit" class="btn btn-primary btnm"
                           value='${message(code: "springSecurity.login.button")}'/>
                </div>
            </div>
        </form>

        <div class="row ">
            <div class="col-lg-6" align="center">
                <small><a href="https://dashboard.tawk.to/" style="color:#FFF" target="_blank"> chat <i class="fa fa-user"></i></a></small>
            </div>
            <div class="col-lg-6" align="center">
                <small><a href="https://mail.darproperty.net" style="color:#FFF" target="_blank"> mail <i class="fa fa-envelope-square"></i></a></small>
            </div>

        </div>
    </div>
</div>
<script type='text/javascript'>
    <!--
    (function () {
        document.forms['loginForm'].elements['j_username'].focus();
    })();
    // -->
</script>
</body>
</html>
