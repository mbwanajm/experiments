<%@ page import="com.mbwanajm.darproperty.Region" %>



<div class="fieldcontain ${hasErrors(bean: regionInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="region.name.label" default="Name" />
		
	</label>
	<g:textField name="name" value="${regionInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: regionInstance, field: 'region_areas', 'error')} ">
	<label for="region_areas">
		<g:message code="region.region_areas.label" default="Regionareas" />
		
	</label>
	<g:select name="region_areas" from="${com.mbwanajm.darproperty.Region_areas.list()}" multiple="multiple" optionKey="id" size="5" value="${regionInstance?.region_areas*.id}" class="many-to-many"/>
</div>

