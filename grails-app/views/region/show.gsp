
<%@ page import="com.mbwanajm.darproperty.Region" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'region.label', default: 'Region')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
	<div class="container">
		
	
			<div class="row">
			<div class="col-sm-5">
			
			<a href="#show-region" class="skip" tabindex="-1" title="<g:message code="default.link.skip.label" default="Skip to content&hellip;"/>">
		<span class="glyphicon glyphicon-refresh"></span></a>
	
			</div>
			
			</div>
	
	<br>
		
		
		<div class="nav" role="navigation">
			<ul>
				<li class="btn btn-default"><span class="glyphicon glyphicon-th-list"></span> 
				<g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li  class="btn btn-default"> <span class="glyphicon glyphicon-plus"></span>
				<g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-region" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list region">
			
				<g:if test="${regionInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="region.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${regionInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${regionInstance?.region_areas}">
				<li class="fieldcontain">
					<span id="region_areas-label" class="property-label"><g:message code="region.region_areas.label" default="Regionareas" /></span>
					
						<g:each in="${regionInstance.region_areas}" var="r">
						<span class="property-value" aria-labelledby="region_areas-label"><g:link controller="region_areas" action="show" id="${r.id}">${r?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
			</ol>
			<div class="row">
			<div class="col-sm-12">
			
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${regionInstance?.id}" />
					<g:link  class="btn btn-warning"  action="edit" id="${regionInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="btn btn-danger" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
			</div>
			</div>
			
		</div>
		</div>
		<br>
	</body>
</html>
