<%@ page import="com.mbwanajm.darproperty.Interestrates" %>



<div class="fieldcontain ${hasErrors(bean: interestratesInstance, field: 'country', 'error')} required">
	<label for="country">
		<g:message code="interestrates.country.label" default="Country" />
		<span class="required-indicator">*</span>
	</label>
	%{--<g:textField name="country" required="" value="${interestratesInstance?.country}"/>--}%
	<g:select id="country" name="country"
			  from="${com.mbwanajm.darproperty.Country.list()}"
			  optionKey="name" required=""
			  value="${interestratesInstance?.country}"
			  />
</div>

<div class="fieldcontain ${hasErrors(bean: interestratesInstance, field: 'bank', 'error')} required">
	<label for="bank">
		<g:message code="interestrates.bank.label" default="Bank" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="bank" name="bank"
			  from="${com.mbwanajm.darproperty.Banks.list()}"
			  optionKey="id" required=""
			  value="${interestratesInstance?.bank?.id}"
			  class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: interestratesInstance, field: 'rate', 'error')} required">
	<label for="rate">
		<g:message code="interestrates.rate.label" default="Rate" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="rate" value="${fieldValue(bean: interestratesInstance, field: 'rate')}" required="" />
</div>

