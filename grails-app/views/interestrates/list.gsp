
<%@ page import="com.mbwanajm.darproperty.Interestrates" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'interestrates.label', default: 'Interestrates')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-interestrates" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="create" controller="config" action="index"> <span class="glyphicon glyphicon-wrench"></span></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-interestrates" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table class="table table-responsive">
				<thead class="hd2">
					<tr>
					
						<g:sortableColumn property="country" title="${message(code: 'interestrates.country.label', default: 'Country')}" />
					
						<th><g:message code="interestrates.bank.label" default="Bank" /></th>
					
						<g:sortableColumn property="rate" title="${message(code: 'interestrates.rate.label', default: 'Rate')}" />
					
					</tr>
				</thead>
				<tbody class="hd2">
				<g:each in="${interestratesInstanceList}" status="i" var="interestratesInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${interestratesInstance.id}">${fieldValue(bean: interestratesInstance, field: "country")}</g:link></td>
					
						<td>${fieldValue(bean: interestratesInstance, field: "bank")}</td>
					
						<td>${fieldValue(bean: interestratesInstance, field: "rate")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${interestratesInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
