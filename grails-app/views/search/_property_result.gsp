<%@ page import="com.mbwanajm.darproperty.CurrencyService;" %>
<%@ page import="java.text.NumberFormat" %>
<% CurrencyService currencyService = new CurrencyService(); %>
<div class="hidden-sm hidden-xs">
    <br>
    <br>
    <br>
</div>

<div class="container">
    <div class='row smooth-border'>
        <div class="col-sm-4  wing " align="center">

            <g:link controller="property" action="viewProperty"
                    params="${[id: property.id, agentId: property.agent.id]}">
                <g:each var="photo" in="${property.photos}">
                    <g:if test="${photo.name.contentEquals('main')}">
                        <img class="img-responsive"
                             src="<g:createLink controller='image' action='renderPropertyPhoto' id='${photo.id}'/>"/>

                        <g:if test="${property.approved}">
                            <i class="txt text-uppercase" title="<g:message code="approved"/>"><span
                                    class="img-rounded fa fa-check-circle"></span></i>
                        </g:if>


                        <h2 class="text-uppercase">
                            ${property.saleType.name}
                        </h2>

                        <% def Show = "thumb" %>

                        <g:render template="../tool/autoValueCalculator"
                                  model="[pid: property.id, stype: property.saleType, psize: property.size, regionLocated: property.region, areaLocated: property.area, show: Show, ptype: property.type, selectedCurrency: property.currency, price: property.price]"></g:render>

                    </g:if>

                </g:each>

            </g:link>

        </div>
        %{--Contents--}%
        <div class="col-sm-5">
            <div class="row">
                <div class="col-xs-12 ">
                    <g:link controller="property" action="viewProperty"
                            params="${[id: property.id, agentId: property.agent.id]}">
                        <h4 class="text-capitalize">
                            <g:if test="${property.saleType.name == 'for rent'}">
                                ${property.type}  <g:message
                                    code="property.saleType.forRent"/>, ${"$property.area,  $property.region"}
                            </g:if>
                            <g:elseif test="${property.saleType.name == 'for sale'}">
                                ${property.type}   ${property.saleType.name}, ${"$property.area,  $property.region"}
                            </g:elseif>
                            <g:else>
                                ${property.type}   <g:message
                                    code="property.saleType.jointVenture"/>, ${"$property.area,  $property.region"}
                            </g:else>
                        </h4>

                    </g:link>
                    <p class="text-capitalize hidden-xs hidden-sm">
                        ${property.location} ${property.area} ${property.region} ${property.country}
                    </p>

                </div>
            </div>

            <div class='row'>
                <div class="col-xs-12" align="left">
                    <table class="res-table" style="">
                        <tbody>
                        <tr class="hd1s text-center pricetag">
                            <td class="">
                                <div class="hidden-lg hidden-md">
                                    <span class="big-price" style="font-size:10pt;">

                                        <% def main = 'main' %>
                                        <g:render template="../property/priceViewer"
                                                  model="[property: property, main: main]"></g:render>

                                    </span>
                                    <span class="mid-price" style="font-size:9pt;">${property.currency}
                                        <a tabindex="0"
                                           class="text-info"
                                           role="button" data-toggle="popover"
                                           data-placement="bottom"
                                           data-trigger="focus"
                                           title="Price Convertor"
                                           data-content="
                                          TZS ${NumberFormat.getInstance().format(currencyService.convert(property.price, property.currency, 'TZS'))}
                    , RWF ${NumberFormat.getInstance().format(currencyService.convert(property.price, property.currency, 'RWF'))}
               , KES ${NumberFormat.getInstance().format(currencyService.convert(property.price, property.currency, 'KES'))}
               , USD ${NumberFormat.getInstance().format(currencyService.convert(property.price, property.currency, 'USD'))}
                                           "><span
                                                class="fa fa-chevron-circle-down"></span></a></span>
                                </div>

                                <div class="hidden-sm hidden-xs">
                                    <span class="big-price">

                                        <% main = 'main' %>
                                        <g:render template="../property/priceViewer"
                                                  model="[property: property, main: main]"></g:render>

                                    </span>
                                    <span class="mid-price" style="font-size:9pt;">${property.currency}
                                        <a tabindex="0"
                                           class="text-info"
                                           role="button" data-toggle="popover"
                                           data-placement="bottom"
                                           data-trigger="focus"
                                           title="Price Convertor"
                                           data-content="
                                             TZS ${NumberFormat.getInstance().format(currencyService.convert(property.price, property.currency, 'TZS'))}
                    , RWF ${NumberFormat.getInstance().format(currencyService.convert(property.price, property.currency, 'RWF'))}
               , KES ${NumberFormat.getInstance().format(currencyService.convert(property.price, property.currency, 'KES'))}
               , USD ${NumberFormat.getInstance().format(currencyService.convert(property.price, property.currency, 'USD'))}
                                           "><span
                                                class="fa fa-chevron-circle-down"></span></a></span>
                                </div>
                            </td>
                            <td class="">

                                <% Show = "short" %>

                                <g:render template="../tool/autoValueCalculator"
                                          model="[pid: property.id, stype: property.saleType, psize: property.size, regionLocated: property.region, areaLocated: property.area, show: Show, ptype: property.type, selectedCurrency: property.currency, price: property.price]"></g:render>

                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
                <br>

                <div class="row " align="left">
                    <div class="col-xs-12">

                        <table class="res-table" style="padding: 2%;">
                            <tbody>
                            <tr>
                                <td class="hidden-sm hidden-xs">
                                    <small><g:render template="../property/post_age_counter"
                                                     model="[property: property]"></g:render></small>
                                </td>

                                <td class="hidden-sm hidden-xs">
                                    <g:link controller="property" action="viewProperty"
                                            params="${[id: property.id, agentId: property.agent.id]}"
                                            style="color:#bc0417;font-size:18pt" title="view">
                                        <span class="fa fa-eye">${property.views}</span>
                                    </g:link>
                                </td>
                                <td>
                                    <a href="#" data-toggle="modal" data-target=".contact-us${property.id}">
                                        <span class="glyphicon glyphicon-envelope" style="color:#bc0417;font-size:18pt"
                                              title="Contact Us"></span>
                                    </a>
                                </td>
                                <td class="">
                                    <a href="http://www.facebook.com/dialog/feed?app_id=1670558763159469
                                    &redirect_uri=http%3A%2F%2Fwww.darproperty.co.tz%2Fproperty%2FviewProperty%2F${property.id}?agentId=${property.agent.id}
                                    &link=http%3A%2F%2Fwww.darproperty.co.tz%2Fproperty%2FviewProperty%2F${property.id}?agentId=${property.agent.id}%3FSThisFB"
                                       target="_blank" style="color:#4a6ea9;font-size:19pt" title="Facebook share">
                                        <span class="fa fa-facebook-square"></span>
                                    </a>
                                </td>
                                <td class="">
                                    <!--class="twitter-share-button"{count}  -->
                                    <a href="https://twitter.com/intent/tweet?text=See%20now%20${property.type}%20${property.saleType}%20at%20${property.area}%20,%20${property.region},%20${property.country}%20VIA%20@thedarproperty%20in&amp;url=http%3A%2F%2Fwww.darproperty.co.tz%2Fproperty%2FviewProperty%2F${property.id}?agentId=${property.agent.id}"
                                       class="shortenUrl"
                                       data-social-url="https://twitter.com/intent/tweet?text=See+now+${property.type}+${property.saleType}+at+${property.area}+,+${property.region},+${property.country}+VIA+@thedarproperty+in&amp;url="
                                       data-target-url="http://www.darproperty.co.tz/property/viewProperty/${property.id}?agentId=${property.agent.id}"
                                       target="_blank" style="color:#659fcb;font-size: 19pt;" title="Twitter share">
                                        <sapn class="fa fa-twitter-square"></sapn>
                                    </a>
                                </td>
                                <td class="hidden-lg">
                                    <a
                                            href="whatsapp://send?text= Check  ${property.type} ${property.saleType} at  ${property.area}, ${property.region}, ${property.country} READ MORE:-> http://www.darproperty.co.tz/property/viewProperty/${property.id}?agentId=${property.agent.id}"
                                            data-action="share/whatsapp/share"
                                            target="_blank" style="color:#2ab200;font-size:19pt;"
                                            title="Whatsapp share">
                                        <img
                                                src="${resource(dir: 'bootstrap/imgs/', file: 'whatsappgreen.png')}"
                                                alt="Whatsapp"
                                                width="35"
                                                title="Share on Whatsapp"/>
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
                <br>

                <div class="row">
                    <dp:ifCurrentlyLoggedIn id="${property.agent.id}">
                        <div class="col-xs-6" align="right">
                            <g:link class='btn btn-danger' controller="property" action="delete"
                                    params="${[propertyId: property.id]}">
                                delete</g:link>
                        </div>

                        <div class="col-xs-6" align="left">
                            <g:link class='btn btn-success' controller="property" action="editProperty"
                                    params="${[propertyId: property.id]}">
                                edit</g:link>
                        </div>

                    </dp:ifCurrentlyLoggedIn>
                    <dp:ifCurrentlyLoggedIn id="${2}">
                        <g:if test="${property.approved}">
                            <div class="col-xs-12" align="right">
                                <br>
                                <g:link class='btn btn-warning btn-block' controller="property" action="disapprove"
                                        params="${[propertyId: property.id]}">
                                    <i class="fa fa-close"></i> disapprove</g:link>
                            </div>
                        </g:if><g:else>


                        <div class="col-xs-12" align="right">
                            <br>
                            <g:link class='btn btn-primary btn-block' controller="property" action="approve"
                                    params="${[propertyId: property.id]}">
                                <i class="fa fa-check-circle"></i> approve</g:link>
                        </div>
                    </g:else>

                    </dp:ifCurrentlyLoggedIn>

                </div>
            </div>

        </div>


        <div class="col-sm-offset-1 col-sm-2">
            <div class="row">

            </div>

            <br>
            <br>

            <div class="row">
                <g:render template="../property/property_icons_search" model="[property: property]"></g:render>

            </div>

            <br>
            <br>

            <div class="row">
                <div class="margine2"></div>

                <g:link controller="agent" action="profile" id="${property.agent.id}">
                    <g:if test="${property.agent.profile.photo == null}">
                        <img class='img-polaroid img-responsive'
                             src="${resource(dir: 'images/property/', file: 'agent.png')} " width="100"/>
                    </g:if>
                    <g:else>
                        <img class="agent-photo img-responsive" title="${property.agent.username}"
                             src="<g:createLink controller='image' action='renderAgentPhoto'
                                                id='${property.agent.id}'/>" width="100"/>
                    </g:else>

                </g:link>

            </div>

        </div>
    </div>
</div>

<g:render template="../search/extras" model="[property: property]"></g:render>
