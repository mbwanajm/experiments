<form action="../search/searchbyName" method="post">
    <div class="row form-group">
        <div class="col-sm-offset-2 col-sm-5 col-sm-offset-2">
            <input type="hidden" name="tab" value="forsale">
            <input type="text" class="form-control" required=""
                   placeholder="<g:message code="search.byname"></g:message>"
                   value="${params.name?params.name:''}"
                   name="name" aria-label="..."
                   style="text-align: center;"/>

        </div>

        <div class="col-sm-2 col-sm-offset-1">
            <button type="submit" class="form-control btn-primary"><span class="glyphicon glyphicon-search"></span>
            </button>
        </div>
    </div>
</form>
<br>

