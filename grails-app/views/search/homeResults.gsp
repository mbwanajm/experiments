<!DOCTYPE html>



<%@ page import="java.io.*" %>

<html lang="en">
<head>
    <title>DarProperty>>Search Results</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="layout" content="main"/>


<%@ page import="com.mbwanajm.darproperty.Profile; com.mbwanajm.darproperty.Country; com.mbwanajm.darproperty.Tag; com.mbwanajm.darproperty.SaleType; com.mbwanajm.darproperty.Region; com.mbwanajm.darproperty.Type" %>

<g:javascript src='darproperty/CountryLoader.js'/>
<g:javascript src='darproperty/RegionLoader.js'/>
<g:javascript src='darproperty/PriceRangeUpdater.js'/>
<script>

</script>
</head>

<div class="container-fluid">
    <div class="row">
        <g:render template="../search/homeSearch" model="[tab1: tab1, tab2: tab2, tab3: tab3]"></g:render>

    </div>
</div>

<div class="margine"></div>

<div class="container">

    <div class="row">
        <div class="col-xs-12">

            <g:if test="${searchResult.size() != 0}">
                <p class="alert alert-success text-center">${searchResult.totalCount} <g:message
                        code="property.results.found"/>.
                    <br>
                    <a href="../search/user_enquiry#addenquiry">
                        If you don't find what are you looking for.  <i><span
                            class="glyphicon glyphicon-info-sign"></span> [Click Here]</i>
                    </a>
                </p>

                <div class="row hd4">
                    <div class="col-sm-2">
                        <span><g:message code="property.search.sort"/></span>
                    </div>

                    <div class="col-sm-2">
                        <form action="${sortfrom}" method="post" params="${searchTerms}">
                            <g:if test="${!params.name}">
                                <input type="hidden" name="saleType" value="${params.saleType}">
                                <input type="hidden" name="tab" value="${params.tab}">
                                <input type="hidden" name="type" value="${params.type}">
                                <input type="hidden" name="agent" value="${params.agent}">
                                <input type="hidden" name="country" value="${params.country}">
                                <input type="hidden" name="region" value="${params.region}">
                                <input type="hidden" name="area" value="${params.area}">
                                <input type="hidden" name="sortBy" value="latest">
                                <button type="submit" name="latest" class="form-control ${latest}">
                                    <span class="glyphicon glyphicon-fire"></span>
                                    <g:message code="property.search.sort.latest"/>
                                </button>
                            </g:if><g:else>

                            <input type="hidden" name="tab" value="${params.tab}">
                            <input type="hidden" name="name" value="${params.name}">
                            <input type="hidden" name="sortBy" value="latest">
                            <button type="submit" name="latest" class="form-control ${latest}">
                                <span class="glyphicon glyphicon-fire"></span>
                                <g:message code="property.search.sort.latest"/>
                            </button>
                        </g:else>

                        </form>
                    </div>

                    <div class="col-sm-2">
                        <form action="${sortfrom}" method="post" params="${searchTerms}">
                            <g:if test="${!params.name}">
                                <input type="hidden" name="saleType" value="${params.saleType}">
                                <input type="hidden" name="tab" value="${params.tab}">
                                <input type="hidden" name="type" value="${params.type}">
                                <input type="hidden" name="agent" value="${params.agent}">
                                <input type="hidden" name="country" value="${params.country}">
                                <input type="hidden" name="region" value="${params.region}">
                                <input type="hidden" name="area" value="${params.area}">
                                <input type="hidden" name="sortBy" value="oldest">
                                <button type="submit" name="oldest" class="form-control ${old}">
                                    <span class="glyphicon glyphicon-tint"></span>
                                    <g:message code="property.search.sort.oldest"/>
                                </button>
                            </g:if><g:else>
                            <input type="hidden" name="tab" value="${params.tab}">
                            <input type="hidden" name="name" value="${params.name}">
                            <input type="hidden" name="sortBy" value="oldest">
                            <button type="submit" name="oldest" class="form-control ${old}">
                                <span class="glyphicon glyphicon-tint"></span>
                                <g:message code="property.search.sort.oldest"/>
                            </button>
                        </g:else>
                        </form>
                    </div>

                    <div class="col-sm-2">
                        <form action="${sortfrom}" method="post" params="${searchTerms}">
                            <g:if test="${!params.name}">
                                <input type="hidden" name="saleType" value="${params.saleType}">
                                <input type="hidden" name="tab" value="${params.tab}">
                                <input type="hidden" name="type" value="${params.type}">
                                <input type="hidden" name="agent" value="${params.agent}">
                                <input type="hidden" name="country" value="${params.country}">
                                <input type="hidden" name="region" value="${params.region}">
                                <input type="hidden" name="area" value="${params.area}">
                                <input type="hidden" name="sortBy" value="cheapest">
                                <button type="submit" name="cheapest" class=" form-control ${cheap}">
                                    <span class="glyphicon glyphicon-minus"></span>
                                    <g:message code="property.search.sort.cheapest"/>
                                </button>
                            </g:if><g:else>
                            <input type="hidden" name="tab" value="${params.tab}">
                            <input type="hidden" name="name" value="${params.name}">
                            <input type="hidden" name="sortBy" value="cheapest">
                            <button type="submit" name="cheapest" class=" form-control ${cheap}">
                                <span class="glyphicon glyphicon-minus"></span>
                                <g:message code="property.search.sort.cheapest"/>
                            </button>
                        </g:else>
                        </form>
                    </div>

                    <div class="col-sm-2">
                        <form action="${sortfrom}" method="post" params="${searchTerms}">
                            <g:if test="${!params.name}">
                                <input type="hidden" name="saleType" value="${params.saleType}">
                                <input type="hidden" name="tab" value="${params.tab}">
                                <input type="hidden" name="type" value="${params.type}">
                                <input type="hidden" name="agent" value="${params.agent}">
                                <input type="hidden" name="country" value="${params.country}">
                                <input type="hidden" name="region" value="${params.region}">
                                <input type="hidden" name="area" value="${params.area}">
                                <input type="hidden" name="sortBy" value="expensive">
                                <button type="submit" name="expensive" class="form-control ${expensive}">
                                    <span class="glyphicon glyphicon-plus"></span>
                                    <g:message code="property.search.sort.expensive"/>
                                </button>
                            </g:if><g:else>
                            <input type="hidden" name="tab" value="${params.tab}">
                            <input type="hidden" name="name" value="${params.name}">
                            <input type="hidden" name="sortBy" value="expensive">
                            <button type="submit" name="expensive" class="form-control ${expensive}">
                                <span class="glyphicon glyphicon-plus"></span>
                                <g:message code="property.search.sort.expensive"/>
                            </button>
                        </g:else>
                        </form>
                    </div>

                    <div class="col-sm-2">
                        <form action="${sortfrom}" method="post" params="${searchTerms}">
                            <g:if test="${!params.name}">
                                <input type="hidden" name="saleType" value="${params.saleType}">
                                <input type="hidden" name="tab" value="${params.tab}">
                                <input type="hidden" name="type" value="${params.type}">
                                <input type="hidden" name="agent" value="${params.agent}">
                                <input type="hidden" name="country" value="${params.country}">
                                <input type="hidden" name="region" value="${params.region}">
                                <input type="hidden" name="area" value="${params.area}">
                                <input type="hidden" name="sortBy" value="views">
                                <button type="submit" name="views" class="form-control ${view}">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                    <g:message code="property.search.sort.views"/>
                                </button>
                            </g:if><g:else>
                            <input type="hidden" name="tab" value="${params.tab}">
                            <input type="hidden" name="name" value="${params.name}">
                            <input type="hidden" name="sortBy" value="expensive">
                            <input type="hidden" name="sortBy" value="views">
                            <button type="submit" name="views" class="form-control ${view}">
                                <span class="glyphicon glyphicon-eye-open"></span>
                                <g:message code="property.search.sort.views"/>
                            </button>
                        </g:else>
                        </form>
                    </div>

                </div>
            </g:if>
            <g:else>
                <p class="alert alert-danger"><g:message code="property.results.notfound"/>.</p>
            %{--<g:render template="search_suggestion"></g:render>--}%
            %{--<g:link controller="search" action="user_enquiry">--}%
                <a href="../search/user_enquiry#addenquiry">
                    I can't find what am looking for.  <i><span
                        class="glyphicon glyphicon-info-sign"></span> [Click Here]</i>
                </a>
            %{--<img src="../images/place_add.png" alt="Place your addvert here" class="img-responsive blink_me" title="place your advert" style="overflow: auto; ">--}%
            %{--</g:link>--}%
            </g:else>
            <br>
            <g:each var="property" in="${searchResult}">
                <g:render template="search_result" model="[property: property]"></g:render>
            </g:each>
            <div class="row" align="center">
                <div class='col-sm-12 pagination' align="center">

                    <g:if test="${searchResult}">
                        <g:paginate controller="search" action="newSearch"
                                    params="${searchTerms}"
                                    total="${searchResult?.totalCount}"
                                    prev="&lt;&lt;" next="&gt;&gt;"/>
                    </g:if>
                </div>

            </div>
        </div>

    </div>
</div>

</div>
<br>
</body>
</html>