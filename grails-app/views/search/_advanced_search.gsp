<%@ page import="com.mbwanajm.darproperty.Profile; com.mbwanajm.darproperty.Country; com.mbwanajm.darproperty.Tag; com.mbwanajm.darproperty.SaleType; com.mbwanajm.darproperty.Region; com.mbwanajm.darproperty.Type" %>

<g:javascript src='darproperty/CountryLoader.js'/>
<g:javascript src='darproperty/RegionLoader.js'/>
<g:javascript src='darproperty/PriceRangeUpdater.js'/>

<br>
<button type="button" class="btn btn-default">
    <span class="glyphicon glyphicon-search"></span></button>

<div class="d1">
    <div class="collapse">

        <div class="container" style="background-color:#000; color:#ccc; border:2px solid #000;">
            <div class="panel panel-default" style="background-color:#000; color:#ccc; border:2px solid #000;">
                <div class="panel-body" style="background-color:#000; color:#ccc; border:2px solid #000;">
                    <g:form class="form-inline" controller="search" action="search">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-3">
                                    <span class="hd2">TYPE:</span>
                                    <dp:i18Select
                                            id="formdt"
                                            name="type"
                                            optionPrefix="property.type"
                                            options="${Type.all}"
                                            showAnyOption="true"
                                            noSelectText="property.type"
                                            noSelectValue="any"/>
                                </div>

                                <div class="col-sm-3">
                                    <span class="hd2">Sale TYPE:</span>
                                    <dp:i18Select
                                            id='sale-type'
                                            id="formdt"
                                            name='saleType'
                                            optionPrefix="property.saleType"
                                            options="${SaleType.findAll().collect { it.name }}"
                                            showAnyOption="true"
                                            noSelectText="property.saleType"
                                            noSelectValue="any"/>
                                </div>

                                <div class="col-sm-3">
                                    <span class="hd2">From COUNTRY:</span>
                                    <dp:i18Select
                                            id="property_country"
                                            id="formdt"
                                            name='country'
                                            optionPrefix=""
                                            options="${Country.findAll().collect { it.name }}"
                                            showAnyOption="true"
                                            noSelectText="property.country"
                                            noSelectValue="any"/>
                                </div>

                                <div class="col-sm-2">
                                    <span class="hd2">From REGION:</span>
                                    <dp:i18Select
                                            id='property_region'
                                            id="formdt"
                                            name="region"
                                            optionPrefix=""
                                            options="${Region.findAll().collect { it.name }}"
                                            showAnyOption="true"
                                            noSelectText="property.region"
                                            noSelectValue="any"/>
                                </div>

                            </div>
                            <br>

                            <div class="row">
                                <div class="col-sm-3">
                                    <span class="hd2">From AREA:</span>
                                    <dp:i18Select
                                            id='property_areas'
                                            id="formdt"
                                            name="area"
                                            optionPrefix=""
                                            options="${Region.findByName("Dar es salaam").areas.sort()}"
                                            showAnyOption="true"
                                            noSelectText="property.area"
                                            noSelectValue="any"/>
                                </div>

                                <div class="col-sm-3">
                                    <span class="hd2">Price RANGE:</span>
                                    <dp:i18Select
                                            id='price'
                                            name="price"
                                            id="formdt"
                                            optionPrefix=""
                                            options="${['500 - 1000', '1000 - 2000', '2000 - 3000', '3000 - 4000', '>  5000']}"
                                            showAnyOption="true"
                                            noSelectText="property.selectPrice"
                                            noSelectValue="any"/>
                                </div>

                                <div class="col-sm-3">
                                    <span class="hd2">No. ROOMS:</span>
                                    <dp:i18Select
                                            id="formdt"
                                            name="numberOfRooms"
                                            optionPrefix=""
                                            options="${['1', '2', '3', '4', '5', '6', '7', '8', '>  8']}"
                                            showAnyOption="true"
                                            noSelectText="property.rooms"
                                            noSelectValue="any"/>

                                </div>

                            </div>
                            <br>

                            <div class="row">
                                <div class="col-sm-3">
                                    <span class="hd2">AGENT:</span>
                                    <dp:i18Select
                                            id="formdt"
                                            name="agent"
                                            optionPrefix=""
                                            options="${Profile.findAll().collect { it.company }.sort()}"
                                            showAnyOption="true"
                                            noSelectText="property.agent"
                                            noSelectValue="any"/>

                                </div>

                                <div class="col-sm-6" align="right">

                                    <g:submitButton class="btn btn-success btnm"
                                                    name="${g.message(code: 'property.search')}"/>

                                </div>
                            </div>
                        </div>
                    </g:form>
                </div>
            </div>
        </div>

    </div>
</div>
<br>


