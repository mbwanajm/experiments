<div class="row">
    <div class="col-sm-12">
        <ul class="nav nav-tabs" role="tablist">

            <li role="presentation" class="active"><a data-toggle="tab" href="#byTitle" class="hd2"><span
                    class="glyphicon glyphicon-th-large"></span> <g:message code="article.search.byTitle"></g:message>
            </a></li>
            <li role="presentation"><a data-toggle="tab" href="#byDate" class="hd2"><span
                    class="glyphicon glyphicon-th-list"></span> <g:message code="article.search.byName"></g:message></a>
            </li>
        </ul>
        <br>

        <div class="tab-content">
            <div id="byTitle" class="tab-pane fade in active">
                <form action="../search/searchArticle" method="post">
                    <input type="hidden" name="bytitle" value="title">

                    <div class="row form-group">
                        <div class="col-sm-offset-2 col-sm-5 col-sm-offset-2">

                            <input type="text" class="form-control" required=""
                                   placeholder="<g:message code="search.article"></g:message>" name="title"
                                   aria-label="..."
                                   style="text-align: center;"/>

                        </div>

                        <div class="col-sm-2 col-sm-offset-1">
                            <button type="submit" class="form-control btn-primary"><span
                                    class="glyphicon glyphicon-search"></span>
                            </button>
                        </div>
                    </div>
                </form>

            </div>

            <div id="byDate" class="tab-pane fade">

                <g:form controller="search" action="searchArticle" method="post">
                    <div class="row form-group">
                        <input type="hidden" name="date" value="date">

                        <div class="col-sm-offset-2 col-sm-5 col-sm-offset-2">
                            <p class="text-capitalize text-left">Articles from:</p>
                            <%
                                int currentYear = new GregorianCalendar().get(Calendar.YEAR)
                                String chooser = "<div class='row'>";
                                int i = 1;
                                chooser += "<div class='col-sm-4'> Day: <select name='day' class='form-control'>";
                                while (i <= 31) {
                                    chooser += "<option value='" + i + "'>" + i + "</option> ";
                                    i++;
                                }
                                chooser += "</select></div>";
                                chooser += "<div class='col-sm-4'>Month: <select name='month' class='col-sm-4 form-control'>";

                                i = 1;
                                while (i <= 12) {
                                    def a = ""
                                    switch (i) {
                                        case 1:
                                            a = "Jan";
                                            break;
                                        case 2:
                                            a = "Feb";
                                            break;
                                        case 3:
                                            a = "Mar";
                                            break;
                                        case 4:
                                            a = "Apr";
                                            break;
                                        case 5:
                                            a = "May";
                                            break;
                                        case 6:
                                            a = "Jun";
                                            break;
                                        case 7:
                                            a = "Jul";
                                            break;
                                        case 8:
                                            a = "Aug";
                                            break;
                                        case 9:
                                            a = "Sept";
                                            break;
                                        case 10:
                                            a = "Oct";
                                            break;

                                        case 11:
                                            a = "Nov";
                                            break;

                                        case 12:
                                            a = "Dec";
                                            break;
                                        default:
                                            a = null;

                                    }
                                    chooser += "<option value='" + i + "'>" + a + "</option> ";
                                    i++;
                                }
                                chooser += "</select></div>";
                                chooser += "<div class='col-sm-4'>Year: <select name='year' class='col-sm-4 form-control'>";

                                i = 2010;
                                while (i <= currentYear) {

                                    chooser += "<option value='" + i + "'>" + i + "</option> ";
                                    i++;
                                }
                                chooser += "</select></div></div>";
                                out.print(chooser);
                            %>
                        </div>

                        <div class="col-sm-2 col-sm-offset-1">
                            <br>
                            <button type="submit" class="form-control btn-primary"><span
                                    class="glyphicon glyphicon-search"></span>
                            </button>
                        </div>
                    </div>
                </g:form>
            </div>
        </div>
    </div>
</div>




<br>

