<%--
  Created by IntelliJ IDEA.
  User: Aloyce PM
  Date: 1/28/2016
  Time: 10:57 AM
--%>
<%@ page import="javax.mail.internet.InternetAddress; com.mbwanajm.darproperty.Agent; com.mbwanajm.darproperty.Enquiries" %>
<%@ page import="java.awt.geom.Arc2D.Double" %>


<html>
<head>
    <title>PROPERTY ENQUIRY</title>
    <meta name="layout" content="main"/>

    <%@ page
            import="com.mbwanajm.darproperty.Profile; com.mbwanajm.darproperty.Country; com.mbwanajm.darproperty.Tag; com.mbwanajm.darproperty.SaleType; com.mbwanajm.darproperty.Region; com.mbwanajm.darproperty.Type" %>
    <%@ page import="com.mbwanajm.darproperty.Enquiries" %>
    <g:javascript src='darproperty/CountryLoader.js'/>
    <g:javascript src='darproperty/RegionLoader.js'/>
    <g:javascript src='darproperty/PriceRangeUpdater.js'/>
<style>
input{
    font-size: 9pt;
    padding: 5px;
}
input[type="text"]{
         font-size: 9pt;
    padding: 5px;
     }
input[type="tel"]{
    font-size: 9pt;
    padding: 5px;
}
input[type="number"]{
    font-size: 9pt;
    padding: 5px;
}
input[type="email"]{
    font-size: 9pt;
    padding: 5px;
}
  select  option{
        font-size: 9pt;
        padding: 5px;
    }
</style>
</head>

<body id="top">

<br>

<div class="container">
    <div class="row">
        <div class="col-lg-12" align="center">
            <g:render template="process_enquiry"></g:render>
            <g:hasErrors bean="${flash.errorBean}">
                <div class="alert alert-block alert-warning">
                    <g:renderErrors bean="${flash.errorBean}" as="list"/>
                </div>
            </g:hasErrors>

            <g:if test="${flash.successMessage}">
                <div class='alert alert-success'>${flash.successMessage}</div>
            </g:if>
            <g:if test="${flash.errorMessage}">
                <div class='alert alert-danger'>${flash.errorMessage}</div>
            </g:if>
            <%
//                def agents = Agent.list()
//                String[] lis = agents.profile.email
//                String mails = ""
//                for (int i = 0; i < lis.length; i++) {
//
//                    if (lis[i] && lis[i] != "info@darproperty.net") {
//                        mails += "\""+lis[i]+"\""
//                        mails += ","
//
//                    }
//                }
////                mails += "\"firstajep@gmail.com\""
////                out.print("MAILS: ["+mails+"]")
//                String [] recipientList = mails.split(",")
//                InternetAddress[] recipientAddress = new InternetAddress[recipientList.length];
//                //loop and trim each address
//                int counter = 0;
//                            for (String recipient : recipientList) {
//                                recipientAddress[counter] = new InternetAddress(recipient.trim());
//                                counter++;
//
//                            }
//                recipientList.each {out.print(it+",\"firstajep@gmail.com\"")}
//                for(int i = 0; i < recipientAddress.length; i++){
//                    out.print(recipientAddress[i])
//                }
            %>
        </div>
    </div>

    <div class="row">

        <div class="col-sm-6 roundbox " align="">

            <a href="#addenquiry" class="btn btn-default pull-right"><span
                    class="glyphicon glyphicon-pushpin"></span> Post Your Enquiries
            </a> <br>

            <!-- LIST OF ENQUIRIES -->
            <span class="wid-view-more" align='center'><span class=''>PROPERTY</span> ENQUIRIES
            </span>



            <hr class='style-one'>

            <div class="row">
                %{--<g:form controller="search" action="searchArticle">--}%
                <div class="col-lg-12">
                    <p class="hd4">*This is the list of property enquiries from our visitors.</p>
                    %{--<g:textField name="searchkey" placeholder="Requester/Location/Sale type/Property type" class="form-control"></g:textField>--}%
                </div>
                %{--</g:form>--}%
            </div>

            <div class="row pagination ">
                <g:paginate total="${allEnquiry.count {}}"></g:paginate>
            </div>

            <div class="row table-autoflow-lg" style="height:1500px;">
                <g:render template="../enquiries/allEnquiries" collection="${allEnquiry}" var="enquiry"></g:render>
            </div>

            <div class="row pagination ">

                <g:paginate total="${allEnquiry.count {}}"></g:paginate>
            </div>

            <!-- LIST OF ENQUIRIES -->
            <br>

            <a href="#addenquiry" class="btn btn-default"><span
                    class="glyphicon glyphicon-pushpin"></span> Post Your Enquiries
            </a>

            <p class="hidden-lg hidden-md" align="right">
                <a title="Go to Top" href="#top"><small
                        class="gotop alert-primary"><span
                            class="glyphicon glyphicon-chevron-up "></span> Top</small></a>
            </p>

        </div>

        <div class="col-sm-6" align="center">

            <h3 class="wid-view-more">Are you looking for a property?<br> Fill the form below.</h3>
            <a href="#elist" class="hidden-lg hidden-md">
                <span class="glyphicon glyphicon-list-alt">
                </span> Enquiries List
            </a>

            <br>
        %{--FORM--}%

            <g:form class="form-horizontal suggestion" controller="enquiries"
                    action="sendEnquiries" method="post">
                <div class="form-group" id="addenquiry">
                    <label class="control-label col-sm-5">About <span
                            class="hd2">You</span></label>
                </div>
                <hr>

                <div class="form-group">
                    <label class="control-label col-sm-5">Name:</label>

                    <div class="col-sm-7">
                        <input type="text" name="names" id="names" class="form-control"
                               placeholder="Firstname Lastname" required="required" value="${params.names}"
                               autocomplete="off">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-5">Phone No:</label>

                    <div class="col-sm-7">
                        <input type="number" name="phone" id="phone"
                               class="form-control" maxlength="12"
                               placeholder="Eg: 0800000000 " required="required" value="${params.phone}"
                               autocomplete="off">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-5">Email:</label>

                    <div class="col-sm-7">
                        <input type="email" name="email" id="email" class="form-control"
                               placeholder="address@domain" required="required" value="${params.email}"
                               autocomplete="off">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-5">Due Date:</label>

                    <div class="col-sm-7">
                        <input type="text" name="duedate" class="form-control datepicker"
                               placeholder="End of your advert (YYYY-MM-DD)" required="required"
                               autocomplete="off">
                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label col-sm-5">Property <span
                            class="hd2">Descriptions</span></label>
                </div>
                <hr>

                <div class="form-group">
                    <label class="control-label col-sm-5">Property Type:</label>

                    <div class="col-sm-7 select_control">
                        <dp:i18Select name="type" optionPrefix="property.type"
                                      options="${Type.findAll().collect() { it.name }}"
                                      noSelectText="property.type"
                                      noSelectValue="any"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-5">Rooms <i class="text-muted">(If house/appartment)</i>:</label>

                    <div class="col-sm-7 select_control">
                        <dp:i18Select name="rooms" options="${1..10}" noSelectText="0" noSelectValue="0"/>
                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label col-sm-5">Sale Type:</label>

                    <div class="col-sm-7 select_control">
                        <dp:i18Select id='sale-type' name='saleType'
                                      optionPrefix="property.saleType"
                                      options="${SaleType.findAll().collect { it.name }}"
                                      noSelectText="property.saleType"
                                      noSelectValue="any"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-5">Property <span
                            class="hd2">Location</span></label>
                </div>
                <hr>

                <div class="form-group">
                    <label class="control-label col-sm-5">Country:</label>

                    <div class="col-sm-7 select_control">
                        <dp:i18Select id="property_country" name='country'
                                      optionPrefix=""
                                      options="${Country.findAll().collect { it.name }}"
                                      showAnyOption="true" noSelectText="property.country"
                                      noSelectValue="any"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-5">Region:</label>

                    <div class="col-sm-7 select_control">
                        <dp:i18Select id='property_region' name="region" optionPrefix=""
                                      options="${Region.findAll().collect { it.name }}"
                                      showAnyOption="true" noSelectText="property.region"
                                      noSelectValue="any"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-5">Area:</label>

                    <div class="col-sm-7 select_control">
                        <dp:i18Select id='property_areas' name="area" optionPrefix=""
                                      options="${Region.findByName("Dar es salaam").areas.sort()}"
                                      showAnyOption="true" noSelectText="property.area"
                                      noSelectValue="any"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-5">Property <span
                            class="hd2">Price Range</span></label>
                </div>
                <hr>

                <div class="form-group">
                    <label class="control-label col-sm-5">Currency:</label>

                    <div class="col-sm-7">
                        <select name="currency" class="form-control">
                            <option value="USD">US, $</option>
                            <option value="TZS">Tanzania, TZS</option>
                            <option value="KES">Kenya, KES</option>
                            <option value="RWF">Rwanda, RWF</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-5"><span class="hd3"><span
                            class="glyphicon glyphicon-arrow-up"></span> Max. Price:</span>
                    </label>

                    <div class="col-sm-7">
                        <input type="number" size="8" class="form-control"
                               name="maxPrice" value="value="${params.maxPrice}"" required="required">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-5"><span class="hd2"><span
                            class="glyphicon glyphicon-arrow-down"></span> Min. Price</span></label>

                    <div class="col-sm-7">
                        <input type="number" size="8" class="form-control"
                               value="${params.minPrice == null ? 0 : params.minPrice}"
                               name="minPrice" required="required">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-5">Other <span
                            class="hd2">Details</span></label>
                </div>
                <hr>

                <div class="form-group">
                    <label class="control-label col-sm-3"><span
                            class="glyphicon glyphicon-pencil"></span> Description
                    </label>

                    <div class="col-sm-9 ">
                        <textarea id='wysiwyg' name="desc"
                                  required="required">
                            <g:if test="${params.desc}">
                                ${params.desc}
                            </g:if>
                        </textarea>
                    </div>
                </div>

                <div class='form-group'>
                    <div class='col-sm-offset-5 col-sm-7'>
                        <img src="${createLink(controller: 'simpleCaptcha', action: 'captcha')}"/>
                        <br>
                        <label for="captcha">Type the letters above in the box below:</label>
                        <g:textField name="captcha" autocomplete="off"/>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-5 offset">
                        <div id="loading1">

                            <img src="${resource(dir: 'bootstrap/imgs/', file: '3.gif')}"/>
                            <span class="hd3">Sending...</span>
                            <!-- loading image -->

                        </div>
                    </div>

                    <div class="col-sm-7">
                        <button type="submit" name="suggest" id="suggest"
                                class="btn btn-success">
                            <span class="glyphicon glyphicon-envelope"></span> SUBMIT
                        </button>
                    </div>
                </div>
                <hr>

            </g:form>
        </div>

    </div>
</div>
<br>
</body>
</html>