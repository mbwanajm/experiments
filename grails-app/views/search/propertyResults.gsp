<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page
	import="grails.plugin.searchable.internal.util.StringQueryUtils"%>
<%@ page import="java.text.NumberFormat"%>
<html>
<head>
<title>DarProperty: Search Results</title>
<meta name="layout" content="main" />

<script type="text/javascript">
	$(document).ready(function() {
		$(".btn-default").click(function() {
			$(".collapse").collapse('toggle');
		});

	});
</script>
</head>

<body>
	<div class="container-fluid">
		<g:render template="../pageTitle" model="[title: 'Search Result(s)']"></g:render>
		<fieldset class="fieldparts"
			style="background-color: #000; color: #ccc;">

			<g:render template="/search/homeSearch" />
		</fieldset>
		<br>
		<div class="row">
			<div class='span12'>

				<g:if test="${searchResult.isEmpty()}">
					<div class='alert alert-error' align="center">
						<g:message code="property.search.noPropertyFound" />
					</div>
				</g:if>

				<g:else>
					<div class='alert alert-info' align="center">
						<g:message code="property.search.propertyFound" />
						:
						${searchResult?.getTotalCount()}
					</div>
				</g:else>

			</div>
		</div>

		<g:render template="/search/property_result"
			collection="${searchResult}" var="property" />

		<div class="row">
			<div class='span12 pagination' align="center">
				<br />
				<g:if test="searchResult">
					<g:paginate controller="search" action="search"
						params="${searchTerms}" total="${searchResult?.getTotalCount()}"
						prev="&lt; previous" next="next &gt;" />
				</g:if>
			</div>
		</div>
		<br> <br>
	</div>
</body>
</html>