<%@ page import="com.mbwanajm.darproperty.Profile; com.mbwanajm.darproperty.Country; com.mbwanajm.darproperty.Tag; com.mbwanajm.darproperty.SaleType; com.mbwanajm.darproperty.Region; com.mbwanajm.darproperty.Type" %>

<g:javascript src='darproperty/CountryLoader.js'/>
<g:javascript src='darproperty/RegionLoader.js'/>
<g:javascript src='darproperty/PriceRangeUpdater.js'/>

<%@ page import="tz.co.darproperty.*" %>
%{--<% SearchEngine search = new SearchEngine(); %>--}%
<div class="container zch">
    %{--<g:render template="../search/search_by_name"></g:render>--}%
    <div class="row">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="${tab1}"><a href="#home" aria-controls="home" role="tab"
                                                       data-toggle="tab"><g:message
                        code="property.search.title.buy"/></a>
            </li>
            <li role="presentation" class="${tab2}"><a href="#profile" aria-controls="profile" role="tab"
                                                       data-toggle="tab"><g:message
                        code="property.search.title.rent"/></a></li>
            <li role="presentation" class="${tab4}"><a href="#name"
                                                       aria-controls="messages"
                                                       role="tab"
                                                       data-toggle="tab"><g:message
                        code="property.search.title.name"/></a>
            </li>
            <li role="presentation" class="${tab3}" style="border-right: solid 0px #000"><a href="#messages"
                                                                                            aria-controls="messages"
                                                                                            role="tab"
                                                                                            data-toggle="tab"><g:message
                        code="property.search.title.advanced"/></a>
            </li>

        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane ${tab4}" id="name">

                <div class="container-fluid searchPad advanced">
                    <form action="../search/searchbyName" method="post">
                        <div class="row form-group">
                            <div class="col-sm-2" align="right">
                                Search by Name
                            </div>

                            <div class="col-sm-6 advanced">
                                <input type="hidden" name="tab" value="name">
                                <input type="hidden" name="sortBy" value="latest">
                                <input type="text" class="form-control" required=""
                                       placeholder="<g:message code="search.byname"></g:message>"
                                       value="${params.name ? params.name : ''}"
                                       name="name" aria-label="..."
                                       style="text-align: center;height:40px;"/>

                            </div>

                            <div class="col-sm-4">
                                <button type="submit" class="form-control btn-primary" name="sellSearch" style="height:40px;"><span
                                        class="glyphicon glyphicon-search"  ></span></button>
                            </div>
                        </div>
                    </form>

                </div>

            </div>

            <div role="tabpanel" class="tab-pane ${tab1}" id="home">
                %{--FORSALE--}%
                <div class="container-fluid searchPad advanced">

                    <br>
                    <g:form class="form-horizontal" controller="search" action="newSearch">
                        <input type="hidden" name="sortBy" value="latest">
                        <input type="hidden" name="tab" value="forsale">
                        <input type="hidden" name="saleType" value="for sale">

                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label for="type" class="control-label col-sm-4">
                        <g:message code="Type"/>:
                                </label>

                        <div class="col-sm-8">
                            <select class="form-control" name="type" id="type">
                                <option value="house"><g:message code="property.type.house"/></option>
                                <option value="plot"><g:message code="property.type.plot"/></option>
                                <option value="apartment"><g:message code="property.type.apartment"/></option>
                                <option value="farm"><g:message code="property.type.farm"/></option>
                                <option value="hotel"><g:message code="property.type.hotel"/></option>
                                <option value="warehouse"><g:message code="property.type.warehouse"/></option>
                                <option value="office space"><g:message
                                        code="property.type.officeSpace"/></option>
                            </select>
                        </div>

                        </div>

                        <div class="form-group col-sm-4 ">
                            <label for="country" class="control-label col-sm-4">
                                <g:message code="property.country"/>:
                            </label>

                            <div class="col-sm-8">
                                <dp:i18Select

                                        name='country'
                                        optionPrefix=""
                                        options="${Country.findAll().collect { it.name }}"
                                        showAnyOption="true"
                                        noSelectText="${params.country}"
                                        noSelectValue="any"/>
                            </div>

                        </div>


                        <div class="form-group col-sm-4">
                            <div class="col-sm-4 offset"></div>

                            <div class="col-sm-8">
                                <button type="submit" class="form-control btn-primary" name="sellSearch"><span
                                        class="glyphicon glyphicon-search"></span></button>
                            </div>
                        </div>

                    </g:form>
                </div>
                </div>

            </div>

            <div role="tabpanel" class="tab-pane ${tab2}" id="profile">

                <div class="container-fluid searchPad">
                    <div class="row">
                        <br>
                        <g:form class="form-horizontal" controller="search" action="newSearch">
                            <input type="hidden" name="sortBy" value="latest">
                            <input type="hidden" name="tab" value="forrent">
                            <input type="hidden" name="saleType" value="for rent">

                            <div class="form-group col-sm-4">
                                <label for="type" class="control-label col-sm-4">
                                    <g:message code="Type"/>:
                                </label>

                                <div class="col-sm-8">
                                    <select class="form-control" name="type" id="type">
                                        <option value="house"><g:message code="property.type.house"/></option>
                                        <option value="plot"><g:message code="property.type.plot"/></option>
                                        <option value="apartment"><g:message
                                                code="property.type.apartment"/></option>
                                        <option value="farm"><g:message code="property.type.farm"/></option>
                                        <option value="hotel"><g:message code="property.type.hotel"/></option>
                                        <option value="warehouse"><g:message
                                                code="property.type.warehouse"/></option>
                                        <option value="office space"><g:message
                                                code="property.type.officeSpace"/></option>
                                    </select>
                                </div>

                            </div>

                            <div class="form-group col-sm-4 ">
                                <label for="country" class="control-label col-sm-4">
                                    <g:message code="property.country"/>:
                                </label>

                                <div class="col-sm-8">
                                    <select name="country" class="form-control">
                                        <g:each var="country" in="${Country.findAll().collect { it.name }}">
                                            <option value="${country}">${country}</option>
                                        </g:each>
                                    </select>
                                </div>

                            </div>


                            <div class="form-group col-sm-4">
                                <div class="col-sm-4 offset"></div>

                                <div class="col-sm-8">
                                    <button type="submit" class="form-control btn-primary" name="rentsearch"><span
                                            class="glyphicon glyphicon-search"></span></button>
                                </div>
                            </div>

                        </g:form>
                    </div>
                </div>

            </div>
            <!-- ADVANCED -->
            <div role="tabpanel" class="tab-pane ${tab3}" id="messages">

                <div class="container searchPad advanced">
                    <div class="row">
                        <br>

                        <br>

                        <g:form class="form-horizontal" controller="search" action="advancedSearch">
                            <input type="hidden" name="sortBy" value="latest">
                            <input type="hidden" name="tab" value="advanced">

                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <span class=""><g:message code="property.type"/>:</span>
                                        <dp:i18Select
                                                name="type"
                                                optionPrefix="property.type"
                                                options="${Type.findAll().collect() { it.name }}"
                                                showAnyOption="true"
                                                noSelectText="${params.type}"
                                                noSelectValue="any"/>
                                    </div>

                                    <div class="col-sm-3">
                                        <span class=""><g:message code="property.saleType"/>:</span>
                                        <dp:i18Select
                                                id='sale-type'
                                                name='saleType'
                                                optionPrefix="property.saleType"
                                                options="${SaleType.findAll().collect { it.name }}"
                                                showAnyOption="true"
                                                noSelectText="${params.saleType}"
                                                noSelectValue="any"/>
                                    </div>

                                    <div class="col-sm-3">
                                        <span class=""><g:message code="property.country"/>:</span>
                                        <dp:i18Select
                                                id="property_country"
                                                name='country'
                                                optionPrefix=""
                                                options="${Country.findAll().collect { it.name }}"
                                                showAnyOption="true"
                                                noSelectText="${params.country}"
                                                noSelectValue="any"/>
                                    </div>

                                    <div class="col-sm-3">
                                        <span class=""><g:message code="property.agent"/>:</span>
                                        <dp:i18Select

                                                name="agent"
                                                optionPrefix=""
                                                options="${Profile.findAll().collect { it.company }.sort()}"
                                                showAnyOption="true"
                                                noSelectText="${params.agent}"
                                                noSelectValue="any"/>

                                    </div>

                                </div>
                                <br>

                                <div class="row">

                                    <div class="col-sm-3">
                                        <span class=""><g:message code="property.region"/>:</span>
                                        <dp:i18Select
                                                id='property_region'
                                                name="region"
                                                optionPrefix=""
                                                options="${Region.findAll().collect { it.name }}"
                                                showAnyOption="true"
                                                noSelectText="${params.region}"
                                                noSelectValue="any"/>
                                    </div>

                                    <div class="col-sm-3">
                                        <span class=""><g:message code="property.area"/>:</span>
                                        <dp:i18Select
                                                id='property_areas'

                                                name="area"
                                                optionPrefix=""
                                                options="${Region.findByName("Dar es salaam").areas.sort()}"
                                                showAnyOption="true"
                                                noSelectText="${params.region}"
                                                noSelectValue="any"/>
                                    </div>

                                    <div class="col-sm-3">

                                        <span><g:message code="property.currency"/>:</span>
                                        <select name="currency" class="form-control">
                                            <option value="USD"><% out.print(params.currency ? params.currency : 'USD'); %></option>
                                            <option value="USD">US, $</option>
                                            <option value="TZS">Tanzania, TZS</option>
                                            <option value="KES">Kenya, KES</option>
                                            <option value="RWF">Rwanda, RWF</option>
                                        </select>

                                    </div>

                                    <div class="col-sm-3 visible-lg visible-md">
                                        <br>
                                        <button type="submit" class="form-control btn-primary" name="advsearch"><span
                                                class="glyphicon glyphicon-search"></span></button>

                                    </div>
                                </div>

                                <br>

                                <div class="row">
                                    <div class="col-sm-offset-6 col-sm-3">
                                        <fieldset class="adv-search-fieldset">
                                            <legend><g:message code="property.priceRange"></g:message></legend>
                                            <span class="hd3"><span
                                                    class="glyphicon glyphicon-arrow-up"></span> <g:message
                                                    code="property.priceRange.max"/>
                                            </span>

                                            <input type="number" size="8" class="form-control" name="maxPrice"
                                                   value="<% out.print(params.maxPrice ? params.maxPrice : 0); %>">


                                            <br>
                                            <span class="hd2"><span
                                                    class="glyphicon glyphicon-arrow-down"></span>  <g:message
                                                    code="property.priceRange.min"/>
                                            </span>

                                            <input type="number" size="8" class="form-control"
                                                   value="<% out.print(params.minPrice ? params.minPrice : 0); %>"
                                                   name="minPrice"
                                                   required="required">

                                        </fieldset>

                                    </div>

                                    <div class="col-sm-3 visible-sm visible-xs ">
                                        <br>
                                        <button type="submit" class="form-control btn-primary" name="advsearch"><span
                                                class="glyphicon glyphicon-search"></span></button>

                                    </div>
                                </div>
                            </div>
                        </g:form>
                        <br>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<p class="hidden-desktop">
    <br>
    <br>
    <br>
</p>