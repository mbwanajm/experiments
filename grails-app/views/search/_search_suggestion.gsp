
<%@ page import="com.mbwanajm.darproperty.Profile; com.mbwanajm.darproperty.Country; com.mbwanajm.darproperty.Tag; com.mbwanajm.darproperty.SaleType; com.mbwanajm.darproperty.Region; com.mbwanajm.darproperty.Type" %>
    
   
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		 <g:javascript src="lib/processing.js"/>
		<g:javascript src='darproperty/CountryLoader.js'/>
		<g:javascript src='darproperty/RegionLoader.js'/>
		<g:javascript src='darproperty/PriceRangeUpdater.js'/>

<%@page import="tz.co.darproperty.*"%>
%{--<% SearchEngine search = new SearchEngine(); %>--}%





<div class="collapse suggestion" id="enquiry">
 <!-- FORM -->

 <h1 align="center"> Are you looking for any among these ?</h1>
 <p class="hidden-desktop hidden-tablet hd2">House, Apartment, Office Space, Hotel, Plot, Or Farm  </p>

	<p align="center" class="hidden-phone">
	<table align="center">
	<tr>
	<td align="center" class="hd4 hidden-phone"><img src="${resource(dir: 'images/property/', file: 'house.png')}" title="House"/><br><small>House</small></td>
	<td align="center" class="hd4 hidden-phone"><img src="${resource(dir: 'images/property/', file: 'apartment.png')}" title="Apartment"/><br><small>Apartment</small></td>
	<td align="center" class="hd4 hidden-phone"><img src="${resource(dir: 'images/property/', file: 'office.png')}" title="Office Space"/><br><small>Office Space</small></td>
	<td align="center" class="hd4 hidden-phone"><img src="${resource(dir: 'images/property/', file: 'hotel.png')}" title="Hotel"/><br><small>Hotel</small></td>
	<td align="center" class="hd4 hidden-phone"><img src="${resource(dir: 'images/property/', file: 'plot.png')}" title="Plot"/><br><small>Plot</small></td>
	<td align="center" class="hd4 hidden-phone"><img src="${resource(dir: 'images/property/', file: 'farm.png')}" title="Farm"/><br><small>Farm</small></td>
	</tr>
	</table>
	</p>
	 <h2 align="center"> FOR </h1>
	  <p class="hidden-desktop hidden-tablet hd2">Sale, Rent, Or Joint Venture </p>
	<p align="center">
	<table align="center">
	<tr>
	<td align="center" class="hd4 hidden-phone"><img src="${resource(dir: 'images/property/', file: 'for_sale.png')}" title="For Sale"/><br><small>Sale</small></td>
	<td>  </td>
	<td align="center" class="hd4 hidden-phone"><img src="${resource(dir: 'images/property/', file: 'for_rent.png')}" title="For Rent"/><br><small>Rent</small></td>
	<td>  </td>
	<td align="center" class="hd4 hidden-phone"><img src="${resource(dir: 'images/property/', file: 'joint.png')}" title="Joint Venture"/><br><small>Joint Venture</small></td>
	</tr>
	</table>
	</p>
	<br>
	
	<h1 align="center" id="addenquiry">Please fill the form below;</h1>
 <br>


<g:form class="form-horizontal suggestion"  controller="user_enquiry"  method="post">
<div class="form-group">
		<label class="control-label col-sm-5">About <span class="hd2">You</span></label>
		</div>
		 <hr>
        <div class="form-group">
		<label class="control-label col-sm-5"> Name: </label>
		<div class="col-sm-7">
		<input type="text" name="names" id="names" class="form-control" placeholder="Firstname Lastname"  required="required" autocomplete="off">
		</div>
		</div>
		<div class="form-group">
		<label class="control-label col-sm-5"> Phone No: </label>
		<div class="col-sm-7">
		<input type="text" name="phone" id="phone" class="form-control" maxlength="13" placeholder="Eg: 0800000000 " required="required" autocomplete="off">
		</div>
		</div>
		<div class="form-group">
		<label class="control-label col-sm-5"> Email: </label>
		<div class="col-sm-7">
		<input type="email" name="email" id="email" class="form-control" placeholder="address@domain" required="required" autocomplete="off">
		</div>
		</div>
		<div class="form-group">
		<label class="control-label col-sm-5">Property <span class="hd2">Descriptions</span></label>
		</div>
		<hr>
		<div class="form-group">
		<label class="control-label col-sm-5"> Property Type: </label>
		<div class="col-sm-7">
			<dp:i18Select
            name="type"
            optionPrefix="property.type"
            options="${Type.findAll().collect(){it.name}}"
            showAnyOption="true"
            noSelectText="property.type"
            noSelectValue="any"
      		
            />
            </div>
		</div>
		<div class="form-group">
		<label class="control-label col-sm-5"> Sale Type: </label>
		<div class="col-sm-7">
		<dp:i18Select
            id='sale-type'
            name='saleType'
            optionPrefix="property.saleType"
            options="${SaleType.findAll().collect { it.name }}"
            showAnyOption="true"
            noSelectText="property.saleType"
            noSelectValue="any"/>
            </div>
		</div>
		<div class="form-group">
		<label class="control-label col-sm-5">Property <span class="hd2">Location</span></label>
		</div>
		<hr>
		<div class="form-group">
		<label class="control-label col-sm-5"> Country: </label>
		<div class="col-sm-7">
		<dp:i18Select
            id="property_country"
            name='country'
            optionPrefix=""
            options="${Country.findAll().collect { it.name }}"
            showAnyOption="true"
            noSelectText="property.country"
            noSelectValue="any"/>
            </div>
		</div>
		<div class="form-group">
		<label class="control-label col-sm-5"> Region: </label>
		<div class="col-sm-7">
		<dp:i18Select
            id='property_region'
            name="region"
            optionPrefix=""
            options="${Region.findAll().collect { it.name }}"
            showAnyOption="true"
            noSelectText="property.region"
            noSelectValue="any"/>
            </div>
		</div>
		<div class="form-group">
		<label class="control-label col-sm-5"> Area: </label>
		<div class="col-sm-7">
		<dp:i18Select
            id='property_areas'
           
            name="area"
            optionPrefix=""
            options="${Region.findByName("Dar es salaam").areas.sort()}"
            showAnyOption="true"
            noSelectText="property.area"
            noSelectValue="any"/>
            </div>
		</div>
		<div class="form-group">
		<label class="control-label col-sm-5">Property <span class="hd2">Price Range</span></label>
		</div>
		<hr>
		<div class="form-group">
		<label class="control-label col-sm-5"> Currency: </label>
		<div class="col-sm-7">
		<select name="currency"  class="form-control">
			<option value="USD">US, $</option>
			<option value="TZS">Tanzania, TZS</option>
			<option value="KES">Kenya, KES</option>
			<option value="RWF">Rwanda, RWF</option>
		</select>
		</div>
		</div>
		<div class="form-group">
		<label class="control-label col-sm-5"> <span class="hd3"><span class="glyphicon glyphicon-arrow-up"></span> Max. Price:</span> </label>
		<div class="col-sm-7">
		 <input type="number" size="8" class="form-control"  name="maxPrice" required="required"> 
		</div>
		</div>
		<div class="form-group">
		<label class="control-label col-sm-5"><span class="hd2"><span class="glyphicon glyphicon-arrow-down" ></span> Min. Price</span></label>
		<div class="col-sm-7">
		 <input type="number" size="8" class="form-control" name="minPrice" value ="0" required="required">
		</div>
		</div>
		<div class="form-group">
		<label class="control-label col-sm-5">Other <span class="hd2">Details</span></label>
		</div>
		<hr>
		<div class="form-group">
		<label class="control-label col-sm-5"> <span class="glyphicon glyphicon-pencil"> </span> Description</label>
		<div class="col-sm-7">
		<textarea cols="60" rows="10" maxlength="100" name="desc">
		
		</textarea>
		</div>
		</div>
		<div class="form-group">
		<div class="col-sm-5 ">
		<div id="loading2">
		
			<img src="${resource(dir: 'bootstrap/imgs/', file: '3.gif')}" />
			<span class="hd3"> Sending... </span>
		<!-- loading image -->
		
		</div>
		</div>
		<div class="col-sm-7">
		<button type="submit" name="suggest" class="btn btn-success"> <span class="glyphicon glyphicon-envelope"></span> SUBMIT </button>
		</div>
		</div>
		<hr>

</g:form>
 
 <!-- /FORM -->
 
 
</div>

<a  data-toggle="collapse" href="#enquiry" aria-expanded="false" aria-controls="enquiry">
  I can't find what am looking for.  <i> <span class="glyphicon glyphicon-info-sign"></span> [Click Here]</i>
</a>
