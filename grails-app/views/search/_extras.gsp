


%{--MODAL FOR CONTACT US--}%
<div class="modal fade contact-us${property.id}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true"><span class="fa fa-close"></span></span></button>
                <h4 class="text-capitalize text-center">
                <g:if test="${property.saleType.name == 'for rent'}">
                    ${property.type}  <g:message
                        code="property.saleType.forRent"/>, ${"$property.area,  $property.region"}
                </g:if>
                <g:elseif test="${property.saleType.name == 'for sale'}">
                    ${property.type}   ${property.saleType.name}, ${"$property.area,  $property.region"}
                </g:elseif>
                <g:else>
                    ${property.type}   <g:message
                        code="property.saleType.jointVenture"/>, ${"$property.area,  $property.region"}
                </g:else>
            </h4>

                <h4 class="modal-title text-center text-success">CONTACT SELLER</h4>
            </div>

            <div class="modal-body table-autoflow-md">

                <g:hasErrors bean="${flash.errorBean}">
                    <div class="alert alert-block alert-error">
                        <g:renderErrors bean="${flash.errorBean}" as="list"/>
                    </div>
                </g:hasErrors>

                <g:if test="${flash.successMessage}">
                    <div class='alert alert-success'>${flash.successMessage}</div>
                </g:if>
                <g:if test="${flash.errorMessage}">
                    <div class='alert alert-error'>${flash.errorMessage}</div>
                </g:if>
                <g:if test="${!property.isFsbo() || property.fsboDetails.email}">
                    <g:form controller="property" action="addMessage" id="${property.id}" class="form form-horizontal">
                        <div class='form-group'>
                            %{-- <label for="email" class='control-label col-sm-4'><g:message
                                     code="property.enquiry.email"/></label>--}%

                            <div class='col-sm-offset-4 col-sm-5'>
                                <g:textField name="email" id="formdt" autocomplete="off" placeholder="Email"
                                             required="required"></g:textField>
                            </div>
                        </div>

                        <div class='form-group'>
                            %{--<label class='control-label col-sm-4'><g:message code="property.enquiry.phone"/></label>--}%

                            <div class='col-sm-offset-4 col-sm-5'>
                                <g:textField name="phone" id="formdt" autocomplete="off" placeholder="Phone Number"
                                             class="form-control"></g:textField>
                            </div>
                        </div>

                        <div class='form-group'>
                            %{--<label class='control-label col-sm-4'><g:message code="property.enquiry.message"/></label>
    --}%
                            <div class='col-sm-offset-4 col-sm-5'>
                                <g:textArea name="message" cols="40" placeholder="Message" rows="10"
                                            class="form-control"></g:textArea>
                            </div>
                        </div>

                        <div class='form-group'>
                            <div class='col-sm-offset-4 col-sm-5'>
                                <img src="${createLink(controller: 'simpleCaptcha', action: 'captcha')}"/>
                                <br>
                                <label for="captcha">Type the letters above in the box below:</label>
                                <g:textField name="captcha"/>
                            </div>
                        </div>


                        <div class='form-group'>
                            <div class='col-sm-offset-4 col-sm-5'>
                                <br>
                                <g:submitButton name="${g.message(code: 'property.enquiry.sendMessage')}"
                                                class="btn btn-success"></g:submitButton>
                            </div>
                        </div>
                    </g:form>
                </g:if>
                <g:else>
                    <p><span class="hd2"><span class=" glyphicon glyphicon-tag"></span>
                    </span> <small>${property.fsboDetails.name}</small></p>

                    <p><span class="hd2"><span class=" glyphicon glyphicon-certificate"></span>
                    </span> <small>For Sale By Owner</small></p>

                    <p><span class="hd2"><span class=" glyphicon glyphicon-phone"></span>
                    </span> <small>${property.fsboDetails.phone}</small></p>

                    <p><span class="hd2"><span class=" glyphicon glyphicon-envelope"></span>
                    </span> <small>${property.fsboDetails?.email}</small></p>
                </g:else>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><span
                        class="fa fa-close"></span> Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

