<%@ page import="com.mbwanajm.darproperty.CurrencyService;java.text.NumberFormat" %>
<% CurrencyService currencyService = new CurrencyService(); %>

<div class="row smooth-border">
    <!-- PROPERTY MAIN IMAGE-->
    <div class="col-sm-4 wing" align="center">
        <div class="row" align="left">

        </div>

        <g:link controller="property" action="viewProperty"
                params="${[id: property.id, agentId: property.agent.id]}">
            <g:each var="photo" in="${property.photos}">
                <g:if test="${photo.name.contentEquals('main')}">
                    <img class="img-responsive"
                         src="<g:createLink controller='image' action='renderPropertyPhoto' id='${photo.id}'/>"/>

                    <h2 class="text-uppercase">
                        ${property.saleType.name}
                    </h2>
                    <g:if test="${property.approved}">
                        <i class="txt text-uppercase" title="<g:message code="approved"/>"><span
                                class="img-rounded fa fa-check-circle"></span></i>
                    </g:if>

                    <% def Show = "thumb" %>

                    <g:render template="../tool/autoValueCalculator"
                              model="[pid: property.id, stype: property.saleType, psize: property.size, regionLocated: property.region, areaLocated: property.area, show: Show, ptype: property.type, selectedCurrency: property.currency, price: property.price]"></g:render>

                </g:if>

            </g:each>

        </g:link>

    </div>
    <!-- PROPERTY CONTENTS-->


    <div class="col-sm-6">
        <div class="row">
            <div class="col-xs-12">
                <g:link controller="property" action="viewProperty"
                        params="${[id: property.id, agentId: property.agent.id]}">
                    <h4 class="text-capitalize">
                        <g:if test="${property.saleType.name == 'for rent'}">
                            ${property.type}  <g:message
                                code="property.saleType.forRent"/>, ${"$property.area,  $property.region"}
                        </g:if>
                        <g:elseif test="${property.saleType.name == 'for sale'}">
                            ${property.type}   ${property.saleType.name}, ${"$property.area,  $property.region"}
                        </g:elseif>
                        <g:else>
                            ${property.type}   <g:message
                                code="property.saleType.jointVenture"/>, ${"$property.area,  $property.region"}
                        </g:else>
                    </h4>

                </g:link>
                <p class="text-capitalize hidden-xs hidden-sm">
                    ${property.location} ${property.area} ${property.region} ${property.country}
                </p>

            </div>
        </div>

        <div class="row">
            <div class="col-xs-12" align="left">
                <table class="res-table">

                    <tbody>
                    <tr class="hd1s text-center pricetag">
                        <td class="">
                            <div class="hidden-lg hidden-md">
                                <span class="big-price" style="font-size:10pt;">
                                    <g:if test="${property.price}">

                                        ${java.text.NumberFormat.getInstance().format(property.price)}

                                    </g:if><g:else>
                                        0
                                    </g:else>
                                </span>
                                <span class="mid-price" style="font-size:9pt;">${property.currency}
                                    <a tabindex="0"
                                       class="text-info"
                                       role="button" data-toggle="popover"
                                       data-placement="bottom"
                                       data-trigger="focus"
                                       title="Price Convertor"
                                       data-content="
                                        TZS ${NumberFormat.getInstance().format(currencyService.convert(property.price, property.currency, 'TZS'))}
                    , RWF ${NumberFormat.getInstance().format(currencyService.convert(property.price, property.currency, 'RWF'))}
               , KES ${NumberFormat.getInstance().format(currencyService.convert(property.price, property.currency, 'KES'))}
               , USD ${NumberFormat.getInstance().format(currencyService.convert(property.price, property.currency, 'USD'))}

                                       "><span
                                            class="fa fa-chevron-circle-down"></span></a></span>
                            </div>

                            <div class="hidden-sm hidden-xs">
                                <span class="big-price">

                                    <% def main = "main" %>
                                    <g:render template="../property/priceViewer"
                                              model="[property: property, main: main]"></g:render>
                                </span>
                                <g:if test="${property.price}">
                                    <span class="mid-price" style="font-size:9pt;">${property.currency}
                                        <a tabindex="0"
                                           class="text-info"
                                           role="button" data-toggle="popover"
                                           data-placement="bottom"
                                           data-trigger="focus"
                                           title="Price Convertor"
                                           data-content="
                                        TZS ${NumberFormat.getInstance().format(currencyService.convert(property.price, property.currency, 'TZS'))}
                    , RWF ${NumberFormat.getInstance().format(currencyService.convert(property.price, property.currency, 'RWF'))}
               , KES ${NumberFormat.getInstance().format(currencyService.convert(property.price, property.currency, 'KES'))}
               , USD ${NumberFormat.getInstance().format(currencyService.convert(property.price, property.currency, 'USD'))}
                                           "><span
                                                class="fa fa-chevron-circle-down"></span></a></span>
                                </g:if>
                            </div>
                        </td>
                        <td class="">

                            <% Show = "short" %>

                            <g:render template="../tool/autoValueCalculator"
                                      model="[pid: property.id, stype: property.saleType, psize: property.size, regionLocated: property.region, areaLocated: property.area, show: Show, ptype: property.type, selectedCurrency: property.currency, price: property.price]"></g:render>

                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
        <br>

        <div class="row" align="left">
            <div class="col-xs-12">

                <table class="res-table" style="padding: 2%;">
                    <tbody>
                    <tr>
                        <td class="hidden-sm hidden-xs">
                            <small><g:render template="../property/post_age_counter"
                                             model="[property: property]"></g:render></small>
                        </td>

                        <td class="hidden-sm hidden-xs">
                            <g:link controller="property" action="viewProperty"
                                    params="${[id: property.id, agentId: property.agent.id]}"
                                    style="color:#bc0417;font-size:18pt" title="view">
                                <span class="fa fa-eye">${property.views}</span>
                            </g:link>
                        </td>
                        <td>
                            <a href="#" data-toggle="modal" data-target=".contact-us${property.id}">
                                <span class="glyphicon glyphicon-envelope" style="color:#bc0417;font-size:18pt"
                                      title="Contact this seller"></span>
                            </a>
                        </td>
                        <td class="">

                            <a class="custom_share_button custom_fb_button  fb_share_track"
                               data-fbtrackid="gallery"
                               href="https://www.facebook.com/sharer/sharer.php?u=http://www.darproperty.co.tz/property/viewProperty/${property.id}?agentId=${property.agent.id}"
                               onclick="window.open($(this).attr('href'), 'fb_popup', 'width=600,height=310');
                               return false;"
                               gallery_share_url="http://www.darproperty.co.tz/property/viewProperty/${property.id}?agentId=${property.agent.id}"
                               appId='1670558763159469'
                               title="Facebook Share"
                               target="_blank" style="color:#4a6ea9;font-size:19pt">
                                <span class="fa fa-facebook-square"></span>
                            </a>
                        </td>
                        <td class="">
                            <!--class="twitter-share-button"{count}  -->


                            <a href="https://twitter.com/intent/tweet?text=See%20now%20${property.type}%20${property.saleType}%20at%20${property.area}%20,%20${property.region},%20${property.country}%20VIA%20@thedarproperty%20in&amp;url=http%3A%2F%2Fwww.darproperty.co.tz%2Fproperty%2FviewProperty%2F${property.id}?agentId=${property.agent.id}"
                               class="shortenUrl"
                               data-social-url="https://twitter.com/intent/tweet?text=See+now+${property.type}+${property.saleType}+at+${property.area}+,+${property.region},+${property.country}+VIA+@thedarproperty+in&amp;url="
                               data-target-url="http://www.darproperty.co.tz/property/viewProperty/${property.id}?agentId=${property.agent.id}"
                               target="_blank" style="color:#659fcb;font-size: 19pt;" title="Twitter share">
                                <sapn class="fa fa-twitter-square"></sapn>
                            </a>
                        </td>
                        <td class="hidden-lg">
                            <a
                                    href="whatsapp://send?text= Check  ${property.type} ${property.saleType} at  ${property.area}, ${property.region}, ${property.country} READ MORE:-> http://www.darproperty.co.tz/property/viewProperty/${property.id}?agentId=${property.agent.id}"
                                    data-action="share/whatsapp/share"
                                    target="_blank" style="color:#2ab200;font-size:19pt;" title="Whatsapp share">
                                <img
                                        src="${resource(dir: 'bootstrap/imgs/', file: 'whatsappgreen.png')}"
                                        alt="Whatsapp"
                                        width="35"
                                        title="Share on Whatsapp"/>
                            </a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

        </div>

    </div>

    <div class="col-sm-2">
        <div class="row">

        </div>

        <br>
        <br>

        <div class="row">
            <g:render template="../property/property_icons_search" model="[property: property]"></g:render>

        </div>

        <br>

        <div class="row">
            <div class="margine2"></div>

            <g:link controller="agent" action="profile" id="${property.agent.id}">
                <g:if test="${property.agent.profile.photo == null}">
                    <img class='img-polaroid img-responsive'
                         src="${resource(dir: 'images/property/', file: 'agent.png')} " width="100"/>
                </g:if>
                <g:else>
                    <img class="agent-photo img-responsive" title="${property.agent.username}"
                         src="<g:createLink controller='image' action='renderAgentPhoto'
                                            id='${property.agent.id}'/>" width="100"/>
                </g:else>

            </g:link>
        </div>

    </div>

</div>
<g:render template="extras" model="[property: property]"></g:render>

