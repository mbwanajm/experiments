<div class="container">
<div class="row">
 
        <img class='img-polaroid'
             src="<g:createLink controller='image' action='renderPropertyPhoto' id='${photo.id}'/>" width="200px" height="100px"/>

  <br>
  <br>
   
        <g:if test="${photo.name == 'main'}">
            <span style="color: orange;">main photo</span>
        </g:if>
        <g:else>
            <g:link controller="property" action="setAsMainPhoto"
                    params="[photoId: photo.id, propertyId: property.id]">set as main photo</g:link>

        </g:else>
        |
        <g:link controller="property" action="deletePhoto"
                params="[photoId: photo.id, propertyId: property.id]">delete photo</g:link>
   
   
    </div>


</div>

<hr>

