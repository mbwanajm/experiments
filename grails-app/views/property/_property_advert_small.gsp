<%@ page import="java.text.NumberFormat" %>

<div class="col-sm-6 col-md-3" align="center" style="box-shadow: 0px 0px 0px #000; border: 1px solid rgba(143, 143, 143, 0.5); padding: 10px; height: 100%; ">
        <g:link controller="property" action="viewProperty"
                params="${[id: property.id, agentId: property.agent.id]}">
            <g:each var="photo" in="${property.photos}">
                <g:if test="${photo.name.contentEquals('main')}">

                    <!-- 180 -->
                    <img src="<g:createLink controller='image' action='renderPropertyPhoto'
                                            id='${photo.id}'/>" class="img-responsive"/>



                </g:if>
            </g:each>
        </g:link>

        <div class="caption"  >
            <h5 class ="hd1 text-uppercase" align="center" style=''><dp:message code="${property.saleType.name}" codePrefix="property.saleType"/></h5>


            <p align="center"> ${property.area} - ${property.region},${property.country} </p>
            <hr class="style-two">
            <p align="center"> <small><span class="hd2"> ${property.currency}</span><g:link controller="property" action="viewProperty"
                                                                                            params="${[id: property.id, agentId: property.agent.id]}">
                <g:if test="${property.price}">
                    <%
                        def num = property.price
                        def pr = 0
                        def rep = ""
                        def hold = num / 1000000
                        if(hold >=1){
                            pr = hold
                            rep = "M"
                        }else{
                            hold = num / 1000
                            if(hold >= 1){
                                pr = hold
                                rep = "K"
                            }else{
                                pr = num
                            }
                        }

                    %>
                    ${NumberFormat.getInstance().format(pr)}${rep} <small style="color: #FFF; ">${property.currency == "USD"? '$': property.currency}</small>
                </g:if>
            </g:link></small></p>
            <p align="center"><span class="hd2"> Views </span><span class="badge">${property.version}</span></p>




        </div>
    </div>
<br>