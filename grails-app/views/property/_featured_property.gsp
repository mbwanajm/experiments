<div class="container">

    <div class="row">
        <div class="col-md-9">

            <div class="maincontent-area">
                <div class="zigzag-bottom"></div>

                <div class="latest-product">
                    <div class="row">
                        <div class="col-lg-12">

                            <g:link controller="agent" action="viewProperty" id="${2}" class="pull-right">
                                <i class="fa fa-list"></i> <g:message code="home.viewAll"/>
                            </g:link>
                            <span class="wid-view-more pull-left">
                                <g:message code="home.intro.featured"/> <g:message
                                        code="home.intro.property"/>
                            </span>

                        </div>

                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <hr class="style-one"/>
                        </div>

                    </div>

                    <div class="product-carousel" align="center">
                        %{--List--}%
                        <g:render template="../property/property_homeview" collection="${featuredProperty}"
                                  var="property"></g:render>
                        %{--End List--}%
                    </div>

                </div>
            </div>
            <br>

            <div class="maincontent-area">
                <div class="zigzag-bottom"></div>

                <div class="latest-product" align="center">
                    <div class="row">
                        <div class="col-lg-12">

                            <g:link controller="property" action="showAllProperties" class="pull-right">
                                <i class="fa fa-list"></i> <g:message code="home.viewAll"/>
                            </g:link>
                            <span class="wid-view-more pull-left">
                                <g:message code="home.intro.latest"/> <g:message
                                        code="home.intro.new.property"/>
                            </span>

                        </div>

                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <hr class="style-one"/>
                        </div>

                    </div>

                    <div class="product-carousel">
                        %{--List--}%
                        <g:render template="/property/property_homeview" collection="${featuredProperty2}"
                                  var="property"></g:render>
                        %{--End List--}%
                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-3">
            <g:render template="/home/midpage_slide_show" model="${footerAdverts}"></g:render>
        </div>
    </div>
</div>








