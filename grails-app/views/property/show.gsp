<%@ page import="com.mbwanajm.darproperty.Property" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'property.label', default: 'Property')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<div class="container">

    <div class="row">
        <div class="col-sm-5">

            <a href="#show-property" class="skip" tabindex="-1"
               title="<g:message code="default.link.skip.label" default="Skip to content&hellip;"/>">
                <span class="glyphicon glyphicon-refresh"></span></a>

        </div>

    </div>

    <br>

    <div class="container">

        <div class="nav" role="navigation">
            <ul>
                <li class="btn btn-default"><g:link controller="config" action=""><span
                        class="glyphicon glyphicon-wrench"></span></g:link></li>
                <li class="btn btn-default"><span class="glyphicon glyphicon-th-list"></span>
                    <g:link class="list" action="list"><g:message code="default.list.label"
                                                                  args="[entityName]"/></g:link></li>
                <li class="btn btn-default"><span class="glyphicon glyphicon-plus"></span>
                    <g:link class="create" action="create"><g:message code="default.new.label"
                                                                      args="[entityName]"/></g:link></li>
            </ul>
        </div>

        <div id="show-property" class="content scaffold-show" role="main">
            <h1><g:message code="default.show.label" args="[entityName]"/></h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
            <ol class="property-list property">

                <g:if test="${propertyInstance?.name}">
                    <li class="fieldcontain">
                        <span id="name-label" class="property-label"><g:message code="property.name.label"
                                                                                default="Name"/></span>

                        <span class="property-value" aria-labelledby="name-label"><g:fieldValue
                                bean="${propertyInstance}"
                                field="name"/></span>

                    </li>
                </g:if>

                <g:if test="${propertyInstance?.description}">
                    <li class="fieldcontain">
                        <span id="description-label" class="property-label"><g:message code="property.description.label"
                                                                                       default="Description"/></span>

                        <span class="property-value" aria-labelledby="description-label"><g:fieldValue
                                bean="${propertyInstance}"
                                field="description"/></span>

                    </li>
                </g:if>

                <g:if test="${propertyInstance?.region}">
                    <li class="fieldcontain">
                        <span id="region-label" class="property-label"><g:message code="property.region.label"
                                                                                  default="Region"/></span>

                        <span class="property-value" aria-labelledby="region-label"><g:fieldValue
                                bean="${propertyInstance}"
                                field="region"/></span>

                    </li>
                </g:if>

                <g:if test="${propertyInstance?.area}">
                    <li class="fieldcontain">
                        <span id="area-label" class="property-label"><g:message code="property.area.label"
                                                                                default="Area"/></span>

                        <span class="property-value" aria-labelledby="area-label"><g:fieldValue
                                bean="${propertyInstance}"
                                field="area"/></span>

                    </li>
                </g:if>

                <g:if test="${propertyInstance?.agent}">
                    <li class="fieldcontain">
                        <span id="agent-label" class="property-label"><g:message code="property.agent.label"
                                                                                 default="Agent"/></span>

                        <span class="property-value" aria-labelledby="agent-label"><g:link controller="agent"
                                                                                           action="show"
                                                                                           id="${propertyInstance?.agent?.id}">${propertyInstance?.agent?.encodeAsHTML()}</g:link></span>

                    </li>
                </g:if>

                <g:if test="${propertyInstance?.views}">
                    <li class="fieldcontain">
                        <span id="views-label" class="property-label"><g:message code="property.views.label"
                                                                                 default="Views"/></span>

                        <span class="property-value" aria-labelledby="views-label"><g:fieldValue
                                bean="${propertyInstance}"
                                field="views"/></span>

                    </li>
                </g:if>

                <g:if test="${propertyInstance?.dateAdded}">
                    <li class="fieldcontain">
                        <span id="dateAdded-label" class="property-label"><g:message code="property.dateAdded.label"
                                                                                     default="Date Added"/></span>

                        <span class="property-value" aria-labelledby="dateAdded-label"><g:formatDate
                                date="${propertyInstance?.dateAdded}"/></span>

                    </li>
                </g:if>

                <g:if test="${propertyInstance?.location}">
                    <li class="fieldcontain">
                        <span id="location-label" class="property-label"><g:message code="property.location.label"
                                                                                    default="Location"/></span>

                        <span class="property-value" aria-labelledby="location-label"><g:fieldValue
                                bean="${propertyInstance}"
                                field="location"/></span>

                    </li>
                </g:if>

                <g:if test="${propertyInstance?.deleted}">
                    <li class="fieldcontain">
                        <span id="deleted-label" class="property-label"><g:message code="property.deleted.label"
                                                                                   default="Deleted"/></span>

                        <span class="property-value" aria-labelledby="deleted-label"><g:formatBoolean
                                boolean="${propertyInstance?.deleted}"/></span>

                    </li>
                </g:if>

                <g:if test="${propertyInstance?.deletedOn}">
                    <li class="fieldcontain">
                        <span id="deletedOn-label" class="property-label"><g:message code="property.deletedOn.label"
                                                                                     default="Deleted On"/></span>

                        <span class="property-value" aria-labelledby="deletedOn-label"><g:formatDate
                                date="${propertyInstance?.deletedOn}"/></span>

                    </li>
                </g:if>

                <g:if test="${propertyInstance?.price}">
                    <li class="fieldcontain">
                        <span id="price-label" class="property-label"><g:message code="property.price.label"
                                                                                 default="Price"/></span>

                        <span class="property-value" aria-labelledby="price-label"><g:fieldValue
                                bean="${propertyInstance}"
                                field="price"/></span>

                    </li>
                </g:if>

                <g:if test="${propertyInstance?.currency}">
                    <li class="fieldcontain">
                        <span id="currency-label" class="property-label"><g:message code="property.currency.label"
                                                                                    default="Currency"/></span>

                        <span class="property-value" aria-labelledby="currency-label"><g:fieldValue
                                bean="${propertyInstance}"
                                field="currency"/></span>

                    </li>
                </g:if>

                <g:if test="${propertyInstance?.descriptionSW}">
                    <li class="fieldcontain">
                        <span id="descriptionSW-label" class="property-label"><g:message
                                code="property.descriptionSW.label"
                                default="Description SW"/></span>

                        <span class="property-value" aria-labelledby="descriptionSW-label"><g:fieldValue
                                bean="${propertyInstance}"
                                field="descriptionSW"/></span>

                    </li>
                </g:if>

                <g:if test="${propertyInstance?.messages}">
                    <li class="fieldcontain">
                        <span id="messages-label" class="property-label"><g:message code="property.messages.label"
                                                                                    default="Messages"/></span>

                        <g:each in="${propertyInstance.messages}" var="m">
                            <span class="property-value" aria-labelledby="messages-label"><g:link controller="message"
                                                                                                  action="show"
                                                                                                  id="${m.id}">${m?.encodeAsHTML()}</g:link></span>
                        </g:each>

                    </li>
                </g:if>

                <g:if test="${propertyInstance?.numberOfBathRooms}">
                    <li class="fieldcontain">
                        <span id="numberOfBathRooms-label" class="property-label"><g:message
                                code="property.numberOfBathRooms.label"
                                default="Number Of Bath Rooms"/></span>

                        <span class="property-value" aria-labelledby="numberOfBathRooms-label"><g:fieldValue
                                bean="${propertyInstance}" field="numberOfBathRooms"/></span>

                    </li>
                </g:if>

                <g:if test="${propertyInstance?.numberOfRooms}">
                    <li class="fieldcontain">
                        <span id="numberOfRooms-label" class="property-label"><g:message
                                code="property.numberOfRooms.label"
                                default="Number Of Rooms"/></span>

                        <span class="property-value" aria-labelledby="numberOfRooms-label"><g:fieldValue
                                bean="${propertyInstance}"
                                field="numberOfRooms"/></span>

                    </li>
                </g:if>

                <g:if test="${propertyInstance?.photos}">
                    <li class="fieldcontain">
                        <span id="photos-label" class="property-label"><g:message code="property.photos.label"
                                                                                  default="Photos"/></span>

                        <g:each in="${propertyInstance.photos}" var="p">
                            <span class="property-value" aria-labelledby="photos-label"><g:link controller="photo"
                                                                                                action="show"
                                                                                                id="${p.id}">${p?.encodeAsHTML()}</g:link></span>
                        </g:each>

                    </li>
                </g:if>

                <g:if test="${propertyInstance?.saleType}">
                    <li class="fieldcontain">
                        <span id="saleType-label" class="property-label"><g:message code="property.saleType.label"
                                                                                    default="Sale Type"/></span>

                        <span class="property-value" aria-labelledby="saleType-label"><g:link controller="saleType"
                                                                                              action="show"
                                                                                              id="${propertyInstance?.saleType?.id}">${propertyInstance?.saleType?.encodeAsHTML()}</g:link></span>

                    </li>
                </g:if>

                <g:if test="${propertyInstance?.size}">
                    <li class="fieldcontain">
                        <span id="size-label" class="property-label"><g:message code="property.size.label"
                                                                                default="Size"/></span>

                        <span class="property-value" aria-labelledby="size-label"><g:fieldValue
                                bean="${propertyInstance}"
                                field="size"/></span>

                    </li>
                </g:if>

                <g:if test="${propertyInstance?.tags}">
                    <li class="fieldcontain">
                        <span id="tags-label" class="property-label"><g:message code="property.tags.label"
                                                                                default="Tags"/></span>

                        <g:each in="${propertyInstance.tags}" var="t">
                            <span class="property-value" aria-labelledby="tags-label"><g:link controller="tag"
                                                                                              action="show"
                                                                                              id="${t.id}">${t?.encodeAsHTML()}</g:link></span>
                        </g:each>

                    </li>
                </g:if>

                <g:if test="${propertyInstance?.type}">
                    <li class="fieldcontain">
                        <span id="type-label" class="property-label"><g:message code="property.type.label"
                                                                                default="Type"/></span>

                        <span class="property-value" aria-labelledby="type-label"><g:link controller="type"
                                                                                          action="show"
                                                                                          id="${propertyInstance?.type?.id}">${propertyInstance?.type?.encodeAsHTML()}</g:link></span>

                    </li>
                </g:if>

            </ol>

            <div class="row">
                <div class="col-sm-12">
                    <g:form>
                        <fieldset class="buttons">
                            <g:hiddenField name="id" value="${propertyInstance?.id}"/>
                            <g:link class="btn btn-warning" action="edit" id="${propertyInstance?.id}"><g:message
                                    code="default.button.edit.label"
                                    default="Edit"/></g:link>
                            <g:actionSubmit class="btn btn-danger" action="delete"
                                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                        </fieldset>
                    </g:form>
                </div>
            </div>
        </div>

    </div>
</div>
<br>
</body>
</html>
