<g:if test="${property.size != 0}">
    <div class='property_result_info_block'>
        <img class="property_result_icon"
             src="${resource(dir: 'images/property', file: 'house.png')}"/>
        ${property.size} sqm
    </div>
</g:if>
<g:if test="${property.numberOfRooms != 0}">
    <div class='property_result_info_block'>
        <img class="property_result_icon"
             src="${resource(dir: 'images/property', file: 'bedroom.png')}"/>
        ${property.numberOfRooms} <g:message code='property.rooms'/>
    </div>
</g:if>
<g:if test="${property.numberOfBathRooms != 0}">
    <div class='property_result_info_block'>
        <img class="property_result_icon"
             src="${resource(dir: 'images/property', file: 'bathroom.png')}"/>
        ${property.numberOfBathRooms} <g:message code='property.bathrooms'/>
    </div>
</g:if>
<g:if test="${(property.tags.collect {it.name}).contains('parking')}">
    <div class='property_result_info_block'>
        <img class="property_result_icon"
             src="${resource(dir: 'images/property', file: 'garage.png')}"/>
        <g:message code="property.parking"/>
    </div>
</g:if>