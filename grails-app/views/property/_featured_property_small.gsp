<%@ page import="java.text.NumberFormat" %>


<div style="box-shadow: 0px 0px 0px #000; border: 1px solid rgba(143, 143, 143, 0.5); padding: 10px; height: 100%; ">
    <g:link controller="property" action="viewProperty"
            params="${[id: property.id, agentId: property.agent.id]}">
        <g:each var="photo" in="${property.photos}">
            <g:if test="${photo.name.contentEquals('main')}">

                <!-- 180 -->
                <img src="<g:createLink controller='image' action='renderPropertyPhoto'
                                        id='${photo.id}'/>" alt="property photo" class="img-responsive" height="150"
                     style="width:100%;"/>

            </g:if>
        </g:each>
    </g:link>

    <div class="caption">
        <p align="center" style=" font-size: 10pt;" class="text-capitalize"><b>
            <dp:message code="${property.type}" codePrefix="property.type"/>
            <dp:message code="${property.saleType.name}" codePrefix="property.saleType"/>
            ${property.area} </b>
            %{--- ${property.region}</b>--}%
        </p>
        <h5 class="hd1 text-uppercase" style="padding: 5px; font-size: 14pt; color:#FFF">
            <div class="row">
                <div class="col-xs-12" align="center">

                    %{--</div>--}%

                    %{--<div class="col-xs-6" style="">--}%
                    <g:render template="../property/priceViewer" model="[property: property]"></g:render>
                </div>
            </div>

        </h5>


        <% def Show = "front" %>

        <g:render template="../tool/autoValueCalculator"
                  model="[pid: property.id, stype: property.saleType, psize: property.size, regionLocated: property.region, areaLocated: property.area, show: Show, ptype: property.type, selectedCurrency: property.currency, price: property.price]"></g:render>

    </div>
</div>
<br>