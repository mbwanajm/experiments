<%--
  Created by IntelliJ IDEA.
  User: DAR PROPERTY
  Date: 8/30/2016
  Time: 8:59 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>

<%@ page import="com.mbwanajm.darproperty.CurrencyService; java.text.NumberFormat" %>
<% CurrencyService currencyService = new CurrencyService(); %>
<html>
<head>

    <link rel="stylesheet" href="${resource(dir: 'js/engine1', file: 'style.css')}"/>
    <meta name="layout" content="main"/>
    <title>View Property | <g:if test="${property.saleType.name == 'for rent'}">
        ${property.type}  <g:message
                code="property.saleType.forRent"/>, ${"$property.area,  $property.region"}
    </g:if>
    <g:elseif test="${property.saleType.name == 'for sale'}">
        ${property.type}   ${property.saleType.name}, ${"$property.area,  $property.region"}
    </g:elseif>
    <g:else>
        ${property.type}   <g:message
            code="property.saleType.jointVenture"/>, ${"$property.area,  $property.region"}
    </g:else>
    </title>
</head>

<body>
<div class="container">
    <div class="row">

        <div class="col-md-6">
            <div class="row"  style="font-size: 8pt">
                <i><g:link controller="home" action="home"><g:message code="menu.home"></g:message></g:link> >>
                  <i> <g:link controller="property" action="showAllProperties" >
                        <g:message code="property.viewAll"/></g:link></i> >>
                </i> <i><g:link controller="agent" action="viewProperty" id="${property.agent.id}">${property.agent.profile.company}</g:link></i>

            </div>

            %{--**********IMAGE SLIDER**********--}%
            %{--Slider--}%
            <div class="row">
                <g:render template="property_photos_slideshow"/>
            </div>

            <div class="row" align="center">
                <p>
                    <span class="" style="color:#4a6ea9; font-size:20pt;">
                        <g:message code="home.share"></g:message><i class="fa fa-share"></i>
                    </span>

                    <span class="" style="color:#4a6ea9; font-size:30pt;">
                        <!-- FACEBOOK -->


                        <a class="custom_share_button custom_fb_button  fb_share_track"
                           data-fbtrackid="gallery"
                           href="https://www.facebook.com/sharer/sharer.php?u=http://www.darproperty.co.tz/property/viewProperty/${property.id}?agentId=${property.agent.id}"
                           onclick="window.open($(this).attr('href'), 'fb_popup', 'width=600,height=310');
                           return false;"
                           gallery_share_url="http://www.darproperty.co.tz/property/viewProperty/${property.id}?agentId=${property.agent.id}"
                           appId='1670558763159469'
                           title="Facebook Share"
                           target="_blank">

                            <span class="fa fa-facebook-square hidden-sm hidden-xs"></span>
                            <span class="fa fa-facebook hidden-lg"></span>

                        </a>

                    </span>

                    <span class=" hidden-xs hidden-sm"
                          style="background-color:#EEE; border-color:transparent; border-width: 0px;">
                    </span>

                    <span class=" hidden-lg" style="color:#2ab200;font-size:30pt;">
                        <!-- WHATSAPP -->
                        <a
                                href="whatsapp://send?text= Check  ${property.type} ${property.saleType} at  ${property.area}, ${property.region}, ${property.country} READ MORE:-> http://www.darproperty.co.tz/property/viewProperty/${property.id}?agentId=${property.agent.id}"
                                data-action="share/whatsapp/share"
                                target="_blank"
                                title="Whatsapp share">
                            <img
                                    src="${resource(dir: 'bootstrap/imgs/', file: 'whatsappgreen.png')}"
                                    alt="Whatsapp"
                                    width="40"
                                    title="Share on Whatsapp"/>
                        </a>
                    </span>

                    <span class="" style="color:#659fcb; font-size:30pt;">
                        <!-- TWITTER -->

                        <!--class="twitter-share-button"{count}  -->
                        <a href="https://twitter.com/intent/tweet?text=See%20now%20${property.type}%20${property.saleType}%20at%20${property.area}%20,%20${property.region},%20${property.country}%20VIA%20@thedarproperty%20in&amp;url=http%3A%2F%2Fwww.darproperty.co.tz%2Fproperty%2FviewProperty%2F${property.id}?agentId=${property.agent.id}"
                           class="shortenUrl"
                           data-social-url="https://twitter.com/intent/tweet?text=See+now+${property.type}+${property.saleType}+at+${property.area}+,+${property.region},+${property.country}+VIA+@thedarproperty+in&amp;url="
                           data-target-url="http://www.darproperty.co.tz/property/viewProperty/${property.id}?agentId=${property.agent.id}"
                           target="_blank">
                            <span class="fa fa-twitter-square hidden-sm hidden-xs"></span>
                            <span class="fa fa-twitter hidden-lg hidden-md"></span>
                        </a>
                    </span>
                    <span class="" style="color:#659fcb; font-size:30pt;">
                        <!-- MAIL -->
                        <a href="mailto:?Subject=DARPROPERTY:%20${property.type}%20${property.saleType.name},%20${"$property.area,%20$property.region"}%20&body=Hi%20there,%20%0d%0dI%20found%20this%20advert%20at%20Darproperty,%20though%20you%20will%20be%20interested%20Contact%20%0dEMAIL:${property.agent.profile.email}%0dCALL:${property.agent.profile.phone}%0d%20%20%0dOr%20read%20more%20at%20http://www.darproperty.co.tz/property/viewProperty/${property.id}?agentId=${property.agent.id}%0d"

                           target="_top">
                            <i class="fa fa-envelope"></i>
                        </a>
                    </span>
                </p>
            </div>

        </div>

        <div class=" col-md-6" style="">
            %{--**********PARTICULARS***********--}%
            %{--Title--}%

            <div class="row">
                <div class="col-xs-12">
                    <h3 class="text-capitalize">
                        <b>
                            <g:if test="${property.saleType.name == 'for rent'}">
                                ${property.type}  <g:message
                                    code="property.saleType.forRent"/>, ${"$property.area,  $property.region"}
                            </g:if>
                            <g:elseif test="${property.saleType.name == 'for sale'}">
                                ${property.type}   ${property.saleType.name}, ${"$property.area,  $property.region"}
                            </g:elseif>
                            <g:else>
                                ${property.type}   <g:message
                                    code="property.saleType.jointVenture"/>, ${"$property.area,  $property.region"}
                            </g:else>
                        </b>

                    </h3>
                </div>
            </div>
            %{--Price--}%
            <div class="row">
                <div class="col-sm-8">

                    <p>
                        <g:if test="${property.saleType.name == 'for sale'}">
                            <small class="text-uppercase"><b><g:message code="property.price"></g:message>:</b></small>
                        </g:if>
                        <g:else>
                            <small class="text-uppercase"><b><g:message code="property.rent"></g:message>:</b></small>
                        </g:else>
                        <% def main = 'main' %>
                        <br>
                        <b class="big-price">

                            <g:render template="../property/priceViewer"
                                      model="[property: property, main: main]"></g:render>

                        </b>
                        <g:if test="${property.price}">
                            <small class="mid-price">${property.currency}</small>
                        </g:if>

                    </p>

                </div>

                <div class="col-sm-4">
                    <% def Show = "mobo" %>

                    <g:render template="../tool/autoValueCalculator"
                              model="[pid: property.id, stype: property.saleType, psize: property.size, regionLocated: property.region, areaLocated: property.area, show: Show, ptype: property.type, selectedCurrency: property.currency]"></g:render>

                </div>

            </div>
            %{--Age and Location--}%
            <div class="row">
                <div class="col-xs-6">
                    <p><small><g:render template="../property/post_age_counter"
                                        model="[property: property]"></g:render></small></p>

                </div>

                <div class="col-xs-6">
                    <small class="text-muted text-capitalize">(<i
                            class="fa fa-map-marker"></i> ${property.region} - ${property.location})</small>
                </div>
            </div>

            %{--Description--}%
            <div class="row">

                <div class="col-xs-12 well well-sm">
                    <i class="fa fa-quote-left"></i>
                    <dp:propertyDescription property="${property}"/>
                    <i class="fa fa-quote-right"></i>

                </div>
            </div>
            %{--Amenities & Tags--}%
            <div class="row">

                <div class="col-xs-12">
                    <div class="row  amenities text-capitalize">
                        <div class="col-xs-6">
                            <g:render template="viewproperty_icons" model="[property: property]"/>
                        </div>

                        <div class="col-xs-6 left-border">
                            <ul class="list-unstyled">
                                <g:each in="${property.tags}" var="tag">
                                    <g:if test="${tag.name != "parking"}">
                                        <li class="col-xs-12">
                                            <span class="text-danger fa fa-check"
                                                  style="font-size: 5pt;"></span> <dp:message codePrefix="property"
                                                                                              code="${tag.name}"/>
                                        </li>
                                    </g:if>
                                </g:each>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <g:if test="${property.isFsbo()}">

                    <div class="col-sm-8">
                        <table class="table table-responsive table-striped">
                            <thead>
                            <th><b><g:message code="contactDetails"></g:message>:</b></th>
                            </thead>
                            <tbody>
                            <tr>
                                <td><span class="hd2"><span class=" glyphicon glyphicon-home"></span>
                                </span></td>

                                <td><small>
                                    <% String type = property.type; %>
                                    <% def title = " " + type + " (${property.name})" %>
                                    ${title}
                                </small></td>

                            </tr>
                            <tr>
                                <td><span class="hd2"><span class=" glyphicon glyphicon-tag"></span>
                                </span></td>
                                <td><small>${property.fsboDetails.name}</small></td>

                            </tr>
                            <tr>
                                <td><span class="hd2"><span class=" glyphicon glyphicon-certificate"></span>
                                </span></td>

                                <td><small>For Sale By Owner</small></td>
                            </tr>
                            <tr>
                                <td><span class="hd2"><span class=" glyphicon glyphicon-phone"></span>
                                </span></td>
                                <td><small>${property.fsboDetails.phone}</small></td>
                            </tr>
                            <tr>
                                <td><span class="hd2"><span class=" glyphicon glyphicon-envelope"></span>
                                </span></td>

                                <td><small>${property.fsboDetails?.email}</small></td>

                            </tr>

                            </tbody>
                        </table>

                    </div>

                    <div class="col-sm-4" align="center">
                        <table class="table table-responsive">
                            <thead align="">
                            <td>
                                <g:link controller="agent" action="profile" id="${property.agent.id}">
                                    <g:if test="${property.agent.profile.photo == null}">
                                        <img class=' img-responsive '
                                             src="${resource(dir: 'images/property/', file: 'agent.png')} "
                                             width="100"/>
                                    </g:if>
                                    <g:else>
                                        <img class="agent-photo img-responsive" title="${property.agent.username}"
                                             src="<g:createLink controller='image' action='renderAgentPhoto'
                                                                id='${property.agent.id}'/>" width="100"/>
                                    </g:else>

                                </g:link>
                            </td>
                            </thead>

                            <tbody>
                            <tr>
                                <td>
                                    <g:link controller="agent" action="viewProperty" id="${property.agent.id}"
                                            class=" btn btn-default hd2">
                                        <span class="glyphicon glyphicon-list"></span>
                                        <g:message code="property.fsbo.viewOtherProperty"/></g:link><br><br>
                                    <g:link controller="property" action="showAllProperties"
                                            class=" btn btn-default hd2">
                                        <span class="glyphicon glyphicon-list-alt"></span>
                                        <g:message code="property.viewAll"/></g:link>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="btn btn-default hd2" role="button" data-toggle="collapse"
                                       href="#collapseExample" aria-expanded="false"
                                       aria-controls="collapseExample">
                                        <span><i class="fa fa-envelope-square"></i> <g:message
                                                code="property.messageSeller"></g:message></span>
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </g:if>
                <g:else>
                    <div class="col-sm-8">
                        <table class="table table-responsive table-striped">
                            <thead>
                            <th><b><g:message code="contactDetails"></g:message>:</b></th>
                            </thead>
                            <tbody>
                            <tr>
                                <td title="property name"><span class="hd2"><span
                                        class=" glyphicon glyphicon-home"></span>
                                </span></td>

                                <td><small>
                                    <%  type = property.type; %>
                                    <%  title = " " + type + " (${property.name})" %>
                                    ${title}
                                </small></td>

                            </tr>
                            <tr>
                                <td>
                                    <span class="hd2"><span class=" glyphicon glyphicon-user"></span></span>
                                </td>
                                <td><small>
                                    ${property.agent.profile.firstName} &nbsp ${property.agent.profile.middleName} &nbsp;${property.agent.profile.lastName}</small>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="hd2"><span class=" glyphicon glyphicon-tag"></span></span>
                                </td>
                                <td>
                                    <small>${property.agent.profile.company}</small>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="hd2"><span class=" glyphicon glyphicon-phone"></span></span>
                                </td>
                                <td>
                                    <small>${property.agent.profile.phone}</small>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="hd2"><span class=" glyphicon glyphicon-envelope"></span></span>
                                </td>
                                <td>
                                    <small>${property.agent.profile.email}</small>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-sm-4" align="center">
                        <table class="table table-responsive">
                            <thead align="">
                            <td>
                                <g:link controller="agent" action="profile" id="${property.agent.id}">
                                    <g:if test="${property.agent.profile.photo == null}">
                                        <img class=' img-responsive '
                                             src="${resource(dir: 'images/property/', file: 'agent.png')} "
                                             width="100"/>
                                    </g:if>
                                    <g:else>
                                        <img class="agent-photo img-responsive" title="${property.agent.username}"
                                             src="<g:createLink controller='image' action='renderAgentPhoto'
                                                                id='${property.agent.id}'/>" width="100"/>
                                    </g:else>

                                </g:link>
                            </td>
                            </thead>

                            <tbody>

                            <tr>
                                <td>
                                    <g:link controller="agent" action="viewProperty" id="${property.agent.id}"
                                            class=" btn btn-default hd2">
                                        <span class="glyphicon glyphicon-list"></span>
                                        <g:message code="property.fsbo.viewOtherProperty"/></g:link><br><br>
                                    <g:link controller="property" action="showAllProperties"
                                            class=" btn btn-default hd2">
                                        <span class="glyphicon glyphicon-list-alt"></span>
                                        <g:message code="property.viewAll"/></g:link>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="btn btn-default hd2" role="button" data-toggle="collapse"
                                       href="#collapseExample" aria-expanded="false"
                                       aria-controls="collapseExample">
                                        <span><i class="fa fa-envelope-square "></i> <g:message
                                                code="property.messageSeller"></g:message></span>
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </g:else>
            </div>
            %{--Contact Seller--}%


            <div class="collapse" id="collapseExample">
                <div class="row">
                    <div class="col-xs-12 text-capitalize">
                        <sec:ifNotLoggedIn>
                            <hr>

                            <h4 class="text-uppercase text-center text-danger"><b>Contact seller</b></h4>

                            <g:hasErrors bean="${flash.errorBean}">
                                <div class="alert alert-block alert-error">
                                    <g:renderErrors bean="${flash.errorBean}" as="list"/>
                                </div>
                            </g:hasErrors>

                            <g:if test="${flash.successMessage}">
                                <div class='alert alert-success'>${flash.successMessage}</div>
                            </g:if>
                            <g:if test="${flash.errorMessage}">
                                <div class='alert alert-error'>${flash.errorMessage}</div>
                            </g:if>
                            <g:if test="${!property.isFsbo() || property.fsboDetails.email}">
                                <g:form controller="property" action="addMessage" id="${property.id}"
                                        class="form form-horizontal">
                                    <div class='form-group'>
                                        <label for="email" class='control-label col-sm-4'><g:message
                                                code="property.enquiry.email"/></label>

                                        <div class='col-sm-8'>
                                            <g:textField name="email" id="formdt" autocomplete="off"
                                                         placeholder="Email" class="form-control"></g:textField>
                                        </div>
                                    </div>

                                    <div class='form-group'>
                                        <label class='control-label col-sm-4'><g:message
                                                code="property.enquiry.phone"/></label>

                                        <div class='col-sm-8'>
                                            <g:textField name="phone" id="formdt" autocomplete="off"
                                                         placeholder="Phone Number" maxlength="13"
                                                         class="form-control" required=""></g:textField>
                                        </div>
                                    </div>

                                    <div class='form-group'>
                                        <label class='control-label col-sm-4'><g:message
                                                code="property.enquiry.message"/></label>

                                        <div class='col-sm-8'>
                                            <g:textArea name="message" cols="40" placeholder="Message" rows="10"
                                                        class="form-control" required=""></g:textArea>
                                        </div>
                                    </div>

                                    <div class='form-group'>
                                        <div class='col-sm-offset-4 col-sm-8'>
                                            <img src="${createLink(controller: 'simpleCaptcha', action: 'captcha')}"/>
                                            <br>
                                            <label for="captcha">Type the letters above in the box below:</label>
                                            <g:textField name="captcha"/>
                                        </div>
                                    </div>


                                    <div class='form-group'>
                                        <div class='col-sm-offset-4 col-sm-8'>
                                            <br>
                                            <g:submitButton
                                                    name="${g.message(code: 'property.enquiry.sendMessage')}"
                                                    class="btn btn-success"></g:submitButton>
                                        </div>
                                    </div>
                                </g:form>

                            </g:if>

                            <g:else>
                                <p><span class="hd2"><span class=" glyphicon glyphicon-tag"></span>
                                </span> <small>${property.fsboDetails.name}</small></p>

                                <p><span class="hd2"><span class=" glyphicon glyphicon-certificate"></span>
                                </span> <small>For Sale By Owner</small></p>

                                <p><span class="hd2"><span class=" glyphicon glyphicon-phone"></span>
                                </span> <small>${property.fsboDetails.phone}</small></p>

                                <p><span class="hd2"><span class=" glyphicon glyphicon-envelope"></span>
                                </span> <small>${property.fsboDetails?.email}</small></p>
                            </g:else>
                        </sec:ifNotLoggedIn>
                    </div>
                </div>
            </div>




            %{--Disquise--}%
            <div class="row">
                %{--<!-- START -->--}%

                <div id="disqus_thread">
                    <h4><span class="glyphicon glyphicon-comment"></span><b>Comments:</b></h4>

                </div>
                <script>
                    /**
                     * RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                     * LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
                     */
                    /*
                     var disqus_config = function () {
                     this.page.url = PAGE_URL; // Replace PAGE_URL with your page's canonical URL variable
                     this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                     };
                     */
                    (function () { // DON'T EDIT BELOW THIS LINE
                        var d = document, s = d.createElement('script');

                        s.src = '//darpropertyforum.disqus.com/embed.js';

                        s.setAttribute('data-timestamp', +new Date());
                        (d.head || d.body).appendChild(s);
                    })();
                </script>
                <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript"
                                                                  rel="nofollow">comments powered by Disqus.</a>
                </noscript>
                %{--<!-- END -->--}%
            </div>

        </div>
    </div>
</div>
%{--==========================================================================================================--}%
%{--<div class="row text-capitalize" style="font-size: 20pt;">--}%
%{--<% String type = property.type; %>--}%
%{--<% def title = " " + type + " (${property.name})" %>--}%
%{--<g:render template="../pageTitle" model="[title: title]"></g:render>--}%
%{--</div>--}%

%{--<div class="row">--}%
%{--<div class=" col-sm-6 ">--}%
%{--<g:render template="property_photos_slideshow"/>--}%
%{--</div>--}%
%{--</div>--}%

%{--<div class="row">--}%
%{--<div class="margine2"></div>--}%
%{--</div>--}%

%{--<div class="row">--}%
%{--DETAILS--}%
%{--<div class="col-sm-6">--}%
%{--<div class="row">--}%
%{--<div class="col-xs-12">--}%
%{--<g:link controller="property" action="viewProperty"--}%
%{--params="${[id: property.id, agentId: property.agent.id]}">--}%
%{--<h4 class="text-capitalize">--}%
%{--<g:if test="${property.saleType.name == 'for rent'}">--}%
%{--${property.type}  <g:message--}%
%{--code="property.saleType.forRent"/>, ${"$property.area,  $property.region"}--}%
%{--</g:if>--}%
%{--<g:elseif test="${property.saleType.name == 'for sale'}">--}%
%{--${property.type}   ${property.saleType.name}, ${"$property.area,  $property.region"}--}%
%{--</g:elseif>--}%
%{--<g:else>--}%
%{--${property.type}   <g:message--}%
%{--code="property.saleType.jointVenture"/>, ${"$property.area,  $property.region"}--}%
%{--</g:else>--}%
%{--</h4>--}%

%{--</g:link>--}%
%{--<p class="text-capitalize">--}%
%{--${property.location} ${property.area} ${property.region} ${property.country}--}%
%{--</p>--}%

%{--</div>--}%
%{--</div>--}%

%{--<div class="row">--}%
%{--<div class="col-xs-12" align="left">--}%
%{--<table class="res-table" style="width:100%;">--}%

%{--<tbody>--}%
%{--<tr class="hd1s text-center pricetag">--}%
%{--<td class="">--}%
%{--<div class="hidden-xs hidden-sm">--}%
%{--<span class="big-price">--}%
%{--<% def main = 'main' %>--}%
%{--<g:render template="../property/priceViewer"--}%
%{--model="[property: property, main: main]"></g:render>--}%
%{--</span>--}%
%{--<g:if test="${property.price}">--}%
%{--<span class="mid-price">${property.currency}--}%
%{--<a tabindex="0"--}%
%{--class="text-info"--}%
%{--role="button" data-toggle="popover"--}%
%{--data-placement="bottom"--}%
%{--data-trigger="focus"--}%
%{--title="Price Convertor"--}%
%{--data-content="--}%
%{--TZS ${NumberFormat.getInstance().format(currencyService.convert(property.price, property.currency, 'TZS'))}--}%
%{--, RWF ${NumberFormat.getInstance().format(currencyService.convert(property.price, property.currency, 'RWF'))}--}%
%{--, KES ${NumberFormat.getInstance().format(currencyService.convert(property.price, property.currency, 'KES'))}--}%
%{--, USD ${NumberFormat.getInstance().format(currencyService.convert(property.price, property.currency, 'USD'))}--}%
%{--"><span--}%
%{--class="fa fa-chevron-circle-down"></span></a></span>--}%
%{--</g:if>--}%
%{--</div>--}%

%{--<div class="hidden-lg hidden-md">--}%
%{--<span class="big-price" style="font-size:10pt;">--}%
%{--<g:if test="${property.price}">--}%
%{--${java.text.NumberFormat.getInstance().format(property.price)}--}%

%{--</g:if>--}%
%{--</span>--}%
%{--<g:if test="${property.price}">--}%
%{--<span class="mid-price" style="font-size:9pt;">${property.currency}--}%

%{--<a tabindex="0"--}%
%{--class="text-info"--}%
%{--role="button" data-toggle="popover"--}%
%{--data-placement="bottom"--}%
%{--data-trigger="focus"--}%
%{--title="Price Convertor"--}%
%{--data-content="--}%
%{--TZS ${NumberFormat.getInstance().format(currencyService.convert(property.price, property.currency, 'TZS'))}--}%
%{--, RWF ${NumberFormat.getInstance().format(currencyService.convert(property.price, property.currency, 'RWF'))}--}%
%{--, KES ${NumberFormat.getInstance().format(currencyService.convert(property.price, property.currency, 'KES'))}--}%
%{--, USD ${NumberFormat.getInstance().format(currencyService.convert(property.price, property.currency, 'USD'))}--}%
%{--<%=CurrencyConvertor.multipleValues(property.price, property.currency)%>--}%
%{--"><span--}%
%{--class="fa fa-chevron-circle-down"></span></a></span>--}%
%{--</g:if>--}%
%{--</div>--}%
%{--<br>--}%

%{--</td>--}%
%{--<td class="">--}%

%{--<% def Show = "mobo" %>--}%

%{--<g:render template="../tool/autoValueCalculator"--}%
%{--model="[pid: property.id, stype: property.saleType, psize: property.size, regionLocated: property.region, areaLocated: property.area, show: Show, ptype: property.type, selectedCurrency: property.currency]"></g:render>--}%

%{--</td>--}%
%{--<td>--}%
%{--<g:link controller="tool" action="valueCalculator">--}%

%{--<i class="fa fa-calculator"></i>--}%
%{--</g:link>--}%
%{--</td>--}%
%{--</tr>--}%

%{--</tbody>--}%
%{--</table>--}%
%{--</div>--}%
%{--</div>--}%

%{--<div class="margine2"></div>--}%

%{--<div class="row">--}%

%{--<div class="col-sm-12 reslt">--}%
%{--<p>--}%
%{--<dp:propertyDescription property="${property}"/>--}%
%{--</p>--}%
%{--</div>--}%
%{--</div>--}%

%{--<div class="margine2"></div>--}%

%{--<div class="row  hidden-xs hidden-sm">--}%
%{--Includes;--}%
%{--<div class="col-sm-12">--}%
%{--<div class="row  amenities text-capitalize">--}%
%{--<div class="col-xs-6">--}%
%{--<g:render template="viewproperty_icons" model="[property: property]"/>--}%
%{--</div>--}%

%{--<div class="col-xs-6 left-border">--}%
%{--<ul class="list-unstyled">--}%
%{--<g:each in="${property.tags}" var="tag">--}%
%{--<g:if test="${tag.name != "parking"}">--}%
%{--<li class="col-xs-12">--}%
%{--<span class="text-danger fa fa-check"--}%
%{--style="font-size: 5pt;"></span> <dp:message codePrefix="property"--}%
%{--code="${tag.name}"/>--}%
%{--</li>--}%
%{--</g:if>--}%
%{--</g:each>--}%
%{--</ul>--}%
%{--</div>--}%
%{--</div>--}%

%{--</div>--}%
%{--</div>--}%

%{--<div class="row hidden-sm hidden-xs">--}%

%{--<div class="col-lg-12">--}%
%{--<hr class="style-three">--}%
%{--<!-- START -->--}%

%{--<div id="disqus_thread">--}%
%{--<h4><span class="glyphicon glyphicon-comment"></span><b>Comments:</b></h4>--}%

%{--</div>--}%
%{--<script>--}%
%{--/**--}%
%{--* RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.--}%
%{--* LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables--}%
%{--*/--}%
%{--/*--}%
%{--var disqus_config = function () {--}%
%{--this.page.url = PAGE_URL; // Replace PAGE_URL with your page's canonical URL variable--}%
%{--this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable--}%
%{--};--}%
%{--*/--}%
%{--(function () { // DON'T EDIT BELOW THIS LINE--}%
%{--var d = document, s = d.createElement('script');--}%

%{--s.src = '//darpropertyforum.disqus.com/embed.js';--}%

%{--s.setAttribute('data-timestamp', +new Date());--}%
%{--(d.head || d.body).appendChild(s);--}%
%{--})();--}%
%{--</script>--}%
%{--<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript"--}%
%{--rel="nofollow">comments powered by Disqus.</a>--}%
%{--</noscript>--}%
%{--<!-- END -->--}%
%{--</div>--}%
%{--</div>--}%
%{--</div>--}%
%{--CONTACTS--}%
%{--<div class="col-sm-6">--}%
%{--<br>--}%

%{--<div class="row">--}%

%{--<div class="row">--}%
%{--<h4 class="text-uppercase text-center text-danger">share</h4>--}%
%{--</div>--}%

%{--<div class="row" align="center">--}%
%{--<p>--}%
%{--<span class="" style="color:#4a6ea9; font-size:30pt;">--}%
%{--<!-- FACEBOOK -->--}%


%{--<a class="custom_share_button custom_fb_button  fb_share_track"--}%
%{--data-fbtrackid="gallery"--}%
%{--href="https://www.facebook.com/sharer/sharer.php?u=http://www.darproperty.co.tz/property/viewProperty/${property.id}?agentId=${property.agent.id}"--}%
%{--onclick="window.open($(this).attr('href'), 'fb_popup', 'width=600,height=310');--}%
%{--return false;"--}%
%{--gallery_share_url="http://www.darproperty.co.tz/property/viewProperty/${property.id}?agentId=${property.agent.id}"--}%
%{--appId='1670558763159469'--}%
%{--title="Facebook Share"--}%
%{--target="_blank">--}%

%{--<span class="fa fa-facebook-square hidden-sm hidden-xs"></span>--}%
%{--<span class="fa fa-facebook hidden-lg"></span>--}%

%{--</a>--}%

%{--</span>--}%

%{--<span class=" hidden-xs hidden-sm"--}%
%{--style="background-color:#EEE; border-color:transparent; border-width: 0px;">--}%
%{--</span>--}%

%{--<span class=" hidden-lg" style="color:#2ab200;font-size:30pt;">--}%
%{--<!-- WHATSAPP -->--}%
%{--<a--}%
%{--href="whatsapp://send?text= Check  ${property.type} ${property.saleType} at  ${property.area}, ${property.region}, ${property.country} READ MORE:-> http://www.darproperty.co.tz/property/viewProperty/${property.id}?agentId=${property.agent.id}"--}%
%{--data-action="share/whatsapp/share"--}%
%{--target="_blank"--}%
%{--title="Whatsapp share">--}%
%{--<img--}%
%{--src="${resource(dir: 'bootstrap/imgs/', file: 'whatsappgreen.png')}"--}%
%{--alt="Whatsapp"--}%
%{--width="40"--}%
%{--title="Share on Whatsapp"/>--}%
%{--</a>--}%
%{--</span>--}%

%{--<span class="" style="color:#659fcb; font-size:30pt;">--}%
%{--<!-- TWITTER -->--}%

%{--<!--class="twitter-share-button"{count}  -->--}%
%{--<a href="https://twitter.com/intent/tweet?text=See%20now%20${property.type}%20${property.saleType}%20at%20${property.area}%20,%20${property.region},%20${property.country}%20VIA%20@thedarproperty%20in&amp;url=http%3A%2F%2Fwww.darproperty.co.tz%2Fproperty%2FviewProperty%2F${property.id}?agentId=${property.agent.id}"--}%
%{--class="shortenUrl"--}%
%{--data-social-url="https://twitter.com/intent/tweet?text=See+now+${property.type}+${property.saleType}+at+${property.area}+,+${property.region},+${property.country}+VIA+@thedarproperty+in&amp;url="--}%
%{--data-target-url="http://www.darproperty.co.tz/property/viewProperty/${property.id}?agentId=${property.agent.id}"--}%
%{--target="_blank">--}%
%{--<span class="fa fa-twitter-square hidden-sm hidden-xs"></span>--}%
%{--<span class="fa fa-twitter hidden-lg"></span>--}%
%{--<img--}%
%{--src="${resource(dir: 'bootstrap/imgs/', file: 'twitter.png')}"--}%
%{--alt="Twitter"--}%
%{--class=""--}%
%{--title="Share on Twitter"/>--}%
%{--</a>--}%
%{--</span>--}%
%{--</p>--}%
%{--</div>--}%

%{--</div>--}%

%{--<div class="row">--}%
%{--<div class="col-xs-8">--}%
%{--<g:if test="${property.isFsbo()}">--}%

%{--<p><span class="hd2"><span class=" glyphicon glyphicon-tag"></span>--}%
%{--</span> <small>${property.fsboDetails.name}</small></p>--}%

%{--<p><span class="hd2"><span class=" glyphicon glyphicon-certificate"></span>--}%
%{--</span> <small>For Sale By Owner</small></p>--}%

%{--<p><span class="hd2"><span class=" glyphicon glyphicon-phone"></span>--}%
%{--</span> <small>${property.fsboDetails.phone}</small></p>--}%

%{--<p><span class="hd2"><span class=" glyphicon glyphicon-envelope"></span>--}%
%{--</span> <small>${property.fsboDetails?.email}</small></p>--}%
%{--<g:link controller="agent" action="viewProperty" id="${property.agent.id}"--}%
%{--class=" btn btn-default hd2">--}%
%{--<span class="glyphicon glyphicon-list"></span>--}%
%{--<g:message code="property.fsbo.viewOtherProperty"/></g:link><br><br>--}%
%{--<g:link controller="property" action="showAllProperties"--}%
%{--class=" btn btn-default hd2">--}%
%{--<span class="glyphicon glyphicon-list-alt"></span>--}%
%{--<g:message code="property.viewAll"/></g:link>--}%

%{--</g:if>--}%
%{--<g:else>--}%

%{--<p><span class="hd2"><span class=" glyphicon glyphicon-user"></span></span> <small>--}%
%{--${property.agent.profile.firstName} &nbsp ${property.agent.profile.middleName} &nbsp;${property.agent.profile.lastName}</small>--}%
%{--</p>--}%

%{--<p><span class="hd2"><span class=" glyphicon glyphicon-tag"></span>--}%
%{--</span> <small>${property.agent.profile.company}</small></p>--}%

%{--<p><span class="hd2"><span class=" glyphicon glyphicon-phone"></span>--}%
%{--</span> <small>${property.agent.profile.phone}</small></p>--}%
%{--<g:link controller="agent" action="viewProperty" id="${property.agent.id}"--}%
%{--class=" btn btn-default hd2">--}%
%{--<span class="glyphicon glyphicon-list"></span>--}%
%{--<g:message code="property.viewOtherPropertyAgent"/></g:link><br><br>--}%
%{--<g:link controller="property" action="showAllProperties"--}%
%{--class=" btn btn-default hd2">--}%
%{--<span class="glyphicon glyphicon-list-alt"></span> <g:message--}%
%{--code="property.viewAll"/></g:link><br><br>--}%

%{--</g:else>--}%
%{--</div>--}%

%{--<div col-xs-4>--}%
%{--<g:link controller="agent" action="profile" id="${property.agent.id}">--}%
%{--<g:if test="${property.agent.profile.photo == null}">--}%
%{--<img class='img-polaroid img-responsive'--}%
%{--src="${resource(dir: 'images/property/', file: 'agent.png')} " width="200"/>--}%
%{--</g:if>--}%
%{--<g:else>--}%
%{--<img class="agent-photo img-responsive" title="${property.agent.username}"--}%
%{--src="<g:createLink controller='image' action='renderAgentPhoto'--}%
%{--id='${property.agent.id}'/>" width="100"/>--}%
%{--</g:else>--}%

%{--</g:link>--}%
%{--<br>--}%
%{--<g:if test="${property.saleType.name != 'for sale' && property.saleType.name != 'for rent'}">--}%
%{--<img--}%
%{--src="/images/property/joint.png"--}%
%{--alt="${property.saleType}"/>--}%
%{--</g:if>--}%
%{--<g:else>--}%
%{--<img--}%
%{--src="/images/property/${property.saleType.name == 'for sale' ? 'for_sale' : 'for_rent'}.png"--}%
%{--alt="${property.saleType}"/>--}%
%{--</g:else>--}%
%{--<p><small><g:render template="../property/post_age_counter"--}%
%{--model="[property: property]"></g:render></small></p>--}%
%{--</div>--}%
%{--</div>--}%

%{--<div class="row">--}%
%{--<div class="col-lg-12">--}%
%{--<sec:ifNotLoggedIn>--}%

%{--<h4 class="text-uppercase text-center text-danger">Contact seller</h4>--}%

%{--<g:hasErrors bean="${flash.errorBean}">--}%
%{--<div class="alert alert-block alert-error">--}%
%{--<g:renderErrors bean="${flash.errorBean}" as="list"/>--}%
%{--</div>--}%
%{--</g:hasErrors>--}%

%{--<g:if test="${flash.successMessage}">--}%
%{--<div class='alert alert-success'>${flash.successMessage}</div>--}%
%{--</g:if>--}%
%{--<g:if test="${flash.errorMessage}">--}%
%{--<div class='alert alert-error'>${flash.errorMessage}</div>--}%
%{--</g:if>--}%
%{--<g:if test="${!property.isFsbo() || property.fsboDetails.email}">--}%
%{--<g:form controller="property" action="addMessage" id="${property.id}"--}%
%{--class="form form-horizontal">--}%
%{--<div class='form-group'>--}%
%{-- <label for="email" class='control-label col-sm-4'><g:message--}%
%{--code="property.enquiry.email"/></label>--}%

%{--<div class=''>--}%
%{--<g:textField name="email" id="formdt" autocomplete="off"--}%
%{--placeholder="Email"></g:textField>--}%
%{--</div>--}%
%{--</div>--}%

%{--<div class='form-group'>--}%
%{--<label class='control-label col-sm-4'><g:message code="property.enquiry.phone"/></label>--}%

%{--<div class=''>--}%
%{--<g:textField name="phone" id="formdt" autocomplete="off"--}%
%{--placeholder="Phone Number" maxlength="13"--}%
%{--class="form-control" required=""></g:textField>--}%
%{--</div>--}%
%{--</div>--}%

%{--<div class='form-group'>--}%
%{--<label class='control-label col-sm-4'><g:message code="property.enquiry.message"/></label>--}%
%{----}%
%{--<div class=''>--}%
%{--<g:textArea name="message" cols="40" placeholder="Message" rows="10"--}%
%{--class="form-control" required=""></g:textArea>--}%
%{--</div>--}%
%{--</div>--}%

%{--<div class='form-group'>--}%
%{--<div class=''>--}%
%{--<img src="${createLink(controller: 'simpleCaptcha', action: 'captcha')}"/>--}%
%{--<br>--}%
%{--<label for="captcha">Type the letters above in the box below:</label>--}%
%{--<g:textField name="captcha"/>--}%
%{--</div>--}%
%{--</div>--}%


%{--<div class='form-group'>--}%
%{--<div class=''>--}%
%{--<br>--}%
%{--<g:submitButton--}%
%{--name="${g.message(code: 'property.enquiry.sendMessage')}"--}%
%{--class="btn btn-success"></g:submitButton>--}%
%{--</div>--}%
%{--</div>--}%
%{--</g:form>--}%

%{--</g:if>--}%

%{--<g:else>--}%
%{--<p><span class="hd2"><span class=" glyphicon glyphicon-tag"></span>--}%
%{--</span> <small>${property.fsboDetails.name}</small></p>--}%

%{--<p><span class="hd2"><span class=" glyphicon glyphicon-certificate"></span>--}%
%{--</span> <small>For Sale By Owner</small></p>--}%

%{--<p><span class="hd2"><span class=" glyphicon glyphicon-phone"></span>--}%
%{--</span> <small>${property.fsboDetails.phone}</small></p>--}%

%{--<p><span class="hd2"><span class=" glyphicon glyphicon-envelope"></span>--}%
%{--</span> <small>${property.fsboDetails?.email}</small></p>--}%
%{--</g:else>--}%
%{--</sec:ifNotLoggedIn>--}%
%{--</div>--}%

%{--</div>--}%

%{--</div>--}%

%{--</div>--}%
%{--</div>--}%

<g:render template="../search/extras" model="[property: property]"></g:render>
%{-- View property --}%
<g:javascript src="lib/propertySlider.js"/>

</body>

</html>