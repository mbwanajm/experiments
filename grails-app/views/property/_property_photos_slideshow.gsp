%{--<div class="container">--}%
%{--<div id="main_area">--}%
<!-- Slider -->
<div class="row" align="center" style="background-color: transparent;padding: 10px;">
    <div class="col-xs-12" id="slider">
        <!-- Top part of the slider -->
        <div class="row">
            <div class="col-sm-12" align="center" id="carousel-bounding-box">
                <div class="carousel slide" id="myCarousel">
                    <!-- Carousel items -->
                    <div class="carousel-inner">

                        <g:each status='index' var="photo" in="${property.photos}">
                            <g:if test="${index == 0}">
                                <div class="active item" data-slide-number="${index}">
                                    <img src="<g:createLink controller='image'
                                                            action='renderPropertyPhoto'
                                                            id='${photo.id}'/>"></div>
                            </g:if>
                            <g:else>
                                <div class="item" data-slide-number="${index}">
                                    <img src="<g:createLink controller='image'
                                                            action='renderPropertyPhoto'
                                                            id='${photo.id}'/>"></div>
                            </g:else>
                        </g:each>

                    </div><!-- Carousel nav -->
                    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div>
            </div>

        </div>
    </div>
</div><!--/Slider-->

<div class="row hidden-xs" id="slider-thumbs" align="center" style="background-color:transparent;padding: 5px">
    <!-- Bottom switcher of slider -->
    <ul class="hide-bullets">

        <g:each status='index' var="photo" in="${property.photos}">

            <li class="col-sm-4" style="padding: 10px; width: 110px; border:1px  #fff;">
                <a class="" id="carousel-selector-${index}"><img src="<g:createLink controller='image'
                                                                                             action='renderPropertyPhoto'
                                                                                             id='${photo.id}'/>" >
                </a>
            </li>

        </g:each>
    </ul>
</div>
%{--</div>--}%
%{--</div>--}%