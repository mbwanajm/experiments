<html>
<head>

    <meta name="layout" content="main"/>
    <title>DarProperty >> Property</title>
</head>

<body>
<div class="container">
    <g:render template="../search/homeSearch" model="[tab1: tab1, tab2: tab2, tab3: tab3]"></g:render>

    <div class="margine"></div>


    <div class="margine"></div>

    <div class='row'>
        <div class="span12">

            <!--<g:if test="${propertys.size == 0}">
                <div class='alert alert-error' align="center">Sorry, no property were found</div>
            </g:if>
            <g:else>
                <div class='alert alert-info' align="center">${propertyCount} property found</div>
            </g:else>-->

        </div>
    </div>

    <g:render template="/search/property_result" collection="${propertys}" var="property"/>



    <div class='row'>
        <div class='col-sm-12 pagination' align="center">
            <g:paginate controller="property" action="showAllProperties" total="${propertyCount}"/>
        </div>
    </div>




    <br>
    <br>

</div>


%{--<div class="container hidden-lg hidden-md hidden-print">--}%

    %{--<div class="row">--}%

        %{--<div class="col-xs-12">--}%

            %{--<!-- PANEL -->--}%


            %{--<div class='row'>--}%
                %{--<div class="col-sm-12">--}%
                    %{--<h2 class="text-center text-capitalize">View All Properties</h2>--}%
                    %{--<br>--}%
                    %{--<g:render template="/search/homeSearch"/>--}%

                %{--</div>--}%
            %{--</div>--}%

            %{--<div class='row'>--}%
                %{--<div class="col-sm-12">--}%

                    %{--<g:render template="/search/property_result" collection="${propertys}" var="property"/>--}%
                %{--</div>--}%

            %{--</div>--}%

            %{--<div class='row'>--}%
                %{--<div class='col-sm-6 col-sm-offset-3 pagination'>--}%
                    %{--<g:paginate controller="property" class="" action="showAllProperties" total="${propertyCount}"/>--}%
                %{--</div>--}%
            %{--</div>--}%
            %{--<!--  -->--}%

        %{--</div>--}%

    %{--</div>--}%
%{--</div>--}%
</body>

</html>