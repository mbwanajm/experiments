<%@ page import="java.text.NumberFormat" %>
<g:if test="${main != "main"}">

    <g:if test="${property.price}">
        <%
            def num = property.price
            def pr = 0
            def rep = ""
            def hold = num / 1000000000
            if(hold < 1){
                hold = num / 1000000
                if (hold >= 1) {
                    pr = num
                    rep = "M"
                } else {
                    hold = num / 1000
                    if (hold >= 1) {
                        pr = num
                        rep = "K"
                    } else {
                        pr = num

                    }

                }
            }else{
                rep = "Bil"
                pr = hold
            }


        %>
        <g:if test="${rep == "M"}">
            <b
                    style="color: #FFF; font-size: 12pt; "> ${NumberFormat.getInstance().format(pr)}</b>
            <small
                    style="color: #FFF; font-size: 12pt; ">${
                property.currency == "USD" ? '$' : property.currency}</small>
        </g:if>
        <g:elseif test="${rep == "K"}">
            <b
                    style="color: #FFF; font-size: 12pt; ">${NumberFormat.getInstance().format(pr)} </b><small
                style="color: #FFF; font-size: 12pt; ">${
                property.currency == "USD" ? '$' : property.currency}</small>
        </g:elseif>
        <g:elseif test="${rep == "Bil"}">
            <b
                    style="color: #FFF; font-size: 12pt; ">${NumberFormat.getInstance().format(pr)} ${rep} </b><small
                style="color: #FFF; font-size: 12pt; ">${
                property.currency == "USD" ? '$' : property.currency}</small>
        </g:elseif>
        <g:else>
            <b
                    style="color: #FFF; font-size: 12pt; ">${NumberFormat.getInstance().format(pr)} </b><small
                style="color: #FFF; font-size: 9pt; ">${
                property.currency == "USD" ? '$' : property.currency}</small>
        </g:else>

    </g:if>
    <g:else>
        <g:message code="not.provided"></g:message>
        %{--0 <small--}%
            %{--style="color: #FFF; font-size: 12pt;">${--}%
            %{--property.currency == "USD" ? '$' : property.currency}</small>--}%
    </g:else>
</g:if>
<g:else>

    <g:if test="${property.price}">
        <%
             num = property.price
             pr = 0
             rep = ""
             hold = num / 1000000000
            if(hold < 1){
                hold = num / 1000000
                if (hold >= 1) {
                    pr = num
                    rep = "M"
                } else {
                    hold = num / 1000
                    if (hold >= 1) {
                        pr = num
                        rep = "K"
                    } else {
                        pr = num

                    }

                }
            }else{
                rep = "Bil"
                pr = hold
            }


        %>
        <g:if test="${rep == "M"}">
            <span
                    style="color: inherit; "> ${NumberFormat.getInstance().format(pr)}</span>

        </g:if>
        <g:elseif test="${rep == "K"}">
            <span
                    style="color: inherit;  ">  ${NumberFormat.getInstance().format(pr)} </span>
        </g:elseif>
        <g:elseif test="${rep == "Bil"}">
            <span
                    style="color: inherit;  ">  ${NumberFormat.getInstance().format(pr)} Billion </span>
        </g:elseif>
        <g:else>
            <span
                    style="color: inherit;  ">  ${NumberFormat.getInstance().format(pr)} </span>
        </g:else>

    </g:if>
    <g:else>
        <g:message code="not.provided"></g:message>
    </g:else>
</g:else>
