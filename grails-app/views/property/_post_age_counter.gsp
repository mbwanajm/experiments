<span class="hd2 hidden-xs hidden-sm" style="font-family: inherit;"><g:message code="property.posted"></g:message>:</span>
<span class="hd2 hidden-md hidden-lg" title="Uploaded" style="font-family: inherit;"><i class="fa fa-clock-o"></i> </span>
<g:dateFromNow date="${property.dateAdded}"/>


