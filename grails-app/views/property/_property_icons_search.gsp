<ul class="list-unstyled">
    <li class="col-xs-12">
        <span class="property-icon fa fa-tag"></span>
        <span class="icon-label" class="text-capitalize"
              title="<g:message code="property.ref"/>">${property.name}</span>
    </li>
    <g:if test="${property.size != 0}">
        <li class="col-xs-12">
            <img class="property-icon"
                 src="${resource(dir: 'images/property', file: 'area.png')}"
                 title="<g:message code='application.sqm'/>"/>
            <span class="icon-label" title="<g:message code='application.sqm'/>">${property.size} sqm</span>
        </li>
    </g:if>
    <g:if test="${property.numberOfRooms != 0}">
        <li class="col-xs-12">
            <img class="property-icon"
                 src="${resource(dir: 'images/property', file: 'bedroom.png')}"
                 title="<g:message code='property.rooms'/>"/>
            <span class="icon-label" title="<g:message code='property.rooms'/>">${property.numberOfRooms} <g:message
                    code='property.rooms'/></span>
        </li>
    </g:if>
    <g:if test="${property.numberOfBathRooms != 0}">
        <li class="col-xs-12">
            <img class="property-icon"
                 src="${resource(dir: 'images/property', file: 'bathroom.png')}"
                 title="<g:message code='property.bathrooms'/>"/>
            <span class="icon-label"
                  title="<g:message code='property.bathrooms'/>">${property.numberOfBathRooms} <g:message
                    code='property.bathrooms'/></span>
        </li>
    </g:if>
    <g:if test="${(property.tags.collect { it.name }).contains('parking')}">
        <li class="col-xs-12">
            <img class="property-icon"
                 src="${resource(dir: 'images/property', file: 'parking.png')}"
                 title="<g:message code='property.parking'/>"/>
            <span class="icon-label text-danger fa fa-check" title="<g:message code='property.parking'/>"></span>
        </li>
    </g:if>
    <li class="col-xs-12 hidden-lg hidden-md">
        <small>
            <g:render template="../property/post_age_counter"
                      model="[property: property]"></g:render>
        </small>
    </li>

</ul>

