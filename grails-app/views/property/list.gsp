<%@ page import="com.mbwanajm.darproperty.Property" %>

<%@ page contentType="text/html;charset=windows-1252" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'property.label', default: 'Property')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="container" align="left">
    <div class="row">
        <div class="col-sm-5">

            <a href="#list-property" class="skip" tabindex="-1"
               title="<g:message code="default.link.skip.label" default="Skip to content&hellip;"/>">
                <span class="glyphicon glyphicon-refresh"></span></a>

        </div>

    </div>

    <br>


    <div class="nav" role="navigation">
        <ul>
            <li class="btn btn-default"><g:link controller="config" action=""><span
                    class="glyphicon glyphicon-wrench"></span></g:link></li>
            <li class="btn btn-default"><span class="glyphicon glyphicon-plus"></span>
                <g:link class="create" action="create"><g:message code="default.new.label"
                                                                  args="[entityName]"/></g:link></li>
        </ul>
    </div>

    <div id="list-property" class="content scaffold-list" role="main">
        <h1><span class="glyphicon glyphicon-th-list"></span> <g:message code="default.list.label" args="[entityName]"/>
        </h1>
        <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
        </g:if>




        <table class="table">
            <thead>
            <tr>

                <g:sortableColumn property="name" title="${message(code: 'property.name.label', default: 'Name')}"/>

                <g:sortableColumn property="description"
                                  title="${message(code: 'property.description.label', default: 'Description')}"/>

                <g:sortableColumn property="region"
                                  title="${message(code: 'property.region.label', default: 'Region')}"/>

                <g:sortableColumn property="area" title="${message(code: 'property.area.label', default: 'Area')}"/>

                <th><g:message code="property.agent.label" default="Agent"/></th>

                <g:sortableColumn property="views" title="${message(code: 'property.views.label', default: 'Views')}"/>

            </tr>
            </thead>



            <tbody>
            <g:each in="${propertyInstanceList}" status="i" var="propertyInstance">
                <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                    <td class="hd2"><g:link action="show"
                                            id="${propertyInstance.id}">${fieldValue(bean: propertyInstance, field: "name")}</g:link></td>

                    <td class="hd2">${fieldValue(bean: propertyInstance, field: "description")}</td>

                    <td class="hd2">${fieldValue(bean: propertyInstance, field: "region")}</td>

                    <td class="hd2">${fieldValue(bean: propertyInstance, field: "area")}</td>

                    <td class="hd2">${fieldValue(bean: propertyInstance, field: "agent")}</td>

                    <td class="hd2">${fieldValue(bean: propertyInstance, field: "views")}</td>

                </tr>
            </g:each>
            </tbody>
        </table>

        <div class="pagination hd2">
            <g:paginate total="${propertyInstanceTotal}"/>
        </div>
    </div>
</div>
<br>



</body>
</html>
<!--  <% //response.setContentType("application/vnd.ms-excel");
//response.setHeader("Content-Disposition", "attachment; filename=" + "online-dashboard.csv"); %> -->