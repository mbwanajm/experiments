<ul class="list-unstyled">
<g:if test="${property.size != 0}">
    <li class='col-xs-12'>
        <img class="property-icon"
             src="${resource(dir: 'images/property', file: 'house.png')}"/>
        ${property.size} sqm
    </li>
</g:if>
<g:if test="${property.numberOfRooms != 0}">
    <li class='col-xs-12'>
        <img class="property-icon"
             src="${resource(dir: 'images/property', file: 'bedroom.png')}"/>
        ${property.numberOfRooms} <g:message code='property.rooms'/>
    </li>
</g:if>
<g:if test="${property.numberOfBathRooms != 0}">
    <li class='col-xs-12'>
        <img class="property-icon"
             src="${resource(dir: 'images/property', file: 'bathroom.png')}"/>
        ${property.numberOfBathRooms} <g:message code='property.bathrooms'/>
    </li>
</g:if>
<g:if test="${(property.tags.collect {it.name}).contains('parking')}">
    <li class='col-xs-12'>
        <img class="property-icon"
             src="${resource(dir: 'images/property', file: 'garage.png')}"/>
        <g:message code="property.parking"/>
    </li>
</g:if>
</ul>