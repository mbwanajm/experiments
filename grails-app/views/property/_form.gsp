<%@ page import="com.mbwanajm.darproperty.Property" %>


<div class="container" align="center">
<div class="panel panel-default">
 <div class="panel-header"></div>
  <div class="panel-body" align="left">
  
 <div class="well"> 
<div class="fieldcontain ${hasErrors(bean: propertyInstance, field: 'name', 'error')} required">
<div class="form-group" >
    <label for="name" class="control-label col-sm-5" >
        <g:message code="property.name.label" default="Name"/>
        <span class="required-indicator">*</span>
    </label>
    <div class="col-sm-7" align="left">
    <g:textField name="name" required="" value="${propertyInstance?.name}" class="form-control"/>
    </div>
</div>
</div>
</div>

<div class="well">
<div class="fieldcontain ${hasErrors(bean: propertyInstance, field: 'description', 'error')} required">
<div class="form-group">
    <label for="description" class="control-label col-sm-5" >
        <g:message code="property.description.label" default="Description"/>
        <span class="required-indicator">*</span>
    </label>
   <div class="col-sm-7" align="left">
    <g:textArea name="description" cols="40" rows="5" maxlength="500" required=""
                value="${propertyInstance?.description}" class="form-control"/>
                </div>
</div>
</div>
</div>
<br>
<br>
<br>

<div class="well">
<div class="fieldcontain ${hasErrors(bean: propertyInstance, field: 'country', 'error')} required">
<div class="form-group">
    <label for="region" class="control-label col-sm-5">
        <g:message code="property.country.label" default="Country"/>
        <span class="required-indicator">*</span>
    </label>
    <div class="col-sm-7" align="left">
    <g:textField name="region" required="" value="${propertyInstance?.country}" class="form-control"/>
	</div>
</div>
</div>
</div>

<div class="well">
<div class="fieldcontain ${hasErrors(bean: propertyInstance, field: 'region', 'error')} required">
<div class="form-group">
    <label for="region" class="control-label col-sm-5">
        <g:message code="property.region.label" default="Region"/>
        <span class="required-indicator">*</span>
    </label>
    <div class="col-sm-7" align="left">
    <g:textField name="region" required="" value="${propertyInstance?.region}" class="form-control"/>
    </div>
</div>
</div>
</div>

<div class="well">
<div class="fieldcontain ${hasErrors(bean: propertyInstance, field: 'area', 'error')} required">
<div class="form-group">
    <label for="area" class="control-label col-sm-5" >
        <g:message code="property.area.label" default="Area"/>
        <span class="required-indicator">*</span>
    </label>
    <div class="col-sm-7">
    <g:textField name="area" required="" value="${propertyInstance?.area}" class="form-control"/>
    </div>
</div>
</div>
</div>

<div class="well">
<div class="fieldcontain ${hasErrors(bean: propertyInstance, field: 'agent', 'error')} ">
 <div class="form-group">
    <label for="agent" class="control-label col-sm-5">
        <g:message code="property.agent.label" default="Agent"/>

    </label>
    <div class="col-sm-7">
    <g:select id="agent" name="agent.id" from="${com.mbwanajm.darproperty.Agent.list()}" optionKey="id"
              value="${propertyInstance?.agent?.id}" class="many-to-one" noSelection="['null': '']" class="form-control"/>
	</div>
</div>
</div>
</div>

<div class="well">
<div class="fieldcontain ${hasErrors(bean: propertyInstance, field: 'views', 'error')} ">
<div class="form-group">
    <label for="views" class="control-label col-sm-5">
        <g:message code="property.views.label" default="Views"/>

    </label>
    <div class="col-sm-7">
    <g:field name="views" type="number" value="${propertyInstance.views}" class="form-control"/>
    </div>
 </div>
</div>
</div>

<div class="well">
<div class="fieldcontain ${hasErrors(bean: propertyInstance, field: 'dateAdded', 'error')} ">
<div class="form-group">
    <label for="dateAdded" class="control-label col-sm-5">
        <g:message code="property.dateAdded.label" default="Date Added"/>

    </label>
    <div class="col-sm-7">
    <g:datePicker name="dateAdded" precision="day" value="${propertyInstance?.dateAdded}" default="none"
                  noSelection="['': '']" class="form-control"/>
                  </div>
</div>
</div>
</div>

<div class="well">
<div class="fieldcontain ${hasErrors(bean: propertyInstance, field: 'location', 'error')} ">
<div class="form-group">
    <label for="location" class="control-label col-sm-5">
        <g:message code="property.location.label" default="Location"/>

    </label>
    <div class="col-sm-7">
    <g:textField name="location" value="${propertyInstance?.location}" class="form-control"/>
 </div>
</div>
</div>
</div>

<div class="well">
<div class="fieldcontain ${hasErrors(bean: propertyInstance, field: 'deleted', 'error')} ">
<div class="form-group">
    <label for="deleted" class="control-label col-sm-5">
        <g:message code="property.deleted.label" default="Deleted"/>

    </label>
     <div class="col-sm-3" align="left">
    <g:checkBox name="deleted" value="${propertyInstance?.deleted}" class="form-control"/>
</div>
</div>
</div>
</div>

<div class="well">
<div class="fieldcontain ${hasErrors(bean: propertyInstance, field: 'deletedOn', 'error')} ">
<div class="form-group">
    <label for="deletedOn" class="control-label col-sm-5">
        <g:message code="property.deletedOn.label" default="Deleted On"/>

    </label>
    <div class="col-sm-7" align="left">
    <g:datePicker name="deletedOn" precision="day" value="${propertyInstance?.deletedOn}" default="none"
                  noSelection="['': '']" class="form-control"/>
</div>
</div>
</div>
</div>

<div class="well">
<div class="fieldcontain ${hasErrors(bean: propertyInstance, field: 'price', 'error')} ">
  <div class="form-group">
    <label for="price" class="control-label col-sm-5">
        <g:message code="property.price.label" default="Price"/>

    </label>
    <div class="col-sm-7" align="left">
    <g:field name="price" value="${fieldValue(bean: propertyInstance, field: 'price')}" class="form-control"/>
</div>
</div>
</div>
</div>

<div class="well">
<div class="fieldcontain ${hasErrors(bean: propertyInstance, field: 'currency', 'error')} ">
 <div class="form-group">
    <label for="currency" class="control-label col-sm-5">
        <g:message code="property.currency.label" default="Currency"/>

    </label>
     <div class="col-sm-7" align="left">
    <g:textField name="currency" value="${propertyInstance?.currency}" class="form-control"/>
</div>
</div>
</div>
</div>

<div class="well">
<div class="fieldcontain ${hasErrors(bean: propertyInstance, field: 'descriptionSW', 'error')} ">
    <div class="form-group">
    <label for="descriptionSW" class="control-label col-sm-5">
        <g:message code="property.descriptionSW.label" default="Description SW" />

    </label>
    <div class="col-sm-7" align="left">
    <g:textField name="descriptionSW" value="${propertyInstance?.descriptionSW}" class="form-control"/>
</div>
</div>
</div>
</div>

<div class="well">
<div class="fieldcontain ${hasErrors(bean: propertyInstance, field: 'messages', 'error')} ">
<div class="form-group">
    <label for="messages" class="control-label col-sm-5">
        <g:message code="property.messages.label" default="Messages"/>

    </label>
    <div class="col-sm-7" align="left">
    <g:select name="messages" from="${com.mbwanajm.darproperty.Message.list()}" multiple="multiple" optionKey="id"
              size="5" value="${propertyInstance?.messages*.id}" class="many-to-many" class="form-control"/>
</div>
</div>
</div>
</div>
<br>
<br>
<br>

<div class="well">
<div class="fieldcontain ${hasErrors(bean: propertyInstance, field: 'numberOfBathRooms', 'error')} required">
<div class="form-group">
    <label for="numberOfBathRooms" class="control-label col-sm-5">
        <g:message code="property.numberOfBathRooms.label" default="Number Of Bath Rooms" class="control-label col-sm-5"/>
        <span class="required-indicator">*</span>
    </label>
    <div class="col-sm-7" align="left">
    <g:field name="numberOfBathRooms" type="number" value="${propertyInstance.numberOfBathRooms}" required="" class="form-control"/>
</div>
</div>
</div>
</div>


<div class="well">
<div class="fieldcontain ${hasErrors(bean: propertyInstance, field: 'numberOfRooms', 'error')} required">
 <div class="form-group" >
    <label for="numberOfRooms"  class="control-label col-sm-5">
        <g:message code="property.numberOfRooms.label" default="Number Of Rooms"/>
        <span class="required-indicator">*</span>
    </label>
    <div class="col-sm-7" align="left">
    <g:field name="numberOfRooms" type="number" value="${propertyInstance.numberOfRooms}" required="" class="form-control"/>
</div>
</div>
</div>
</div>

<div class="well">
<div class="fieldcontain ${hasErrors(bean: propertyInstance, field: 'photos', 'error')} ">
 <div class="form-group" >
    <label for="photos"class="control-label col-sm-5">
        <g:message code="property.photos.label" default="Photos"/>

    </label>
    <div class="col-sm-7" align="left">
    <g:select name="photos" from="${com.mbwanajm.darproperty.Photo.list()}" multiple="multiple" optionKey="id" size="5"
              value="${propertyInstance?.photos*.id}" class="many-to-many" class="form-control"/>
</div>
</div>
</div>
</div>
<br>
<br>
<br>

<div class="well">
<div class="fieldcontain ${hasErrors(bean: propertyInstance, field: 'saleType', 'error')} required">
 <div class="form-group" >
    <label for="saleType" class="control-label col-sm-5">
        <g:message code="property.saleType.label" default="Sale Type"/>
        <span class="required-indicator">*</span>
    </label>
    <div class="col-sm-7" align="left">
    <g:select id="saleType" name="saleType.id" from="${com.mbwanajm.darproperty.SaleType.list()}" optionKey="id"
              required="" value="${propertyInstance?.saleType?.id}" class="many-to-one" class="form-control"/>
</div>
</div>
</div>
</div>

<div class="well">
<div class="fieldcontain ${hasErrors(bean: propertyInstance, field: 'size', 'error')} required">
  <div class="form-group" >
    <label for="size" class="control-label col-sm-5">
        <g:message code="property.size.label" default="Size"/>
        <span class="required-indicator">*</span>
    </label>
     <div class="col-sm-7" align="left">
    <g:field name="size" value="${fieldValue(bean: propertyInstance, field: 'size')}" required="" class="form-control"/>
</div>
</div>
</div>
</div>


<div class="well">
<div class="fieldcontain ${hasErrors(bean: propertyInstance, field: 'tags', 'error')} ">
   <div class="form-group" >
    <label for="tags " class="control-label col-sm-5">
        <g:message code="property.tags.label" default="Tags"/>

    </label>
    <div class="col-sm-7" align="left">
    <g:select name="tags" from="${com.mbwanajm.darproperty.Tag.list()}" multiple="multiple" optionKey="id" size="5"
              value="${propertyInstance?.tags*.id}" class="many-to-many" class="form-control"/>
</div>
</div>
</div>
</div>
<br>
<br>
<br>

<div class="well">
<div class="fieldcontain ${hasErrors(bean: propertyInstance, field: 'type', 'error')} required">
 <div class="form-group" >
    <label for="type" class="control-label col-sm-5">
        <g:message code="property.type.label" default="Type"/>
        <span class="required-indicator">*</span>
    </label>
    <div class="col-sm-7" align="left">
    <g:select id="type" name="type.id" from="${com.mbwanajm.darproperty.Type.list()}" optionKey="id" required=""
              value="${propertyInstance?.type?.id}" class="many-to-one" class="form-control"/>
</div>
</div>
</div>
</div>

