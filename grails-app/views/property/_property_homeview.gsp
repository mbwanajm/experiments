<%@ page import="java.text.NumberFormat" %>

%{--new--}%
<div class="single-product">
    <div class="product-f-image thumb certi">
        <g:each var="photo" in="${property.photos}">
            <g:if test="${photo.name.contentEquals('main')}">

                <!-- 180 -->
                <img src="<g:createLink controller='image' action='renderPropertyPhoto'
                                        id='${photo.id}'/>" alt="property photo" class="img-centered"/>

                <div class="product-hover">
                    <g:link controller="property" action="viewProperty"
                            params="${[id: property.id, agentId: property.agent.id]}" class="view-details-link">
                        <i class="fa fa-link"></i> <g:message code="application.seeDetails"></g:message>
                    </g:link>
                </div>
                <g:if test="${property.approved}">
                    <i class="txt text-uppercase" title="<g:message code="approved"/>"><span
                            class="img-rounded fa fa-check-circle"></span></i>
                </g:if>

                <% def Show = "thumb" %>

                <g:render template="../tool/autoValueCalculator"
                          model="[pid: property.id, stype: property.saleType, psize: property.size, regionLocated: property.region, areaLocated: property.area, show: Show, ptype: property.type, selectedCurrency: property.currency, price: property.price]"></g:render>

            </g:if>
        </g:each>

    </div>

    <h2 class="text-capitalize ">
        <g:link controller="property" action="viewProperty"
                params="${[id: property.id, agentId: property.agent.id]}"><dp:message code="${property.type.name}"
                                                                                      codePrefix="property.type"/>
            <dp:message code="${property.saleType.name}" codePrefix="property.saleType"/>
            ${property.area}</g:link>
    </h2>

    <div class="product-carousel-price">

        <g:if test="${property.price}">
            <%
                def num = property.price
                def pr = 0
                def rep = ""
                def hold = num / 1000000000
                if (hold < 1) {
                    hold = num / 1000000
                    if (hold >= 1) {
                        pr = num
                        rep = "M"
                    } else {
                        hold = num / 1000
                        if (hold >= 1) {
                            pr = num
                            rep = "K"
                        } else {
                            pr = num

                        }

                    }
                } else {
                    rep = "Bil"
                    pr = hold
                }


            %>

            ${property.currency == "USD" ? '$' : property.currency} ${NumberFormat.getInstance().format(pr)}

        </g:if><g:else>
            <g:message code="not.provided"></g:message>
        </g:else>

        <br>

    </div>
    <% Show = "mobo" %>

    <g:render template="../tool/autoValueCalculator"
              model="[pid: property.id, stype: property.saleType, psize: property.size, regionLocated: property.region, areaLocated: property.area, show: Show, ptype: property.type, selectedCurrency: property.currency, price: property.price]"></g:render>

</div>