<%@ page import="java.text.SimpleDateFormat; com.mbwanajm.darproperty.Country; com.mbwanajm.darproperty.Tag; com.mbwanajm.darproperty.Region; com.mbwanajm.darproperty.SaleType; com.mbwanajm.darproperty.Type" %>
<html>
<head>
    <title>${property.id ? 'Edit Property' : 'Add Property'}</title>
    <meta name="layout" content="main"/>
    <g:javascript src="darproperty/CountryLoader.js"/>
    <g:javascript src="darproperty/RegionLoader.js"/>
    <g:javascript src="darproperty/AddProperty.js"/>
    <style>
    input{
        font-size: 9pt;
        padding: 5px;
    }
    input[type="text"]{
        font-size: 9pt;
        padding: 5px;
    }
    input[type="tel"]{
        font-size: 9pt;
        padding: 5px;
    }
    input[type="number"]{
        font-size: 9pt;
        padding: 5px;
    }
    input[type="email"]{
        font-size: 9pt;
        padding: 5px;
    }
    select  option{
        font-size: 9pt;
        padding: 5px;
    }
    </style>
</head>

<body>
<div class="container">
    <g:render template="../pageTitle" model="[title: property.id ? 'Edit Property' : 'Add Property']"></g:render>
    <br>

    <br>

    <div class="row">
        <div class="col-sm-12">
            <g:if test="${flash.message}">
                <div class="alert alert-success">
                    ${flash.message}
                </div>
            </g:if>
            <g:hasErrors bean="${flash.property}">
                <div class="alert alert-error alert-block">
                    <g:renderErrors bean="${flash.property}" as="list"/>
                </div>
            </g:hasErrors>
            <g:hasErrors bean="${flash.photo}">
                <div class="alert alert-error alert-block">
                    <g:renderErrors bean="${flash.photo}" as="list"/>
                </div>
            </g:hasErrors>


            <g:if test="${property.id}">
                <br><br>
                <g:link class="btn btn-info" action="addProperty">create a different new property</g:link>
                <br><br>
            </g:if>
        </div>
    </div>
</div>


<div class="container">


    <div class='row text-capitalize'>
        <g:uploadForm class='form-horizontal' controller="property"
                      action="${property.id ? 'updateProperty' : 'createProperty'}">

            <div class='col-xs-6'>
                <g:hiddenField name="id" value="${property.id}"/>
                <br>

                <div class='control-group'><div class="row">

                    <label class='control-label col-sm-5 text-danger'>ref no/ name/ id:</label>

                    <div class='controls col-sm-7'>
                        <%  def sdf = new SimpleDateFormat("yyyy/MM/dd/hh/mm/ss")
                        def datetime = sdf.format(new Date()) %>
                        <g:textField name="name"
                                     value="${property.name?property.name:(agent?.username == 'fsbo' || agent.profile.id==1?"FSBO"+(datetime):"DRP"+(datetime))}" class="form-control" required="" style="height: 40px;"/>
                        <i><small class="text-muted">*You can change it above</small></i>
                    </div>
                </div>
                    <br>

                    <div class='control-group'><div class="row">
                        <label class='control-label col-sm-5 text-danger'>type:</label>

                        <div class='controls col-sm-7'><g:select name="type" id='property_type'
                                                                 from="${Type.findAll().collect { it.name }}"
                                                                 value="${property.type?.name}"
                                                                 class="form-control"/></div></div>
                    </div>
                    <br>

                    <div class='control-group'><div class="row">
                        <label class='control-label col-sm-5 text-danger'>Sale Type:</label>

                        <div class='controls col-sm-7'><g:select name="saleType"
                                                                 from="${SaleType.findAll().collect { it.name }}"
                                                                 value="${property.saleType?.name}"
                                                                 class="form-control"/></div></div>
                    </div>
                    <br>

                    <div class='control-group'><div class="row">
                        <label class='control-label col-sm-5 text-danger'>country:</label>

                        <div class='controls col-sm-7'><g:select
                                id='property_country'
                                name="country"
                                from="${Country.findAll().collect { it.name }}"
                                value="${property.country}" class="form-control"/>
                        </div></div>
                    </div>
                    <br>

                    <div class='control-group'><div class="row">
                        <label class='control-label col-sm-5 text-danger'>region:</label>

                        <div class='controls col-sm-7'><g:select
                                id='property_region'
                                name="region"
                                from="${Region.findAll().collect { it.name }}"
                                value="${property.region}" class="form-control"/>
                        </div></div>
                    </div>
                    <br>

                    <div class='control-group '><div class="row">
                        <label class='control-label col-sm-5 text-danger'>area:</label>

                        <div class='controls  col-sm-7'><g:select
                                id='property_areas'
                                name="area"
                                from="${Region.findByName("Dar es salaam").areas.sort()}"
                                value="${property.area}" class="form-control"/>
                        </div></div></div>
                </div>
                <br>

                <div class='control-group'><div class="row">
                    <label class='control-label col-sm-5 text-danger'>area description:</label>

                    <div class='controls col-sm-7'><g:textField name="location" value="${property.location}"
                                                                class="form-control" required=""/>
                        <i><small class="text-muted">*Location description <i class="fa fa-arrow-up"></i></small></i>
                    </div>

                </div>
                </div>
                <br>

                <div class='control-group'><div class="row">
                    <label class='control-label col-sm-5'>price:</label>

                    <div class='controls col-sm-7'><g:textField type="number" name="price" value="${property.price}"
                                                                class="form-control" required=""/>
                        <i><small class="text-muted">*Numeric only <i class="fa fa-arrow-up"></i></small></i>
                    </div></div>
                </div>
                <br>

                <div class='control-group'><div class="row">
                    <label class='control-label col-sm-5'>currency:</label>

                    <div class='controls col-sm-7'><g:select name="currency" from="${["USD", "TZS", "RWF", "KES"]}"
                                                             value="${property.currency}" class="form-control"/>
                    </div>
                </div>
                </div>
                <br>

                <div class='control-group'><div class="row">
                    <label class='control-label col-sm-5'>size(sqm):</label>

                    <div class='controls col-sm-7'><g:textField type="number" name="size" value="${property.size}"
                                                                class="form-control" required=""/>
                    </div>
                </div>
                </div>
                <br>

                <div class='control-group'>
                    <div class="row">
                        <label class="control-label property_number_of_rooms col-sm-5">number of rooms:</label>

                        <div class='controls property_number_of_rooms col-sm-7'><g:textField name="numberOfRooms"
                                                                                             id='property_number_of_rooms'
                                                                                             value="${property.numberOfRooms}"
                                                                                             class="form-control"
                                                                                             required=""/>
                        </div>
                    </div>
                </div>
                <br>

                <div class='control-group'>
                    <div class="row">
                        <label class="control-label field_title property_number_of_rooms col-sm-5">number of bathrooms:</label>

                        <div class='controls property_number_of_rooms col-sm-7'><g:textField name="numberOfBathRooms"
                                                                                             value="${property.numberOfBathRooms}"
                                                                                             class="form-control"
                                                                                             required=""/>
                        </div>
                    </div>
                </div>
                <br>

                <div class='control-group'>
                    <label class="control-label col-sm-5 ">
                        <span class="glyphicon glyphicon-ok"></span>
                        <span>if available</span>
                        :</label>

                    <div class="controls col-sm-7 hd1" style="border:1px groove #fff;">

                        <g:each in="${Tag.findAll()}" var="tag">
                            <div class=" col-sm-4">
                                <g:checkBox name="${tag.name}" value="${property.tags?.contains(tag) ? 'on' : null}"
                                            class="form-control"/>
                                &nbsp&nbsp&nbsp${tag.name}
                            </div>
                        </g:each>
                    </div>
                </div>
            </div>

            <div class='col-xs-6'>
                <div class='control-group'><div class="row">
                    <label class='control-label col-sm-5 text-danger'>description(english):</label>

                    <div class='controls col-sm-7'>
                        <g:textArea name="description"  rows="4" cols="5"
                                    value="${property?.isFsbo() ? property.fsboDetails.description : property?.description}"
                                    class="form-control" required=""/>
                    </div></div>
                </div>
                <br>

                <div class='control-group'><div class="row">
                    <label class='control-label col-sm-5  '>description(swahili):</label>

                    <div class='controls col-sm-7'>
                        <g:textArea name="descriptionSW" rows="4" cols="5" value="${property?.descriptionSW}"
                                    class="form-control" required=""/>
                    </div></div>
                </div>


                <g:if test="${agent?.username == 'fsbo'}">


                    <div class='control-group '>
                        <div class='control-label col-sm-5'>
                            <label>Owner's Name:</label>
                        </div>

                        <div class='controls col-sm-7'>

                            <g:textField name="fsboOwnersName" value="${property?.fsboDetails?.name}"
                                         class="form-control" required=""></g:textField>
                        </div>
                    </div>
                    <br>

                    <div class='control-group'>
                        <div class='control-label col-sm-5'>
                            <label>Phone(s):</label>
                        </div>

                        <div class='controls col-sm-7'>
                            <g:textField name="fsboPhone" value="${property?.fsboDetails?.phone}" class="form-control"
                                         required=""></g:textField>
                        </div>
                    </div>
                    <br>

                    <div class='control-group'>
                        <div class='control-label col-sm-5'>
                            <label>email:</label>
                        </div>

                        <div class='controls col-sm-7'>
                            <g:textField name="fsboEmail" value="${property?.fsboDetails?.email}" class="form-control" placeholder="classifieds.darproperty.net"
                                         required=""></g:textField>
                        </div>

                    </div>
                    <div class='control-group '>
                        <div class='control-label col-sm-5'>
                            <label class="hd3">Property is approved:</label>
                        </div>

                        <div class='controls col-sm-7'>
                            <input type="checkbox" name="approved" value="true"  class="control-box"/>
                            <i><small class="text-muted">*Mark it if property is approved/ meets standards <i class="fa fa-arrow-up"></i></small></i>
                        </div>
                    </div>
                </g:if>




                <div class='control-group'>
                    <div class="col-sm-5 offset"></div>

                    <div class="property_photos col-sm-7">
                        <fieldset>
                            <legend >Property Photos:</legend>
                            <g:if test="${property.id}">

                                <g:render template="/property/property_photo_editor"
                                          collection="${property.photos}" var="photo" model="[property: property]"/>

                            </g:if>
                            <g:else>
                                <div class='control-group'>
                                    <label class='control-label col-sm-5 text-danger'>main photo:</label>

                                    <div class='controls col-sm-7'><input name="photo" type="file"/></div>
                                </div>
                            </g:else>
                        </fieldset>
                    </div>
                </div>


                <div class='control-group'>
                    <div class="row">
                        <div class='controls col-sm-5 pull-right'>
                            <div id='property_extra_photos'></div>
                        </div>
                    </div>
                </div>
                <br>

                <div class="control-group">
                    <div class="row">
                        <div class='controls col-sm-5 pull-right'>
                            <span id='property_add_photo' class='btn btn-info'>add another photo</span>
                            <br>
                            <br>
                            <g:submitButton class="btn btn-success" name="save"/>
                            <br>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </g:uploadForm>

    </div>
</div>
<br>
<br>
</body>

</html>