<%@ page import="com.mbwanajm.darproperty.*" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'advert.label', default: 'Advert')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<div class="container">

    <div class="row">
        <div class="col-sm-5">

            <a href="#show-advert" class="skip" tabindex="-1"
               title="<g:message code="default.link.skip.label" default="Skip to content&hellip;"/>">
                <span class="glyphicon glyphicon-refresh"></span></a>

        </div>

    </div>

    <br>

    <div class="nav" role="navigation">
        <ul>
            <li class="btn btn-default"><g:link controller="config" action=""><span
                    class="glyphicon glyphicon-wrench"></span></g:link></li>

            <li class="btn btn-default"><span class="glyphicon glyphicon-th-list"></span>
                <g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]"/></g:link>
            </li>
            <li class="btn btn-default"><span class="glyphicon glyphicon-plus"></span>
                <g:link class="create" action="create"><g:message code="default.new.label"
                                                                  args="[entityName]"/></g:link></li>
        </ul>
    </div>

    <div id="show-advert" class="content scaffold-show" role="main">
        <h1><g:message code="default.show.label" args="[entityName]"/></h1>
        <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
        </g:if>
        <ol class="property-list advert">

            <g:if test="${advertInstance?.url}">
                <li class="fieldcontain">
                    <span id="url-label" class="property-label"><g:message code="advert.url.label"
                                                                           default="Url"/></span>

                    <span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${advertInstance}"
                                                                                           field="url"/></span>

                </li>
            </g:if>

            <g:if test="${advertInstance?.photo}">
                <li class="fieldcontain">
                    <span id="photo-label" class="property-label"><g:message code="advert.photo.label"
                                                                             default="Photo"/></span>

                </li>
            </g:if>

            <g:if test="${advertInstance?.caption}">
                <li class="fieldcontain">
                    <span id="caption-label" class="property-label"><g:message code="advert.caption.label"
                                                                               default="Caption"/></span>

                    <span class="property-value" aria-labelledby="caption-label"><g:fieldValue bean="${advertInstance}"
                                                                                               field="caption"/></span>

                </li>
            </g:if>

            <g:if test="${advertInstance?.description}">
                <li class="fieldcontain">
                    <span id="description-label" class="property-label"><g:message code="advert.description.label"
                                                                                   default="Description"/></span>

                    <span class="property-value" aria-labelledby="description-label"><g:fieldValue
                            bean="${advertInstance}" field="description"/></span>

                </li>
            </g:if>

            <g:if test="${advertInstance?.type}">
                <li class="fieldcontain">
                    <span id="type-label" class="property-label"><g:message code="advert.type.label"
                                                                            default="Type"/></span>

                    <span class="property-value" aria-labelledby="type-label"><g:link controller="advertType"
                                                                                      action="show"
                                                                                      id="${advertInstance?.type?.id}">${advertInstance?.type?.encodeAsHTML()}</g:link></span>

                </li>
            </g:if>

            <g:if test="${advertInstance?.photo}">
                <li class="fieldcontain hd2">
                    <span id="photo-label" class="photo-label"> <g:message code="advert.image.label"
                                                                               default="photo"/></span>

                    <img src="<g:createLink controller='image' action='renderAdvert'
                                            id='${advertInstance?.id}'/>" alt="Article Icon" />
                </li>
            </g:if>

        </ol>

        <div class="row">
            <div class="col-sm-12">
                <g:form>

                    <fieldset class="buttons">
                        <g:hiddenField name="id" value="${advertInstance?.id}"/>
                        <g:link class="btn btn-warning" action="edit" id="${advertInstance?.id}"><g:message
                                code="default.button.edit.label" default="Edit"/></g:link>
                        <g:actionSubmit class="btn btn-danger" action="delete"
                                        value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                        onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                    </fieldset>
                </g:form>
            </div>
        </div>
    </div>
</div>
<br>
</body>
</html>
