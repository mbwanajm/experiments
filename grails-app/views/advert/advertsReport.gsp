<%@ page import="java.util.concurrent.TimeUnit; java.text.ParseException; com.mbwanajm.darproperty.Advert;" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%

    def cal = new GregorianCalendar()
    cal.setTime(new Date()); // Now use today date.
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");//set date format
    String fulldate = sdf.format(cal.getTime());//set display as formatted
    int year = cal.get(Calendar.YEAR)
    long advertid = 0;
    int views = 0;
    int version = 0;
    String type = "";
    Date date;

    String dateInput1 = "";
    String dateInput2 = "";



    advertid = Long.parseLong(request.getParameter("advert"));
    def adverts = Advert.findById(advertid)

    views = adverts.views;
    version = adverts.version;
    date = adverts.date;

%>
<html>
<head>
    <title>ADVERTISING REPORT</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="layout" content="main">
    <title>DarProperty >> Adverts Report</title>
</head>

<body>
<div class="container">
    <!-- LOGO -->
    <div class="row">
        <div class="col-sm-12" align="center">
            <g:link controller="home" action="home">
                <img alt="Logo" src="${resource(dir: 'bootstrap/imgs/', file: 'logo.png')}"/>
            </g:link>
        </div>
    </div>
    <!-- LOGO -->

    <!-- BODY -->
    <div class="col-sm-12">
        <div class="panel panel-primary">
            <div class="panel-heading" align="center">
                <h2>ADVERTISING REPORT</h2>
            </div>

            <div class="panel-body">

                <div class="row">
                    <div class="col-sm-12" align="center">
                        <img class="img-polaroid"
                             src="<g:createLink controller='image' action='renderAdvertRepo' id='${advertid}'/>"
                             alt=""/>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <h5 align="center">STATISTICS</h5>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6" align="right">Advert Added on:</div>

                    <div class="col-sm-6" align="left">${date}</div>
                </div>

                <%
                    dateInput1 = "" + date;
                    long no_dates = 0
                    dateInput2 = fulldate;


                    SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");

                    try {
                        Date date1 = myFormat.parse(dateInput1);
                        Date date2 = myFormat.parse(dateInput2);
                        long diff = date2.getTime() - date1.getTime();
                        no_dates = (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
                    } catch (ParseException e) {
                        out.print(e);
                    }

                    long days = no_dates == 0 ? 1 : no_dates;
                    double people_per_day = (double) (views / (days));
                    double clicks_per_day = (double) (version / days);
                %>

                <div class="row">
                    <div class="col-sm-6" align="right">Days online:</div>

                    <div class="col-sm-6" align="left">${days} Day(s)</div>
                </div>

                <% type = adverts.type; %>

                <div class="row">
                    <div class="col-sm-6" align="right">Advert Type:</div>

                    <div class="col-sm-6" align="left">${type} advert</div>
                </div>

                <div class="row">
                    <div class="col-sm-6" align="right">Impressions:</div>

                    <div class="col-sm-6" align="left">${views} Times</div>
                </div>

                <div class="row">
                    <div class="col-sm-6" align="right">Clicks:</div>

                    <div class="col-sm-6" align="left">${version} Times</div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <h5 align="center">Rates</h5>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6" align="right">Rate of People see it per day:</div>

                    <div class="col-sm-6" align="left">${people_per_day}</div>
                </div>

                <div class="row">
                    <div class="col-sm-6" align="right">Rate Url clicks per day:</div>

                    <div class="col-sm-6" align="left">${clicks_per_day}</div>
                </div>
                <div class="row">
                    <div class="col-sm-12" align="center">
                        <button type="button" name="print" class="btn btn-primary hidden-print pull-right" title="Print" onClick="window.print()"> <i class="fa fa-print"></i> Print This Report</button>
                    </div>
                </div>

            </div>

            <div class="panel-footer ">
                <small>Copyright © <%=year %>. <a href="www.darproperty.co.tz" class="hidden-print">Darproperty</a></small>

            </div>
        </div>
    </div>
</div>

</body>
</html>
