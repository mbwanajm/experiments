<%@ page import="java.lang.Integer" %>
<%@ page import="com.mbwanajm.darproperty.Advert" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'advert.label', default: 'Advert')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="container" align="left">
    <div class="row">
        <div class="col-sm-5">

            <a href="#list-advert" class="skip" tabindex="-1"
               title="<g:message code="default.link.skip.label" default="Skip to content&hellip;"/>">
                <span class="glyphicon glyphicon-refresh"></span></a>

        </div>

    </div>

    <div class="nav" role="navigation">
        <ul>
            <li class="btn btn-default"><g:link controller="config" action=""><span
                    class="glyphicon glyphicon-wrench"></span></g:link></li>

            <li class="btn btn-default"><g:link class="create" action="create"><g:message code="default.new.label"
                                                                                          args="[entityName]"/></g:link></li>
        </ul>
    </div>

    <div id="list-advert" class="content scaffold-list" role="main">
        <h1><span class="glyphicon glyphicon-list"></span> <g:message code="default.list.label" args="[entityName]"/>
        </h1>
        <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
        </g:if>
        <table class="table">
            <thead>
            <tr>

                <g:sortableColumn property="url" title="${message(code: 'advert.url.label', default: 'Url')}"/>

                <g:sortableColumn property="caption"
                                  title="${message(code: 'advert.caption.label', default: 'Caption')}"/>

                <g:sortableColumn property="description"
                                  title="${message(code: 'advert.description.label', default: 'Description')}"/>

                <th class="hd2 hd1"><g:message code="advert.type.label" default="Type"/></th>

                <th class="hd2 hd1"><g:message code="advert.version.label" default="Version"/></th>

                <th class="hd2 hd1"><g:message code="advert.version.label" default="Views"/></th>

            </tr>
            </thead>
            <tbody>
            <g:each in="${advertInstanceList}" status="i" var="advertInstance">
                <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                    <td class="hd2"><g:link action="show"
                                            id="${advertInstance.id}">${fieldValue(bean: advertInstance, field: "url")}</g:link></td>

                    <td class="hd2">${fieldValue(bean: advertInstance, field: "caption")}</td>

                    <td class="hd2">${fieldValue(bean: advertInstance, field: "description")}</td>

                    <td class="hd2">${fieldValue(bean: advertInstance, field: "type")} = ${fieldValue(bean: advertInstance, field: "typeId")}</td>

                    <td class="hd2">${fieldValue(bean: advertInstance, field: "version")}</td>

                    <td class="hd2">${fieldValue(bean: advertInstance, field: "views")}</td>

                    <td class="hd2"><a href="../advert/advertsReport?advert=${advertInstance.id}">Print Report</a></td>

                </tr>
            </g:each>
            </tbody>
        </table>

        <div class="pagination">
            <g:paginate total="${advertInstanceTotal}"/>
        </div>
    </div>
</div>

<%

    %>

</body>
</html>
