<%@ page import="com.mbwanajm.darproperty.Advert" %>



<div class="container" align="center">
    <div class="panel panel-default">
        <div class="panel-header"></div>

        <div class="panel-body">
            <div class="row ${hasErrors(bean: advertInstance, field: 'url', 'error')} ">
                <div class="col-sm-3 hd2" align="right">
                    <label for="url">
                        <g:message code="advert.url.label" default="Url"/>

                    </label>
                </div>

                <div class="col-sm-9" align="left">
                    <g:field type="url" name="url" value="${advertInstance?.url}" class="form-control"/>
                </div>
            </div>

            <div class="row ${hasErrors(bean: advertInstance, field: 'photo', 'error')} required">
                <div class="col-sm-3 hd2" align="right">
                    <label for="photo">
                        <g:message code="advert.photo.label" default="Photo"/>
                        <span class="required-indicator">*</span>
                    </label>
                </div>

                <div class="col-sm-9" align="left">
                    <input type="file" id="photo" name="photo" class=""/>
                </div>
            </div>

            <div class="row ${hasErrors(bean: advertInstance, field: 'caption', 'error')} ">
                <div class="col-sm-3 hd2" align="right">
                    <label for="caption">
                        <g:message code="advert.caption.label" default="Caption"/>

                    </label>
                </div>

                <div class="col-sm-9" align="left">
                    <g:textField name="caption" value="${advertInstance?.caption}" class="form-control"/>
                </div>
            </div>

            <div class="row ${hasErrors(bean: advertInstance, field: 'description', 'error')} ">
                <div class="col-sm-3 hd2" align="right">
                    <label for="description">
                        <g:message code="advert.description.label" default="Description"/>

                    </label>
                </div>

                <div class="col-sm-9" align="left">
                    <g:textField name="description" value="${advertInstance?.description}" class="form-control"/>
                </div>
            </div>

            <div class="row ${hasErrors(bean: advertInstance, field: 'type', 'error')} required">
                <div class="col-sm-3 hd2" align="right">
                    <label for="type">
                        <g:message code="advert.type.label" default="Type"/>
                        <span class="required-indicator">*</span>
                    </label>
                </div>

                <div class="col-sm-9" align="left">
                    <g:select id="type" name="type.id" from="${com.mbwanajm.darproperty.AdvertType.list()}"
                              optionKey="id" required="" value="${advertInstance?.type?.id}" class="many-to-one"
                              class="form-control"/>
                </div>
            </div>

        </div>
    </div>
</div>
