<%--
  Created by IntelliJ IDEA.
  User: mbwana
  Date: 16/01/13
  Time: 16:06
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>DarProperty >> Agent Messages</title>
    <meta content="main" name='layout'>
</head>

<body id="top">

<g:render template="../pageTitle" model='[title: "$agent.profile.company"]'></g:render>
<g:render template="profile_menu" model='[activeTab: "Messages"]'></g:render>
<dp:ifCurrentlyLoggedIn id="${agent.id}">
    <div class="container ">
        <div class='row'>
   <div class='col-sm-12'>
    <g:if test="${flash.messageDeletionSuccess}">
        <div class='alert alert-success'>
            ${flash.messageDeletionSuccess}
        </div>
    </g:if>

    <g:if test="${messageCount == 0}">
        <div class='alert alert-error'>
            You currently don't have any messages in your inbox
        </div>
    </g:if>
    <g:else>
        <div class='alert alert-info'>

            You have ${messageCount} message(s) in your inbox
        </div>
    </g:else>

    </div>
</div>
    <br>

    <div class="table-autoflow-lg">
        <g:each in="${propertyWithMessages}" var='property'>
            <g:each in="${property.messages}" var='message'>
                <div class="row">
                    <div class='col-sm-4'>
                        <g:link controller="property" action="viewProperty"
                                params="${[id: property.id, agentId: property.agent.id]}">
                            <g:each var="photo" in="${property.photos}">
                                <g:if test="${photo.name.contentEquals('main')}">
                                    <img class='img-polaroid img-responsive'
                                         src="<g:createLink controller='image' action='renderPropertyPhoto'
                                                            id='${photo.id}'/>"/>
                                </g:if>
                            </g:each>
                        </g:link>
                    </div>

                    <div class='col-sm-8'>
                        <h6><g:dateFromNow date="${message.dateCreated}"></g:dateFromNow></h6>
                        <pre>
                            ${message.message}

                            my phone is ${message.phone}
                            my email is ${message.email}
                        </pre>
                        <br>
                        <br>
                        <g:link controller="agent" action="deleteMessage" id="${message.id}"
                                params="[propertyId: property.id]"
                                class='btn btn-danger'>Delete this message</g:link>
                    </div>
                </div>
<br>
            </g:each>
        </g:each>
    </div>

</dp:ifCurrentlyLoggedIn>
</div>

</body>
</html>