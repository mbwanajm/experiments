<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>DarProperty>>Agents Directory</title>
    <meta name="layout" content="main"/>
    <style type="text/css">
    .caption {
        border-radius: 0px;
        background: #E6E6FA;;
        background: -webkit-linear-gradient(right, #E6E6FA 10%, #FFFFFF 90%); /* Chrome10+,Safari5.1+*/
    }

    .container {

    }

    .media {
        padding: 12px;
    }

    .media img {
        /*box-shadow:0px 5px 5px #c0c0c0;*/
        border-radius: 0px;
        width: 150px;

    }

    .boxing {

        background-color: #FFF;

    }

    .boxing:hover {
        background-color: #E6E6FA;
        background: -webkit-linear-gradient(right, #E6E6FA 10%, #FFFFFF 90%); /* Chrome10+,Safari5.1+*/
    }

    .btn .btn-default {
        text-align: left;
    }
    </style>
</head>

<body>
<div class="container">

    <div class="row">
        <div class="col-sm-12">
            <h3 align="center">${g.message(code: 'agents.agentsDirectory')}</h3>
        </div>
    </div>

    <div class="margine2"></div>

    <div class="row">
        <div class="col-sm-12">
            <ul class="nav nav-tabs" role="tablist">

                <li role="presentation" class="active"><a data-toggle="tab" href="#grid" class="hd2"><span
                        class="glyphicon glyphicon-th-large"></span> <g:message code="agents.gridview"></g:message></a>
                </li>
                <li role="presentation"><a data-toggle="tab" href="#list" class="hd2"><span
                        class="glyphicon glyphicon-th-list"></span> <g:message code="agents.listview"></g:message></a>
                </li>
            </ul>
        </div>
        <br>

    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="tab-content">
                <div id="grid" class="tab-pane fade in active">
                    <g:render template="gridView" model="[agents: agents, agentsCount: agentsCount]"></g:render>
                </div>

                <div id="list" class="tab-pane fade">
                    <div class="row">
                        <g:render template="listView" model="[agents: agents, agentsCount: agentsCount]"></g:render>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>