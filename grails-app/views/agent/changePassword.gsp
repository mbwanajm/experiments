<html>
<head>
    <title>DarProperty: Agents Change Password</title>
    <meta name="layout" content="main"/>
</head>

<body>
<g:render template="../pageTitle" model='[title: "${agent.profile.company}"]'/>
<g:render template="/agent/profile_menu" model="[agent: agent, activeTab:'Change Password']"/>
<div class="container">
<div class="row">
    <div class="span12">
        <g:if test="${flash.message}">
            <div class="alert alert-error">
                ${flash.message}
            </div>
        </g:if>
 <dp:ifCurrentlyLoggedIn id="${agent.id}">
        <g:form class='form-horizontal' name="savePassword" controller="agent" action="savePassword">
        <div class="well">
            <div class='control-group '>
                <label class='control-label col-sm-5'>new password</label>

                <div class='controls col-sm-7'>
                    <g:passwordField name="password" class="form-control"/>
                </div>
            </div>
		</div>
		 <div class="well">
            <div class='control-group '>
                <label class='control-label col-sm-5'>repeat password</label>

                <div class='controls col-sm-7'>
                    <g:passwordField name="passwordRepeat" class="form-control"/>
                </div>
            </div>
	     </div>
	      <div class="well">
            <div class='control-group '>
            <div class="col-sm-5 offset"></div>
                <div class='controls col-sm-7'>
                    <g:submitButton class='btn btn-success' name="changePassword" value="change password"/>
                </div>
            </div>
           </div>
        </g:form>
</dp:ifCurrentlyLoggedIn>
    </div>
</div>
</div>
<br>
<br>
</body>

</html>
