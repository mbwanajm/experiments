<html>
<head>
    <title>Dar Property Agents</title>
    <meta name="layout" content="main"/>
</head>

<body>

<div class="main">
    <div class="accord_top">
        <div style="width: 950px; height: 24px; margin: auto" alt="About ">
            <h2>Agents</h2>
        </div>
    </div>

    <g:render template="/search/search_bar"/>


    <div class="body_resize">
        <div class="full">
            <p></p>

            <h3>About Agents</h3>

            <p>
                Agents in DarProperty can upload new property and manage the property they have previously listed
                if you are a real estate agent and wish to list your property on DarProperty please contact
                us at info@darproperty.net Currently this service is free

            <p>

            <p>If you have dar property account please
                <g:link controller="agent" action="authenticate"><strong>CLICK HERE</strong></g:link>
                to login and manage your property</p>

            <div class="clr"></div>
        </div>
    </div>

    <div class="clr"></div>

    <div class="body">

    </div>
</div>
</body>

</html>

