


<div class="container">
    <div class="row">
        <g:each var="agent" in="${agents}">
            <g:link action="profile" id="${agent.id}">
                <div class="col-sm-6 boxing">
                <div class="media">
                    <div class="media-left media-middle">

                        <g:if test="${!agent.profile.photo}">
                            <img class='img-polaroid img-responsive'
                                 src="${resource(dir: 'images/property/', file: 'agent.png')} "/>
                        </g:if>
                        <g:else>
                            <img class='img-polaroid ' src="../image/renderAgentPhoto?id=${agent.id}"/>
                        </g:else>

                    </div>

                    <div class="media-body">
                        <g:if test="${agent.profile.company.empty}">
                            <p><span class="hd2"><g:message
                                    code="agents.id"></g:message>:</span> ${agent.id}
                            </p>
                        </g:if>
                        <g:else>
                            <p><span class="hd2"><g:message
                                    code="agents.company"></g:message>:</span>${agent.profile.company}</p>
                        </g:else>
                        <p><span class="hd2"><g:message
                                code="agents.name"></g:message>:</span>${agent.profile.firstName} ${agent.profile.middleName} ${agent.profile.lastName}
                        </p>

                        <p><span class="hd2"><g:message
                                code="agents.email"></g:message>:</span>${agent.profile.email}</p>

                        <p><span class="hd2"><g:message
                                code="agents.phone"></g:message>:</span>${agent.profile.phone}</p>


                        <p><span class="hd2"><g:message code="agents.rate"></g:message>:</span> <g:render template="agentsrate" model='[ids: agent.id]'></g:render></p>

                    </div>
                </div>
                </div>
            </g:link>
        </g:each>

    </div>
</div>


<br>
  	
 