<%--
  Created by IntelliJ IDEA.
  User: DAR PROPERTY
  Date: 1/3/2017
  Time: 10:29 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>DarProperty>> Sign Up</title>
    <meta name="layout" content="main"/>
    <style>
    input {
        font-size: 9pt;
        padding: 5px;
    }

    input[type="text"] {
        font-size: 9pt;
        padding: 5px;
    }

    input[type="tel"] {
        font-size: 9pt;
        padding: 5px;
    }

    input[type="number"] {
        font-size: 9pt;
        padding: 5px;
    }

    input[type="email"] {
        font-size: 9pt;
        padding: 5px;
    }

    select option {
        font-size: 9pt;
        padding: 5px;
    }
    </style>
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h2 align="center">
                <g:message code="menu.signup"></g:message>
            </h2>
        </div>
    </div>
    <div class="row">
        <g:if test="${flash.message}">
            ${flash.message}
        </g:if>
        <g:if test="${flash.error}">
            ${flash.error}
        </g:if>
        <g:form action="agentSignup" method="post" autocomplete="off">

            <div class="col-md-6">

                <fieldset class="form">

                    <div class="fieldcontain  ">
                        <label for="email">
                            Email

                        </label>
                        <input name="email" value="${params.email}" id="email" class="form-control" type="email">
                    </div>

                    <div class="fieldcontain  required">
                        <label for="firstName">
                            First Name
                            <span class="required-indicator">*</span>
                        </label>
                        <input name="firstName" required="" class="form-control" value="${params.firstName}" id="firstName" type="text">
                    </div>


                    <div class="fieldcontain  required">
                        <label for="lastName">
                            Last Name
                            <span class="required-indicator">*</span>
                        </label>
                        <input name="lastName" required="" class="form-control" value="${params.lastName}" id="lastName" type="text">
                    </div>


                    <div class="fieldcontain  ">
                        <label for="company">
                            Company

                        </label>
                        <input name="company" class="form-control" value="${params.company}" id="company" type="text">
                    </div>

                    <div class="fieldcontain  required hidden">
                        <label for="mailing">
                            Mailing
                            <span class="required-indicator">*</span>
                        </label>
                        <input name="mailing" class="form-control" value="0" id="mailing" type="number">
                    </div>

                    <div class="fieldcontain  ">
                        <label for="phone">
                            Phone*

                        </label>
                        <input name="phone" value="${params.phone}" class="form-control" id="phone" type="text">
                    </div>

                </fieldset>



            </div>
            <div class="col-md-6">

                <div class="fieldcontain  ">
                    <label for="email">
                        Password

                    </label>
                    <input name="password" value="" id="password" placeholder="6 characters" min="6" class="form-control" type="password">
                </div>
                <div class="fieldcontain  ">
                    <label for="email">
                        Confirm Password

                    </label>
                    <input name="confirm" value="" id="confirm" min="6" class="form-control" type="password">
                </div>
                <fieldset class="buttons">
                    <br>
                    <input name="create" class="save btn btn-block" class="form-control" value="Create" id="create"
                           type="submit">
                </fieldset>
            </div>
        </g:form>
    </div>
</div>

</body>
</html>