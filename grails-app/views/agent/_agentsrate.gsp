

<%

    int sum = 0;
    def ag = com.mbwanajm.darproperty.Agent.findById(ids)
    def rt =  com.mbwanajm.darproperty.Agent_rates.findAllByAgent(ag)
    int counter =  com.mbwanajm.darproperty.Agent_rates.countByAgent(ag)
    int i = 0;
    while(i < counter){
        sum += rt.rate[i]
        i++;
    }
    int rate = counter > 0 ? (sum / counter): 0;
    out.print(rate == 0 ? " <span class='hd4'><span class='glyphicon glyphicon-star-empty'></span><span class='glyphicon glyphicon-star-empty'></span><span class='glyphicon glyphicon-star-empty'></span><span class='glyphicon glyphicon-star-empty'></span><span class='glyphicon glyphicon-star-empty'></span></span>" : "");
%>


<span class="hd-gold">

    <g:if test="${rate == 1}">

        <span class="glyphicon glyphicon-star"></span>
        <span class="glyphicon glyphicon-star-empty"></span>
        <span class="glyphicon glyphicon-star-empty"></span>
        <span class="glyphicon glyphicon-star-empty"></span>
        <span class="glyphicon glyphicon-star-empty"></span>
        %{--<i><small class="pull-right">(${rate})</small></i>--}%
    </g:if>
    <g:if test="${rate == 2}">

        <span class="glyphicon glyphicon-star"></span>
        <span class="glyphicon glyphicon-star"></span>
        <span class="glyphicon glyphicon-star-empty"></span>
        <span class="glyphicon glyphicon-star-empty"></span>
        <span class="glyphicon glyphicon-star-empty"></span>
        %{--<i><small class="pull-right">(${rate})</small></i>--}%
    </g:if>
    <g:if test="${rate == 3}">

        <span class="glyphicon glyphicon-star"></span>
        <span class="glyphicon glyphicon-star"></span>
        <span class="glyphicon glyphicon-star"></span>
        <span class="glyphicon glyphicon-star-empty"></span>
        <span class="glyphicon glyphicon-star-empty"></span>
        %{--<i><small class="pull-right">(${rate})</small></i>--}%
    </g:if>
    <g:if test="${rate == 4}">

        <span class="glyphicon glyphicon-star"></span>
        <span class="glyphicon glyphicon-star"></span>
        <span class="glyphicon glyphicon-star"></span>
        <span class="glyphicon glyphicon-star"></span>
        <span class="glyphicon glyphicon-star-empty"></span>
        %{--<i><small class="pull-right">(${rate})</small></i>--}%
    </g:if>
    <g:if test="${rate == 5}">

        <span class="glyphicon glyphicon-star"></span>
        <span class="glyphicon glyphicon-star"></span>
        <span class="glyphicon glyphicon-star"></span>
        <span class="glyphicon glyphicon-star"></span>
        <span class="glyphicon glyphicon-star"></span>
        %{--<i><small class="pull-right">(${rate})</small></i>--}%
    </g:if>

</span>
           