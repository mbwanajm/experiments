<html>
<head>
    <title>DarProperty >> Agents Properties</title>
    <meta name="layout" content="main"/>
</head>

<body>
<div class="container hidden-sm hidden-xs">
<h2 align="center">
<u> <g:link action="profile" id="${agent.id}">${agent.profile.firstName} ${agent.profile.middleName} ${agent.profile.lastName}</g:link></u></h2>

<dp:ifCurrentlyLoggedIn id="${agent?.id}">
    <g:render template="profile_menu" model="[activeTab: 'Property']"></g:render>

    <div class="row">
        <div class='col-md-12'>
            <g:link class="btn btn-default" controller="property" action="addProperty"
                    id="${agent.id}"><i class="fa fa-plus"></i> Add Property</g:link>

        </div>
    </div>
</dp:ifCurrentlyLoggedIn>

	<div class="row">
		<g:render template="/search/home_only_search"></g:render>
	</div>

<br>

<div class='row'>
    <g:render template="/search/property_result" collection="${propertys}" var="property"/>
</div>


<div class='row' align="center">
    <div class='col-sm-12  pagination pagination-centered'>
        <g:paginate controller="agent" action="viewProperty" total="${propertyCount}"
                    params="${[id: agent.id]}"/>
    </div>
</div>


</div>
<!-- MOBILE VIEW -->
	<div class="container hidden-lg hidden-md">
	
	
	
	
		<div class="row">
		
			<div class="col-md-12">
			
			<!-- PANEL -->

			<div class="row" align="center">
			
			<h2 align="center">
<u> <g:link action="profile" id="${agent.id}">${agent.profile.firstName} ${agent.profile.middleName} ${agent.profile.lastName}</g:link></u></h2>

			<br>
			<dp:ifCurrentlyLoggedIn id="${agent?.id}">
    <g:render template="profile_menu" model="[activeTab: 'Property']"></g:render>

    <div class="row">
        <div class='col-md-12'>
            <g:link class="btn btn-navbar" controller="property" action="addProperty"
                    id="${agent.id}">Add Property</g:link>

        </div>
    </div>
</dp:ifCurrentlyLoggedIn>
<br>
		
			</div>
			<div class="row">
			<g:render template="/search/property_result" collection="${propertys}" var="property"/>
			</div>

			<div class="row">
			 <div class='col-sm-12 pagination' align="center">
        <g:paginate controller="agent" action="viewProperty" total="${propertyCount}"
                    params="${[id: agent.id]}"/>
    </div>
			</div>
			<!--  -->		
			</div>
		
			

			
			<div class="col-xs-2 hidden">
			
				<!--SOCIAL-->		
			
				<div class="container fxn">
				<div class="row"><small class="hd4">You can:</small></div>
				<div class="row"><br></div>
				<div class="row">

				<div class="col-xs-2 ">
				<div class="fb-share-button" 
				data-href="http://www.darproperty.co.tz/showAllProperties"
				data-layout="button_count"></div>
				
				</div>
					<br>
				</div>
				<div class="row"><br></div>
				<div class="row">

				<div class="col-xs-2 ">
				<a href="https://twitter.com/share"
				 class="twitter-share-button"{count}
				  data-url="http://www.darproperty.co.tz/showAllProperties" 
				  data-text="See-now affordable & Latest Properties in Darproperty." 
				  data-via="thedarproperty" 
				  data-related="thedarproperty" 
				  data-hashtags="DarpropertyLatests"
				  title="Share On Twitter">Tweet</a>
				 </div>
				  
				</div>
				<div class="row"><br></div>
				<div class="row  hidden-lg hidden-md">

				<div class="col-xs-2  hidden-md hidden-lg">
				<a href="whatsapp://send?text=http://www.darproperty.co.tz/property/showAllProperties"
				 target="_blank" >
				<img src="${resource(dir: 'bootstrap/imgs/', file: 'whatsappgreen.png')}" 
				 alt="Whatsapp" class="" 
				 width="30"
				  height="30" 
				  title="Whatsapp"  />
				</a>
				</div>

				</div>
				</div>
				
				<!--SOCIAL-->
			
			</div>
			<div class="col-xs-1 offset"></div>
		
		</div>
	
	</div>
<br><br>
</body>

</html>