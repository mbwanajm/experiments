<html>
<head>
    <title>DarProperty: Edit Profile</title>
    <meta name="layout" content="main"/>
    <style>
    input{
        font-size: 9pt;
        padding: 5px;
    }
    input[type="text"]{
        font-size: 9pt;
        padding: 5px;
    }
    input[type="tel"]{
        font-size: 9pt;
        padding: 5px;
    }
    input[type="number"]{
        font-size: 9pt;
        padding: 5px;
    }
    input[type="email"]{
        font-size: 9pt;
        padding: 5px;
    }
    select  option{
        font-size: 9pt;
        padding: 5px;
    }
    </style>
</head>

<body>
<g:render template="../pageTitle" model='[title: "${agent.profile.company}"]'/>
<g:render template="/agent/profile_menu" model="[agent: agent, activeTab:'Edit Profile']"/>

<div class="center">
<div class="row">

    <div class="span12">
        <g:hasErrors>
            <div class="alert alert-block alert-error">
                <g:renderErrors bean="${apc}" as="list"/>
            </div>
        </g:hasErrors>

 <dp:ifCurrentlyLoggedIn id="${agent.id}">
 
 

 
 
        <g:uploadForm class='form-horizontal' name="editProfile" contoller="agent" action="saveProfile">
            <g:hiddenField name="id" value="${agent.id}"/>

<div class="well">
             <div class='control-group'>
                <label class='control-label col-sm-6'>first name</label>

                <div class='controls col-sm-6'><g:textField name="firstName" value="${agent.profile.firstName}" class="form-control"/></div>
            </div>
</div>
 <div class="well">
            <div class='control-group'>
                <label class='control-label col-sm-6'>middle name</label>

                <div class='controls col-sm-6'><g:textField name="middleName" value="${agent.profile.middleName}" class="form-control"/></div>
            </div>
</div>
 <div class="well">
            <div class='control-group'>
                <label class='control-label col-sm-6'>last name</label>

                <div class='controls col-sm-6'><g:textField name="lastName" value="${agent.profile.lastName}" class="form-control"/></div>
            </div>
</div>
 <div class="well">
            <div class='control-group'>
                <label class='control-label col-sm-6'>company</label>

                <div class='controls col-sm-6'><g:textField name="company" value="${agent.profile.company}" class="form-control"/></div>
            </div>
</div>
 <div class="well">
            <div class='control-group'>
                <label class='control-label col-sm-6'>phone</label>

                <div class='controls col-sm-6'><g:textField name="phone" value="${agent.profile.phone}" class="form-control"/></div>
            </div>
</div>
 <div class="well">
            <div class='control-group'>
                <label class='control-label col-sm-6'>email</label>

                <div class='controls col-sm-6'><g:textField name="email" value="${agent.profile.email}" class="form-control"/></div>
            </div>
</div>
 <div class="well">
            <div class='control-group'>
                <label class='control-label col-sm-6'>photo</label>

                <div class='controls col-sm-6'><input type="file" name="photo"/></div>
            </div>
</div>
 <div class="well">
            <div class='control-group'>
                <div class='controls col-sm-6 pull-right'>
                    <g:submitButton class='btn btn-success' name="save" value="save"/>
                </div>
            </div>
</div>
        </g:uploadForm>
 %{--<div class="well"> --}%
 %{----}%
 %{--<%--}%
 %{--try{--}%
 %{--out.print(manage.mail_action(agentid));--}%
 %{----}%
 %{----}%
 %{--}catch(Exception e){--}%
 %{--out.print(e);--}%
 %{--}--}%
 %{----}%
 %{----}%
 %{--%>--}%
 %{----}%
 %{--</div>--}%
</dp:ifCurrentlyLoggedIn>

    </div>
</div>
</div>
<br>
<br>
</body>

</html>