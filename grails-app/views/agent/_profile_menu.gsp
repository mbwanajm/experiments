<br>
<%
    def details = [
            [controller: 'agent', action: 'profile', text: g.message(code:'agents.summary'), secured: false],
            [controller: 'agent', action: 'viewProperty', text: g.message(code:'agents.property'), secured: false],
            [controller: 'agent', action: 'showMessages', text: 'Messages', secured: true],
            [controller: 'agent', action: 'editProfile', text: 'Edit Profile', secured: true],
            [controller: 'agent', action: 'changePassword', text: 'Change Password', secured: true],
    ]
%>

<div class='row'>
    <div class='col-md-12'>
        <ul class="nav nav-tabs">
            <g:each var='detail' in="${details}">
                <g:if test="${detail.secured}">
                    <sec:ifAllGranted roles="ROLE_AGENT">
                        ${activeTab == detail.text ? '<li class="active">' : '<li>'}
                        <g:link class="tab"
                                controller="${detail.controller}"
                                action="${detail.action}"
                                id="${agent.id}">
                            ${detail.text}
                        </g:link>
                    </sec:ifAllGranted>
                </g:if>
                <g:else>
                    ${activeTab == detail.text ? '<li class="active">' : '<li>'}
                    <g:link class="tab"
                            controller="${detail.controller}"
                            action="${detail.action}"
                            id="${agent.id}">
                        ${detail.text}
                    </g:link>
                </g:else>
                </li>

            </g:each>
        </ul>
    </div>
</div>
<br>

