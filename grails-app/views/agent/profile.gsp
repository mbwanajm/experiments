<%@ page import="com.mbwanajm.darproperty.Agent_rates; com.mbwanajm.darproperty.Agent" %>
<html>
<head>
    <title>Agents Profile</title>
    <meta name="layout" content="main"/>
    <style type="text/css">
    .profpic {
        border-radius: 5px;
        /*box-shadow:0px 5px 5px #888888;*/
    }

    .profpic img {
        width: 30%;
    }
    </style>
</head>

<body>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3 align="center"><g:if
                    test="${!agent.profile.company.isEmpty()}">${agent.profile.company.toUpperCase()}</g:if></h3>
        </div>
    </div>
    <g:render template="/agent/profile_menu" model="[agent: agent, activeTab: g.message(code: 'agents.summary')]"/>

    <div class="row">
        <div class="col-md-12">
            <g:if test="${flash.message}">
                <div class="alert alert-success">
                    ${flash.message}
                </div>
            </g:if>
        </div>
    </div>

    <div class="row ">

        <div class="col-md-4 ">
            <g:if test="${agent.profile.photo}">
                <img class="img-responsive profpic hidden-xs hidden-sm"
                     src="<g:createLink controller='image' action='renderAgentPhoto' id='${agent.id}'/>"
                     alt="${agent.profile.firstName}"/>
                <img class="img-responsive hidden-lg hidden-md" width="100"
                     src="<g:createLink controller='image' action='renderAgentPhoto' id='${agent.id}'/>"
                     alt="${agent.profile.firstName}"/>
            </g:if><g:else>
            <img class="img-responsive"
                 src="${resource(dir:"images/property",file: "agent.png" )}"
                 alt="${agent.profile.firstName}"/>
<br>
            <sec:ifLoggedIn>
             <i class="text-danger">Please upload your company logo [edit your profile now]</i>
            </sec:ifLoggedIn>
            </g:else>

        </div>

        <div class="col-md-6">

            <div class='outlined  propbody' align="center">

                <p class="text-capitalize"><span class="hd2 text-capitalize"><g:message
                        code="agents.name"></g:message>:</span> ${agent.profile.firstName} ${agent.profile.middleName} ${agent.profile.lastName}
                </p>

                <p class="text-capitalize"><span class="hd2 text-capitalize"><g:message
                        code="agents.company"></g:message>:</span> ${agent.profile.company}</p>

                <p class="text-capitalize"><span class="hd2 text-capitalize"><g:message
                        code="agents.phone"></g:message>:</span> ${agent.profile.phone}</p>

                <p class="text-capitalize"><span class="hd2 text-capitalize"><g:message
                        code="agents.email"></g:message>:</span> ${agent.profile.email}</p>

                <p class="text-capitalize"><span class="hd2 text-capitalize"><g:message
                        code="agents.rate"></g:message>:</span> <g:render template="agentsrate"
                                                                          model='[ids: agent.id]'></g:render>
                </p>
                <sec:ifNotLoggedIn>

                    <p align="right">

                        <g:link controller="agent" action="showAllAgents"><small><g:message
                                code="agents.all"></g:message></small></g:link>

                    </p>

                    <p>

                        <button type="submit" class="btn btn-default hd4" name="vote" data-toggle="modal"
                                data-target=".rating"><span
                                class="glyphicon glyphicon-star"></span> <g:message
                                code="agents.vote.rate"></g:message></button>
                    </p>
                </sec:ifNotLoggedIn>
            %{--<g:render template="rateAgent" model="[ids: agent.id]"></g:render>--}%

            </div>
        </div>

    </div>

</div>

%{--Rating--}%
<div class="modal fade rating" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header panel-warning">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center text-capitalize">RATE THIS AGENT</h4>
            </div>

            <div class="modal-body">

                <div class="row">
                    <div class="col-lg-12">
                        %{--FORM--}%
                        <%

                            def ip = request.getHeader("X-Forwarded-For");
                            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                                ip = request.getHeader("Proxy-Client-IP");
                            }
                            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                                ip = request.getHeader("WL-Proxy-Client-IP");
                            }
                            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                                ip = request.getHeader("HTTP_CLIENT_IP");
                            }
                            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                                ip = request.getHeader("HTTP_X_FORWARDED_FOR");
                            }
                            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                                ip = request.getRemoteAddr();
                            }
                        %>


                        <g:form controller="agent_rates"  action="rateAgent" method="post" class="form-inline">
                            <g:hiddenField id="agent" name="agent.id" value="${agent.id}"/>
                            <g:hiddenField name="ip" required="" value="${ip}"/>
                            <div class="hd-gold">

                                <div class="">
                                    <div class="form-group">
                                        <input type="radio" value="1" name="rate"><span
                                            class="glyphicon glyphicon-star"></span><span
                                            class="glyphicon glyphicon-star-empty"></span><span
                                            class="glyphicon glyphicon-star-empty"></span><span
                                            class="glyphicon glyphicon-star-empty"></span><span
                                            class="glyphicon glyphicon-star-empty"></span>
                                    </div>

                                    <div class="form-group">
                                        <input type="radio" value="2" name="rate"><span
                                            class="glyphicon glyphicon-star"></span><span
                                            class="glyphicon glyphicon-star"></span><span
                                            class="glyphicon glyphicon-star-empty"></span><span
                                            class="glyphicon glyphicon-star-empty"></span><span
                                            class="glyphicon glyphicon-star-empty"></span>
                                    </div>

                                    <div class="form-group">

                                        <input type="radio" value="3" name="rate"><span
                                            class="glyphicon glyphicon-star"></span><span
                                            class="glyphicon glyphicon-star"></span><span
                                            class="glyphicon glyphicon-star"></span><span
                                            class="glyphicon glyphicon-star-empty"></span><span
                                            class="glyphicon glyphicon-star-empty"></span>
                                    </div>

                                    <div class="form-group">
                                        <input type="radio" value="4" name="rate"><span
                                            class="glyphicon glyphicon-star"></span><span
                                            class="glyphicon glyphicon-star"></span><span
                                            class="glyphicon glyphicon-star"></span><span
                                            class="glyphicon glyphicon-star"></span><span
                                            class="glyphicon glyphicon-star-empty"></span>
                                    </div>

                                    <div class="form-group">

                                        <input type="radio" value="5" name="rate"><span
                                            class="glyphicon glyphicon-star"></span><span
                                            class="glyphicon glyphicon-star"></span><span
                                            class="glyphicon glyphicon-star"></span><span
                                            class="glyphicon glyphicon-star"></span><span
                                            class="glyphicon glyphicon-star"></span>
                                    </div>

                                    <div class="form-group">

                                        <g:submitButton name="create" class="save" value="${message(code: 'agents.vote.rate', default: 'Rate')}" />

                                    </div>

                                </div>
                            </div>





                        </g:form>

                        %{--FORM--}%
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="margine"></div>
</body>

</html>
