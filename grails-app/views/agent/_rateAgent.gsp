<!-- out.print( request.getRemoteAddr()+"<br>" );
out.print( request.getRemoteHost()+"<br>" );
out.print("VIA"); -->
<%

    String ip = request.getHeader("X-Forwarded-For");
    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
        ip = request.getHeader("Proxy-Client-IP");
    }
    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
        ip = request.getHeader("WL-Proxy-Client-IP");
    }
    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
        ip = request.getHeader("HTTP_CLIENT_IP");
    }
    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
        ip = request.getHeader("HTTP_X_FORWARDED_FOR");
    }
    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
        ip = request.getRemoteAddr();
    }
%>

<sec:ifNotLoggedIn>

    <g:if test="${request.getParameter("vote") != null}">
        <fieldset class="hidden-phone hidden-tablet">
            <u class="hd4"><g:message code="agents.rateagent"></g:message></u>
            <g:form controller="agent"   action="rateAgent" class="form-inline">
                <input type="hidden" name="id" value="${ids}">
                <input type="hidden" name="ip" value="${ip}">

                <div class="form-group col-xs-3 ">

                    <div class="container hd4">
                        <div class="row">
                            <div class="col-xs-12 ">
                                <input type="radio" value="1" name="rate"><span
                                    class="glyphicon glyphicon-star"></span><span
                                    class="glyphicon glyphicon-star-empty"></span><span
                                    class="glyphicon glyphicon-star-empty"></span><span
                                    class="glyphicon glyphicon-star-empty"></span><span
                                    class="glyphicon glyphicon-star-empty"></span>
                                <input type="radio" value="2" name="rate"><span
                                    class="glyphicon glyphicon-star"></span><span
                                    class="glyphicon glyphicon-star"></span><span
                                    class="glyphicon glyphicon-star-empty"></span><span
                                    class="glyphicon glyphicon-star-empty"></span><span
                                    class="glyphicon glyphicon-star-empty"></span>
                                <input type="radio" value="3" name="rate"><span
                                    class="glyphicon glyphicon-star"></span><span
                                    class="glyphicon glyphicon-star"></span><span
                                    class="glyphicon glyphicon-star"></span><span
                                    class="glyphicon glyphicon-star-empty"></span><span
                                    class="glyphicon glyphicon-star-empty"></span>
                                <input type="radio" value="4" name="rate"><span
                                    class="glyphicon glyphicon-star"></span><span
                                    class="glyphicon glyphicon-star"></span><span
                                    class="glyphicon glyphicon-star"></span><span
                                    class="glyphicon glyphicon-star"></span><span
                                    class="glyphicon glyphicon-star-empty"></span>
                                <input type="radio" value="5" name="rate"><span
                                    class="glyphicon glyphicon-star"></span><span
                                    class="glyphicon glyphicon-star"></span><span
                                    class="glyphicon glyphicon-star"></span><span
                                    class="glyphicon glyphicon-star"></span><span
                                    class="glyphicon glyphicon-star"></span>
                                <button type="submit" class="btn btn-default hd4" name="rating"><span
                                        class="glyphicon glyphicon-star"></span> <g:message
                                        code="agents.vote.rate"></g:message></button>
                            </div>
                        </div>
                    </div>
                </div>

            </g:form>
        </fieldset>
        <fieldset class="hidden-desktop">
            <u class="hd4"><g:message code="agents.rateagent"></g:message></u>
            <g:form controller="agent" action="rateAgent" class="form-inline">
                <input type="hidden" name="id" value="${ids}">
                <input type="hidden" name="ip" value="${ip}">

                <div class="form-group col-xs-3 ">

                    <div class="container hd4">
                        <div class="row">
                            <div class="col-xs-12 ">
                                <select name='rate'>
                                    <option value='1'>1</option>
                                    <option value='2'>2</option>
                                    <option value='3'>3</option>
                                    <option value='4'>4</option>
                                    <option value='5'>5</option>
                                </select>
                                <button type="submit" class="btn btn-default hd4" name="rating"><span
                                        class="glyphicon glyphicon-star"></span> <g:message
                                        code="agents.vote.rate"></g:message></button>
                            </div>
                        </div>
                    </div>
                </div>

            </g:form>
        </fieldset>
    </g:if><g:else>
    <form action="" method="post">
        <button type="submit" class="btn btn-default hd4" name="vote"><span
                class="glyphicon glyphicon-star"></span> <g:message code="agents.vote"></g:message></button>
    </form>
</g:else>

</sec:ifNotLoggedIn>