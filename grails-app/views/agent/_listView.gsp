
<script type="text/javascript">

    var ip = $get("uip").innerHTML;

</script>

<div class="body">

    <div class="container">
        <div class="row">
            <table class="table">
                <thead>
                <tr class="hd4">
                    <th><g:message code="agents.company"></g:message></th>
                    <th><g:message code="agents.name"></g:message></th>
                    <th><g:message code="agents.phone"></g:message></th>
                    <th><g:message code="agents.email"></g:message></th>
                    <th><g:message code="agents.ratings"></g:message></th>
                </tr>

                </thead>
                <tbody>
                <g:each var="agent" in="${agents}">

                    <tr class="hd5">
                    <td>
                    <g:link action="profile" id="${agent.id}">
                        <button class="btn btn-default" title="View Profile"><span
                                class="fa fa-user"></span></button>
                    </g:link>
                    <a href="profile/${agent.id}">
                    <g:if test="${agent.profile.company.empty}">
                        ${agent.id}
                    </g:if>
                    <g:else>
                        ${agent.profile.company}
                    </g:else>
                    </a>
                    </td>
                    <td>
                        ${agent.profile.firstName} ${agent.profile.middleName} ${agent.profile.lastName}
                    </td>
                    <td>
                        ${agent.profile.phone}
                    </td>
                    <td>
                        ${agent.profile.email}
                    </td>
                    <td style="font-size: 5pt;" align="center">
                        <g:render template="agentsrate" model='[ids: agent.id]'></g:render>
                    </td>

                </g:each>
                <td class="row pagination ">
                    <g:paginate total="${agentsCount}"></g:paginate>
                </td>
                </tbody>
            </table>
        </div>
    </div>
</div>


<br>
  	
 