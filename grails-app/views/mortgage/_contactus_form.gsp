<div class="row">
    <g:form controller="contacts" action="save" calss="form form-horizontal">
        <g:hiddenField name="page" value="mortgage"></g:hiddenField>
        <div class='control-group' >
            <label for="name" class='control-label'><g:message code="property.enquiry.name" /></label>

            <div class='controls'>
                <g:textField name="name" id="formdt" autocomplete="off"></g:textField>
            </div>
        </div>
        <div class='control-group' >
            <label for="email" class='control-label'><g:message code="property.enquiry.email" /></label>

            <div class='controls'>
                <g:textField name="email" id="formdt" autocomplete="off"></g:textField>
            </div>
        </div>

        <div class='control-group'>
            <label class='control-label'><g:message code="property.enquiry.phone"/></label>

            <div class='controls'>
                <g:textField name="phone" id="formdt" autocomplete="off"></g:textField>
            </div>
        </div>

        <div class='control-group'>
            <label class='control-label'><g:message code="property.enquiry.message"/></label>

            <div class='controls'>
                <g:textArea name="message"  cols="50" rows="10" ></g:textArea>
            </div>
        </div>


        <div class='control-group'>
            <div class='controls'>
                <br>
                <g:submitButton name="${g.message(code: 'property.enquiry.sendMessage')}"
                                class="btn btn-success"></g:submitButton>
            </div>
        </div>
    </g:form>
</div>
