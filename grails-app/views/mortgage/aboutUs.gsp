<%--
  Created by IntelliJ IDEA.
  User: DAR PROPERTY
  Date: 3/31/2016
  Time: 10:55 AM
--%>

<html>
<head>
    <title>Dar Property>>About Us</title>
    <meta name="layout" content="mortgage"/>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <g:javascript src="lib/gmaps.js"/>
    <style>
    #map {
        width: 500px;
        height: 400px;
        background-color: #CCC;
    }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script>

        var map;
        $(document).ready(function () {
            map = new GMaps({
                el: '#map',

                lat: -6.752480,
                lng: 39.243173
            });
            map.addMarker({
                lat: -6.752480,
                lng: 39.243173,
                title: 'DarProperty HQ',
                details: {
                    database_id: 42,
                    author: 'HPNeo'
                },
                click: function (e) {
                    if (console.log)
                        console.log(e);
                    alert('You clicked in this marker');
                },
                mouseover: function (e) {
                    if (console.log)
                        console.log(e);
                }
            });
            map.addMarker({
                lat: -6.752480,
                lng: 39.243173,
                title: 'DarProperty HQ',
                infoWindow: {
                    content: '<p>0.5Km From Business Bus Stand (Mikocheni B) <br>Msuya Street Block no. 29.</p>'
                }
            });
        });
    </script>
</head>

<body>

<div class="container">
    <div class="row">
        <h2 align="center"><u><g:message code="about.us"/></u></h2>
    </div>

    <div class="row">

        <div class="col-sm-2 off"></div>

        <div class="col-sm-8">
            <g:message code="aboutUs.intro"/>
        </div>

        <div class="col-sm-2 offset"></div>
    </div>


    <div class="row">

        <div class="col-sm-2 offset"></div>

        <div class="col-sm-4">
            <h2><g:message code="aboutUs.missionTitle"/></h2>
            <g:message code="aboutUs.missionContent"/>
        </div>

        <div class="col-sm-4">
            <h2><g:message code="aboutUs.visionTitle"/></h2>
            <g:message code="aboutUs.visionContent"/>
        </div>

        <div class="col-sm-2 offset"></div>

    </div>


    <div class="row">
        <div class="col-sm-2 offset"></div>

        <div class="col-sm-4">
            <h2><g:message code="about.location"/></h2>

            <p><g:message code="aboutUs.location"/></p>

            <div id="map"></div>

        </div>

        <div class="col-sm-2 offset"></div>

        <div class="col-sm-4">

            <h2><g:message code='contactDetails'/></h2>

            <address>
                <strong>DarProperty</strong><br>
                Mikocheni<br>
                P.O BOX 105499<br>
                Dar-es-salaam<br>
                Tanzania<br>
            </address>

            <p>

            <p><strong>Email</strong>: mortgage@darproperty.net<br>
                <strong>Call Us</strong>: +255 756 012 987 / +255 713751868 <br>
            <g:link controller="mortgage" action="contactus" class="btn btn-primary"><span
                    class="glyphicon glyphicon-envelope"></span> CONTACT US</g:link><br>
                <br><strong>You may find us on:</strong><br>
                <br><a href="http://www.facebook.com/pages/DarProperty/114594825280789" target="_blank"><img
                    src="${resource(dir: 'bootstrap/imgs/', file: 'fb.png')}" alt="Facebook" class="img-circle"
                    width="30" height="30" title="Facebook"></a> <i>DarProperty</i>
                <br><a href="https://twitter.com/thedarproperty" target="_blank"><img
                    src="${resource(dir: 'bootstrap/imgs/', file: 'twt.png')}" alt="Twitter" class="img-circle"
                    width="30" height="30" title="Twitter"></a> <i>thedarproperty</i>
                <br><a href="https://www.instagram.com/darproperty/" target="_blank"><img
                    src="${resource(dir: 'bootstrap/imgs/', file: 'inst.png')}" alt="Instagram" class="img-circle"
                    width="30" height="30" title="Instagram"></a> <i>darproperty</i>
                <br><a href="https://www.skype.com" target="_blank"><img
                    src="${resource(dir: 'bootstrap/imgs/', file: 'skype.png')}" alt="Instagram" class="img-circle"
                    width="30" height="30" title="Skype"></a> <i>thedarproperty</i>
                <!-- <br><span class="hidden-desktop"><a href="whatsapp://send?text= Visit now The East Africa's Property Guide http://www.darproperty.co.tz" target="_blank" > <img src="${resource(dir: 'bootstrap/imgs/', file: 'whatsapp.png')}"  alt="Whatsapp" class="" width="30" height="30" title="Whatsapp"  /></a> <i>+255 713751868</i></span>
		 -->

        </div>
    </div>

</div>


<br>
<br>
</body>

</html>
