<%@ page import="com.mbwanajm.darproperty.Article;" %>
<html>
<head>

    <title>DarProperty Mortgage</title>
    <meta name="layout" content="mortgage"/>
</head>

<body>
<!-- BODY -->

<div class="container bodylogo">
    <div class="row">
        <div class="col-sm-12">
            <img src="${resource(dir: 'mortgage/img/', file: '15.jpg')}" alt=" Mortgage Photo"/>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <h3>TODAY'S TIP</h3>

        </div>

        <div class="col-sm-9">
            <h3>WELCOME TO DARPROPERTY MORTGAGE</h3>

        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <%
                    //                    MortgageService mservice = new MortgageService();
//                String article = mservice.view_font_article();
//                String sql = "";
//                String photo = mservice.view_article_image(sql);

                    def article = com.mbwanajm.darproperty.Article.list([sort: 'id', order: 'desc'])[0]
                %>
                <div class="thumbnail">
                    <g:if test="${article.image}">
                        <img src='/image/renderArticlePhoto/${article.id}' width='100'
                             class='img-responsive pull-left article-image'/>
                    </g:if>
                    <g:else>
                        <img src='/img/article.png' width='100' class='img-responsive pull-left article-image'/>
                    </g:else>

                %{--<%=photo%>--}%
                    <div class="caption">

                        <p class="text-capitalize">
                            ${article.title}...<br>
                            <a href='mortgageArticles?read=${article.id}' class="btn btn-dafault pull-right">
                                <g:message code="home.newsAndArticles.readmore"/></a>

                        </p>


                    </div>
                </div>

            </div>

            <div class="col-sm-9 bodytxt">
                <!-- EDIT THESE PARAGRAPHS -->
                <p>
                    Darproperty mortgage is a mortgage brokerage service provide by Darproperty Tanzania,
                    this service gives an applicant a wide and proper knowledge about the mortgage finance in Tanzania through consultation services provided by the company.
                    Darproperty mortgage has a goal to help people get the best rates and terms they can find on home loans and to provide them with the information they need
                    to become well-informed mortgage customers.
                </p>

                <p>
                    Darproperty mortgage established to reduce the gap between lenders and applicants,
                    the current situation shows that real estate is the fastest growing sector in
                    Tanzania but financing it necessitate a proper knowledge. Number of people who
                    apply for loan is greater compared to the number of people who qualify for it, approximately 3
                    percent of people in Tanzania qualify for the mortgage. With experience in real estate market,
                    Darproperty Tanzania see the importance of establishing mortgage assistance to applicants to reduce mortgage application challenges.

                </p>

                <p>For that reason, Darproperty mortgage makes it easy and fast to compare rate offers
                from our network of mortgage lenders, so you can make the best financial decisions for yourself and your family.
                With the support of TMRC and increasing numbers of mortgage providers, Darproperty
                mortgage is dedicated to increase the number of home owners in the country.
                As an independent player in the mortgage industry Darproperty mortgage’s reliability lies with the
                customer and is committed to ensuring that the customer gets the best and most straightforward deal
                while acquiring property through advice, support and guidance to the
                applicants.

                </p>
            </div>
        </div>
    </div>

</div>

<!-- BOTTOM -->
<div class="container ">
    <div class="row">

    </div>

    <div class="row ">
        <div class="col-sm-4 ">
            <h3>TOOLS</h3>

            <div class="bodybottom">
                <p>
                <ul>
                    <li><g:link controller="tool" action="mortgageCalculator" tabindex="-1">
                        <g:message code="menu.tools.mortgageCalculator"/>
                    </g:link></li>
                    <li><g:link controller="tool" action="valueCalculator"
                                tabindex="-1"><g:message code="menu.tools.valueEstimator"/>
                    </g:link>
                    </li>
                    <!--<li>
             <a href="${resource(dir: 'img', file: 'rates.pdf')}" target="_blank">
             Buying Guide
              </a>
             </li>-->
                </ul>
            </p>
            </div>
        </div>

        <div class="col-sm-4 ">
            <h3>ABOUT US</h3>

            <div class="bodybottom">
                <p>
                    Darproperty mortgage is a mortgage brokerage service provide by
                    Darproperty Tanzania,
                </p>

                <p class="text-capitalize" align="right"><g:link controller="mortgage" action="aboutUs" class="btn btn-dafault"
                                         role="button"><g:message code="home.newsAndArticles.readmore"/>>>></g:link></p>
            </div>
        </div>

        <div class="col-sm-4">
            <h3>FAQ</h3>

            <div class="bodybottom">
                <p>
                    <span class="topic">Is Dar property the mortgage provider?</span><br>
                    We as Darproperty we don’t give a mortgage
                    but we provide assistance and guidance toward mortgage application
                    <g:link controller="mortgage" action="faq" class="btn btn-dafault pull-right text-capitalize"
                            role="button"><g:message code="home.newsAndArticles.readmore"/>>>></g:link>
                </p>

                <p align="right"></p>
            </div>
        </div>
    </div>
    <br><br>
</div>
<!-- FOOT -->

</body>
</html>