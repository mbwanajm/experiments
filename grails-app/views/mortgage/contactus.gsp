<html>
<head>

<title>Contact Us</title>
<meta name="layout" content="mortgage"/>
 <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <g:javascript src="lib/gmaps.js"/>
        <style>
     #map {
    width: 500px;
    height: 400px;
    background-color: #CCC;
  }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script>
    var map;
    $(document).ready(function () {
        map = new GMaps({
            el: '#map',
           
            lat: -6.752480,
            lng: 39.243173
        });
        map.addMarker({
            lat: -6.752480,
            lng: 39.243173,
            title: 'DarProperty HQ',
            details: {
                database_id: 42,
                author: 'HPNeo'
            },
            click: function (e) {
                if (console.log)
                    console.log(e);
                alert('You clicked in this marker');
            },
            mouseover: function (e) {
                if (console.log)
                    console.log(e);
            }
        });
        map.addMarker({
            lat: -6.752480,
            lng: 39.243173,
            title: 'DarProperty HQ',
            infoWindow: {
                content: '<p>0.5Km From Business Bus Stand <br>Maji Mengi Street house no. 29.</p>'
            }
        });
    });
    </script>
</head>

<body>
<div class="container">
<div class="row">
    <div class="col-sm-7">
   
        <div class='outlined'>
         
            <p><!--Feel free to contact us for any enquries, questions, comments and feedback,
            we promise to get back to you as soon as possible</p> --> <br>

<h2><u>Send us Message</u></h2>
           <!-- <g:render template="../sendMessage" model="[controller: 'tool', action: 'sendEmailToDarproperty', id: 0]"/> -->
            <g:render template="contactus_form"></g:render>
        </div>
    </div>

    <div class="col-sm-4">
        <h2><u><g:message code='contactDetails'/></u></h2>

        <address>
            <strong>DarProperty</strong><br>
            Mikocheni<br>
            P.O BOX 105499<br>
            Dar-es-salaam<br>
            Tanzania<br>
        </address>

        <p>

        <p><strong>Email</strong>: mortgage@darproperty.net<br>
            <strong>Call Us</strong>: +255 756 012 987 / +255 713751868 <br>
        <br>
        <div id='map'></div>

    </div>
    </div>



</div>
<br>
<br>

</body>
</html>