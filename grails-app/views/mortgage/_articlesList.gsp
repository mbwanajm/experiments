<!-- LIST VIEW -->


        <div class="col-sm-12">
            <div class='panel panel-default'>
                <div class='panel-heading' align='center'>
                    <h2>
                        <g:if test="${a.image}">
                            <img src='/image/renderArticlePhoto/${a.id}' width='50'
                                 class='img-responsive pull-left article-image'/>
                        </g:if><g:else>
                        <img src='/img/article.png' width='50' class='img-responsive pull-left article-image'/>
                    </g:else>
                        ${a.title}
                    </h2>
                </div>

                <div class='panel-body'>
                    ${a.content[0..98]}....<i class='hd2'>
                    <span class=' glyphicon glyphicon-option-vertical '></span>
                    <a href='?read=${a.id}'>Read more</a></i>
                </div>

                <div class='panel-footer'>
                    <small>
                        <span class='glyphicon glyphicon-bookmark'></span>
                        <span class='hd2'>${a.source}</span> <br>
                        <span class='glyphicon glyphicon-tags'></span>
                        <span class='hd2'>${a.tag}</span>
                        <br> ${a.dateCreated}
                    </small>
                </div></div>
        </div>

<!-- LIST VIEW -->