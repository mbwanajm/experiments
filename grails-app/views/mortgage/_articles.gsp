<%@ page import="com.mbwanajm.darproperty.Article" %>

<!-- READ VIEW -->
<%
    def article = Article.findAllByTag('mortgage', [max: 5])
%>

<g:if test="${request.getParameter("read") != null}">
    <div class="container">
        <div class="hidden-lg"><h3 align="center"><b>READ ARTICLES</b></h3></div>
        <br>
        <%
            Long id = Long.parseLong(params.read);
            def read = Article.findById(id)

        %>



        <div class='col-sm-7 pull-left'>
            <div class='panel panel-default'>
                <div class='panel-heading' align='center'>
                    <h2>${read.title}</h2></div>

                <div class='panel-body'>
                    <g:if test="${read.image}">
                        <img src='/image/renderArticlePhoto/${read.id}' width='150'
                             class='img-responsive pull-left article-image'/>
                    </g:if>
                    <g:else>
                        <img src='/img/article.png' width='150' class='img-responsive pull-left article-image'/>
                    </g:else>
                    <p>${read.content}</p></div>

                <div class='panel-footer'>
                    <small>
                        <span class='glyphicon glyphicon-bookmark'></span>
                        <span class='hd2'>${read.source}</span>
                        <br>
                        <span class='glyphicon glyphicon-tags'></span>
                        <span class='hd2'>${read.tag}</span>
                        <br> ${read.dateCreated}
                    </small>
                </div>
            </div>
        </div>

        <div class="col-sm-5 pull-right">
            <div class="hidden-lg hidden-md"><h3 align="center"><b>OTHER ARTICLES</b></h3></div>

            <g:render template="articlesList" collection="${article}" var="a"></g:render>
            <br>

            <div class="row pagination ">
                <g:paginate total="${article.count {}}"></g:paginate>
            </div>
        </div>

    </div>

</g:if>
<g:else>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <g:render template="articlesList" collection="${article}" var="a"></g:render>

            </div>
        </div>

        <div class="row pagination ">
            <g:paginate total="${article.count {}}"></g:paginate>
        </div>
    </div>

</g:else>
