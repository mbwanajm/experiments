<%@ page import="javax.validation.constraints.Null; com.mbwanajm.darproperty.Property; com.mbwanajm.darproperty.Agent;" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>DarProperty >> Export csv</title>
</head>

<body>
<div class="container">
    <div class="row" align="center">
        <%




            //            if (request.getParameter("export") != null) {
//                String from = request.getParameter("startyear");
//                String to = request.getParameter("endyear");
//                try {
//                    int fr = Integer.parseInt(from);
//                    int t = Integer.parseInt(to);
//                    if (fr > t) {
////                        //sort
////                        String sql = "SELECT * FROM property WHERE agent_id = 2 AND date_added BETWEEN '" + to + "-01-01' AND '" + from + "-01-01'";
////                        //print the csv here
////                        tz.co.darproperty.ExportData.export_clean(sql);
////                        out.println("<p class='alert alert-success' align='center'> CSV Document Created </p>");
//
//                    } else {
//
////                        //sort
////                        String sql = "SELECT * FROM property WHERE agent_id = 2 AND date_added BETWEEN '" + from + "-01-01' AND '" + to + "-01-01'";
////                        //print the csv here
////                        tz.co.darproperty.ExportData.export_clean(sql);
////                        out.println("<p class='alert alert-success' align='center'> CSV Document Created. Wait it opens </p>");
//                    }
//
//                } catch (Exception e) {
//                    out.println("<p class='alert alert-danger' align='center'> [No results found between ("+from+" and "+to+") years,  Please try again]  </p>");
//
//                }
//
//            }
            def cal = new GregorianCalendar()
            int hour = cal.get(Calendar.HOUR_OF_DAY);
            int minute = cal.get(Calendar.MINUTE);
            int second = cal.get(Calendar.SECOND);
            int year = cal.get(Calendar.YEAR);
            int month = (cal.get(Calendar.MONTH) + 1);
            int day = cal.get(Calendar.DAY_OF_YEAR);

            Date start = new Date().parse("yyyy", startyear)
            Date end = new Date().parse("yyyy", endyear)
            String dt = "" + hour + "_" + minute + "_" + second + "_" + year + "_" + month + "_" + day;
            String sFileName = "web-app\\csvfiles\\classified_contacts.csv";
            def property = Property.findAllByAgentAndDateAddedBetween(Agent.findById(2),start,end)
            String[] desc = property.description
            int count = 0

            try {
                File fl = new File("web-app\\csvfiles\\");
                if (!fl.exists()) {
                    fl.mkdir();
                }

                FileWriter writer = new FileWriter(sFileName);
                writer.append("Name");
                writer.append(',');
                writer.append("Email");
                writer.append(',');
                writer.append("Phone No");
                writer.append('\n');

                String combine = "";

                for (int i = 0; i < desc.length; i++) {
                    String sub = desc[i].substring(desc[i].indexOf("{{") + 2, desc[i].indexOf("}}"));
                    String[] contacts = sub.split(",");
                    String name = contacts[0];
                    String phone = contacts[1];
                    String[] phones = Null
                    String email = "";

                    if (contacts.length > 2) {
                        email = contacts[2];
                        email = (email ? emailService.checkEmail(emailService.srink_email(email)) : "");
                    }
                    String com = ""
                    if (phone.indexOf("/") != -1 || phone.indexOf("|") != -1 || phone.indexOf(",") != -1) {
                        phone = phone.indexOf("|") != -1 ? phone.replace("|", "/") : phone;
                        phone = phone.indexOf("|") != -1 ? phone.replace(",", "/") : phone;
                        phones = phone.split("/");

                        com = phones.each {it}.toString().replace(",", "/")
                    } else {
                        com = phone
                    }

                    writer.append(name);
                    writer.append(',');
                    writer.append(email);
                    writer.append(',');
                    writer.append(com)
                    writer.append('\n');

                    // out.print("Name: "+name+" Phone: "+(phone.indexOf("/") != -1 || phone.indexOf("|") != -1 || phone.indexOf(",") != -1? phones.each {it}:phone)+" Email: "+email+" <br> ")
                }

                writer.flush();
                writer.close();
                //JOptionPane.showMessageDialog(null, "Done !"); (cleanerdetails[i].replace("/", ",")).length() < 3 ? date+"," : cleanerdetails[i].replace("/", ",")
                try {
                    Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + sFileName);

                } catch (Exception e) {
                    out.print("File Error" + e);

                }
            } catch (Exception e) {

                out.print("File Error" + e);
            }
        %>

    </div>

    <div class="row" align="center">
        <g:if test="${request.getParameter("export") != null}">
            <p><a href="${resource(dir: "csvfiles", file: "classified_contacts.csv")}" class="btn btn-default"
                  target="_blank"><span
                        class="glyphicon glyphicon-download-alt"></span>  Download Here if Doesn't Open</a></p>
        </g:if>
        <g:else>
            <p><a href="${resource(dir: "csvfiles", file: "classified_contacts.csv")}" class="btn btn-default"
                  target="_blank"><span
                        class="glyphicon glyphicon-download-alt"></span>  Download Saved Data</a></p>
        </g:else>
    </div>

    <div class="row" align="center">
        <p><g:link controller="config" class="btn btn-primary"><span
                class="glyphicon glyphicon-hand-left"></span> Back...</g:link></p>
    </div>
</div>
</body>

</html>

