<html>
<head>
    <title>Dar Property Classifieds</title>
    <meta name="layout" content="main"/>
</head>

<body>

<div class="main">
    <div class="accord_top">
        <div style="width: 950px; height: 24px; margin: auto" alt="About ">
            <h2>Classfieds</h2>
        </div>
    </div>

    <div class="blog_body_resize">
        <div class="blog_body">
            <div class="search">
                <g:form controller="classfiedsSearch" action="search">
                    <label>
                        <span>
                            <g:textField name="searchTerm" class="keywords" value="Search.."/>
                        </span>

                        <g:submitButton name="searchGeneral" value="Search"
                                        src="${resource(dir: 'images/home', file: 'search.gif')}" class="button"
                                        type="image"/>
                    </label>
                </g:form>

                <p><a href="#">Post a Classified</a></p>

            </div>
        </div>

        <div class="clr"></div>
    </div>
</div>

<div class="body_resize">
    <div class="full">
        Classfied service is currently unavailable, but it will be soon, please try again some other time

        <!--
        <div class="port">
            <h2>item</h2>

            <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec scelerisque dictum turpis accumsan adipiscing.</p>
        </div>

        <div class="port">
            <h2>item</h2>

            <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec scelerisque dictum turpis accumsan adipiscing.</p>
        </div>

        <div class="port last">
            <h2>item</h2>

            <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec scelerisque dictum turpis accumsan adipiscing.</p>
        </div>

        <div class="port">
            <h2><a href="#">item</a></h2>

            <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec scelerisque dictum turpis accumsan adipiscing.</p>
        </div>

        <div class="port">
            <h2>item</h2>

            <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec scelerisque dictum turpis accumsan adipiscing.</p>
        </div>

        <div class="port last">
            <h2>item</h2>

            <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec scelerisque dictum turpis accumsan adipiscing.</p>
        </div>

        <div class="port">
            <h2>item</h2>

            <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec scelerisque dictum turpis accumsan adipiscing.</p>
        </div>

        <div class="port">
            <h2>item</h2>

            <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec scelerisque dictum turpis accumsan adipiscing.</p>
        </div>

        <div class="port last">
            <h2>item</h2>

            <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec scelerisque dictum turpis accumsan adipiscing.</p>
        </div>
           -->
        <div class="clr"></div>
    </div>
</div>


</div>
</div>
</body>

</html>

