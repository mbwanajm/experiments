
<div class="row" style="margin-top:0px;">

<g:hasErrors bean="${flash.errorBean}">
    <div class="alert alert-block alert-error">
        <g:renderErrors bean="${flash.errorBean}" as="list"/>
    </div>
</g:hasErrors>

<g:if test="${flash.successMessage}">
    <div class='alert alert-success'>${flash.successMessage}</div>
</g:if>
<g:if test="${flash.errorMessage}">
    <div class='alert alert-error'>${flash.errorMessage}</div>
</g:if>

<g:form controller="$controller" action="$action" id="$id" class="form form-horizontal">
    <div class='form-group' >
        <label for="email" class='control-label col-sm-4'><g:message code="property.enquiry.email" /></label>

        <div class='col-sm-5'>
            <g:textField name="email" id="formdt" autocomplete="off"></g:textField>
        </div>
    </div>

    <div class='form-group'>
        <label class='control-label col-sm-4'><g:message code="property.enquiry.phone"/></label>

        <div class='col-sm-5'>
            <g:textField name="phone" id="formdt"  autocomplete="off"></g:textField>
        </div>
    </div>

    <div class='form-group'>
        <label class='control-label col-sm-4'><g:message code="property.enquiry.message"/></label>

        <div class='col-sm-8'>
            <g:textArea name="message"  id='wysiwyg'  cols="50" rows="10" >${msg? msg: ""}</g:textArea>
        </div>
    </div>

    <div class='form-group'>
        <div class='col-sm-offset-4 col-sm-5'>
            <img src="${createLink(controller: 'simpleCaptcha', action: 'captcha')}"/>
            <br>
            <label for="captcha">Type the letters above in the box below:</label>
            <g:textField name="captcha"/>
        </div>
    </div>


    <div class='form-group'>
        <div class='col-sm-offset-4 col-sm-5'>
        <br>
            <g:submitButton name="${g.message(code: 'property.enquiry.sendMessage')}"
                            class="btn btn-success"></g:submitButton>
        </div>
    </div>
</g:form>
</div>