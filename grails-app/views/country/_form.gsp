<%@ page import="com.mbwanajm.darproperty.Country" %>



<div class="fieldcontain ${hasErrors(bean: countryInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="country.name.label" default="Name" />
		
	</label>
	<g:textField name="name" value="${countryInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: countryInstance, field: 'regions', 'error')} ">
	<label for="regions">
		<g:message code="country.regions.label" default="Regions" />
		
	</label>
	<g:select name="regions" from="${com.mbwanajm.darproperty.Region.list()}" multiple="multiple" optionKey="id" size="5" value="${countryInstance?.regions*.id}" class="many-to-many"/>
</div>

