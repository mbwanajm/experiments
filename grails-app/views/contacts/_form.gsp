<%@ page import="com.mbwanajm.darproperty.Contacts" %>



<div class="fieldcontain ${hasErrors(bean: contactsInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="contacts.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${contactsInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: contactsInstance, field: 'email', 'error')} required">
	<label for="email">
		<g:message code="contacts.email.label" default="Email" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="email" name="email" required="" value="${contactsInstance?.email}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: contactsInstance, field: 'phone', 'error')} required">
	<label for="phone">
		<g:message code="contacts.phone.label" default="Phone" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="phone" required="" value="${contactsInstance?.phone}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: contactsInstance, field: 'message', 'error')} ">
	<label for="message">
		<g:message code="contacts.message.label" default="Message" />
		
	</label>
	<g:textArea name="message" cols="40" rows="5" maxlength="1200" value="${contactsInstance?.message}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: contactsInstance, field: 'page', 'error')} ">
	<label for="page">
		<g:message code="contacts.page.label" default="Page" />
		
	</label>
	<g:textField name="page" value="${contactsInstance?.page}"/>
</div>

