
<%@ page import="com.mbwanajm.darproperty.Contacts" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'contacts.label', default: 'Contacts')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-contacts" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="create" controller="config" action="index"> <span class="glyphicon glyphicon-wrench"></span></g:link></li>
			</ul>
		</div>
		<div id="list-contacts" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table class="table">
				<thead>
					<tr>
					
						<g:sortableColumn property="id" title="${message(code: 'contacts.name.label', default: 'Name')}" />
					
						<g:sortableColumn property="email" title="${message(code: 'contacts.email.label', default: 'Email')}" />
					
						<g:sortableColumn property="phone" title="${message(code: 'contacts.phone.label', default: 'Phone')}" />
					
						<g:sortableColumn property="message" title="${message(code: 'contacts.message.label', default: 'Message')}" />
					
						<g:sortableColumn property="page" title="${message(code: 'contacts.page.label', default: 'Page')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${contactsInstanceList}" status="i" var="contactsInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="edit" id="${contactsInstance.id}">${fieldValue(bean: contactsInstance, field: "name")}</g:link></td>
					
						<td class="hd2">${fieldValue(bean: contactsInstance, field: "email")}</td>
					
						<td class="hd2">${fieldValue(bean: contactsInstance, field: "phone")}</td>
					
						<td class="hd2">${fieldValue(bean: contactsInstance, field: "message")}</td>
					
						<td class="hd2">${fieldValue(bean: contactsInstance, field: "page")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${contactsInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
