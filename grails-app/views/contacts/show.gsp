<%@ page import="com.mbwanajm.darproperty.Contacts" %>
<!DOCTYPE html>
<html>
<head>
    <g:if test="${contactsInstance.page == "mortgage"}">
        <meta name="layout" content="mortgage">
    </g:if>

    <g:if test="${contactsInstance.page == "insurance"}">
        <meta name="layout" content="insurance2">
    </g:if>

    <g:if test="${contactsInstance.page == ""}">
        <meta name="layout" content="main">
    </g:if>


    <g:set var="entityName" value="${message(code: 'contacts.label', default: 'Contacts')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-lg-offset-4 col-lg-5">
            <div id="show-contacts" class="content scaffold-show" role="main">
                <h1 align="center">Thank you for contact with us.</h1>
                <g:if test="${flash.message}">
                    <div class="message alert alert-success" role="status" align="center"> ${flash.message} </div>

                </g:if>
            </div>
        </div>

        <div class="row">

            <ul class="property-list contacts">

                <g:if test="${contactsInstance?.name}">
                    <li class="col-lg-offset-4 col-lg-5">
                        <span id="name-label" class="property-label text-red"><g:message code="contacts.name.label"
                                                                                         default="Name"/></span>
                        <br>
                        <span class="property-value" aria-labelledby="name-label"><g:fieldValue
                                bean="${contactsInstance}"
                                field="name"/></span>

                    </li>
                </g:if>

                <g:if test="${contactsInstance?.email}">
                    <li class="col-lg-offset-4 col-lg-5">
                        <span id="email-label" class="property-label text-red"><g:message code="contacts.email.label"
                                                                                          default="Email"/></span>
                        <br>
                        <span class="property-value" aria-labelledby="email-label"><g:fieldValue
                                bean="${contactsInstance}"
                                field="email"/></span>

                    </li>
                </g:if>

                <g:if test="${contactsInstance?.phone}">
                    <li class="col-lg-offset-4 col-lg-5">
                        <span id="phone-label" class="property-label text-red"><g:message code="contacts.phone.label"
                                                                                          default="Phone"/></span>
                        <br>
                        <span class="property-value" aria-labelledby="phone-label"><g:fieldValue
                                bean="${contactsInstance}"
                                field="phone"/></span>

                    </li>
                </g:if>

                <g:if test="${contactsInstance?.message}">
                    <li class="col-lg-offset-4 col-lg-5">
                        <span id="message-label" class="property-label text-red"><g:message
                                code="contacts.message.label"
                                default="Message"/></span>
                        <br>
                        <span class="property-value" aria-labelledby="message-label"><g:fieldValue
                                bean="${contactsInstance}" field="message"/></span>

                    </li>
                </g:if>

                <g:if test="${contactsInstance?.page}">
                    <li class=" hidden col-lg-offset-4 col-lg-5">
                        <span id="page-label" class="property-label text-red"><g:message code="contacts.page.label"
                                                                                         default="Page"/></span>
                        <br>
                        <span class="property-value" aria-labelledby="page-label"><g:fieldValue
                                bean="${contactsInstance}"
                                field="page"/></span>

                    </li>
                </g:if>

            </ul>

            <br>

            <g:form>
                <fieldset class="buttons">
                    <g:hiddenField name="id" value="${contactsInstance?.id}"/>

                </fieldset>
            </g:form>
        </div>
    </div>
    <div class="row" align="center">

        <g:if test="${contactsInstance.page == "insurance"}">
            <g:link controller="insurance">Back..</g:link>
        </g:if>

        <g:if test="${contactsInstance.page == "mortgage"}">
            <g:link controller="mortgage" action="index">Back..</g:link>
        </g:if>

        <g:if test="${contactsInstance.page == null}">
            <g:link controller="contacts">Back..</g:link>
            <g:actionSubmit class="btn btn-danger" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" formnovalidate="" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />

        </g:if>

    </div>
</div>
<hr>
</body>
</html>
