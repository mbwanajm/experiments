<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><g:layoutTitle default="DarProperty Insurance "/></title>
    <link rel="shortcut icon" href="${resource(dir: 'images', file: 'shortcutIcon.png')}" type="image/x-icon"/>

    <link rel="stylesheet" href="${resource(dir: 'insurance/css', file: 'bootstrap.min.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'insurance/css', file: 'agency.css')}"/>

    <!-- Custom Fonts -->
    <link href="${resource(dir: 'insurance/font-awesome/css', file: 'font-awesome.min.css')}" rel="stylesheet"
          type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet'
          type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- JS -->
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <!-- Google map -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <g:javascript src="lib/gmaps.js"/>
    <style>
    #map {
        width: 500px;
        height: 400px;
        background-color: #CCC;
    }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script>

        var map;
        $(document).ready(function () {
            map = new GMaps({
                el: '#map',

                lat: -6.752480,
                lng: 39.243173
            });
            map.addMarker({
                lat: -6.752480,
                lng: 39.243173,
                title: 'DarProperty HQ',
                details: {
                    database_id: 42,
                    author: 'HPNeo'
                },
                click: function (e) {
                    if (console.log)
                        console.log(e);
                    alert('You clicked in this marker');
                },
                mouseover: function (e) {
                    if (console.log)
                        console.log(e);
                }
            });
            map.addMarker({
                lat: -6.752480,
                lng: 39.243173,
                title: 'DarProperty HQ',
                infoWindow: {
                    content: '<p>0.5Km From Business Bus Stand (Mikocheni B) <br>Msuya Street Block no. 29.</p>'
                }
            });
        });
    </script>


    <g:layoutHead/>
</head>

<body id="page-top" class="index">

<!-- Navigation -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand page-scroll" href="#page-top"><img
                    src="${resource(dir: 'insurance/img/logos/', file: 'Insurance _Logo.png')}" width="150"/></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="hidden">
                    <a href="#page-top"></a>
                </li>
                <li>
                    <a class="page-scroll" href="#profile">Profile</a>
                </li>
                <li>
                    <a class="page-scroll" href="#portfolio">Main Coverage</a>
                </li>
                <li>
                    <a class="page-scroll" href="#other">Other Coverage</a>
                </li>
                <li>
                    <a class="page-scroll" href="#info">Info</a>
                </li>
                <li>
                    <a class="page-scroll" href="#contact">Contact</a>
                </li>
                <li>
                    <g:link controller="home" action="home" class="page-scroll">Exit</g:link>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>

<!-- Header -->
<header>
    <div class="container">
        <div class="intro-text">
            <div class="intro-lead-in " >You take care of your Investments.</div>

            <div class="intro-heading " >We take care of your Risks.</div>
            <a href="#profile" class="page-scroll btn btn-xl">Know More</a>
        </div>
    </div>
</header>
<br>
<g:layoutBody/>

<div class="container" style="height:10px;">
    <div class="row">
        <div class="col-sm-12 pull-right">
            <p align="right"><a href="#top"><small class="gotop alert-primary"><span
                    class="glyphicon glyphicon-chevron-up "></span> Top</small></a></p>
        </div>
    </div>
</div>
<br>
<!-- FOOT -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <span class="copyright">Copyright &copy; Darproperty  <% def cal = new GregorianCalendar()
                def y = cal.get(Calendar.YEAR)
                %>
                    ${y}</span>
            </div>

            <div class="col-md-4">
                <ul class="list-inline social-buttons">
                    <li><a href="https://twitter.com/thedarproperty" target="_blank"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li><a href="http://www.facebook.com/pages/DarProperty/114594825280789" target="_blank"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a>
                    </li>
                </ul>
            </div>

            <div class="col-md-4">
                <ul class="list-inline quicklinks">
                    <li><g:link controller='home' action="home">Darproperty</g:link>
                    </li>
                    <li>|</li>
                    <li><g:link controller='termsAndConditions' action="termsAndConditions">Terms & Conditions</g:link>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/57a49ae28e7071af3c78f313/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
<!-- jQuery -->

<script src="${resource(dir: 'insurance/js', file: 'jquery.js')}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="${resource(dir: 'insurance/js', file: 'bootstrap.min.js')}"></script>

<!-- Plugin JavaScript -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="${resource(dir: 'insurance/js', file: 'classie.js')}"></script>
<script src="${resource(dir: 'insurance/js', file: 'cbpAnimatedHeader.js')}"></script>


<!-- Contact Form JavaScript -->
<script src="${resource(dir: 'insurance/js', file: 'jqBootstrapValidation.js')}"></script>
<script src="${resource(dir: 'insurance/js', file: 'contact_me.js')}"></script>

<!-- Custom Theme JavaScript -->
<script src="${resource(dir: 'insurance/js', file: 'agency.js')}"></script>

</body>

</html>
