<!DOCTYPE html>
<html lang="en">
<head>
    <title><g:layoutTitle default="DarProperty Mortgage Service"/></title>
    <link rel="shortcut icon" href="${resource(dir: 'images', file: 'shortcutIcon.png')}" type="image/x-icon"/>
    <link rel="stylesheet" href="${resource(dir: 'bootstrap/css', file: 'bootstrap.min.css')}"/>

    <link rel="stylesheet" href="${resource(dir: 'mortgage/css', file: 'custom.css')}"/>
    <g:javascript src="lib/jquery-1.8.3.js"/>
    <g:javascript src="lib/bootstrap.js"/>
    <g:javascript src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"/>
    <g:javascript src="bootstrap/js/bootstrap.min.js"/>
    <g:javascript src="lib/scroll.js"/>

    <g:layoutHead/>
</head>

<body id="top">

<header>
    <div class="container">
        <div class="row">
            <div class="col-sm-6" align="left">Welcome to Darproperty Mortgage</div>

            <div class="col-sm-6" align="right"><g:link controller="home"
                                                        action="home">DarProperty Home</g:link> | Site Map</div>
        </div>
    </div>
</header>

<div class="container">
    <div class="row">
        <div class="col-sm-6" align="left">
            <img alt="Logo" class="logoimg" src="${resource(dir: 'mortgage/img/', file: 'banner1.jpg')}"/>

        </div>

        <div class="col-sm-6" align="right">
            <p style="font-family: Sans-Serif;">
                <br>
                <b><span style="font-size:14pt;">DIAL FOR HOTLINE</span></b>
                <br>
                <b><span style="font-size:17pt;">+255 756 012 987</span></b>
                <br>
                <b>mortgage@darproperty.net</b>
            </p>

        </div>
    </div>
    <br>

    <div class="row menuslide">
        <div class="span12 ">
            <div class="navbar navbar-default">
                <div class="container-fluid ">
                    <!-- HEAD -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed"
                                data-toggle="collapse" data-target="#menucollapse" aria-expanded="false">
                            <span class="sr-only"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <!-- MENU -->

                    <div class="collapse navbar-collapse" id="menucollapse">
                        <ul class="nav navbar-nav">
                            <li class="active"><g:link controller="mortgage" action="">HOME <span
                                    class="sr-only">(current)</span></g:link></li>
                            <li><g:link controller="mortgage" action="aboutUs">ABOUT US</g:link></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-haspopup="true" aria-expanded="false">
                                    OUR SERVICES<span class="caret"></span></a>
                                <ul class="dropdown-menu">

                                    <li><a href="http://expo.darproperty.co.tz" target="_blank">Exhibition</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><g:link controller="insurance" action="home" target="_blank">Insurance</g:link></li>
                                    <li role="separator" class="divider"></li>
                                    <li><g:link controller="MonthlyIssue" action="showAllMagazines" target="_blank">
                                        <g:message code="menu.magazine"/>
                                    </g:link></li>

                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-haspopup="true" aria-expanded="false">
                                    TOOLS<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><g:link controller="tool" action="mortgageCalculator" tabindex="-1">
                                        <g:message code="menu.tools.mortgageCalculator"/>
                                    </g:link></li>

                                    <!--
                                      <li role="separator" class="divider"></li>
                                     <li>
							             <a href="${resource(dir: 'img', file: 'rates.pdf')}" target="_blank">
							             Buying Guide
							              </a>
							             </li>-->
                                    <li role="separator" class="divider"></li>
                                    <li><g:link controller="tool" action="valueCalculator"
                                                tabindex="-1"><g:message code="menu.tools.valueEstimator"/>
                                    </g:link>
                                    </li>
                                </ul>

                            <li><g:link controller="mortgage" action="mortgageArticles">ARTICLES</g:link></li>
                            <li><g:link controller="mortgage" action="faq">FAQ</g:link></li>
                            <li><g:link controller="mortgage" action="contactus">CONTACT US</g:link></li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<br>
<g:layoutBody/>

<div class="container" style="height:10px;">
    <div class="row">
        <div class="col-sm-12 pull-right">
            <p align="right"><a href="#top"><small class="gotop alert-primary"><span
                    class="glyphicon glyphicon-chevron-up "></span> Top</small></a></p>
        </div>
    </div>
</div>
<br>
<!-- FOOT -->
<div class="container ">
    <div class="row footer">
        <div class="span6" align="left">

            <ul class="nav navbar-nav">
                <li class="active"><g:link controller="mortgage" action="">Home</g:link> <span
                        class="sr-only">(current)</span></a></li>
                <li><g:link controller="mortgage" action="aboutUs" >About Us</g:link></li>
                <li><g:link controller='termsAndConditions' action="termsAndConditions">Terms & Conditions</g:link></li>
                <li><g:link controller="mortgage" action="faq">FaQ</g:link></li>
                <li><g:link controller="mortgage" action="contactus">Contact Us</g:link></li>

            </ul>
        </div>

        <div class="span6" align="right">

        </div>
    </div>

    <div class="row">
        <div class="span6">
            <p>Copyright ©  <% def cal = new GregorianCalendar()
            def y = cal.get(Calendar.YEAR)
            %>
            ${y} DarProperty Mortgage. All Rights Reserved. Webiste Designing and Development by DarProperty Developers</p>
        </div>
    </div>
    <br>
</div>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/57a49ae28e7071af3c78f313/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
</body>
</html>