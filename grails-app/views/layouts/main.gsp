<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><g:layoutTitle default="DarProperty: East Africa's Ultimate Property Guide"/></title>
    <!--Web icon-->
    <link rel="shortcut icon"
          href="${resource(dir: 'images', file: 'shortcutIcon.png')}"
          type="image/x-icon"/>

    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet'
          type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="${resource(dir: 'newlook/css', file: 'bootstrap.min.css')}">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="${resource(dir: 'newlook/css', file: 'font-awesome.min.css')}">


    <!--Article Layout-->
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'redactor.css')}"/>
    <!-- Custom CSS -->
    <link rel="stylesheet" href="${resource(dir: 'newlook/css', file: 'owl.carousel.css')}">
    <link rel="stylesheet" href="${resource(dir: 'newlook', file: 'style.css')}">
    <link rel="stylesheet" href="${resource(dir: 'newlook/css', file: 'responsive.css')}">

    <link href="${resource(dir: 'css', file: 'jquery-ui.css')}" rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->



    <script type="text/javascript" src="../js/lib/jquery-1.8.3.js"></script>
    <script type="text/javascript" src="../js/lib/redactor.js"></script>
    <script type="text/javascript" src="../js/lib/wysiwyg.js"></script>
    %{--Google Analytics--}%
    <g:javascript type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-33712962-1']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        })();



    </g:javascript>

</head>
<g:layoutHead/>
<body id="top">
%{--Body start--}%

<div class="header-area">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="user-menu">
                    <ul>
                        <li><g:link controller="monthlyIssue" action="showAllMagazines">
                            <i class="fa fa-book"></i> <g:message code="menu.magazine"></g:message></g:link></li>
                        <li><g:link controller="mortgage" action="index"
                                    target="_blank"><i class="fa fa-bomb"></i> <g:message
                                    code="home.mortgage"></g:message></g:link></li>
                        <li><g:link controller="insurance" action="home" target="_blank"><i
                                class="fa fa-umbrella"></i> <g:message code="Insurance"/>
                        </g:link></li>
                        <li><a href="http://expo.darproperty.co.tz" target="_blank">
                            <i class="fa fa-binoculars"></i> <g:message code="Exhibition"/></a></li>

                    </ul>
                </div>
            </div>

            <div class="col-md-4">
                <div class="header-right">
                    <ul class="list-unstyled list-inline">

                        <li class="dropdown dropdown-small">
                            <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span
                                    class="key">language :</span><span class="value">English</span><b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li><g:link controller='home' action="home"
                                            params="[lang: 'sw']">Swahili</g:link></li>
                                <li><g:link controller='home' action="home" params="[lang: 'en']">
                                    English</g:link></li>
                            </ul>
                        </li>
                        <sec:ifLoggedIn>
                            <span style="cursor:pointer" onclick="openNav()">&#9776;  <g:message code="my.account"></g:message></span>
                        </sec:ifLoggedIn>
                        <sec:ifNotLoggedIn>
                            <span style="cursor:pointer" onclick="openNav()">&#9776;  <g:message code="free.posting"></g:message></span>
                        </sec:ifNotLoggedIn>



                    </ul>
                </div>
            </div>
        </div>
    </div>
</div> <!-- End header area &times;-->

<div id="mySidenav" class="sidenav">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><i class="fa fa-close"></i></a>
<sec:ifNotLoggedIn>
    <g:link controller="agent" action="agentSignup">
        <span class="fa fa-pencil"></span>
        <g:message code="menu.signup"/>
    </g:link>
    <g:link controller="agent" action="authenticate">
        <span class="glyphicon glyphicon-log-in"></span>
        <g:message code="menu.login"/>
    </g:link>
</sec:ifNotLoggedIn>
    <sec:ifLoggedIn>
       <g:link controller="agent" action="profile"
                    id="${sec.loggedInUserInfo(field: 'id')}">
            <span class="glyphicon glyphicon-user"></span> <g:message code="profile"></g:message>
        </g:link>
        <sec:ifAllGranted roles="ROLE_ADMIN">

            <g:link controller="config" action="index">
                <i class="fa fa-dashboard"></i> Admin Panel
            </g:link>


        </sec:ifAllGranted>

        <g:link controller="logout">
            <span class="glyphicon glyphicon-log-out"></span>
            <g:message code="menu.logout"/>
        </g:link>

    </sec:ifLoggedIn>
</div>


<div class="site-branding-area">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="logo">
                    <h4><g:link controller="home" action="home"><img
                            src="${resource(dir: '/newlook/img', file: 'logo.png')}"></g:link></h4>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="shopping-item">
                    <!--<a href="cart.html">Cart - <span class="cart-amunt">$100</span> <i class="fa fa-shopping-cart"></i> <span class="product-count">5</span></a>-->
                    <a href="http://www.facebook.com/pages/DarProperty/114594825280789" class="cart-amunt"
                       target="_blank"><i
                            class="fa fa-facebook"></i></a>
                    <a href="https://twitter.com/thedarproperty" target="_blank" class="cart-amunt"><i
                            class="fa fa-twitter"></i></a>
                    <a href="https://www.youtube.com/channel/UC2IcASWtYiC_FmGvZ8NWasA" target="_blank"
                       class="cart-amunt"><i
                            class="fa fa-youtube"></i></a>
                    <a href="https://www.linkedin.com/company/darproperty-tanzania?trk=top_nav_home"
                       target="_blank" class="cart-amunt"><i class="fa fa-linkedin"></i></a>
                </div>
            </div>
        </div>
    </div>
</div> <!-- End site branding area -->

<div class="mainmenu-area">
    <div class="container">
        <div class="row">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse"
                        aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <div class="navbar-collapse collapse" id="navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="${hometb}"><g:link controller="home" action="home"><g:message
                            code="menu.home"></g:message></g:link></li>
                    <li class="${abouttb}"><g:link controller="aboutUS" action="aboutUS"><g:message
                            code="menu.aboutUs"></g:message></g:link></li>
                    <li class="${agentstb}"><g:link controller="agent" action="showAllAgents"><g:message
                            code="menu.agents"></g:message></g:link></li>
                    <li class="dropdown ${producttb}"><a data-toggle="dropdown" data-hover="dropdown"
                                                         class="dropdown-toggle" href="#">
                        <g:message code="Products"/> <span
                                class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><g:link controller="monthlyIssue" action="showAllMagazines">
                                <i class="fa fa-book"></i> <g:message code="menu.magazine"></g:message></g:link></li>
                            <li><g:link controller="mortgage" action="index"
                                        target="_blank"><i class="fa fa-bomb"></i> <g:message
                                        code="home.mortgage"></g:message></g:link></li>
                            <li><g:link controller="insurance" action="home" target="_blank"><i
                                    class="fa fa-umbrella"></i> <g:message
                                    code="Insurance"/>
                            </g:link></li>
                            <li><a href="http://expo.darproperty.co.tz" target="_blank">
                                <i class="fa fa-binoculars"></i> <g:message code="Exhibition"></g:message></a></li>
                        </ul>

                    </li>
                    <li class="dropdown ${toolstb}"><a data-toggle="dropdown" data-hover="dropdown"
                                                       class="dropdown-toggle" href="#"><g:message
                                code="menu.tools"/><span class="caret"></span>
                    </a>
                        <ul class="dropdown-menu">
                            <li><g:link controller="tool"
                                        action="mortgageCalculator" tabindex="-1">
                                <i class="fa fa-calculator"></i> <g:message code="menu.tools.mortgageCalculator"/>
                            </g:link></li>
                            <li><g:link controller="tool" action="valueCalculator"
                                        tabindex="-1">
                                <i class="fa fa-bar-chart-o"></i> <g:message code="menu.tools.valueEstimator"/>
                            </g:link></li>
                        </ul>
                    </li>
                    <li class="dropdown ${hubtb}"><a data-toggle="dropdown" data-hover="dropdown"
                                                     class="dropdown-toggle" href="#"><g:message code="The-Hub"/><span
                                class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><g:link controller="home" action="infoHub">
                                <i class="fa fa-bookmark"></i> <g:message code="menu.infoHub"/>
                            </g:link></li>
                            <li><g:link controller="article" action="articleLibrary">
                                <i class="fa fa-info"></i> <g:message code="Article-Library"/>
                            </g:link></li>
                        </ul>
                    </li>
                    <li class="dropdown ${advertisetb}"><a data-toggle="dropdown" data-hover="dropdown"
                                                           class="dropdown-toggle" href="#"><g:message
                                code="menu.advertise"/><span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><g:link controller="home" action='whyAdvertise'
                                        tabindex="-1">
                                <g:message code="menu.advertise.whyAdvertise"/>
                            </g:link></li>
                            <li><g:link controller='home' action='advertisingPolicy'
                                        tabindex="-1">
                                <g:message code="menu.advertise.advertisingPolicy"/>
                            </g:link></li>
                            <li><a href="${resource(dir: 'img', file: 'rates.pdf')}">
                                <g:message code="menu.advertise.rateCard"/>
                            </a></li>
                        </ul>
                    </li>
                    <li class="${overseatb}"><a href="http://international.darproperty.co.tz" target="_blank">
                        <g:message code="menu.international"/>
                    </a></li>
                    <li><g:link controller="forums" action="">
                        <g:message code="Forums"/>
                        <span class="sr-only">(current)</span>
                    </g:link></li>
                </ul>
            </div>
        </div>
    </div>
</div> <!-- End mainmenu area -->




<div style="margin: 20px;"></div>
<g:layoutBody/>

<div style="margin: 20px;"></div>

<div class="footer-top-area">
    <div class="zigzag-bottom"></div>

    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="footer-about-us">
                    <h2><span style="color:#fff">Darproperty</span></h2>

                    <p><g:message code="footer.mission"></g:message></p>

                    <div class="footer-social">
                        <a href="http://www.facebook.com/pages/DarProperty/114594825280789" target="_blank"><i
                                class="fa fa-facebook"></i></a>
                        <a href="https://twitter.com/thedarproperty" target="_blank"><i class="fa fa-twitter"></i></a>
                        <a href="https://www.youtube.com/channel/UC2IcASWtYiC_FmGvZ8NWasA" target="_blank"><i
                                class="fa fa-youtube"></i></a>
                        <a href="https://www.linkedin.com/company/darproperty-tanzania?trk=top_nav_home"
                           target="_blank"><i class="fa fa-linkedin"></i></a>
                        <a
                                href="https://www.instagram.com/darproperty/"
                                target="_blank"><img
                                src="${resource(dir: 'bootstrap/imgs/', file: 'inst.png')}"
                                alt="Instagram" class="img-circle" width="30" height="30"
                                title="Instagram"></a>
                        <a
                                href="whatsapp://send?text=Visit now http://www.darproperty.co.tz"
                                target="_blank"><img
                                src="${resource(dir: 'bootstrap/imgs/', file: 'whatsapp.png')}"
                                alt="Whatsapp" class="" width="30" height="30"
                                title="Whatsapp"/>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6">
                <div class="footer-menu">
                    <h2 class="footer-wid-title"><g:message code="application.navigations"></g:message></h2>
                    <ul>
                        <sec:ifLoggedIn>
                            <li><g:link controller="agent" action="profile"
                                        id="${sec.loggedInUserInfo(field: 'id')}">
                                <span class="glyphicon glyphicon-user"></span> <g:message code="my.account"></g:message>
                            </g:link></li>

                            <li><g:link controller="logout">

                                <g:message code="menu.logout"/>
                            </g:link></li>
                        </sec:ifLoggedIn>
                        <sec:ifNotLoggedIn>
                            <li><g:link controller="agent" action="authenticate">

                                <g:message code="menu.login"/>
                            </g:link></li>
                        </sec:ifNotLoggedIn>
                        <li><g:link controller="article" action="articleLibrary"><g:message
                                code="home.article"></g:message></g:link></li>
                        <li><g:link controller="contactUs">Contact Us</g:link></li>
                        <li><g:link controller="search" action="user_enquiry"><g:message
                                code="home.wanted.property"></g:message> <g:message
                                code="home.property"></g:message></g:link></li>
                        <li><g:link controller='home' action='advertisingPolicy'
                                    tabindex="-1">
                            <g:message code="menu.advertise.advertisingPolicy"/>
                        </g:link></li>
                        <li><g:link controller="termsAndConditions"
                                    action="termsAndConditions"><g:message
                                    code="menu.termsAndConditions"></g:message></g:link></li>



                        <li><g:message code="Language"></g:message> :
                            <g:link controller='home' action="home" params="[lang: 'sw']">
                                <img src="${resource(dir: 'bootstrap/imgs/', file: 'ea.png')}" title="Kiswahili"/>
                                <g:message code="Swahili"></g:message></g:link>
                            <g:link controller='home' action="home" params="[lang: 'en']">
                                <img src="${resource(dir: 'bootstrap/imgs/', file: 'gb.png')}" title="English"/>
                                <g:message code="English"></g:message></g:link>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-md-3 col-sm-6">
                <div class="footer-menu">
                    <h2 class="footer-wid-title"><g:message code="application.categories"></g:message></h2>
                    <ul>
                        <li><g:link controller="mortgage" action="index"
                                    target="_blank"><g:message code="home.mortgage"></g:message></g:link></li>
                        <li><g:link controller="insurance" action="home" target="_blank"><g:message
                                code="home.insurance"></g:message></g:link></li>
                        <li><g:link controller="MonthlyIssue"
                                    action="showAllMagazines"><g:message code="menu.magazine"></g:message></g:link></li>
                        <li><a href="http://expo.darproperty.co.tz" target="_blank"><g:message
                                code="Exhibition"></g:message></a></li>
                        <li><a href="http://international.darproperty.co.tz" target="_blank"><g:message
                                code="menu.international"></g:message></a>
                        </li>
                        <li><g:link controller="agent"
                                    action="showAllAgents"><g:message code="menu.agents"></g:message></g:link></li>
                        <li><g:link controller="home" action='whyAdvertise'
                                    tabindex="-1">
                            <g:message code="menu.advertise.whyAdvertise"/>
                        </g:link></li>
                        <li><a href="${resource(dir: 'img', file: 'rates.pdf')}">
                            <g:message code="menu.advertise.rateCard"/>
                        </a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-3 col-sm-6">
                <div class="footer-newsletter">
                    <h2 class="footer-wid-title">Newsletter</h2>

                    <p>Sign up to our newsletter and get exclusive deals you wont find anywhere else straight to your inbox!</p>

                    <div class="newsletter-form">
                        <g:form controller="subscriber" action="save">
                            <input type="text" placeholder="Your Firstname" name="firstname">
                            <input type="text" placeholder="Your Lastname" name="lastname">
                            <input type="email" placeholder="Type your email" name="email" required="">
                            <input type="submit" value="Subscribe" name="subscribe">
                        </g:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- End footer top area -->

<div class="footer-bottom-area">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="copyright">
                    <p>&copy; <%=new java.util.GregorianCalendar().get(Calendar.YEAR)%> DarProperty. <g:message
                            code="footer.allrights"></g:message> .</p>
                </div>
            </div>

            <div class="col-md-4">
                <div class="footer-card-icon">

                    <a title="Go to Top" href="#top"><small
                            class="gotop alert-primary"><span
                                class="glyphicon glyphicon-chevron-up "></span> <g:message
                                code="application.gotop"></g:message></small></a>
                </div>
            </div>
        </div>
    </div>
</div> <!-- End footer bottom area -->


%{--what is this note--}%
<div class="modal fade whatisthis " tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-sm">
        <div class="modal-content table-autoflow-md">
            <div class="row">
                <div class="col-lg-offset-1 col-lg-10 col-lg-offset-1" align="center">
                    <h4 class="text-center text-primary text-capitalize">Darproperty <g:message
                            code="home.tools.valueEstimator"></g:message></h4>

                    <g:message code="home.whatisit"></g:message>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i></button>
            </div>
        </div>
    </div>
</div>
%{--//what is this note--}%


%{--Body end--}%
<!-- Side Menu -->
<script src="../newlook/js/sidemenu.js"></script>
<!-- Latest jQuery form server -->
<script src="https://code.jquery.com/jquery.min.js"></script>

<!-- Bootstrap JS form CDN -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!-- jQuery sticky menu -->
<script src="../newlook/js/owl.carousel.min.js"></script>
<script src="../newlook/js/jquery.sticky.js"></script>

<!-- jQuery easing -->
<script src="../newlook/js/jquery.easing.1.3.min.js"></script>

<!-- Main Script -->
<script src="../newlook/js/main.js"></script>

<!-- Slider -->
<script type="text/javascript" src="../newlook/js/bxslider.min.js"></script>
<script type="text/javascript" src="../newlook/js/script.slider.js"></script>



<!-- Location Loaders -->
<g:javascript src="darproperty/CountryLoader.js"/>
<g:javascript src="darproperty/RegionLoader.js"/>
<g:javascript src="darproperty/PriceRangeUpdater.js"/>





<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

%{--<g:javascript src="lib/bootstrap.js"/>--}%
<g:javascript src="lib/processing.js"/>
<g:javascript src="lib/facebook.js"/>
<g:javascript src="lib/twitter.js"/>



<g:javascript src="lib/jquery-1.10.2.js"/>




<!--scroll-->
<g:javascript src="lib/scroll.js"/>
%{--Article layout--}%

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>


<g:javascript src="lib/jquery-ui.js"/>
<g:javascript src="lib/redactor.js"/>
<g:javascript src="lib/wysiwyg.js"/>
<!-- Mid Page Slider -->
<g:javascript src="darproperty/HomeSlideShows.js"/>
<g:javascript src="lib/responsiveslides.js"/>
%{--Form Processing--}%
<g:javascript src="lib/processing.js"/>
%{--add property--}%
<g:javascript src="darproperty/AddProperty.js"/>

<!--TWIK-->
<!--Start of Tawk.to Script-->
<g:javascript type="text/javascript">
    var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
    (function () {
        var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/57a49ae28e7071af3c78f313/default';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();


</g:javascript>
<script>

    $(function () {
        $("#datepicker").datepicker({
            dateFormat: "yy-mm-dd",
            defaultDate: +30
        });

    });
    $(function () {
        $(".datepicker").datepicker({
            dateFormat: "yy-mm-dd",
            defaultDate: +30
        });

    });
    $(function () {
        $('#datetimepicker3').datetimepicker({
            pickDate: false
        });
    });
</script>

<!--End of Tawk.to Script-->

<!--End of Tawk.to Script-->

</body>
</html>

