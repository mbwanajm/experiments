<%--
  Created by IntelliJ IDEA.
  User: DAR PROPERTY
  Date: 8/31/2016
  Time: 4:20 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>DarProperty >> Agent Rating</title>
    <meta name="layout" content="main"/>
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <g:if test="${flash.message}">
                <span class="" align="center">${flash.message}<br></span>
            </g:if>
            <p class="alert alert-info"> Thank you for rating this agent !</p>
            <g:link controller="agent" action="showAllAgents" class="btn btn-default"> <i class="fa fa-backward"></i> Back</g:link>
        </div>
    </div>
</div>
</body>
</html>