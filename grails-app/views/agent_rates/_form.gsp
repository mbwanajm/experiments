<%@ page import="com.mbwanajm.darproperty.Agent_rates" %>



<div class="fieldcontain ${hasErrors(bean: agent_ratesInstance, field: 'agent', 'error')} ">
	<label for="agent">
		<g:message code="agent_rates.agent.label" default="Agent" />
		
	</label>
	<g:select id="agent" name="agent.id" from="${com.mbwanajm.darproperty.Agent.list()}" optionKey="id" value="${agent_ratesInstance?.agent?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: agent_ratesInstance, field: 'ip', 'error')} required">
	<label for="ip">
		<g:message code="agent_rates.ip.label" default="Ip" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="ip" required="" value="${agent_ratesInstance?.ip}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: agent_ratesInstance, field: 'rate', 'error')} required">
	<label for="rate">
		<g:message code="agent_rates.rate.label" default="Rate" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="rate" type="number" value="${agent_ratesInstance.rate}" required=""/>
</div>

