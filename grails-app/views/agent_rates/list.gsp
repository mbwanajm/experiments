
<%@ page import="com.mbwanajm.darproperty.Agent_rates" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'agent_rates.label', default: 'Agent_rates')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-agent_rates" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>

				<li><g:link class="create" controller="config" action="index"><span
						class="glyphicon glyphicon-wrench"></span></g:link></li>

				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-agent_rates" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table class="table table-responsive">
				<thead>
					<tr class="hd2">
					
						<th><g:message code="agent_rates.agent.label" default="Agent" /></th>
					
						<g:sortableColumn property="ip" title="${message(code: 'agent_rates.ip.label', default: 'Ip')}" />
					
						<g:sortableColumn property="rate" title="${message(code: 'agent_rates.rate.label', default: 'Rate')}" />
					
					</tr>
				</thead>
				<tbody class="hd2">
				<g:each in="${agent_ratesInstanceList}" status="i" var="agent_ratesInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${agent_ratesInstance.id}">${fieldValue(bean: agent_ratesInstance, field: "agent")}</g:link></td>
					
						<td>${fieldValue(bean: agent_ratesInstance, field: "ip")}</td>
					
						<td>${fieldValue(bean: agent_ratesInstance, field: "rate")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${agent_ratesInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
