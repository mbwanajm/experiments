<%@ page import="com.mbwanajm.darproperty.References_table" %>



<div class="fieldcontain ${hasErrors(bean: references_tableInstance, field: 'refNo', 'error')} required">
	<label for="refNo">
		<g:message code="references_table.refNo.label" default="Ref No" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="refNo" required="" value="${references_tableInstance?.refNo}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: references_tableInstance, field: 'date', 'error')} required">
	<label for="date">
		<g:message code="references_table.date.label" default="Date" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="date" precision="day"  value="${references_tableInstance?.date}"  />
</div>

