<html>
<head>
    <title>DarProperty>>Terms and Conditions</title>
    <meta name="layout" content="main"/>
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h3 align="center">${g.message(code: 'menu.termsAndConditions')}</h3>
        </div>
    </div>
</div>

<div class="container">

    <div class="row margin-top-20px small-font">

        <div class="col-lg-12">
            <div class='advertise'>

                <p style="text-align: justify;"><br/>The terms and conditions contained below govern the use
                of the website DarProperty.co.tz.<br/>If you do not agree to the terms and conditions set forth
                below, please do not use the website or any information contained therein. By using the
                website and/or the information contained therein you agree to the terms set forth below.
                </p>

                <p style="text-align: justify;">

                <h3>Use of Site:</h3></p>

                <div>
                    <ul>
                        <li>DarProperty.co.tz is only an Advertising website where real estate owners or their
                        agents and or their real estate brokers upload their property’s sale or rent
                        information with or without pictures on this site.</li>
                    </ul>
                    <ul>
                        <li>DarProperty.co.tz does not take any part in selling, buying,
                        or renting transactions.</li>
                    </ul>
                    <ul>

                        <li>All transactions are governed by your local laws. All offers and promotions on
                        this site are subject to change without notice.</li>
                    </ul>
                    <ul>
                        <li>DarProperty.co.tz provides you, our visitors, a venue for obtaining the information
                        contained in this site and has no control over the legality of any real estate for
                        sale or rent or other offers made by advertisers,
                        the ability of any of the advertisers to complete the sales in accordance with the
                        offers, or the quality of the real estate offered by the advertisers real estate
                        information for sale and or rent is directly published by the advertisers on this
                        website and hence DarProperty.co.tz takes no responsibility of any of their information
                        and prices and any mistakes in printing or typing. DarProperty.co.tz has no control
                        over whether Advertisers will honor the offers shown on this site and does not
                        guarantee the accuracy or completeness of the information contained on this site.</li>
                    </ul>
                    <ul>
                        <li>The advertised real estates and properties on this website may or may not be
                        available for sale and or rent. In the event you have a dispute with an Advertiser in
                        any way relating to this site or the use of information from this site, you agree to
                        waive and release DarProperty.co.tz from any and all claims, demands, actions, damages
                        (actual and consequential), losses, costs or expenses of every kind and nature, known
                        and unknown, disclosed and undisclosed relating to that dispute.</li>
                    </ul>

                    <p style="text-align: justify;">

                    <h3>Unacceptable use:</h3></br>
                    Client’s and visitors to the site must not use www.DarProperty.co.tz Website' service for
                    any of the following purposes:
                </p>
                    <ul>
                        <li>to commit or encourage a criminal offence;</li>

                    </ul>
                    <ul>
                        <li>to send or receive any material which is offensive, or which we believe maybe
                        abusive, indecent,</li>
                    </ul>
                    <ul>
                        <li>obscene or menacing, or in breach of confidence, copyright, privacy or any other
                        rights;</li>
                    </ul>
                    <ul>
                        <li>to do anything which is contrary to the acceptable use policies of any connected
                        networks and Internet standards;</li>
                    </ul>
                    <ul>
                        <li>to insert or knowingly or recklessly transmit or distribute a virus;</li>
                    </ul>

                    <ul>
                        <li>to hack into or disrupt any aspect of the Service. The client must not use their
                        account for the purpose of obtaining unauthorised access to any computer or service.
                        Circumvent, or attempt to seek to circumvent, any of the security safeguards of
                        DarProperty.co.tz Website.</li>
                    </ul>
                    <ul>
                        <li>To cause annoyance to subscribers or others;</li>
                    </ul>
                    <ul>
                        <li>To use any domain name or mailbox name so as to infringe upon the rights of any
                        other person whether in statute or common law, in a corresponding trade mark or
                        name;</li>
                    </ul>
                    <ul>
                        <li>To send any unsolicited advertising or other promotional material, commonly
                        referred to as "spam" by email or by any other electronic means, and the customer
                        must not post articles of a commercial nature to newsgroups which are inappropriate
                        to the subject of the article.</li>
                    </ul>
                    <ul>

                        <li>To send any unsolicited emails advertising a site or service on our network, or
                        allow any third party to do so.</li>
                    </ul>
                    <ul>
                        <li>To send email or any other type of electronic message with the intention or
                        result of affecting the performance or functionality of any computer facilities;</li>
                    </ul>
                    <ul>
                        <li>To email a person after they have specifically requested that they should not be
                        mailed. This applies to any automated mail system employed by clients or visitors to
                        the site.</li>
                    </ul>
                    <ul>
                        <li>To not send email or post articles with headers modified in such a way as to
                        disguise the true source of such mail or article. Clients must ensure that their
                        equipment cannot be used to relay mail for third parties.</li>
                    </ul>
                </div>

                <p style="text-align: justify;">Where applicable, clients are responsible for maintaining the
                confidentiality of their username and the password assigned to them on registration. As a
                result, they are fully responsible for all activities which occur under them. They must
                notify us immediately of any unauthorised use to which you become aware.</p>
                <hr>

                <p style="text-align: justify;">

                <h3>Limitation of Liability:</h3><br/>
                In NO event shall DarProperty.co.tz be liable, whether in Contract, Tort (Including
                Negligence) or otherwise, for any indirect, incidental or consequential damages (including
                lost savings, profit or business interruption even if notified in advance of such
                possibility) arising out of or pertaining to the subject matter of this agreement or the
                use of the site.</p>
                <hr>

                <p style="text-align: justify;">

                <h3>Jurisdiction; Venue.</h3><br/>This agreement shall be governed by and construed in accordance with the substantive laws of the United Republic of Tanzania, and any action shall be initiated and maintained exclusively in a forum of competent jurisdiction in the Republic of Tanzania.<br/>The above Terms of Use are subject to change without any notice. If you have any questions or comments, feel free to contact
                <span class="hd2"><br/><br/>GENERAL MANAGER,<br/>DarProperty.co.tz<br/>P.O BOX 105499<br/>DAR ES SALAAM<br/>TANZANIA<br/>TEL +255 22 2461447<br/>E-MAIL;</span>
            </div>
        </div>
    </div>
    <!--  <div class="row">
    <div class="col-sm-12">
        <h3><g:message code='home.propertyYouMightLike'/></h3>
        <g:render template="/property/property_advert" collection="${topProperty}" var="property"/>
        <p><g:link controller="property" action="showAllProperties"><h3><g:message
            code='home.moreProperty'/> »</h3></g:link></p>
</div>
    </div>
-->

</div>
<br>
<br>

</body>

</html>
