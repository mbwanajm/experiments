<%@ page import="com.mbwanajm.darproperty.Article" %>


<div class="container" align="center">
    <div class="panel panel-default">
        <div class="panel-header"></div>

        <div class="panel-body">

            <div class="fieldcontain ${hasErrors(bean: articleInstance, field: 'title', 'error')} ">
                <div class="col-sm-3" align="right">
                    <label for="title">
                        <g:message code="article.title.label" default="Title"/>

                    </label>
                </div>

                <div class="col-sm-9" align="left">
                    <g:textField name="title" value="${articleInstance?.title}" class="form-control"/>
                </div>
            </div>
            <br>

            <div class="fieldcontain ${hasErrors(bean: articleInstance, field: 'content', 'error')} ">
                <div class="col-sm-3" align="right">
                    <label for="content">
                        <g:message code="article.content.label" default="Content"/>

                    </label>
                </div>

                <div class="col-sm-9" align="left">

                    <g:textArea id='wysiwyg' name="content" cols='90' rows='50' style="height: 900px;" maxlength="20000"
                                value="${articleInstance?.content}" class="form-control"/>
                </div>

            </div>
            <br>

            <div class="fieldcontain ${hasErrors(bean: articleInstance, field: 'source', 'error')} ">
                <div class="col-sm-3" align="right">
                    <label for="source">
                        <g:message code="article.source.label" default="Source"/>

                    </label>
                </div>

                <div class="col-sm-9" align="left">
                    <g:textField name="source" value="${articleInstance?.source}" class="form-control"/>
                </div>
            </div>

            <br>

            <div class="fieldcontain ${hasErrors(bean: articleInstance, field: 'tag', 'error')} ">
                <div class="col-sm-3" align="right">
                    <label for="tag">
                        <g:message code="article.tag.label" default="Tag"/>

                    </label>
                </div>

                <div class="col-sm-9" align="left">
                    <g:textField name="tag" value="${articleInstance?.tag}" class="form-control"/>
                    <p class="hd4"><b>NB:</b> Tag can be:  main / mortgage / insurance / exhibition</p>
                </div>
            </div>

            <div class="fieldcontain ${hasErrors(bean: articleInstance, field: 'image', 'error')} ">
                <div class="col-sm-3 " align="right">
                    <label for="image">
                        <g:message code="advert.image.label" default="Image" />
                    </label>
                </div>
                <div class="col-sm-9" align="left">
                    <input type="file" id="image" name="image" class="form-control"/>
                </div>
            </div>
            <br>
        </div>

    </div>
</div>
