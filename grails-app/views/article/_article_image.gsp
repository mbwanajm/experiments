<g:if test="${article.image}">
    <img src="<g:createLink controller='image' action='renderArticlePhoto'
                            id='${article.id}'/>" alt="Article Icon" class="img-responsive " />
</g:if>
<g:else>
    <img src="${resource(dir: 'img', file: 'article.png')}"
         class="img-responsive "/>
</g:else>