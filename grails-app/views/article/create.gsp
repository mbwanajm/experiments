<%@ page import="com.mbwanajm.darproperty.Article" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'article.label', default: 'Article')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'redactor.css')}"/>
    %{--<g:javascript src="lib/redactor.js"/>--}%
    %{--<g:javascript src="lib/wysiwyg.js"/>--}%

    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div class="container" align="left">
			<div class="row">
			<div class="col-sm-5">
			
			<a href="#list-article" class="skip" tabindex="-1" title="<g:message code="default.link.skip.label" default="Skip to content&hellip;"/>">
		<span class="glyphicon glyphicon-refresh"></span></a>
	
			</div>
			
			</div>
			<br>

<div class="nav" role="navigation">
    <ul>
        <li class="btn btn-default">	<g:link controller ="config" action=""><span class="glyphicon glyphicon-wrench"></span> </g:link></li>
        <li class="btn btn-default"><span class="glyphicon glyphicon-th-list"></span> 
        <g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="create-article" class="content scaffold-create" role="main">
    <h1><span class="glyphicon glyphicon-plus"></span> <g:message code="default.create.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${articleInstance}">
        <ul class="errors" role="alert">
            <g:eachError bean="${articleInstance}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    
    <div class="row">
			<div class="col-sm-12">
    <g:form action="save" enctype="multipart/form-data">
        <fieldset class="form">
            <g:render template="form"/>
        </fieldset>
        <fieldset class="buttons">
            <g:submitButton name="create"  class="btn btn-success"
                            value="${message(code: 'default.button.create.label', default: 'Create')}"/>
        </fieldset>
    </g:form>
    </div>
    </div>
    
</div>
</div>
<br>


%{--Article layout--}%
<g:javascript src="lib/jquery-2.js"/>
<g:javascript src="lib/jquery-ui.js"/>
<g:javascript src="lib/redactor.js"/>
<g:javascript src="lib/wysiwyg.js"/>
</body>
</html>
