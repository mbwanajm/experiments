<html>
<head>
    <title>DarProperty >> Read Article</title>
    <meta name="layout" content="main"/>
</head>

<body>
<div class="container">
    <div class="row in-title">
        <div class="col-sm-12">
            <h3 align="center"><g:message code="read"></g:message> <g:message code="home.article"></g:message></h3>
        </div>
    </div>
</div>

<div class="container">
    <div class="row ">
        <div class="col-sm-2 offset"></div>

        <div class="col-sm-8">

            <div class='outlined'
                 style=" background-color: #f0f0f0;">
                <div class="row">

                    <div class="col-sm-12" align="center">
                        <h4 class="text-uppercase text-center">${article.title}</h4>
                      <g:render template="article_image"
                                  model="[article: article]"></g:render>
                        <br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">${article.content}</div>


                </div>

                <div class="row">
                    <div class="col-sm-9 " style="border:1px solid #EEE; padding:5px;">
                        <b>by: ${article.source}</b>
                        <br>${article.dateCreated.format("dd MMM yyyy")}
                        <br> <span class="glyphicon glyphicon-tags"></span> <small>${article.tag}</small>
                        <br>
                    </div>
                </div>

                <div class="row">
                    <br>

                    <div class="col-sm-9 ">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-2">
                                    <!-- WHT -->
                                    <a href="whatsapp://send?text=${article.title}: ${(article.content[0..100]).replaceAll("<(.|\n)*?>", '')}.. http://www.darproperty.co.tz/article/readArticle/${article.id}"
                                       data-action="share/whatsapp/share"
                                       target="_blank">
                                        <img src="${resource(dir: 'bootstrap/imgs/', file: 'whatsappgreen.png')}"
                                             alt="Whatsapp" class="hidden-desktop" width="30" height="35"
                                             title="Share with Whatsapp"/></a>

                                </div>

                                <div class="col-sm-2">
                                    <!-- TWT -->
                                    <a href="https://twitter.com/share"
                                       class="twitter-share-button" {count}
                                       data-url="http://www.darproperty.co.tz/article/readArticle/${article.id}"
                                       data-text="${article.title} "
                                       data-via="thedarproperty"
                                       data-related="thedarproperty"
                                       data-hashtags="DarArticle"
                                       title="Share On Twitter">Tweet</a>
                                </div>

                                <div class="col-sm-2">
                                    <!-- FB -->
                                    <div class="fb-like"
                                         data-href="http://www.darproperty.co.tz/article/readArticle/${article.id}"
                                         data-layout="standard"
                                         data-action="like"
                                         data-show-faces="true"
                                         data-share="true"></div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-7">
                                    <br>
                                    <!-- START -->
                                    <h4><span class="glyphicon glyphicon-comment"></span><b>Comments:</b></h4>

                                    <div id="disqus_thread">

                                    </div>
                                    <script>
                                        /**
                                         * RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                                         * LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
                                         */
                                        /*
                                         var disqus_config = function () {
                                         this.page.url = PAGE_URL; // Replace PAGE_URL with your page's canonical URL variable
                                         this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                                         };
                                         */
                                        (function () { // DON'T EDIT BELOW THIS LINE
                                            var d = document, s = d.createElement('script');

                                            s.src = '//darpropertyforum.disqus.com/embed.js';

                                            s.setAttribute('data-timestamp', +new Date());
                                            (d.head || d.body).appendChild(s);
                                        })();
                                    </script>
                                    <noscript>Please enable JavaScript to view the <a
                                            href="https://disqus.com/?ref_noscript"
                                            rel="nofollow">comments powered by Disqus.</a>
                                    </noscript>
                                    <!-- END -->

                                    <br>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <br>





                <br>

            </div>
        </div>

        <div class="col-sm-2 offset"></div>
    </div>

    <div class="row">
        <span class="wid-view-more">Other <span class="">Articles</span><small class=" pull-right">
            <g:link controller="article" action="articleLibrary">
                <i class="fa fa-book"></i>
                <g:message code="Article-Library"/>:
            </g:link>
        </small>
        </span><hr class="style-one">
        <div id="other" class="container">
            <div class="row">

                <div class="col-sm-12"><g:render template="/article/article_teaser" collection="${articlesToShow}"
                                                 var="article"/></div>

            </div>

            <div class="row pagination" align="center">
                <g:paginate total="${numberOfArticles}"/>
                <br>
                <br>
            </div>
        </div>

    </div>
</div>

</body>

</html>