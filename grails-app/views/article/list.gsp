<%@ page import="com.mbwanajm.darproperty.Article" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'article.label', default: 'Article')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="container" align="left">
			<div class="row">
			<div class="col-sm-5">
			
			<a href="#list-article" class="skip" tabindex="-1" title="<g:message code="default.link.skip.label" default="Skip to content&hellip;"/>">
		<span class="glyphicon glyphicon-refresh"></span></a>
	
			</div>
			
			</div>
	

<div class="nav" role="navigation">
    <ul>
        <li class="btn btn-default">	<g:link controller ="config" action=""><span class="glyphicon glyphicon-wrench"></span> </g:link></li>
        <li  class="btn btn-default"> <span class="glyphicon glyphicon-plus"></span>
        <g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="list-article" class="content scaffold-list" role="main">
    <h1><span class="glyphicon glyphicon-list"></span> <g:message code="default.list.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <table class="table">
        <thead>
        <tr>

            <g:sortableColumn property="title" title="${message(code: 'article.title.label', default: 'Title')}"/>

            <g:sortableColumn property="content" title="${message(code: 'article.content.label', default: 'Content')}"/>

            <g:sortableColumn property="dateCreated"
                              title="${message(code: 'article.dateCreated.label', default: 'Date Created')}"/>

            <g:sortableColumn property="source" title="${message(code: 'article.source.label', default: 'Source')}"/>
            
             <g:sortableColumn property="tag" title="${message(code: 'article.tag.label', default: 'Tag')}"/>

        </tr>
        </thead>
        <tbody>
        
        <g:each in="${articleInstanceList}" status="i" var="articleInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                <td class="hd2">${i + 1 }) <g:link action="show"
                            id="${articleInstance.id}">${fieldValue(bean: articleInstance, field: "title")}</g:link></td>

                <td class="hd2">${fieldValue(bean: articleInstance, field: "content")[0..98]}...</td>

                <td class="hd2"><g:formatDate date="${articleInstance.dateCreated}"/></td>

                <td class="hd2">${fieldValue(bean: articleInstance, field: "source")}</td>
                
                <td class="hd2">${fieldValue(bean: articleInstance, field: "tag")}</td>

            </tr>
        </g:each>
       
        </tbody>
    </table>

    <div class="pagination">
        <g:paginate total="${articleInstanceTotal}"/>
    </div>
</div>
</div>
<br>
</body>
</html>
