<%--
  Created by IntelliJ IDEA.
  User: DAR PROPERTY
  Date: 3/18/2016
  Time: 10:26 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <title>DarProperty>>Article Library</title>
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h3 align="center"><g:message code="library"></g:message></h3>
        </div>
    </div>

    <div class="margine2"></div>
</div>

<div class="container">

    <g:render template="../search/article_search"></g:render>




    <div class="row">

        <g:if test="${searchResult?.totalCount}">
            <div class="margine2"></div>

            <p class="hd1"><g:message code="article.search.article"></g:message> <span class="hd2"><g:message
                    code="article.search.results"></g:message></span>
            <hr class="style-one"></p>
            <p class="alert alert-success" align="center">${searchResult?.totalCount} results Found !</p>
            <g:each var="article" in="${searchResult}">
                <g:render template="/article/article_result" model="[article: article]"></g:render>
            </g:each>

        </g:if>
        <g:if test="${searchResult?.totalCount == 0}">

            <p class="alert alert-danger" align="center">No results Found !</p>
        </g:if>

    </div>

    <div class="row" align="center">
        <div class="col-md-12 pagination">
            <g:if test="${searchResult?.totalCount}">



                    <g:paginate controller="search" action="searchArticle"
                                params="${searchTerms}"
                                total="${searchResult?.totalCount}"
                                prev="&lt;&lt;  " next="&gt;&gt;"/>


            </g:if>
        </div>

    </div>


    <div class="margine2"></div>
    <g:if test="${!searchResult?.totalCount}">
        <div class="row">
            <span class="wid-view-more"><span><g:message code="home.article"></g:message></span>
        </span><hr class="style-one">
        <g:render template="../article/article_teaser" collection="${articlesToShow}"></g:render>

        </div>
        <div class="row pagination" align="center">

            <g:paginate total="${numberOfArticles}"/>
            <br>
            <br>
        </div>
    </g:if>
</div>

</body>
</html>