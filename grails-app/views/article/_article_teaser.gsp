<div class="col-sm-6 col-md-3">
    <div class="thumbnail"
         style="box-shadow: 0px 0px 0px #000; border: 1px solid rgba(143, 143, 143, 0.5); padding: 10px; height: 280px;">
        <g:link controller="article" action="readArticle" id="${article.id}">
            <g:if test="${article.image}">
                <img src="<g:createLink controller='image' action='renderArticlePhoto'
                                        id='${article.id}'/>" alt="Article Icon" class="img-responsive"
                     style="height:110px;"/>
            </g:if>
            <g:else>
                <img src="${resource(dir: 'img', file: 'article.png')}" class="img-responsive" style="height:110px;"/>
            </g:else>
        </g:link>


        <div class="caption">
            <p class="articlehead">${article.title}</p>

            <p class="articlebody">${(article.content[0..98]).replaceAll("<(.|\n)*?>", '')} ..<br> <span class="hd2"
                                                                                                         style="font-size:10pt;"><b>|</b>

                <g:link controller="article" action="readArticle" id="${article.id}"><g:message
                        code="home.newsAndArticles.readmore"/></g:link> </span></p>
            <i><span style="font-size: 9pt;color:#c0c0c0">(

            <g:dateFromNow date="${article.dateCreated}"/> )</span></i>

        </div>
    </div>
</div>
    