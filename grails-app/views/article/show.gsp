<%@ page import="com.mbwanajm.darproperty.Article" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'article.label', default: 'Article')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
	
	<div class="container">
			<div class="row">
			<div class="col-sm-5">
			
			<a href="#show-article" class="skip" tabindex="-1" title="<g:message code="default.link.skip.label" default="Skip to content&hellip;"/>">
		<span class="glyphicon glyphicon-refresh"></span></a>
	
			</div>
			
			
	
	<br>

<div class="nav" role="navigation">
    <ul>
       <li class="btn btn-default">	<g:link controller ="config" action=""><span class="glyphicon glyphicon-wrench"></span> </g:link></li>
        <li class="btn btn-default"><span class="glyphicon glyphicon-th-list"></span>
        <g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
        <li  class="btn btn-default"> <span class="glyphicon glyphicon-plus"></span>
        <g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="show-article" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <ol class="property-list article ">

        <g:if test="${articleInstance?.title}">
         <h4 class="hd2" align="center"><b>
            <li class="fieldcontain">
           
                <span id="title-label" class="property-label"><g:message code="article.title.label"
                                                                         default="Title"/></span>

                <span class="property-value" aria-labelledby="title-label"><g:fieldValue bean="${articleInstance}"
                                                                                         field="title"/></span>
				
            </li>
            </b>
            </h4>
        </g:if>
<div class="panel panel-default">
<div class="body d1">
        <g:if test="${articleInstance?.content}">
            <li class="fieldcontain hd1">
                <span id="content-label" class="property-label"><g:message code="article.content.label"
                                                                           default="Content"/></span>

                <span class="property-value" aria-labelledby="content-label"><g:fieldValue bean="${articleInstance}"
                                                                                           field="content"/></span>

            </li>
        </g:if>
        </div>
</div>
        <g:if test="${articleInstance?.dateCreated}">
            <li class="fieldcontain hd2">
                <span id="dateCreated-label" class="property-label"><g:message code="article.dateCreated.label"
                                                                               default="Date Created"/></span>

                <span class="property-value" aria-labelledby="dateCreated-label">: <g:formatDate
                        date="${articleInstance?.dateCreated}"/></span>

            </li>
        </g:if>

        <g:if test="${articleInstance?.source}">
            <li class="fieldcontain hd2">
                <span id="source-label" class="property-label"><g:message code="article.source.label"
                                                                          default="Source"/></span>

                <span class="property-value" aria-labelledby="source-label">: <g:fieldValue bean="${articleInstance}"
                                                                                          field="source"/></span>

            </li>
        </g:if>
        
        <g:if test="${articleInstance?.tag}">
            <li class="fieldcontain hd2">
                <span id="tag-label" class="property-label"><g:message code="article.tag.label"
                                                                          default="tag"/></span>

                <span class="property-value" aria-labelledby="tag-label">: <g:fieldValue bean="${articleInstance}"
                                                                                          field="tag"/></span>

            </li>
        </g:if>

        <g:if test="${articleInstance?.image}">
            <li class="fieldcontain hd2">
                <span id="image-label" class="property-label"> <g:message code="article.image.label"
                                                                       default="image"/></span>
               <!-- ${fieldValue(bean: articleInstance, field: "image")} -->
                <img src="<g:createLink controller='image' action='renderArticlePhoto'
                                        id='${articleInstance?.id}'/>" alt="Article Icon" width="50" />
            </li>
        </g:if>

    </ol>
    <div class="row">
	<div class="col-sm-12">
    <g:form>
    
			
        <fieldset class="buttons">
            <g:hiddenField name="id" value="${articleInstance?.id}"/>
            <g:link  class="btn btn-warning" action="edit" id="${articleInstance?.id}"><g:message code="default.button.edit.label"
                                                                                      default="Edit"/></g:link>
            <g:actionSubmit class="btn btn-danger"  action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
        </fieldset>
    </g:form>
   </div>
			</div>
</div>

<br>
</body>
</html>
