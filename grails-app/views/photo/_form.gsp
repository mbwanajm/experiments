<%@ page import="com.mbwanajm.darproperty.Photo" %>



<div class="fieldcontain ${hasErrors(bean: photoInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="photo.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${photoInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: photoInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="photo.description.label" default="Description" />
		
	</label>
	<g:textArea name="description" cols="40" rows="5" maxlength="300" value="${photoInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: photoInstance, field: 'image', 'error')} required">
	<label for="image">
		<g:message code="photo.image.label" default="Image" />
		<span class="required-indicator">*</span>
	</label>
	<input type="file" id="image" name="image" />
</div>

