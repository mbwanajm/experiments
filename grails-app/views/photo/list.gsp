
<%@ page import="com.mbwanajm.darproperty.Photo" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'photo.label', default: 'Photo')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-photo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-photo" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table class="table">
				<thead>
					<tr>
					
						<g:sortableColumn property="name" title="${message(code: 'photo.name.label', default: 'Name')}" />
					
						<g:sortableColumn property="description" title="${message(code: 'photo.description.label', default: 'Description')}" />
					
						<g:sortableColumn property="image" title="${message(code: 'photo.image.label', default: 'Image')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${photoInstanceList}" status="i" var="photoInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td class='hd2'><g:link action="show" id="${photoInstance.id}">${fieldValue(bean: photoInstance, field: "name")}</g:link></td>
					
						<td class="hd2">${fieldValue(bean: photoInstance, field: "description")}</td>
					
						<td> <img src="<g:createLink controller='image'
                                action='photoView'
                                id='${photoInstance.id}'/>" width="200" > <!-- ${fieldValue(bean: photoInstance, field: "image")} --></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${photoInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
