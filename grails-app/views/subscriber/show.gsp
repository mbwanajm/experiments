<%@ page import="com.mbwanajm.darproperty.Subscriber" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'subscriber.label', default: 'Subscriber')}"/>
    <title>DarProperty >> <g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-sm-offset-1 col-sm-7">
            <h1><span class="glyphicon glyphicon-envelope"></span> Thank You For Subscribing</h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-offset-1 col-sm-7">
            <ul class="property-list subscriber">

                <g:if test="${subscriberInstance?.firstname}">
                    <li class="fieldcontain">
                        <span id="firstname-label" class="property-label hd2"><g:message
                                code="subscriber.firstname.label"
                                default="Firstname"/>:</span>

                        <span class="property-value" aria-labelledby="firstname-label"><g:fieldValue
                                bean="${subscriberInstance}" field="firstname"/></span>

                    </li>
                </g:if>

                <g:if test="${subscriberInstance?.lastname}">
                    <li class="fieldcontain">
                        <span id="lastname-label" class="property-label hd2"><g:message code="subscriber.lastname.label"
                                                                                        default="Lastname"/>:</span>

                        <span class="property-value" aria-labelledby="lastname-label"><g:fieldValue
                                bean="${subscriberInstance}" field="lastname"/></span>

                    </li>
                </g:if>

                <g:if test="${subscriberInstance?.email}">
                    <li class="fieldcontain">
                        <span id="email-label" class="property-label hd2"><g:message code="subscriber.email.label"
                                                                                     default="Email"/>:</span>

                        <span class="property-value" aria-labelledby="email-label"><g:fieldValue
                                bean="${subscriberInstance}" field="email"/></span>

                    </li>
                </g:if>

            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-offset-1 col-sm-7">
            <g:form>
                <fieldset class="buttons">
                    <g:hiddenField name="id" value="${subscriberInstance?.id}"/>
                    <g:link class="edit btn btn-warning" action="edit" id="${subscriberInstance?.id}"><g:message
                            code="default.button.edit.label" default="Edit"/></g:link>
                    <g:actionSubmit class="delete btn btn-danger" action="delete"
                                    value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                    onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                </fieldset>
            </g:form>
            <br><a class="home" href="${createLink(uri: '/')}"><span
                class="glyphicon glyphicon-hand-left"></span> <g:message code="application.back"/></a>
        </div>
    </div>

</div>
</body>
</html>
