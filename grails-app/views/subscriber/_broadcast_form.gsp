<div id="broadcasting" class="container">
    <hr class="style-one">
    <div class="row">
        <div class="col-lg-offset-2 col-lg-8">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <span class="glyphicon glyphicon-envelope"></span> E-mail Broadcasting
                </div>

                <div class="panel-body">
                    <g:form method="post" action="email_broadcasting" class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-lg-4">
                                Subject:
                            </label>

                            <div class="col-lg-8">
                                <input type="text" class="form-control" name="subject" maxlength="30" autocomplete="off"
                                       value=""  required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-4">
                                Message:
                            </label>

                            <div class="col-lg-8">
                                <textarea name="message" cols="50" rows="10">

                                </textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-4">
                                All:
                            </label>

                            <div class="col-lg-2 pull-left">
                                <input type="radio" class="form-control" name="receiver" value="all" maxlength="30">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-4">
                                Subscribers only:
                            </label>

                            <div class="col-lg-2 pull-left">
                                <input type="radio" class="form-control" name="receiver" value="subscribers" maxlength="30">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-4">
                                Agents only:
                            </label>

                            <div class="col-lg-2 pull-left">
                                <input type="radio" class="form-control" name="receiver" value="agents" maxlength="30">
                            </div>
                        </div>



                        <div class="form-group">

                            <div class="col-lg-offset-4 col-lg-8">
                                <input type="submit" class="btn btn-primary" value="Broadcast" name="broadcast"
                                       id="broadcast">
                            </div>
                        </div>

                        <div id="loading4">
                            <div class="form-group" align="center">
                                <div class="col-lg-offset-4 col-lg-8">
                                    <img src="${resource(dir: 'bootstrap/imgs/', file: '3.gif')}"/>
                                    <span class="hd3"><g:message code="application.sending"></g:message>...</span>
                                    <!-- loading image -->
                                </div>

                            </div>

                        </div>

                    </g:form>
                </div>
            </div>
        </div>
    </div>
</div>
