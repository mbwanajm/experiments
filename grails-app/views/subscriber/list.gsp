<%@ page import="com.mbwanajm.darproperty.Subscriber" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'subscriber.label', default: 'Subscriber')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="container">
    <a href="#list-subscriber" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                                     default="Skip to content&hellip;"/></a>


    <div class="nav" role="navigation">
        <ul>
            <li><g:link class="create" controller="config" action="index"><span
                    class="glyphicon glyphicon-wrench"></span></g:link></li>

            <li><a href="#broadcasting"><span class="glyphicon glyphicon-envelope"></span> Broadcast E-mails</a></li>

        </ul>
    </div>

    <div id="list-subscriber" class="content scaffold-list" role="main">
        <h1><g:message code="default.list.label" args="[entityName]"/></h1>
        <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
        </g:if>
        <table class="table table-responsive">
            <thead>
            <tr>

                <g:sortableColumn property="firstname"
                                  title="${message(code: 'subscriber.firstname.label', default: 'Firstname')}"/>

                <g:sortableColumn property="lastname"
                                  title="${message(code: 'subscriber.lastname.label', default: 'Lastname')}"/>

                <g:sortableColumn property="email"
                                  title="${message(code: 'subscriber.email.label', default: 'Email')}"/>

            </tr>
            </thead>
            <tbody>
            <g:each in="${subscriberInstanceList}" status="i" var="subscriberInstance">
                <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                    <td><g:link action="show"
                                id="${subscriberInstance.id}">${fieldValue(bean: subscriberInstance, field: "firstname")}</g:link></td>

                    <td class="hd2">${fieldValue(bean: subscriberInstance, field: "lastname")}</td>

                    <td class="hd2">${fieldValue(bean: subscriberInstance, field: "email")}</td>

                </tr>
            </g:each>
            </tbody>
        </table>

        <div class="pagination">

            <g:paginate total="${subscriberInstanceTotal}"/>
        </div>
    </div>
</div>

<div class="margine2"></div>

<!-- Broadcasting menu-->
<g:render template="broadcast_form"></g:render>

</body>
</html>
