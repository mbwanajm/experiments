<%@ page import="com.mbwanajm.darproperty.Subscriber" %>



<div class="fieldcontain ${hasErrors(bean: subscriberInstance, field: 'firstname', 'error')} required">
	<label for="firstname">
		<g:message code="subscriber.firstname.label" default="Firstname" />*
		
	</label>
	<g:textField name="firstname" class="form-control" required="" autocomplete="off" value="${subscriberInstance?.firstname}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: subscriberInstance, field: 'lastname', 'error')} required">
	<label for="lastname">
		<g:message code="subscriber.lastname.label" default="Lastname" />
		
	</label>
	<g:textField name="lastname" class="form-control" autocomplete="off" value="${subscriberInstance?.lastname}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: subscriberInstance, field: 'email', 'error')} required">
	<label for="email">
		<g:message code="subscriber.email.label" default="Email" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="email" class="form-control" required="" autocomplete="off" value="${subscriberInstance?.email}"/>
</div>
<br>

