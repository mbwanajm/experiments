<%@ page import="com.mbwanajm.darproperty.Banks" %>



<div class="fieldcontain ${hasErrors(bean: banksInstance, field: 'bankname', 'error')} ">
	<label for="bankname">
		<g:message code="banks.bankname.label" default="Bankname" />
		
	</label>
	<g:textField name="bankname" value="${banksInstance?.bankname}"/>
</div>
<div class="fieldcontain ${hasErrors(bean: banksInstance, field: 'rate', 'error')} required">
	<label for="rate">
		<g:message code="banks.rates.label" default="Rates" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="rates" value="${fieldValue(bean: banksInstance, field: 'rates')}" required="" />
</div>

