
<%@ page import="com.mbwanajm.darproperty.Banks" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'banks.label', default: 'Banks')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-banks" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="create" controller="config" action="index"> <span class="glyphicon glyphicon-wrench"></span></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-banks" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table class="table table-responsive">
				<thead>
					<tr class="hd2">
					
						<g:sortableColumn property="bankname" title="${message(code: 'banks.bankname.label', default: 'Bankname')}" />
						<g:sortableColumn property="rates" title="${message(code: 'banks.rates.label', default: 'rates')}" />

					</tr>
				</thead>
				<tbody class="hd2">
				<g:each in="${banksInstanceList}" status="i" var="banksInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${banksInstance.id}">${fieldValue(bean: banksInstance, field: "bankname")}</g:link></td>
						<td>${fieldValue(bean: banksInstance, field: "rates")}</td>

					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${banksInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
