<%@ page import="com.mbwanajm.darproperty.Currency" %>



<div class="fieldcontain ${hasErrors(bean: currencyInstance, field: 'usd_value', 'error')} required">
	<label for="usd_value">
		<g:message code="currency.usd_value.label" default="Usdvalue" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="usd_value" value="${fieldValue(bean: currencyInstance, field: 'usd_value')}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: currencyInstance, field: 'country', 'error')} required">
	<label for="country">
		<g:message code="currency.country.label" default="Country" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="country" required="" value="${currencyInstance?.country}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: currencyInstance, field: 'tag', 'error')} ">
	<label for="tag">
		<g:message code="currency.tag.label" default="Tag" />
		
	</label>
	<g:textField name="tag" value="${currencyInstance?.tag}"/>
</div>

