
<%@ page import="com.mbwanajm.darproperty.Currency" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'currency.label', default: 'Currency')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-currency" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="create" controller="config" action="index"> <span class="glyphicon glyphicon-wrench"></span></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-currency" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table class="table table-cell">
				<thead>
					<tr class="hd2">
					
						<g:sortableColumn property="usd_value" title="${message(code: 'currency.usd_value.label', default: 'Usdvalue')}" />
					
						<g:sortableColumn property="country" title="${message(code: 'currency.country.label', default: 'Country')}" />
					
						<g:sortableColumn property="tag" title="${message(code: 'currency.tag.label', default: 'Tag')}" />
					
					</tr>
				</thead>
				<tbody class="hd2">
				<g:each in="${currencyInstanceList}" status="i" var="currencyInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${currencyInstance.id}">${fieldValue(bean: currencyInstance, field: "usd_value")}</g:link></td>
					
						<td>${fieldValue(bean: currencyInstance, field: "country")}</td>
					
						<td>${fieldValue(bean: currencyInstance, field: "tag")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${currencyInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
