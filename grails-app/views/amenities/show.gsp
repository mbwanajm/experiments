
<%@ page import="com.mbwanajm.darproperty.Amenities" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'amenities.label', default: 'Amenities')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
	<div class="container">
		
	
			<div class="row">
			<div class="col-sm-5">
			
			<a href="#list-amenities" class="skip" tabindex="-1" title="<g:message code="default.link.skip.label" default="Skip to content&hellip;"/>">
		<span class="glyphicon glyphicon-refresh"></span></a>
	
			</div>
			
			</div>
	
	<br>
		
		
		<div class="nav" role="navigation">
			<ul>
				<li class="btn btn-default">	<g:link controller ="config" action=""><span class="glyphicon glyphicon-wrench"></span> </g:link></li>
				<li  class="btn btn-default"> <span class="glyphicon glyphicon-plus"></span>
				<g:link class="create hd2" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
				<li class="btn btn-default"><span class="glyphicon glyphicon-th-list"></span>  
				<g:link class="list hd2" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		
	
		<div id="show-amenities" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list amenities">
			
				<g:if test="${amenitiesInstance?.title}">
				<li class="fieldcontain">
					<span id="title-label" class="property-label"><g:message code="amenities.title.label" default="Title" /></span>
					
						<span class="property-value" aria-labelledby="title-label"><g:fieldValue bean="${amenitiesInstance}" field="title"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${amenitiesInstance?.holder}">
				<li class="fieldcontain">
					<span id="holder-label" class="property-label"><g:message code="amenities.holder.label" default="Holder" /></span>
					
						<span class="property-value" aria-labelledby="holder-label"><g:fieldValue bean="${amenitiesInstance}" field="holder"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${amenitiesInstance?.point}">
				<li class="fieldcontain">
					<span id="point-label" class="property-label"><g:message code="amenities.point.label" default="Point" /></span>
					
						<span class="property-value" aria-labelledby="point-label"><g:fieldValue bean="${amenitiesInstance}" field="point"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${amenitiesInstance?.visible}">
				<li class="fieldcontain">
					<span id="visible-label" class="property-label"><g:message code="amenities.visible.label" default="Visible" /></span>
					
						<span class="property-value" aria-labelledby="visible-label"><g:fieldValue bean="${amenitiesInstance}" field="visible"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<div class="row">
			<div class="col-sm-12">
			
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${amenitiesInstance?.id}" />
					<g:link class="btn btn-warning" action="edit" id="${amenitiesInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="btn btn-danger" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
			</div>
			</div>
		</div>
		</div>
		<br>
		<br>
	</body>
</html>
