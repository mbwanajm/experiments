<%@ page import="com.mbwanajm.darproperty.Amenities" %>


<div class="container" align="center">
<div class="panel panel-default">
 <div class="panel-header"></div>
  <div class="panel-body">
   <div class="fieldcontain ${hasErrors(bean: amenitiesInstance, field: 'title', 'error')} required">
   <div class="col-sm-3" align="right">
	<label for="title">
		<g:message code="amenities.title.label" default="Title" />
		<span class="required-indicator">*</span>
	</label>
	</div>
	<div class="col-sm-9" align="left">
	<g:textField name="title" maxlength="200" required="" value="${amenitiesInstance?.title}" class="form-control"/>
	</div>
</div>

<div class="fieldcontain ${hasErrors(bean: amenitiesInstance, field: 'holder', 'error')} ">
	<div class="col-sm-3" align="right">
	<label for="holder">
		<g:message code="amenities.holder.label" default="Holder" />
		
	</label>
	</div>
	<div class="col-sm-9" align="left">
	<g:textField name="holder" value="${amenitiesInstance?.holder}" class="form-control"/>
	</div>
	
</div>

<div class="fieldcontain ${hasErrors(bean: amenitiesInstance, field: 'point', 'error')} required">
 <div class="col-sm-3" align="right">
	<label for="point">
		<g:message code="amenities.point.label" default="Point" />
		<span class="required-indicator">*</span>
	</label>
	</div>
	<div class="col-sm-9" align="left">
	<g:field name="point" value="${fieldValue(bean: amenitiesInstance, field: 'point')}" required="" class="form-control"/>
	</div>
</div>

<div class="fieldcontain ${hasErrors(bean: amenitiesInstance, field: 'visible', 'error')} required">
<div class="col-sm-3" align="right">
	<label for="visible">
		<g:message code="amenities.visible.label" default="Visible" />
		<span class="required-indicator">*</span>
	</label>
	</div>
	<div class="col-sm-9" align="left">
	<g:field name="visible" type="number" value="${amenitiesInstance.visible}" required="" class="form-control"/>
	<br><small class='hd4'> <b>NB:</b> 1 = Visible 0 = Invisible</small>
	</div>
</div>


  </div>
 
</div>
</div>
