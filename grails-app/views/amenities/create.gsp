<%@ page import="com.mbwanajm.darproperty.Amenities" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'amenities.label', default: 'Amenities')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
			<div class="container" align="left">
			<div class="row">
			<div class="col-sm-5">
			
			<a href="#list-amenities" class="skip" tabindex="-1" title="<g:message code="default.link.skip.label" default="Skip to content&hellip;"/>">
		<span class="glyphicon glyphicon-refresh"></span></a>
	
			</div>
			
			</div>
	</div>
	<br>
	
	
		<div class="nav" role="navigation">
			<ul>
				
				<li class="btn btn-default">	<g:link controller ="config" action=""><span class="glyphicon glyphicon-wrench"></span> </g:link></li>
				<li class="btn btn-default"><span class="glyphicon glyphicon-th-list"></span>  
				<g:link class="list hd2" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div class="container">
		<div class="row">
		<div id="create-amenities"  role="main" align="left">
			<h1><g:message code="default.create.label" args="[entityName]" /> <span class="glyphicon glyphicon-plus"></span></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${amenitiesInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${amenitiesInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
		
		</div>
		
			<div class="row">
			<div class="col-sm-12">
			
			<g:form action="save" >
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons">
					<g:submitButton name="create" class="btn btn-success" value="${message(code: 'default.button.create.label', default: 'Create')}" />
				</fieldset>
			</g:form>
			</div>
			
			</div>
		</div>
		</div>
		<br>
		<br>
	</body>
</html>
