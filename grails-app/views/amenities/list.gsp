
<%@ page import="com.mbwanajm.darproperty.Amenities" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'amenities.label', default: 'Amenities')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="container" align="left">
			<div class="row">
			<div class="col-sm-5">

			<a href="#list-amenities" class="skip" tabindex="-1" title="<g:message code="default.link.skip.label" default="Skip to content&hellip;"/>">
		<span class="glyphicon glyphicon-refresh"></span></a>

			</div>
			<div class="col-sm-6" align="right" style="border:1px groove #c0c0c0; border-radius:12px 0px 0px 12px;padding:2px;">
			<div class="col-sm-6" align="right"><a href="#addTag" class="btn btn-default"><span class="glyphicon glyphicon-pushpin"></span> Add Tags</a></div>
			<div class="col-sm-6" align="right"><a href="#updateTag" class="btn btn-default"><span class="glyphicon glyphicon-pushpin"></span> Update Tags</a></div>
			<div class="col-sm-6" align="right"><a href="#joinAmenitiesAndTag" class="btn btn-default"><span class="glyphicon glyphicon-pushpin"></span> Join Amenities & Tags</a></div>
			<div class="col-sm-6" align="right"><a href="#disjoinAmenitiesAndTag" class="btn btn-default"><span class="glyphicon glyphicon-pushpin"></span> Dis-join Amenities & Tags</a></div>
			</div>

			</div>




		<div class="nav" role="navigation">
			<ul>
				<li class="btn btn-default">	<g:link controller ="config" action=""><span class="glyphicon glyphicon-wrench"></span> </g:link></li>
				<li  class="btn btn-default"> <span class="glyphicon glyphicon-plus"></span>
				<g:link class="create hd2" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>




		<div id="list-amenities" class="table-responsive" role="main">
			<h1><span class="glyphicon glyphicon-list"></span> <g:message code="default.list.label" args="[entityName]" /> </h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table class="table">
				<thead >
					<tr >

						<g:sortableColumn property="title" title="${message(code: 'amenities.title.label', default: 'Title')}" />

						<g:sortableColumn property="holder" title="${message(code: 'amenities.holder.label', default: 'Holder')}" />

						<g:sortableColumn property="point" title="${message(code: 'amenities.point.label', default: 'Point')}" />

						<g:sortableColumn property="visible" title="${message(code: 'amenities.visible.label', default: 'Visible')}" />

					</tr>
				</thead>
				<tbody>
				<g:each in="${amenitiesInstanceList}" status="i" var="amenitiesInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

						<td class="hd2"><g:link action="show" id="${amenitiesInstance.id}">${fieldValue(bean: amenitiesInstance, field: "title")}</g:link></td>

						<td class="hd2">${fieldValue(bean: amenitiesInstance, field: "holder")}</td>

						<td class="hd2">${fieldValue(bean: amenitiesInstance, field: "point")}</td>

						<td class="hd2">${fieldValue(bean: amenitiesInstance, field: "visible")}</td>

					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${amenitiesInstanceTotal}" />
			</div>
		</div>


		<a href="#list-amenities" class="skip" tabindex="-1" title="<g:message code="default.link.skip.label" default="Skip to content&hellip;"/>">
		<span class="glyphicon glyphicon-triangle-top"></span> TOP </a>
		</div>
	<br>
	<br>
	</body>
</html>
