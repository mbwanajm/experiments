<%@ page import="com.mbwanajm.darproperty.Enquiries" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'enquiries.label', default: 'Enquiries')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<a href="#show-enquiries" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                                default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="show-enquiries" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <ol class="property-list enquiries">

        <g:if test="${enquiriesInstance?.email}">
            <li class="fieldcontain">
                <span id="email-label" class="property-label"><g:message code="enquiries.email.label"
                                                                         default="Email"/></span>

                <span class="property-value" aria-labelledby="email-label"><g:fieldValue bean="${enquiriesInstance}"
                                                                                         field="email"/></span>

            </li>
        </g:if>

        <g:if test="${enquiriesInstance?.description}">
            <li class="fieldcontain">
                <span id="description-label" class="property-label"><g:message code="enquiries.description.label"
                                                                               default="Description"/></span>

                <span class="property-value" aria-labelledby="description-label"><g:fieldValue
                        bean="${enquiriesInstance}" field="description"/></span>

            </li>
        </g:if>

        <g:if test="${enquiriesInstance?.area}">
            <li class="fieldcontain">
                <span id="area-label" class="property-label"><g:message code="enquiries.area.label"
                                                                        default="Area"/></span>

                <span class="property-value" aria-labelledby="area-label"><g:fieldValue bean="${enquiriesInstance}"
                                                                                        field="area"/></span>

            </li>
        </g:if>

        <g:if test="${enquiriesInstance?.country}">
            <li class="fieldcontain">
                <span id="country-label" class="property-label"><g:message code="enquiries.country.label"
                                                                           default="Country"/></span>

                <span class="property-value" aria-labelledby="country-label"><g:fieldValue bean="${enquiriesInstance}"
                                                                                           field="country"/></span>

            </li>
        </g:if>

        <g:if test="${enquiriesInstance?.currency}">
            <li class="fieldcontain">
                <span id="currency-label" class="property-label"><g:message code="enquiries.currency.label"
                                                                            default="Currency"/></span>

                <span class="property-value" aria-labelledby="currency-label"><g:fieldValue bean="${enquiriesInstance}"
                                                                                            field="currency"/></span>

            </li>
        </g:if>

        <g:if test="${enquiriesInstance?.max_price}">
            <li class="fieldcontain">
                <span id="max_price-label" class="property-label"><g:message code="enquiries.max_price.label"
                                                                             default="Maxprice"/></span>

                <span class="property-value" aria-labelledby="max_price-label"><g:fieldValue bean="${enquiriesInstance}"
                                                                                             field="max_price"/></span>

            </li>
        </g:if>

        <g:if test="${enquiriesInstance?.min_price}">
            <li class="fieldcontain">
                <span id="min_price-label" class="property-label"><g:message code="enquiries.min_price.label"
                                                                             default="Minprice"/></span>

                <span class="property-value" aria-labelledby="min_price-label"><g:fieldValue bean="${enquiriesInstance}"
                                                                                             field="min_price"/></span>

            </li>
        </g:if>

        <g:if test="${enquiriesInstance?.name}">
            <li class="fieldcontain">
                <span id="name-label" class="property-label"><g:message code="enquiries.name.label"
                                                                        default="Name"/></span>

                <span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${enquiriesInstance}"
                                                                                        field="name"/></span>

            </li>
        </g:if>

        <g:if test="${enquiriesInstance?.phone}">
            <li class="fieldcontain">
                <span id="phone-label" class="property-label"><g:message code="enquiries.phone.label"
                                                                         default="Phone"/></span>

                <span class="property-value" aria-labelledby="phone-label"><g:fieldValue bean="${enquiriesInstance}"
                                                                                         field="phone"/></span>

            </li>
        </g:if>

        <g:if test="${enquiriesInstance?.region}">
            <li class="fieldcontain">
                <span id="region-label" class="property-label"><g:message code="enquiries.region.label"
                                                                          default="Region"/></span>

                <span class="property-value" aria-labelledby="region-label"><g:fieldValue bean="${enquiriesInstance}"
                                                                                          field="region"/></span>

            </li>
        </g:if>
        <g:if test="${enquiriesInstance?.dueDate}">
            <li class="fieldcontain">
                <span id="dueDate-label" class="property-label"><g:message code="enquiries.dueDate.label"
                                                                          default="dueDate"/></span>

                <span class="property-value" aria-labelledby="dueDate-label"><g:fieldValue bean="${enquiriesInstance}"
                                                                                           field="dueDate"/></span>

            </li>
        </g:if>
        <g:if test="${enquiriesInstance?.dateAdded}">
            <li class="fieldcontain">
                <span id="dateAdded-label" class="property-label"><g:message code="enquiries.dateAdded.label"
                                                                          default="dateAdded"/></span>

                <span class="property-value" aria-labelledby="dateAdded-label"><g:fieldValue bean="${enquiriesInstance}"
                                                                                             field="dateAdded"/></span>

            </li>
        </g:if>
        <g:if test="${enquiriesInstance?.rooms}">
            <li class="fieldcontain">
                <span id="rooms-label" class="property-label"><g:message code="enquiries.rooms.label"
                                                                         default="rooms"/></span>

                <span class="property-value" aria-labelledby="rooms-label"><g:fieldValue bean="${enquiriesInstance}"
                                                                                         field="rooms"/></span>

            </li>
        </g:if>
    </ol>

    <g:form>
        <fieldset class="buttons">

            <g:hiddenField name="id" value="${enquiriesInstance?.id}"/>
            <g:link class="btn btn-warning" action="edit" id="${enquiriesInstance?.id}"><g:message
                    code="default.button.edit.label" default="Edit"/></g:link>
            <g:actionSubmit class="btn btn-danger" action="delete"  id="${enquiriesInstance?.id}"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            formnovalidate=""
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
        </fieldset>
    </g:form>
    %{--<g:form controller="Enquiries" action="list">--}%
        %{--<input type="hidden" name="id" value="${enquiriesInstance?.id}">--}%
        %{--<input type="submit" class="btn btn-danger" name="delete" value="Delete"/>--}%
    %{--</g:form>--}%
</div>
</body>
</html>
