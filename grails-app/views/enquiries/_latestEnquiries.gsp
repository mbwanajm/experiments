<%@ page import="java.sql.Date; java.sql.Timestamp;  java.text.SimpleDateFormat; java.text.NumberFormat" %>


<div class="list-group-item">

    <div class="panel panel-default">
        <div class="panel-heading text-uppercase">
            wanted ${enquiry.type} ${enquiry.sale_type} , ${enquiry.area}   -   ${enquiry.region}.
            <a href="#" data-toggle="modal"
               data-target=".reporting${enquiry.id}">
                <small class="pull-right text-muted" style="font-size: 8pt;">[<g:message
                        code="application.report"></g:message> <i class="fa fa-flag "></i>]</small>
            </a>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-sm-2">
                    <img src='/images/horn.png' alt='${enquiry.sale_type}'
                         title="<g:message code="home.advert"></g:message>"/>
                </div>

                <div class="col-sm-10">
                    <ul class="list-unstyled">
                        <li class="row">
                            <div class="col-xs-6" align="left">
                                <b class="hd2"><g:message code="home.postedby"></g:message></b> ${enquiry.name}
                            </div>

                            <div class="col-xs-6" align="right">

                                <small class="text-muted"><g:dateFromNow date="${enquiry.dateAdded}"/></small>
                            </div>
                        </li>
                        <li>
                            <%
                                def num = enquiry.max_price
                                def pr = 0
                                def rep = ""
                                def hold = num / 1000000
                                if (hold >= 1) {
                                    pr = hold
                                    rep = "M"
                                } else {
                                    hold = num / 1000
                                    if (hold >= 1) {
                                        pr = hold
                                        rep = "K"
                                    } else {
                                        pr = num
                                    }
                                }

                            %>
                            <b class="hd2"><g:message code="home.maxprice"></g:message></b>
                            <i class="text-danger fa fa-arrow-up" title="Maximum price"></i>
                            ${java.text.NumberFormat.getInstance().format(pr)}${rep}
                            <%
                                num = enquiry.min_price
                                pr = 0
                                rep = ""
                                hold = num / 1000000
                                if (hold >= 1) {
                                    pr = hold
                                    rep = "M"
                                } else {
                                    hold = num / 1000
                                    if (hold >= 1) {
                                        pr = hold
                                        rep = "K"
                                    } else {
                                        pr = num
                                    }
                                }

                            %>
                            <b class="hd2"><g:message code="home.minprice"></g:message></b>
                            <i class="text-success fa fa-arrow-down" title="Minimum price"></i>
                            ${java.text.NumberFormat.getInstance().format(pr)}${rep} ${enquiry.currency}</li>

                        <li><b class="hd2"><i class="fa fa-phone-square"></i></b> ${enquiry.phone}  <b class="hd2"><i
                                class="fa fa-envelope"></i></b> ${enquiry.email}</li>
                        <li class=""><a href="#" class="btn btn-block btn-danger" data-toggle="modal"
                                        data-target=".readmore${enquiry.id}">
                            <b><i class="fa fa-hand-o-up"></i> <g:message code="home.moredetails"></g:message></b></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</div>

<g:render template="../enquiries/enquiryHandles" model="[enquiry: enquiry]"></g:render>