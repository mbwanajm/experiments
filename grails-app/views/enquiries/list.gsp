
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'enquiries.label', default: 'Enquiries')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-enquiries" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li>	<g:link controller ="config" action=""><span class="glyphicon glyphicon-wrench"></span> </g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
			<g:if test="${request.getParameter("delete")!=null}">
			</g:if>
		<div id="list-enquiries" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table class="table">
				<thead class="hd4">
					<tr >
					    
					    
					    
						<g:sortableColumn property="email" title="${message(code: 'enquiries.email.label', default: 'Email')}" />
						
						<g:sortableColumn property="name" title="${message(code: 'enquiries.name.label', default: 'Name')}" />
					
						<g:sortableColumn property="description" title="${message(code: 'enquiries.description.label', default: 'Description')}" />
					
						<g:sortableColumn property="area" title="${message(code: 'enquiries.area.label', default: 'Area')}" />
					
						<g:sortableColumn property="country" title="${message(code: 'enquiries.country.label', default: 'Country')}" />
						
						<g:sortableColumn property="country" title="${message(code: 'enquiries.region.label', default: 'Region')}" />
						
						<g:sortableColumn property="currency" title="${message(code: 'enquiries.currency.label', default: 'Currency')}" />
					
						<g:sortableColumn property="max_price" title="${message(code: 'enquiries.max_price.label', default: 'Maxprice')}" />
						
						<g:sortableColumn property="min_price" title="${message(code: 'enquiries.min_price.label', default: 'Minprice')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${enquiriesInstanceList}" status="i" var="enquiriesInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'} hd2">
					
						<td><g:link action="show" id="${enquiriesInstance.id}">${fieldValue(bean: enquiriesInstance, field: "email")}</g:link></td>
						
						<td>${fieldValue(bean: enquiriesInstance, field: "name")}</td>
					
						<td>${fieldValue(bean: enquiriesInstance, field: "description")}</td>
					
						<td>${fieldValue(bean: enquiriesInstance, field: "area")}</td>
					
						<td>${fieldValue(bean: enquiriesInstance, field: "country")}</td>
						
						<td>${fieldValue(bean: enquiriesInstance, field: "region")}</td>
					
						<td>${fieldValue(bean: enquiriesInstance, field: "currency")}</td>
					
						<td>${fieldValue(bean: enquiriesInstance, field: "max_price")}</td>
						
						<td>${fieldValue(bean: enquiriesInstance, field: "min_price")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate  total="${enquiriesInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
