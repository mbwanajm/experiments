%{--========================================================================================================================--}%
<!--Read more Modal-->
<div class="modal fade readmore${enquiry.id}" tabindex="-1" role="dialog" aria-labelledby="readmore${enquiry.id}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center text-uppercase"><b class="hd2"><i
                        class="fa fa-hand-o-down"></i> <g:message code="home.moredetails"></g:message></b></h4>
            </div>

            <div class="modal-body">

                %{--body--}%
                <ul class="list-unstyled">

                    <g:if test="${enquiry.rooms != 0}">
                        <li><b class="hd2 text-capitalize"><g:message
                                code="property.rooms"></g:message>:</b>  ${enquiry.rooms}  <g:message
                                code="property.rooms"></g:message></li>
                    </g:if>
                    <li>
                        <b class="hd2"><g:message
                                code="home.otherdetails"></g:message>:</b>

                        <p class=''
                           style='text-indent: 10px;background-color:#f0f0f0;border:1px solid #f0f0f0 ;font-size: 9pt;'>
                            <b>${enquiry.description}</b>
                        </p>
                    </li>
                </ul>
                %{--body--}%

            </div>

            <div class="modal-footer">
                <p class="text-left">
                    <b class="hd2"><i class="fa fa-phone-square"></i></b> ${enquiry.phone}<br>
                    <b class="hd2"><i class="fa fa-envelope"></i></b> ${enquiry.email}<br>
                    <b class="hd2"><g:message
                            code="home.contactbefore"></g:message></b> <g:dateFormatting
                        date="${enquiry.dueDate}"></g:dateFormatting>
                </p>
                <button type="button" class="pull-left btn btn-primary" data-toggle="modal"
                        data-target=".sendmail${enquiry.id}">
                    <i class="fa fa-envelope"></i>
                    <g:message
                            code="home.emailthis"></g:message>
                </button>
                <button type="button" class="btn btn-danger fa fa-close" data-dismiss="modal"></button>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

%{--=======================================================================================================================--}%
%{--new modal to send mail--}%
<div class="modal fade sendmail${enquiry.id}" tabindex="-1" role="dialog" aria-labelledby="sendmail${enquiry.id}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-uppercase text-danger"><g:message
                        code="home.emailthis"></g:message> <i class="fa fa-hand-o-down"></i></h4>
            </div>

            <div class="modal-body">

                <g:form method="post" controller="enquiries" action="mailingEnquiries">
                    <input type="hidden" name="receiver" value="${enquiry.email}">

                    <div class="form-group">
                        <label>
                            <g:message code="application.youremail"></g:message>
                        </label>
                        <input type="email" required="required" name="email" class="form-control" autocomplete="off">
                    </div>

                    <div class="form-group">
                        <label>
                            <g:message code="application.yourname"></g:message>
                        </label>
                        <input type="text" maxlength="20" required="required" name="name" class="form-control"
                               autocomplete="off">
                    </div>

                    <div class="form-group">
                        <label>
                            <g:message code="application.yourphone"></g:message>
                        </label>
                        <input type="tel" required="required" name="phone" class="form-control" autocomplete="off">
                    </div>

                    <div class="form-group">
                        <label>
                            <g:message code="property.enquiry.message"></g:message>
                        </label>
                        <g:textArea name="message" class="form-control" required="required"></g:textArea>
                    </div>

                    <div class='form-group'>
                        <img src="${createLink(controller: 'simpleCaptcha', action: 'captcha')}"/>
                        <br>
                        <label for="captcha">Type the letters above in the box:</label>
                        <g:textField name="captcha" autocomplete="off"/>
                    </div>

                    <input type="submit" class="btn btn-primary btn-block" name="send" value="<<< Send >>>">

                </g:form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger fa fa-close" data-dismiss="modal"></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


%{--========================================================================================================================--}%
<div class="modal fade reporting${enquiry.id}" tabindex="-1" role="dialog" aria-labelledby="reporting${enquiry.id}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-capitalize hd2">report abuse</h4>
            </div>

            <div class="modal-body">
                <g:form controller="enquiries" action="mailingAbuse">
                    <input type="hidden" name="name" value="${enquiry.name}">
                    <input type="hidden" name="saletype" value="${enquiry.sale_type}">
                    <input type="hidden" name="type" value="${enquiry.type}">
                    <input type="hidden" name="email" value="${enquiry.email}">
                    <div class="form-group">
                        <label><g:message code="report.whatis"></g:message>:</label>
                        <select name="reason" class="form-control">
                        <option value=" Frauds: misuse/ identity theft."> <g:message code="report.fraud"></g:message> </option>
                        <option value=" Language: abusive language used"> <g:message code="report.language"></g:message>.</option>
                        <option value=" Outdated: the need has been fulfilled"> <g:message code="report.outdated"></g:message></option>
                        <option value=" Others"> <g:message code="report.others"></g:message></option>
                        </select>

                    </div>
                    <div class="form-group">
                        <label><g:message code="report.others"></g:message>:</label>
                        <g:textArea name="other" maxlength="100" class="form-control"></g:textArea>
                    </div>
                    <div class='form-group'>
                        <img src="${createLink(controller: 'simpleCaptcha', action: 'captcha')}"/>
                        <br>
                        <label for="captcha">Type the letters above in the box:</label>
                        <g:textField name="captcha" autocomplete="off"/>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="submit" value="Submit" class="btn btn-block btn-success">
                    </div>
                </g:form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger fa fa-close" data-dismiss="modal"></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->