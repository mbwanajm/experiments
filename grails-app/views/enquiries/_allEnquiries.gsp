<div class='col-xs-12 col-md-12'>

    <div class="panel panel-default">
        <div class="panel-heading text-uppercase">
            wanted ${enquiry.type} ${enquiry.sale_type} , ${enquiry.area}   -   ${enquiry.region}.
            <a href="#" data-toggle="modal"
               data-target=".reporting${enquiry.id}">
                <small class="pull-right text-muted" style="font-size: 8pt;">[<g:message
                        code="application.report"></g:message> <i class="fa fa-flag "></i>]</small>

            </a>
        </div>

        <div class="panel-body ">
            <div class="row">
                <div class="col-sm-2">
                    <img src='/images/horn.png' alt='${enquiry.sale_type}'
                         title="<g:message code="home.advert"></g:message>"/>
                </div>

                <div class="col-sm-10">
                    <ul class="list-unstyled">
                        <li class="row">
                            <div class="col-xs-6" align="left">
                                <b class="hd2"><g:message code="home.postedby"></g:message></b> ${enquiry.name}
                            </div>

                            <div class="col-xs-6" align="right">

                                <small class="text-muted"><g:dateFromNow date="${enquiry.dateAdded}"/></small>
                            </div>
                        </li>
                        <li>
                            <%
                                def num = enquiry.max_price
                                def pr = 0
                                def rep = ""
                                def hold = num / 1000000
                                if (hold >= 1) {
                                    pr = hold
                                    rep = " M"
                                } else {
                                    hold = num / 100000
                                    if (hold >= 1) {
                                        hold = num / 1000
                                        pr = hold
                                        rep = " K"
                                    } else {
                                        pr = num
                                    }
                                }

                            %>
                            <b class="hd2"><g:message code="home.maxprice"></g:message></b>
                            <i class="text-danger fa fa-arrow-up" title="Maximum price"></i>
                            ${java.text.NumberFormat.getInstance().format(pr)}${rep}
                            <%
                                num = enquiry.max_price
                                pr = 0
                                rep = ""
                                hold = num / 1000000
                                if (hold >= 1) {
                                    pr = hold
                                    rep = " M"
                                } else {
                                    hold = num / 100000
                                    if (hold >= 1) {
                                        hold = num / 1000
                                        pr = hold
                                        rep = " K"
                                    } else {
                                        pr = num
                                    }
                                }

                            %>
                            <b class="hd2"><g:message code="home.minprice"></g:message></b>
                            <i class="text-success fa fa-arrow-down" title="Minimum price"></i>
                            ${java.text.NumberFormat.getInstance().format(pr)}${rep} ${enquiry.currency}</li>

                        <li><b class="hd2"><g:message
                                code="home.contacts"></g:message></b> ${enquiry.phone}, ${enquiry.email}</li>
                        <li class=""><a href="#" class="btn btn-block btn-danger" data-toggle="modal"
                                        data-target=".readmore${enquiry.id}">
                            <b><i class="fa fa-hand-o-up"></i> <g:message code="home.moredetails"></g:message></b></a>
                            <br>
                        </li>
                        <li class="pull-right">
                            <a href='whatsapp://send?text= Post from DARPROPERTY. [${enquiry.name} looking for ${enquiry.type} ${enquiry.sale_type}, ${enquiry.area} - ${enquiry.region} in ${enquiry.country} http://www.staging.darproperty.co.tz/search/user_enquiry]'
                               data-action='share/whatsapp/share'
                               target='_blank'>
                                <img src='/bootstrap/imgs/whatsappgreen.png' alt='whatsapp' width='30' height='35'
                                     class='hidden-lg hidden-md'/></a>

                            <span class='fb-share-button'
                                  data-text='[${enquiry.name} looking for ${enquiry.type} ${enquiry.sale_type}, ${enquiry.area} - ${enquiry.region} in ${enquiry.country}'
                                  data-href='http://www.darproperty.co.tz/search/user_enquiry'
                                  data-layout='button_count'>

                            </span>
                            <br>
                            <br>

                            <a href='https://twitter.com/share'
                               class='twitter-share-button' {count}
                               data-text='[ ${enquiry.name} is looking for ${enquiry.type} ${enquiry.sale_type}, ${enquiry.area} - ${enquiry.region} in ${enquiry.country} '
                               data-via='thedarproperty'
                               data-related='thedarproperty'
                               data-hashtags='PropertyEnquiry'
                               title='Share On Twitter'><i class="text-info fa fa-twitter"></i></a>

                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</div>


<g:render template="../enquiries/enquiryHandles" model="[enquiry: enquiry]"></g:render>

