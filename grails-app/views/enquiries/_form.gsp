<%@ page import="com.mbwanajm.darproperty.Enquiries" %>



<div class="fieldcontain ${hasErrors(bean: enquiriesInstance, field: 'email', 'error')} ">
	<label for="email">
		<g:message code="enquiries.email.label" default="Email" />
		
	</label>
	<g:field type="email" name="email" value="${enquiriesInstance?.email}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: enquiriesInstance, field: 'description', 'error')} required">
	<label for="description">
		<g:message code="enquiries.description.label" default="Description" />
		<span class="required-indicator">*</span>
	</label>
	<g:textArea name="description" cols="40" rows="5" maxlength="500" required="" value="${enquiriesInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: enquiriesInstance, field: 'area', 'error')} ">
	<label for="area">
		<g:message code="enquiries.area.label" default="Area" />
		
	</label>
	<g:textField name="area" value="${enquiriesInstance?.area}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: enquiriesInstance, field: 'country', 'error')} ">
	<label for="country">
		<g:message code="enquiries.country.label" default="Country" />
		
	</label>
	<g:textField name="country" value="${enquiriesInstance?.country}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: enquiriesInstance, field: 'currency', 'error')} ">
	<label for="currency">
		<g:message code="enquiries.currency.label" default="Currency" />
		
	</label>
	<g:textField name="currency" value="${enquiriesInstance?.currency}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: enquiriesInstance, field: 'max_price', 'error')} required">
	<label for="max_price">
		<g:message code="enquiries.max_price.label" default="Maxprice" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="max_price" value="${fieldValue(bean: enquiriesInstance, field: 'max_price')}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: enquiriesInstance, field: 'min_price', 'error')} required">
	<label for="min_price">
		<g:message code="enquiries.min_price.label" default="Minprice" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="min_price" value="${fieldValue(bean: enquiriesInstance, field: 'min_price')}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: enquiriesInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="enquiries.name.label" default="Name" />
		
	</label>
	<g:textField name="name" value="${enquiriesInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: enquiriesInstance, field: 'phone', 'error')} ">
	<label for="phone">
		<g:message code="enquiries.phone.label" default="Phone" />
		
	</label>
	<g:textField name="phone" value="${enquiriesInstance?.phone}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: enquiriesInstance, field: 'region', 'error')} ">
	<label for="region">
		<g:message code="enquiries.region.label" default="Region" />
		
	</label>
	<g:textField name="region" id='wysiwyg'  value="${enquiriesInstance?.region}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: enquiriesInstance, field: 'sale_type', 'error')} required">
	<div class="form-group" >
		<label for="saleType" class="control-label col-sm-5">
			<g:message code="property.saleType.label" default="Sale Type"/>
			<span class="required-indicator">*</span>
		</label>
		<div class="col-sm-7" align="left">
			<g:select id="saleType" name="sale_type" from="${com.mbwanajm.darproperty.SaleType.list()}" optionKey="name"
					  required="" value="${enquiriesInstance?.sale_type}" class="many-to-one" class="form-control"/>
		</div>
	</div>
</div>
<div class="well">
	<div class="fieldcontain ${hasErrors(bean: enquiriesInstance, field: 'type', 'error')} required">
		<div class="form-group" >
			<label for="type" class="control-label col-sm-5">
				<g:message code="property.type.label" default="Type"/>
				<span class="required-indicator">*</span>
			</label>
			<div class="col-sm-7" align="left">
				<g:select id="type" name="type" from="${com.mbwanajm.darproperty.Type.list()}" optionKey="name" required=""
						  value="${enquiriesInstance?.type}" class="many-to-one" class="form-control"/>
			</div>
		</div>
	</div>
</div>