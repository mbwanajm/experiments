<!--  ${regionLocated} ,${areaLocated}, ${stype},id= ${pid},size= ${psize} ,${show}<br>-->

<%@ page import="com.mbwanajm.darproperty.Tag; com.mbwanajm.darproperty.Property; com.mbwanajm.darproperty.Amenities; com.mbwanajm.darproperty.Region_areas; com.mbwanajm.darproperty.Region; com.mbwanajm.darproperty.CurrencyService; java.text.NumberFormat" %>

<%



    String pty = ptype;
    String ty = stype;
    boolean pec = (pty != "non" && pty != "null");
    boolean dec = (ty == "for rent");
    int id = 0;
    double builtUp = 0.0;
    double identical_sum = 0.0;
    double point_amenities = 0.0;
    double gsp = 0.0;
    double net_selling_price = 0.0;
    double selling_price = 0.0;
    double rental_price = 0.0;
    double sum = 0.0;
    double sum_amenities = 0.0;
    int areaPoint = 0;
    int region_id = 0;
    int sel = 0;
    int ren = 0;
    double item = 0;
    def areaLocated = "$areaLocated"
    def regionLocated = "$regionLocated"

    double areaPoints = 0.0

    int countAmenities = 0
    CurrencyService currencyService = new CurrencyService()


    try {


        if (pid != null) {
            def region = Region.findByName(regionLocated)
            areaPoints = Region_areas.findByRegion_idAndAreas_string(region.id, areaLocated).point

            builtUp = Double.parseDouble("$psize")
            builtUp = builtUp > 0 ? builtUp : 10 + Math.random() * 100
            gsp = ((builtUp) * 1100) + 500;
            if (builtUp == 0 || builtUp == 1) {
                selling_price = ((gsp * areaPoints) * 100.0) / 100.0
            } else {
                selling_price = ((gsp * (areaPoints / 100.0) * 100.0) / 100.0)
            }

            rental_price = ((selling_price / 96) * 100.0) / 100.0;

            selling_price = currencyService.convert(selling_price, 'USD', selectedCurrency);
            rental_price = currencyService.convert(rental_price, 'USD', selectedCurrency);

            sel = (int) selling_price;
            ren = (int) rental_price;


        }

    } catch (Exception ex) {
        //out.print("" + ex);
    }

%>


<g:if test="${pec}">

    <g:if test="${show == 'full'}">
        <div class="row">
            <div class="col-sm-4">
                <g:if test="${!dec}">
                    <div class="input-group col-xs-11  hidden-sm hidden-xs">
                        <span class="input-group-addon"><span class="hd2"><g:message code="estimate.price"/></span>
                        </span>
                        <input type="text" class="form-control"
                               value="${NumberFormat.getInstance().format(selling_price)} " readonly="readonly"
                               aria-describedby="basic-addon1">
                        <span class="input-group-addon" id="basic-addon3">${selectedCurrency}</span>

                    </div>

                    <div class="hidden-lg hidden-md ">
                        <span class="hd2"><g:message
                                code="estimate.price"/>:</span> ${NumberFormat.getInstance().format(selling_price)}$
                    </div>
                    <br>
                    <small style="color:#999;"><b>NB:</b> The above estimation maybe or may not be accurate due to some factors, based on values of amenities and location
                    </small>
                </g:if>
                <g:else>
                    <div class="input-group col-xs-11 hidden-sm hidden-xs">
                        <span class="input-group-addon"><span class="hd2"><g:message code="estimate.rent"/></span>
                        </span>
                        <input type="text" class="form-control"
                               value="${NumberFormat.getInstance().format(rental_price)}  " readonly="readonly"
                               aria-describedby="basic-addon1">
                        <span class="input-group-addon" id="basic-addon4">${selectedCurrency}/Month</span>

                    </div>

                    <div class="hidden-lg  hidden-md">
                        <span class="hd2"><g:message
                                code="estimate.rent"/>:</span> ${NumberFormat.getInstance().format(rental_price)}
                    </div>
                    <br>
                    <small style="color:#999;"><b>NB:</b> The above estimation maybe or may not be accurate due to some factors, based on values of amenities and location
                    </small>
                </g:else>
            </div>

            <div class="col-sm-8" align="left">
                <g:link controller="tool" action="valueCalculator">
<i class="fa fa-calculator" width="90" height="90"></i>
                    %{--<img src="${resource(dir: 'bootstrap/imgs/', file: 'calculator.png')}" alt="Calculator"--}%
                         %{--class="img-paloroid" width="90" height="90" title="Manual Calculator"/>--}%
                </g:link>
            </div>
        </div>
    </g:if>
    <g:elseif test="${show == 'short'}">
    %{--for sale--}%
        <g:if test="${ty == 'for sale'}">
            <div class="hidden-sm hidden-xs">
                <div class="green-box  ">

                    <%

                        int estimated = sel;
                        try {

                            if (estimated > price && price > 0) {
                                out.print("<span class='inner-rotate'>Good Deal  !</span>");
                            }
                        } catch (Exception e) {
                            out.print(e);
                        }
                    %>

                </div>

                <g:render template="../tool/estimated_priceViewer"
                          model="[prc: sel, selectedCurrency: selectedCurrency, show: show]"></g:render>

                <p class="text-muted">Darproperty Guesstimate

                </p>

                <p class="text-info">
                    <a href="#" data-toggle="modal" data-target=".whatisthis">What is this <span
                            class="fa fa-question-circle"></span></a>
                </p>
            </div>

            <div class="hidden-md hidden-lg">
                <div class="row" align="center">
                    <div class="col-xs-offset-3 col-xs-6 col-xs-offset-3">
                        <g:render template="../tool/estimated_priceViewer"
                                  model="[prc: sel, selectedCurrency: selectedCurrency, show: show]"></g:render>
                    </div>
                </div>

                <div class="row" align="center">
                    <div class="col-xs-offset-3 col-xs-6 col-xs-offset-3">
                        <%

                            estimated = sel;
                            try {

                                if (estimated > price && price > 0) {
                                    out.print("<small class='hd2' style=''>Good Deal</small>");
                                }
                            } catch (Exception e) {
                                out.print(e);
                            }
                        %>
                        <br>
                        <small class="text-muted text-left">Guesstimate</small>
                    </div>
                </div>

                <div class="row" align="center">
                    <div class="col-xs-offset-3 col-xs-6 col-xs-offset-3">
                        <small class="text-info">
                            <a href="#" data-toggle="modal" data-target=".whatisthis">What is this <span
                                    class="fa fa-question-circle"></span></a>
                        </small>
                    </div>
                </div>
            </div>

        </g:if>
    %{--/for sale--}%
        <g:elseif test="${ty == 'for rent'}">
            <div class="hidden-sm hidden-xs">
                <div class="green-box ">

                    <%

                        estimated = ren;
                        try {

                            if (estimated > price && price > 0) {
                                out.print("<span class='inner-rotate'>Good Deal  !</span>");
                            }
                        } catch (Exception e) {
                            out.print(e);
                        }
                    %>

                </div>
                <g:render template="../tool/estimated_priceViewer"
                          model="[prc: ren, selectedCurrency: selectedCurrency, show: show]"></g:render>
            </div>

            <div>
                <p class="text-muted">Darproperty Guesstimate

                </p>

                <p class="text-info">
                    <a href="#" data-toggle="modal" data-target=".whatisthis">What is this <span
                            class="fa fa-question-circle"></span></a>
                </p>
            </div>

            <div class="hidden-md hidden-lg">
                <div class="row" align="center">
                    <div class="col-xs-offset-3 col-xs-6 col-xs-offset-3">
                        <g:render template="../tool/estimated_priceViewer"
                                  model="[prc: ren, selectedCurrency: selectedCurrency, show: show]"></g:render>
                    </div>
                </div>

                <div class="row" align="center">
                    <div class="col-xs-offset-3 col-xs-6 col-xs-offset-3">
                        <g:render template="../tool/estimated_priceViewer"
                                  model="[prc: sel, selectedCurrency: selectedCurrency, show: show]"></g:render>
                    </div>
                </div>

                <div class="row" align="center">
                    <div class="col-xs-offset-3 col-xs-6 col-xs-offset-3">
                        <small class="text-info">
                            <a href="#" data-toggle="modal" data-target=".whatisthis">What is this <span
                                    class="fa fa-question-circle"></span></a>
                        </small>
                    </div>
                </div>
            </div>

        </g:elseif>

    </g:elseif>
    <g:elseif test="${show == 'front'}">
    %{--for sale--}%
        <g:if test="${ty == 'for sale'}">
            <div class="row hidden-sm hidden-xs">
                <div class="col-sm-6">
                    <small class="text-muted">Guesstimate

                    </small>

                    <p class="text-info">
                        <a href="#" data-toggle="modal" data-target=".whatisthis">What is this <span
                                class="fa fa-question-circle"></span></a>
                    </p>
                </div>

                <div class="col-sm-6" style="">
                    <div class="green-box  ">

                        <%

                            estimated = sel;
                            try {

                                if (estimated > price && price > 0) {
                                    out.print("<span class='inner-rotate'>Good Deal !</span>");
                                }
                            } catch (Exception e) {
                                out.print(e);
                            }
                        %>

                        <g:render template="../tool/estimated_priceViewer"
                                  model="[prc: sel, selectedCurrency: selectedCurrency, show: show]"></g:render>
                    </span>

                    </div>
                </div>

            </div>

        %{--MOBILE--}%
            <div class="hidden-md hidden-lg">
                <div class="row" align="center">
                    <div class="col-xs-offset-3 col-xs-6 col-xs-offset-3">
                        <g:render template="../tool/estimated_priceViewer"
                                  model="[prc: sel, selectedCurrency: selectedCurrency, show: show]"></g:render>
                    </div>
                </div>

                <div class="row" align="center">
                    <div class="col-xs-offset-3 col-xs-6 col-xs-offset-3">

                        <%

                            estimated = sel;
                            try {

                                if (estimated > price && price > 0) {
                                    out.print("<small class='hd2' style='border:1px solid #ed1c24; border-radius:10px; padding: 5px;'>Good Deal</small>");
                                }
                            } catch (Exception e) {
                                out.print(e);
                            }
                        %>
                        <br>

                    </div>
                </div>

                <div class="row" align="center">
                    <div class="col-xs-offset-3 col-xs-6 col-xs-offset-3">
                        <small class="text-info">
                            <a tabindex="0"
                               class="text-info"
                               role="button" data-toggle="popover"
                               data-placement="bottom"
                               data-trigger="focus"
                               title="Darproperty Guesstimate"
                               data-content=""><small class="fa fa-question-circle" style="font-size:10pt;color:red;"></small></a>
                        </small>
                    </div>
                </div>
            </div>
        </g:if>
        <g:if test="${ty == 'for rent'}">
            <div class="row hidden-sm hidden-xs">
                <div class="col-sm-5">
                    <small class="text-muted">Guesstimate

                    </small>

                    <p class="text-info">
                        <a href="#" data-toggle="modal" data-target=".whatisthis">What is this <span
                                class="fa fa-question-circle"></span></a>
                    </p>
                </div>

                <div class="col-sm-7" style="">
                    <div class="green-box  ">

                        <%

                            estimated = ren;
                            try {

                                if (estimated > price && price > 0) {
                                    out.print("<span class='inner-rotate'>Good Deal !</span>");
                                }
                            } catch (Exception e) {
                                out.print(e);
                            }
                        %>

                        <g:render template="../tool/estimated_priceViewer"
                                  model="[prc: ren, selectedCurrency: selectedCurrency, show: show]"></g:render>
                    </div>
                </div>

            </div>
        %{--MOBILE--}%
            <div class="hidden-md hidden-lg">
                <div class="row" align="center">
                    <div class="col-xs-offset-3 col-xs-6 col-xs-offset-3">
                        <g:render template="../tool/estimated_priceViewer"
                                  model="[prc: sel, selectedCurrency: selectedCurrency, show: show]"></g:render>
                    </div>
                </div>

                <div class="row" align="center">
                    <div class="col-xs-offset-3 col-xs-6 col-xs-offset-3">

                        <%

                            estimated = ren;
                            try {

                                if (estimated > price && price > 0) {
                                    out.print("<small class='hd2' style='border:1px solid #ed1c24; border-radius:10px; padding: 5px;'>Good Deal</small>");
                                }
                            } catch (Exception e) {
                                out.print(e);
                            }
                        %>
                        <br>

                    </div>
                </div>

                <div class="row" align="center">
                    <div class="col-xs-offset-3 col-xs-6 col-xs-offset-3">
                        <small class="text-info">
                            <a tabindex="0"
                               class="text-info"
                               role="button" data-toggle="popover"
                               data-placement="bottom"
                               data-trigger="focus"
                               title="Darproperty Guesstimate"
                               data-content="

                                "><small class="fa fa-question-circle" style="font-size:10pt;color:red;"></small></a>
                        </small>
                    </div>
                </div>
            </div>

        </g:if>

    %{--/for sale--}%
    </g:elseif>
    <g:elseif test="${show == 'mobo'}">
        <g:if test="${ty == 'for rent'}">
            <div class="">
                <span class="text-muted">Darproperty Guesstimate</span>
                <a href="#" data-toggle="modal" data-target=".whatisthis"><span
                        class="fa fa-question-circle"></span></a><br>
                <span class="text-muted" style="font-size: 12pt">${selectedCurrency == "USD" ? '$' : selectedCurrency} ${NumberFormat.getInstance().format(ren)}</span><br>


                %{--<span class="green-box  ">--}%


                    %{--<%--}%
                        %{--estimated = ren;--}%
                        %{--try {--}%

                            %{--if (estimated > price && price > 0) {--}%
                                %{--out.print("<span class='inner-rotate'> Good Deal !</span><br>");--}%
                            %{--}--}%
                        %{--} catch (Exception e) {--}%
                            %{--out.print(e);--}%
                        %{--}--}%
                    %{--%>--}%
                %{--</span>--}%
            </div>
            <br>


        </g:if><g:elseif test="${ty == 'for sale'}">
        <div class="">
            <span class="text-muted">Darproperty Guesstimate

            </span>

            <a href="#" data-toggle="modal" data-target=".whatisthis"><span
                    class="fa fa-question-circle"></span></a><br>
            <span class="text-muted" style="font-size: 12pt">${selectedCurrency == "USD" ? '$' : selectedCurrency}
                ${NumberFormat.getInstance().format(sel)}</span><br>
            %{--<span class="green-box  ">--}%


                %{--<%--}%
                    %{--estimated = sel;--}%
                    %{--try {--}%

                        %{--if (estimated > price || estimated == price && price > 0) {--}%
                           %{--out.print("<span class='inner-rotate'> Good Deal !</span><br>");--}%
                        %{--}--}%
                    %{--} catch (Exception e) {--}%
                        %{--out.print(e);--}%
                    %{--}--}%
                %{--%>--}%
            %{--</span>--}%





        </div>
        <br>
    </g:elseif>

    </g:elseif>
    <g:elseif test="${show == 'thumb'}">
        <g:if test="${ty == 'for rent'}">

                    <%
                        estimated = ren;
                        try {

                            if (estimated > price && price > 0) {
                                out.print('<span class="wrd text-uppercase" title="Good Deal !"><span class="fa fa-thumbs-up" ></span></span>');
                            }
                        } catch (Exception e) {
                            out.print(e);
                        }
                    %>


        </g:if><g:elseif test="${ty == 'for sale'}">



                <%
                    estimated = sel;
                    try {

                        if (estimated > price && price > 0) {
                            out.print('<span class="wrd text-uppercase" title="Good Deal !"><span class="img-rounded fa fa-thumbs-o-up" ></span></span>');
                        }
                    } catch (Exception e) {
                        out.print(e);
                    }
                %>

    </g:elseif>

    </g:elseif>

</g:if>



