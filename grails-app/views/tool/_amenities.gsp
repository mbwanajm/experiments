<div class='form-group'>
    <div class='col-xs-3'>
        <p class='hd'><span class='glyphicon glyphicon-pushpin'></span>${ame.title}</p>
    </div>

    <div class='col-xs-6'>
        <div class='col-md-5 alert alert-success'>
            <span class='glyphicon glyphicon-thumbs-up' title='Available'></span>
            <input type='radio' name='${ame.holder == NULL ? ame.id : ame.holder}' id='${ame.holder == NULL ? ame.id : ame.holder}'
                   value='${ame.point}'> ${ame.holder == NULL ? "Available" : ""}
        </div>

        <div class='col-md-2 offset'></div>

        <div class='col-md-5 ${ame.holder == NULL ? "alert alert-danger" : ""}'>
            ${ame.holder != NULL ? "Category: <i class='hd2'>(" + ame.holder + ")</i>" :
           "<input type='radio' name='${ame.holder == NULL ? ame.id : ame.holder}' id='${ame.holder == NULL ? ame.id : ame.holder}' value='0' checked='checked'> ${ame.holder == NULL ? "Not" : " "}" }


        </div>
    </div>

</div>