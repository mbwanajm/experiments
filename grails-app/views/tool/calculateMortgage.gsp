<%--
  Created by IntelliJ IDEA.
  User: 1stHands
  Date: 8/7/2016
  Time: 11:08 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>DarProperty: CalculateMortagage</title>
    <meta name='layout' content="main">


    <link rel="stylesheet" href="${resource(dir: 'css', file: 'responsiveslides.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'js/js_slider', file: 'ninja-slider.css')}"/>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Bree+Serif"/>

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://cdn.mlcalc.com/widget-api.js"></script>
    <g:javascript src="lib/responsiveslides.min.js"/>
    <g:javascript src="darproperty/HomeSlideShows.js"/>
    <g:javascript src="lib/mortgageCalculator.js"/>
</head>

<body>
<!-- MORTGAGE LOAN CALCULATOR BEGIN -->
<div class="container">
    <div class="row">
        <!-- MORTGAGE LOAN CALCULATOR BEGIN -->
        <!--

        <%@ page import="com.mbwanajm.darproperty.Banks; com.mbwanajm.darproperty.Interestrates;" %>

<!-- ban.setCountry(country); -->
        <div class="row">
            <div class="col-sm-12">
                <h3 align="center">${g.message(code: 'menu.tools.mortgageCalculator')}</h3>
            </div>

        </div>

        <div class="col-lg-7">

            <form action="http://www.mlcalc.com/" method="post" onsubmit="return mlcalcCalculate(this)">
                <input type="hidden" name="wl" value="en">
                <a href=""><span class="
glyphicon glyphicon-hand-left"></span><g:message code="application.back"></g:message></a> <br>

                <div class="form-group">
                    <label class="control-label col-sm-5"><g:message
                            code="application.purchase.price"></g:message> (${currency}):</label>

                    <div class="col-sm-7">
                        <input type="text" name="ma" value="${price}" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-5"><g:message
                            code="application.down.payment"></g:message> (%):</label>

                    <div class="col-sm-7">
                        <input type="text" name="dp" value="10" class="form-control">
                    </div>
                </div>

                <div class="form-group select_control">
                    <label class="control-label col-sm-5"><g:message
                            code="application.mortgage.term"></g:message> (<g:message
                            code="application.years"></g:message> ):</label>

                    <div class="col-sm-7 select_control">
                        <dp:i18Select name="mt" id="mt" options="${1..50}" noSelectText="15" noSelectValue="15"/>
                    </div>

                </div>

                <div class="form-group select_control">
                    <label class="control-label col-sm-5"><g:message
                            code="application.interest.rate"></g:message> (%):</label>

                    <div class="col-sm-7">

                        <dp:i18Select

                                name="ir" id="ir"
                                optionPrefix=""
                                options="${Banks.findAll().collect {
                                    it.rates + " by " + it.bankname
                                }}"

                                noSelectText="13 by Default"
                                noSelectValue="16"/>


                        <div class="collapse">
                            <input type="text" name="ir" id="ir" placeholder="Add here" class="form-control">
                        </div>
                        <script type="text/javascript">
                            $(document).ready(function () {
                                $(".btn-default").click(function () {
                                    $(".collapse").collapse('toggle');
                                });

                            });
                        </script>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-5"><g:message
                            code="application.property.tax"></g:message> (${currency}):</label>

                    <div class="col-sm-7">
                        <input type="text" name="pt" value="<%=(Integer.parseInt(price) * 0.01)%>"
                               class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-5"><g:message
                            code="application.property.insuarance"></g:message> (${currency}):</label>

                    <div class="col-sm-7">
                        <input type="text" name="pi" value="<%=(Integer.parseInt(price) * 0.25)%>"
                               class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-5"><g:message
                            code="application.property.pmi"></g:message>(%):</label>

                    <div class="col-sm-7">
                        <input type="text" name="mi" value="0.52" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-5"><g:message
                            code="application.firstpayment.month"></g:message>:</label>

                    <div class="col-sm-7">
                        <select name="sm" id="sm" class="form-control">

                            <option label="${a}" value="${mon}" selected="selected">${a}</option>
                            <option label="Jan" value="1">Jan</option>
                            <option label="Feb" value="2">Feb</option>
                            <option label="Mar" value="3">Mar</option>
                            <option label="Apr" value="4">Apr</option>
                            <option label="May" value="5">May</option>
                            <option label="Jun" value="6">Jun</option>
                            <option label="Jul" value="7">Jul</option>
                            <option label="Aug" value="8">Aug</option>
                            <option label="Sep" value="9">Sep</option>
                            <option label="Oct" value="10">Oct</option>
                            <option label="Nov" value="11">Nov</option>
                            <option label="Dec" value="12">Dec</option>
                        </select>
                    </div>

                </div>

                <div class="form-group">
                    <label class="control-label col-sm-5"><g:message
                            code="application.firstpayment.year"></g:message>:</label>

                    <div class="col-sm-7 select_control">

                        <dp:i18Select name="sy" id="sy" options="${year - 3..year + 3}" noSelectText="Current Year"
                                      noSelectValue="${year}"/>

                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label col-sm-5"><g:message
                            code="application.firstpayment.amortization"></g:message>:</label>

                    <div class="col-sm-7">
                        <label>
                            <input type="radio" name="as" value="year"> <g:message
                                code="application.showby.year"></g:message></label><br>

                        <label>
                            <input type="radio" name="as" value="month" checked="checked"> <g:message
                                code="application.showby.month"></g:message></label><br>

                        <label>
                            <input type="radio" name="as" value="none"> <g:message
                                code="application.showby.dontshow"></g:message></label><br>
                    </div>

                </div>


                <div class="form-group">
                    <div class="col-sm-5 offset"></div>

                    <div class="col-sm-7">
                        <button type="submit" name="sbmt" class="btn btn-primary"><g:message
                                code="application.calculate"></g:message></button>
                    </div>
                </div>

            </form>
            <span class="hidden">Powered by <a href="http://www.mlcalc.com/">Mortgage Calculator</a></span>

            <br>

            <div class="col-sm-5 offset"></div>

            <div class="col-sm-7">
                <br>
                <br>
                <g:form controller="tool" action="applyMortgage">
                    <input type="hidden" name="prc" value="${price}">
                    <input type="hidden" name="cnt" value="Tanzania">
                    <input type="hidden" name="crr" value="${currency}">
                    <input type="submit" value="Apply For a Mortgage" name="sbmt2" class="btn btn-primary"
                           style="box-shadow:0px 2px 5px #888;">
                </g:form>


            </div>

        </div>

        <div class="col-lg-5">
            <div class="rslides">

                <g:each var="advert" in="${calculatorAdverts}">
                    <li>
                        <div class="item">
                            <a href="${advert.url}" title="${advert.caption}" target="_blank">
                                <img class="img-polaroid"
                                     src="<g:createLink controller='image' action='renderAdvert' id='${advert.id}'/>"
                                     alt="${advert.caption}">
                            </a>
                        </div>
                    </li>
                </g:each>

            </div>
        </div>

    </div>
</div>

</body>
</html>