<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="org.grails.datastore.gorm.finders.MethodExpression.IsEmpty" %>
<%@ page import="com.mbwanajm.darproperty.Country" %>
<%@ page import="com.mbwanajm.darproperty.Profile; com.mbwanajm.darproperty.Country; com.mbwanajm.darproperty.Tag; com.mbwanajm.darproperty.SaleType; com.mbwanajm.darproperty.Region; com.mbwanajm.darproperty.Type" %>

<html>
<head>
    <title>DarProperty: Home Value Estimate</title>
    <meta content="main" name='layout'/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'responsiveslides.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'js/js_slider', file: 'ninja-slider.css')}"/>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Bree+Serif"/>


    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://cdn.mlcalc.com/widget-api.js"></script>
    <g:javascript src="lib/responsiveslides.min.js"/>
    <g:javascript src="darproperty/HomeSlideShows.js"/>
    <g:javascript src="lib/mortgageCalculator.js"/>
    <g:javascript src='darproperty/CountryLoader.js'/>
    <g:javascript src='darproperty/RegionLoader.js'/>
    <style type="text/css">
    .grpItems {
        background: #F0F8FF;
    }
    </style>

</head>

<body>
<div class="container">
    <!-- TOP -->
    <div class="row">
        <div class="col-sm-12">
            <h3 align="center">${g.message(code: 'application.home')} ${g.message(code: 'menu.tools.valueEstimator')}</h3>
        </div>
    </div>
    <!-- TOP -->

    <!-- CENTER -->


    <div class="margine2"></div>

    <div class="row">

        <div class="col-sm-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="glyphicon glyphicon-flag"></span>
                    <g:message code="application.property"></g:message>
                    <span class="hd2"><g:message
                            code="application.location"></g:message></span>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-7">
                            <g:form class="form-horizontal" role="form" action="calculateValue" method="Post">
                                <div class="form-group">
                                    <div class="col-lg-5"><label class="control-label"><g:message
                                            code="property.country"></g:message> *</label></div>

                                    <div class="col-lg-6 select_control">
                                        <dp:i18Select

                                                id="property_country"
                                                name='country'
                                                optionPrefix=""
                                                options="${Country.findAll().collect { it.name }}"
                                                showAnyOption=""
                                                noSelectText=""
                                                noSelectValue=""
                                                size="50"
                                                required="required"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-5"><label class="control-label"><g:message
                                            code="property.region"></g:message> *</label></div>

                                    <div class="col-lg-6 select_control">
                                        <dp:i18Select
                                                id='property_region'
                                                name="region"
                                                optionPrefix=""
                                                options="${Region.findAll().collect { it.name }}"
                                                showAnyOption=""
                                                noSelectText=""
                                                noSelectValue=""
                                                required="required"/>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-5"><label class="control-label"><g:message
                                            code="property.area"></g:message> *</label></div>

                                    <div class="col-lg-6 select_control">
                                        <dp:i18Select
                                                id='property_areas'
                                                name="area"
                                                optionPrefix=""
                                                options="${Region.findByName("Dar es salaam").areas.sort()}"
                                                showAnyOption=""
                                                noSelectText=""
                                                noSelectValue=""
                                                required="required"/>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-5 offset"></div>

                                    <div class="col-lg-6">
                                        <input type="submit" name="location" value="Proceed >>"
                                               class="btn btn-block btn-primary">
                                    </div>

                                </div>

                            </g:form>
                        </div>

                        <div class="col-lg-5">
                            <div class="rslides">

                                <g:each var="advert" in="${calculatorAdverts}">
                                    <li>
                                        <div class="item">
                                            <a href="${advert.url}" title="${advert.caption}" target="_blank">
                                                <img class="img-polaroid"
                                                     src="<g:createLink controller='image' action='renderAdvert'
                                                                        id='${advert.id}'/>"
                                                     alt="${advert.caption}">
                                            </a>
                                        </div>
                                    </li>
                                </g:each>

                            </div>
                        </div>
                    </div>

                </div>

                <div class="panel-footer">

                    <g:message code="homevaluenote"></g:message>
                    <br>
                    <small class="hd2">Copyright © DarProperty Home Value Calculator</small>

                </div>

            </div>

        </div>
    </div>

</div>

</body>
</html>