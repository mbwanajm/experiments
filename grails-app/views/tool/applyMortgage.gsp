<%--
  Created by IntelliJ IDEA.
  User: 1stHands
  Date: 8/7/2016
  Time: 5:50 PM
--%>

<%@ page import="com.mbwanajm.darproperty.Banks; com.mbwanajm.darproperty.Parteners_banks" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>DarProperty: Apply Mortgage</title>
    <meta name='layout' content="main">
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'responsiveslides.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'js/js_slider', file: 'ninja-slider.css')}"/>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Bree+Serif"/>

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://cdn.mlcalc.com/widget-api.js"></script>
    <g:javascript src="lib/responsiveslides.min.js"/>
    <g:javascript src="darproperty/HomeSlideShows.js"/>
    <g:javascript src="lib/mortgageCalculator.js"/>
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h3 align="center">${g.message(code: 'application.applymortgage')}</h3>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-7">
            <div class="form-group">
                <g:hasErrors bean="${flash.errorBean}">
                    <div class="alert alert-danger text-center">
                        <g:renderErrors bean="${flash.errorBean}" as="list"/>
                    </div>
                </g:hasErrors>

                <g:if test="${flash.successMessage}">
                    <div class='alert alert-success text-center'>${flash.successMessage}</div>
                </g:if>
                <g:if test="${flash.errorMessage}">
                    <div class='alert alert-danger text-center'>${flash.errorMessage}</div>
                </g:if>
            </div>

            <g:form action="applyMortgage" id="form" name="applicationForm" role="form" method="post"
                    class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-5"><g:message code="application.yourname"></g:message>:</label>

                    <div class="col-sm-7">
                        <input type="text" name="names" id="names" value="${params.names}" class="form-control"
                               placeholder="Firstname Lastname"
                               required="" autocomplete="off">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-5"><g:message code="application.yourphone"></g:message>:</label>

                    <div class="col-sm-7">
                        <input type="tel" name="phone" value="${params.phone}" id="phone" class="form-control"
                               maxlength="13"
                               placeholder="Eg: 0800000000 " required="" autocomplete="off">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-5"><g:message code="application.youremail"></g:message>:</label>

                    <div class="col-sm-7">
                        <input type="email" name="email" id="email" value="${params.email}" class="form-control"
                               placeholder="address@domain"
                               required="" autocomplete="off">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-5"><g:message code="property.country"></g:message>:</label>

                    <div class="col-sm-7">
                        <select name="country" class="form-control">
                            ${params.country ? "<option value='" + params.country + "'>" + params.country + "</option>" : ""}
                            <option value="Tanzania">Tanzania</option>
                            <option value="Kenya">Kenya</option>
                            <option value="Rwanda">Rwanda</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-5"><g:message code="application.applyto"></g:message>:</label>

                    <div class="col-sm-7 select_control">
                        <select name="bank" class="form-control" id="bank">
                            ${params.bank ? "<option value=\"" + params.bank + "\">" + params.bank + "</option>" : ""}
                            <g:each var="bank" in="${Banks.list()}">
                                <option value="${bank.id}">${bank.bankname}</option>
                            %{--<option value="${Parteners_banks.findByBank([bank.id])}">${bank.bankname}</option>--}%
                            </g:each>
                        </select>

                    </div>
                </div>

                <div id="loading">
                    <div class="form-group" align="center">
                        <img src="${resource(dir: 'bootstrap/imgs/', file: '3.gif')}"/>
                        <span class="hd3"><g:message code="application.sending"></g:message>...</span>
                        <!-- loading image -->

                    </div>

                </div>

                <fieldset class="fieldparts">
                    <legend><g:message code="application.confirm"></g:message>:</legend>

                    <div class="form-group">
                        <label class="control-label col-sm-5"><g:message
                                code="application.property.price"></g:message> (${currency}):</label>

                        <div class="col-sm-7">
                            <input type="hidden" name="crr" value="${currency}">
                            <input type="number" name="prce" id="prce" class="form-control" value="${price}" required=""
                                   autocomplete="off">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-5"><g:message
                                code="application.down.payment"></g:message> (%):</label>

                        <div class="col-sm-7">
                            <input type="text" name="dp" id="dp" value="${params.dp ? params.dp : 10}"
                                   class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-5"><g:message
                                code="application.mortgage.term"></g:message> (<g:message
                                code="application.years"></g:message>):</label>

                        <div class="col-sm-7 select_control">
                            <dp:i18Select name="terms" id="terms" options="${1..50}" noSelectText="15" noSelectValue="15"/>
                        </div>
                    </div>

                    <div class='form-group'>
                        <div class='col-sm-offset-4 col-sm-5'>
                            <img src="${createLink(controller: 'simpleCaptcha', action: 'captcha')}"/>
                            <br>
                            <label for="captcha">Type the letters above in the box below:</label>
                            <g:textField name="captcha" class="form-control" autocomplete="off" placeholder="Type Here"
                                         value="${params.captcha}"/>
                        </div>
                    </div>
                </fieldset>

                <div class="form-group">
                    <div class="col-sm-5 offset"></div>

                    <div class="col-sm-7">

                        <button type="submit" name="sbmt3" class="btn btn-success" value="apply" id="sbmt3"
                                data-loading-text="Sending..."
                                style="box-shadow:0px 2px 5px #888;">
                            <span class="glyphicon glyphicon-envelope"></span> <g:message
                                code="application.sendapplication"></g:message>
                        </button>

                    </div>
                </div>

            </g:form>
        </div>

        <div class="col-lg-5">
            <div class="rslides">

                <g:each var="advert" in="${calculatorAdverts}">
                    <li>
                        <div class="item">
                            <a href="${advert.url}" title="${advert.caption}" target="_blank">
                                <img class="img-polaroid"
                                     src="<g:createLink controller='image' action='renderAdvert' id='${advert.id}'/>"
                                     alt="${advert.caption}">
                            </a>
                        </div>
                    </li>
                </g:each>

            </div>
        </div>

    </div>

</div>
</body>
</html>