<%--
  Created by IntelliJ IDEA.
  User: DAR PROPERTY
  Date: 8/9/2016
  Time: 11:36 AM
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="org.grails.datastore.gorm.finders.MethodExpression.IsEmpty" %>
<%@ page import="com.mbwanajm.darproperty.Country" %>
<%@ page import="com.mbwanajm.darproperty.Profile; com.mbwanajm.darproperty.Country; com.mbwanajm.darproperty.Tag; com.mbwanajm.darproperty.SaleType; com.mbwanajm.darproperty.Region; com.mbwanajm.darproperty.Type" %>

<html>
<head>
    <title>DarProperty: Calculate Value</title>
    <meta content="main" name='layout'/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'responsiveslides.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'js/js_slider', file: 'ninja-slider.css')}"/>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Bree+Serif"/>

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://cdn.mlcalc.com/widget-api.js"></script>
    <g:javascript src="lib/responsiveslides.min.js"/>
    <g:javascript src="darproperty/HomeSlideShows.js"/>
    <g:javascript src="lib/mortgageCalculator.js"/>
    <g:javascript src='darproperty/CountryLoader.js'/>
    <g:javascript src='darproperty/RegionLoader.js'/>
    <style type="text/css">
    .grpItems {
        background: #F0F8FF;
    }
    </style>
</head>

<body>

%{--CONTENTS--}%
<div class="container">
    <%@ page import="com.mbwanajm.darproperty.Amenities" %>


    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading"><span class="glyphicon glyphicon-list-alt"></span> <g:message
                        code="application.property"></g:message> <span class="hd2"><g:message
                        code="estimate.particulars"></g:message></span></div>

                <div class="panel-body">
                    <g:link action="valueCalculator"><span class="glyphicon glyphicon-repeat"><g:message
                            code="application.restart"></g:message></span></g:link><br>

                    <g:form class="form-horizontal" role="form" action="estimateResults" method="Post" name="anemitiesForm">

                        <span class="hd2"><g:message
                                code="application.location"></g:message>:</span> ${country} , ${region}, ${area}
                        <input type="hidden" name="areaLocated" id="areaLocated" value="${area}"/>
                        <input type="hidden" name="regionLocated" id="regionLocated" value="${region}"/>
                        <hr>
                        <h4><g:message code="application.fillall"></g:message></h4>
                        <hr>

                        <div class="form-group">

                            <div class="col-md-3">
                                <label class="control-label"><g:message
                                        code="estimate.plotarea"></g:message> <small>(<g:message
                                        code="application.sqm"></g:message>)</small>:</label>
                            </div>

                            <div class="col-md-4">
                                <input type="number" name="plotarea" id="plotarea"
                                       placeholder="Put 0, if its' an appartment" class="form-control" value="0"
                                       autocomplete="off" maxlength="5" required="required"/>
                            </div>

                        </div>

                        <div class="form-group">

                            <div class="col-md-3">
                                <label class="control-label"><g:message
                                        code="estimate.builtarea"></g:message> <small>(<g:message
                                        code="application.sqm"></g:message>)</small>* :</label>
                            </div>

                            <div class="col-md-4">
                                <input type="number" name="builtup" id="builtup" placeholder="per Sq.Meters"
                                       class="form-control" maxlength="5" autocomplete="off" required="required"/>
                            </div>

                        </div>
                        <h4><g:message code="application.itemsAvailable"></g:message></h4>
                        <hr>



                        %{--<%=ame.displayUngrouped()%>--}%
                        <g:render template="amenities" collection="${amenities}"
                                  var="ame"></g:render>


                        <hr>

                        <div class="form-group">
                            <div class="col-md-3">
                                <input type="submit" name="calculate" value="ESTIMATE" id="calculate"
                                       class="btn btn-success">
                            </div>
                        </div>
                    </g:form>

                </div>

                <div class="panel-footer"><small class="hd2">Copyright © DarProperty Home Value Calculator</small></div>
            </div>

        </div>
    </div>

</div>
%{--CONTENTS--}%
</body>
</html>