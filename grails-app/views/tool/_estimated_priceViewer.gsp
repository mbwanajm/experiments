<%@ page import="com.mbwanajm.darproperty.CurrencyService; java.text.NumberFormat" %>
<% CurrencyService currencyService = new CurrencyService(); %>
<g:if test="${show == 'front'}">
    <span class="small-price">
        <%
            def num = prc
            def pr = 0
            def rep = ""
            def hold = num / 1000000000
            if (hold < 1) {
                hold = num / 1000000
                if (hold >= 1) {
                    pr = num
                    rep = "M"
                } else {
                    hold = num / 1000
                    if (hold >= 1) {
                        pr = num
                        rep = "K"
                    } else {

                        pr = num

                    }

                }
            } else {
                rep = "Bil"
                pr = hold
            }

        %>
        <g:if test="${rep == "M"}">
            <span style="font-size: 8pt;">${NumberFormat.getInstance().format(pr)}</span>


            <span class="mid-currency">${selectedCurrency == "USD" ? '$' : selectedCurrency}
                <a tabindex="0"
                   class="text-info"
                   role="button" data-toggle="popover"
                   data-placement="bottom"
                   data-trigger="focus"
                   title="Price Convertor"
                   data-content="
               TZS ${NumberFormat.getInstance().format(currencyService.convert(num, selectedCurrency, 'TZS'))}
                    , RWF ${NumberFormat.getInstance().format(currencyService.convert(num, selectedCurrency, 'RWF'))}
               , KES ${NumberFormat.getInstance().format(currencyService.convert(num, selectedCurrency, 'KES'))}
               , USD ${NumberFormat.getInstance().format(currencyService.convert(num, selectedCurrency, 'USD'))}
                   %{--<%=tz.co.darproperty.CurrencyConvertor.multipleValues(selling_price, selectedCurrency)%>--}%
                   "><span
                        class="fa fa-chevron-circle-down"></span></a>
            </span>

        </g:if>
        <g:elseif test="${rep == "K"}">
            <span style="font-size: 9pt;">${NumberFormat.getInstance().format(pr)}</span>


            <span class="mid-currency">${selectedCurrency == "USD" ? '$' : selectedCurrency}
                <a tabindex="0"
                   class="text-info"
                   role="button" data-toggle="popover"
                   data-placement="bottom"
                   data-trigger="focus"
                   title="Price Convertor"
                   data-content="
                 TZS ${NumberFormat.getInstance().format(currencyService.convert(num, selectedCurrency, 'TZS'))}
                    , RWF ${NumberFormat.getInstance().format(currencyService.convert(num, selectedCurrency, 'RWF'))}
               , KES ${NumberFormat.getInstance().format(currencyService.convert(num, selectedCurrency, 'KES'))}
               , USD ${NumberFormat.getInstance().format(currencyService.convert(num, selectedCurrency, 'USD'))}
                   "><span
                        class="fa fa-chevron-circle-down"></span></a>
            </span>
        </g:elseif>
        <g:elseif test="${rep == "Bil"}">
            <span style="font-size: 12pt;">${NumberFormat.getInstance().format(pr)} ${rep}</span>


            <span class="mid-currency">${selectedCurrency == "USD" ? '$' : selectedCurrency}
                <a tabindex="0"
                   class="text-info"
                   role="button" data-toggle="popover"
                   data-placement="bottom"
                   data-trigger="focus"
                   title="Price Convertor"
                   data-content="
            TZS ${NumberFormat.getInstance().format(currencyService.convert(num, selectedCurrency, 'TZS'))}
                    , RWF ${NumberFormat.getInstance().format(currencyService.convert(num, selectedCurrency, 'RWF'))}
               , KES ${NumberFormat.getInstance().format(currencyService.convert(num, selectedCurrency, 'KES'))}
               , USD ${NumberFormat.getInstance().format(currencyService.convert(num, selectedCurrency, 'USD'))}
                   "><span
                        class="fa fa-chevron-circle-down"></span></a>
            </span>
        </g:elseif>
        <g:else>
            <span style="font-size: 12pt;">${NumberFormat.getInstance().format(pr)}</span>


            <span class="mid-currency">${selectedCurrency == "USD" ? '$' : selectedCurrency}
                <a tabindex="0"
                   class="text-info"
                   role="button" data-toggle="popover"
                   data-placement="bottom"
                   data-trigger="focus"
                   title="Price Convertor"
                   data-content="
               TZS ${NumberFormat.getInstance().format(currencyService.convert(num, selectedCurrency, 'TZS'))}
                    , RWF ${NumberFormat.getInstance().format(currencyService.convert(num, selectedCurrency, 'RWF'))}
               , KES ${NumberFormat.getInstance().format(currencyService.convert(num, selectedCurrency, 'KES'))}
               , USD ${NumberFormat.getInstance().format(currencyService.convert(num, selectedCurrency, 'USD'))}
                   "><span
                        class="fa fa-chevron-circle-down"></span></a>
            </span>
        </g:else>

    </span>

</g:if>
<g:else>
    <span class="small-price">
        <%
            num = prc
            pr = 0
            rep = ""
            hold = num / 1000000000
            if (hold < 1) {
                hold = num / 1000000
                if (hold >= 1) {
                    pr = num
                    rep = "M"
                } else {
                    hold = num / 1000
                    if (hold >= 1) {
                        pr = num
                        rep = "K"
                    } else {

                        pr = num

                    }

                }
            } else {
                rep = "Bil"
                pr = hold
            }

        %>
        <g:if test="${rep == "M"}">
            <span style="font-size: 8pt;">${NumberFormat.getInstance().format(pr)}</span>


            <span class="mid-currency">${selectedCurrency == "USD" ? '$' : selectedCurrency}
                <a tabindex="0"
                   class="text-info"
                   role="button" data-toggle="popover"
                   data-placement="bottom"
                   data-trigger="focus"
                   title="Price Convertor"
                   data-content="
                    TZS ${NumberFormat.getInstance().format(currencyService.convert(num, selectedCurrency, 'TZS'))}
                    , RWF ${NumberFormat.getInstance().format(currencyService.convert(num, selectedCurrency, 'RWF'))}
               , KES ${NumberFormat.getInstance().format(currencyService.convert(num, selectedCurrency, 'KES'))}
               , USD ${NumberFormat.getInstance().format(currencyService.convert(num, selectedCurrency, 'USD'))}
                   "><span
                        class="fa fa-chevron-circle-down"></span></a>
            </span>

        </g:if>
        <g:elseif test="${rep == "K"}">
            <span style="font-size: 9pt;">${NumberFormat.getInstance().format(pr)}</span>


            <span class="mid-currency">${selectedCurrency == "USD" ? '$' : selectedCurrency}
                <a tabindex="0"
                   class="text-info"
                   role="button" data-toggle="popover"
                   data-placement="bottom"
                   data-trigger="focus"
                   title="Price Convertor"
                   data-content="
                  TZS ${NumberFormat.getInstance().format(currencyService.convert(num, selectedCurrency, 'TZS'))}
                    , RWF ${NumberFormat.getInstance().format(currencyService.convert(num, selectedCurrency, 'RWF'))}
               , KES ${NumberFormat.getInstance().format(currencyService.convert(num, selectedCurrency, 'KES'))}
               , USD ${NumberFormat.getInstance().format(currencyService.convert(num, selectedCurrency, 'USD'))}
                   "><span
                        class="fa fa-chevron-circle-down"></span></a>
            </span>
        </g:elseif>
        <g:elseif test="${rep == "Bil"}">
            <span style="font-size: 12pt;">${NumberFormat.getInstance().format(pr)} ${rep}</span>


            <span class="mid-currency">${selectedCurrency == "USD" ? '$' : selectedCurrency}
                <a tabindex="0"
                   class="text-info"
                   role="button" data-toggle="popover"
                   data-placement="bottom"
                   data-trigger="focus"
                   title="Price Convertor"
                   data-content="
                  TZS ${NumberFormat.getInstance().format(currencyService.convert(num, selectedCurrency, 'TZS'))}
                    , RWF ${NumberFormat.getInstance().format(currencyService.convert(num, selectedCurrency, 'RWF'))}
               , KES ${NumberFormat.getInstance().format(currencyService.convert(num, selectedCurrency, 'KES'))}
               , USD ${NumberFormat.getInstance().format(currencyService.convert(num, selectedCurrency, 'USD'))}
                   "><span
                        class="fa fa-chevron-circle-down"></span></a>
            </span>
        </g:elseif>
        <g:else>
            <span style="font-size: 12pt;">${NumberFormat.getInstance().format(pr)}</span>


            <span class="mid-currency">${selectedCurrency == "USD" ? '$' : selectedCurrency}
                <a tabindex="0"
                   class="text-info"
                   role="button" data-toggle="popover"
                   data-placement="bottom"
                   data-trigger="focus"
                   title="Price Convertor"
                   data-content="
                    TZS ${NumberFormat.getInstance().format(currencyService.convert(num, selectedCurrency, 'TZS'))}
                    , RWF ${NumberFormat.getInstance().format(currencyService.convert(num, selectedCurrency, 'RWF'))}
               , KES ${NumberFormat.getInstance().format(currencyService.convert(num, selectedCurrency, 'KES'))}
               , USD ${NumberFormat.getInstance().format(currencyService.convert(num, selectedCurrency, 'USD'))}
                   "><span
                        class="fa fa-chevron-circle-down"></span></a>
            </span>
        </g:else>

    </span>

</g:else>