<%--
  Created by IntelliJ IDEA.
  User: DAR PROPERTY
  Date: 8/9/2016
  Time: 11:36 AM
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="org.grails.datastore.gorm.finders.MethodExpression.IsEmpty" %>
<%@ page import="com.mbwanajm.darproperty.Country" %>
<%@ page import="com.mbwanajm.darproperty.Profile; com.mbwanajm.darproperty.Country; com.mbwanajm.darproperty.Tag; com.mbwanajm.darproperty.SaleType; com.mbwanajm.darproperty.Region; com.mbwanajm.darproperty.Type" %>

<html>
<head>
    <title>DarProperty: Estimator Results</title>
    <meta content="main" name='layout'/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'responsiveslides.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'js/js_slider', file: 'ninja-slider.css')}"/>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Bree+Serif"/>

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://cdn.mlcalc.com/widget-api.js"></script>
    <g:javascript src="lib/responsiveslides.min.js"/>
    <g:javascript src="darproperty/HomeSlideShows.js"/>
    <g:javascript src="lib/mortgageCalculator.js"/>
    <g:javascript src='darproperty/CountryLoader.js'/>
    <g:javascript src='darproperty/RegionLoader.js'/>
    <style type="text/css">
    .grpItems {
        background: #F0F8FF;
    }
    </style>
</head>

<body>

%{--CONTENTS--}%
<div class="container">
    <%@ page import="tz.co.darproperty.*" %>
    <%@ page import="java.text.NumberFormat" %>

%{--${count_amenities}--}%


        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><span class="glyphicon glyphicon-list-alt"></span> <g:message
                            code="application.value"></g:message> <span class="hd2"><g:message
                            code="application.results"></g:message></span></div>

                    <div class="panel-body">
                        <g:link action="valueCalculator"><span class="glyphicon glyphicon-repeat"><g:message
                                code="application.restart"></g:message></span></g:link><br><br>



                            <div class="row">

                                <div class="col-sm-5">

                                    <div class="input-group col-xs-12">
                                        <span class="input-group-addon"><span class="hd2"><g:message
                                                code="estimate.price"></g:message></span></span>
                                        <input type="text" class="form-control" style="font-size: 14pt;height: 50px;"
                                               value="${NumberFormat.getInstance().format(selling_price)}"
                                               readonly="readonly" aria-describedby="basic-addon1">
                                        <span class="input-group-addon" id="basic-addon3">$</span>
                                    </div><br>

                                    <div class="input-group col-xs-12">
                                        <span class="input-group-addon"><span class="hd2"><g:message
                                                code="estimate.rent"></g:message></span></span>
                                        <input type="text" class="form-control" style="font-size: 14pt;height: 50px;"
                                               value="${NumberFormat.getInstance().format(rental_price)}"
                                               readonly="readonly" aria-describedby="basic-addon1">
                                        <span class="input-group-addon" id="basic-addon4">$/<g:message
                                                code="application.month"></g:message></span>
                                    </div>

                                </div>

                                <div class="col-sm-2 offset"></div>

                                <div class="col-sm-5">
                                    <h5 class="wid-view-more"><span
                                            class="glyphicon glyphicon-comment"></span> Say Something About Our Home Value Calculator
                                    </h5>

                                    <!-- START -->
                                    <div id="disqus_thread"></div>
                                    <script>
                                        /**
                                         * RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                                         * LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
                                         */
                                        /*
                                         var disqus_config = function () {
                                         this.page.url = PAGE_URL; // Replace PAGE_URL with your page's canonical URL variable
                                         this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                                         };
                                         */
                                        (function () { // DON'T EDIT BELOW THIS LINE
                                            var d = document, s = d.createElement('script');

                                            s.src = '//darpropertyforum.disqus.com/embed.js';

                                            s.setAttribute('data-timestamp', +new Date());
                                            (d.head || d.body).appendChild(s);
                                        })();
                                    </script>
                                    <noscript>Please enable JavaScript to view the <a
                                            href="https://disqus.com/?ref_noscript"
                                            rel="nofollow">comments powered by Disqus.</a></noscript>
                                    <!-- END -->

                                </div>
                            </div>



                    </div>
                </div>
            </div>
        </div>

</div>
%{--CONTENTS--}%
</body>
</html>