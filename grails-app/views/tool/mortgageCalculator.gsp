<%--
  Created by IntelliJ IDEA.
  User: Aloyce JPM
  Date: 16/01/16
  Time: 15:09
  To change this template use File | Settings | File Templates.
--%>



<%@ page import="java.net.Authenticator.RequestorType" %>
<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title>DarProperty: Mortgage calculator</title>
    <meta name='layout' content="main">
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'responsiveslides.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'js/js_slider', file: 'ninja-slider.css')}"/>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Bree+Serif"/>

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://cdn.mlcalc.com/widget-api.js"></script>
    <g:javascript src="lib/responsiveslides.min.js"/>
    <g:javascript src="darproperty/HomeSlideShows.js"/>
    <g:javascript src="lib/mortgageCalculator.js"/>

</head>

<body>

<!-- BANK RATES -->
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h3 align="center">${g.message(code: 'menu.tools.mortgageCalculator')}</h3>
        </div>

    </div>

    <div class="row">



        <div class="col-lg-5">
            <g:form action="calculateMortgage">
                <br><br>
                <div class="form-group">
                    <label class="control-label col-sm-5"><g:message
                            code="application.property.price"></g:message>:</label>

                    <div class="col-sm-7">
                        <input type="number" name="prce" id="prce" maxlength="7" class="form-control"
                               placeholder="Eg. 300000" required="" autocomplete="off">
                    </div>
                </div>
                <br><br>

                <div class="form-group">
                    <label class="control-label col-sm-5"><g:message
                            code="application.country.currency"></g:message>:</label>

                    <div class="col-sm-7">
                        <select name="cont" class="form-control">
                            <option value="USD">USA, USD</option>
                            <option value="TZS">TANZANIA, TZS</option>
                        </select>
                    </div>
                </div>
                <br><br>

                <div class="form-group">
                    <div class="col-sm-5 offset"></div>

                    <div class="col-sm-7">
                        <input type="submit" value="Proceed" class="btn btn-success" name="mort">
                    </div>
                </div>
            </g:form>

        </div>

        <div class="col-lg-7">

            <br><br>

            <div class="rslides">

                <g:each var="advert" in="${calculatorAdverts}">
                    <li>
                        <div class="item">
                            <a href="http://${advert.url}" title="${advert.caption}" target="_blank">
                                <img class="img-polaroid"
                                     src="<g:createLink controller='image' action='renderAdvert' id='${advert.id}'/>"
                                     alt="${advert.caption}">
                            </a>
                        </div>
                    </li>
                </g:each>

            </div>
            <br><br>
            <g:form action="applyMortgage">
                <input type="hidden" name="prc" value="10000">
                <input type="hidden" name="cnt" value="10">
                <input type="hidden" name="crr" value="USD">
                <input type="submit" value="Apply For a Mortgage" name="sbmt2" class="btn btn-primary"
                       style="box-shadow:0px 2px 5px #888;">
            </g:form>

        </div>
    </div>
</div>




<br><br>
</body>
</html>