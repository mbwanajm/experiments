<div class="issue col-lg-10 hidden-sm hidden-xs">
    <h5 style="padding:10px; color:#fff; width:100%;" class="text-center">
        <g:message code="home.latestIssue"/> (${magazine.title})</h5>
    <g:link controller='monthlyIssue' action='viewLatestMagazine'>
        <img class='latestissue img-responsive'
             src="${g.createLink(controller: 'image', action: 'renderMonthlyIssuePhoto', id: magazine.id)}"
             alt=" Latest Issue"/>
    </g:link>
    <span class="visible-xs "><br></span>
    <g:link controller="monthlyIssue" action="showAllMagazines" class="btn btn-default" role="button">
        <small><i class="fa fa-list-alt"></i> <g:message code="home.allissue"/></small>
    </g:link>
</div>

<div class="issue col-lg-10 hidden-md hidden-lg">
    <br>
    <small style="padding:10px; color:#fff; background-color: red; font-size:9pt;">
        <g:message code="home.latestIssue"/> (${magazine.title})</small>
    <br>
    <g:link controller='monthlyIssue' action='viewLatestMagazine'>
        <img class=' img-responsive' width="150"
             src="${g.createLink(controller: 'image', action: 'renderMonthlyIssuePhoto', id: magazine.id)} "
             alt='latest issue'>
    </g:link>
    <span class="visible-xs "></span>
    <g:link controller="monthlyIssue" action="showAllMagazines" class="btn btn-default" role="button">
        <small><i class="fa fa-list-alt"></i> <g:message code="home.allissue"/></small>
    </g:link>
</div>
