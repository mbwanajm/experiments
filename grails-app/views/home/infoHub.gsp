<html>
<head>
    <title>Dar Property>>Info Hub</title>
    <meta name="layout" content="main"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'responsiveslides.css')}"/>
    <g:javascript src="lib/responsiveslides.min.js"/>
    <g:javascript src="darproperty/HomeSlideShows.js"/>
    <g:javascript src="lib/mortgageCalculator.js"/>
    <g:javascript src="lightslider/js/lightslider.js"/>
    <g:javascript src="lib/contentSlider.js"/>
    <g:javascript src="lib/facebook.js"/>
    <g:javascript src="lib/twitter.js"/>
</head>

<body>
<div class="container">

    <div class="row">
        <div class="col-sm-12">
            <h3 align="center"><g:message code="menu.infoHub"></g:message></h3>
        </div>
    </div>
</div>
<br>

<div class="container">

    <div class="row">
        <div class="col-sm-offset-2 col-sm-10 pull-right">
            <g:render template="infohub_slide_show"></g:render>
        </div>
    </div>

    <div class="row" >


                <p class="hd1">
        <br>
                <hr class="style-one"/>
            </p>


        </div>

    <g:render template="infohub_documents"></g:render>


    </div>

    <!--<div class="row">
        <br>

        <p class="hd1"><g:message code="home.article"/> <span class="hd2"><g:message code="library"/></span><small
                class="issue pull-right">

        </small>
        <hr/></p>

        <g:render template="../search/article_search"></g:render>
        <br>
        <g:render template="/article/article_teaser" collection="${latestArticles}" var="article"/>
    </div>

    <div class="row pagination pull-right">
        <g:paginate total="${numberOfArticles}"/>
        <br>
        <br>
    </div> -->
</div>

</body>

</html>


