<%@ page import="com.mbwanajm.darproperty.*" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="layout" content="main" id="hometb "/>
    <!--Web icon-->

</head>
<% def hometb = "active" %>
<body id="top">
%{--Body start--}%


<!-- slider area -->
<div class="slider-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <!-- Slider -->
                <g:render template="toppage_slide_show" model="[mainAdverts: mainAdverts]"></g:render>
                <!-- ./Slider -->
            </div>

            <div class="col-lg-3" align="center">

                <h5 class="redhead text-center">
                    <g:message code="home.latestIssue"/> (${magazine.title})</h5>
                <g:link controller='monthlyIssue' action='viewLatestMagazine'>
                    <img class='latestissue img-responsive'
                         src="${g.createLink(controller: 'image', action: 'renderMonthlyIssuePhoto', id: magazine.id)}"
                         alt=" Latest Issue"/>
                </g:link>
                <span class="visible-xs "><br></span><br>
                <g:link controller="monthlyIssue" action="showAllMagazines" class="btn btn-default" role="button">
                    <small><i class="fa fa-list-alt"></i> <g:message code="home.allissue"/></small>
                </g:link>
            </div>
        </div>
    </div>
</div>
<!-- End slider area -->
<!--search panel-->
<br>
<g:render template="/search/home_only_search"></g:render>
<!--End search panel-->
<!-- Promo area -->
<g:render template="site_features_teaser"></g:render>
<!-- End promo area -->

<!-- Articles -->
<div class="container">
    <div class="row">
        <div class="col-lg-12">

            <g:link controller="article" action="articleLibrary" class=" pull-right">
                <i class="fa fa-book"></i> <g:message code="Article-Library"/>:
            </g:link>
            <span class="wid-view-more pull-left">
                <span class=""><g:message code="home.latest.articles"/></span>
            </span>


        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <hr class="style-one"/>
        </div>

    </div>

    <div class="row">
        <br>
        <g:render template="/article/article_teaser" collection="${latestArticles}" var="article"/>
    </div>
</div>
<!-- End Articles -->



%{--Featured Property--}%
<g:render template="../property/featured_property"
          model="[featuredProperty: featuredProperty,featuredProperty2: latestProperty, footerAdverts: footerAdverts, ]"></g:render>
%{--End Featured Property--}%

%{--Latest Property--}%
%{--<g:render template="../property/latest_property"--}%
          %{--model="[featuredProperty: latestProperty, footerAdverts: footerAdverts2]"></g:render>--}%
%{--End Latest Property--}%

%{--Wanted properties--}%
<g:render template="wantedproperty_section"
          model="[side1: side1, latestEnquiries: latestEnquiries, latestEnquiriesCount: latestEnquiriesCount]"></g:render>
%{--End Wanted properties--}%



</body>
</html>