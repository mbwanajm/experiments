

<div class="promo-area">
    <div class="zigzag-bottom"></div>

    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="single-promo promo1">
                    <g:form controller="tool" action="applyMortgage" method="post">
                        <input type="hidden" name="prc" value="10000">
                        <input type="hidden" name="cnt" value="10">
                        <button  type="submit" value="Apply For a Mortgage" name="sbmt2" class="btn btn-primary"
                                style=" border:0px solid #fff; background-color: inherit; padding: 0px;">
                            <img src="../newlook/img/mortageapplication.png">
                        </button>
                    </g:form>
                    <p class="text-capitalize"><g:message code="application.mortgageapplication"></g:message></p>
                </div>
            </div>

            <div class="col-md-3 col-sm-2">
                <div class="single-promo promo2">
                    <g:link controller="tool" action="valueCalculator" tabindex="-1" title="Guess-Estimator">
                        <img src="${resource(dir:'newlook/img/',file: 'homeestimate.png' )}">
                    </g:link>
                    <p class="text-capitalize"><g:message code="home.tools.valueEstimator"></g:message></p>
                </div>
            </div>

            <div class="col-md-3 col-sm-6">
                <div class="single-promo promo4">
                    <g:link controller="tool" action="mortgageCalculator" tabindex="-1" title="Mortgage Calculator">
                        <img src="${resource(dir:'newlook/img/',file: 'mortgage-calculator.png' )}">
                    </g:link>
                    <p class="text-capitalize"><g:message code="home.tools.mortgageCalculator"></g:message></p>
                </div>
            </div>

            <div class="col-md-3 col-sm-6">
                <div class="single-promo promo3">
                    <g:link controller="monthlyIssue" action="showAllMagazines" title="">
                        <img src="../newlook/img/magazines.png">
                    </g:link>
                    <p class="text-capitalize"><g:message code="menu.magazine"></g:message></p>
                </div>
            </div>
        </div>
    </div>
</div>