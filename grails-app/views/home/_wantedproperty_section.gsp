<div class="container">
    <div class="row">
        <div class="col-md-12">
            %{--<hr class="style-one">--}%
        </div>
    </div>
    <div class="row">
        <g:render template="sideoneadvert" model="${side1}"></g:render>

        <div class="col-lg-6 ">
            <div class="row">
                <div class="col-lg-10">
                    <span class="wid-view-more"><g:message code="home.wanted.property"/>
                        <g:message code="home.wanted"/>
                    </span>
                    <span class="text-danger pull-right hidden-sm hidden-xs blink_me">
                        <g:message code="home.post"></g:message> <i class=" fa fa-hand-o-down"></i>
                    </span>
                    <hr class="style-one visible-lg visible-md"/>
                </div>

                <div class="col-lg-2 ">

                    %{--<g:link controller="search" action="user_enquiry">--}%
                    <a href="../search/user_enquiry#addenquiry">
                        <img src="../images/place_add.png" alt="Place your addvert here"
                             class="img-responsive blink_me" title="place your advert" style="overflow: auto; ">
                    </a>
                    %{--</g:link>--}%

                </div>
            </div>

            <g:render template="enquiries_template"
                      model="${[enquiries: latestEnquiries, countEnquiries: latestEnquiriesCount]}"></g:render>

        </div>
    </div>
</div>
