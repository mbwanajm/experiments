<html>
<head>
    <title>Dar Property>>Why Advertise</title>
    <meta name="layout" content="main"/>
</head>

<body>
<div class="container">
    <g:render template="../pageTitle" model="[title: g.message(code: 'menu.advertise.whyAdvertise')]"></g:render>
    <div class="row margin-top-20px small-font">

        <div class="col-sm-8">
           <g:message code="whyAdvertise"></g:message>
        </div>

        <div class="col-sm-4">
            <g:message code="advertising.menubar"></g:message>
        </div>

    </div>

</div>
</body>

</html>


