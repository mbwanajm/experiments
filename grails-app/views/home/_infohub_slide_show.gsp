<div class="rslides" data-ride="carousel">

    <!-- Wrapper for slides -->


    <div class="item active">
        <a href="${resource(dir: 'downloads', file: 'The Architects and  Quantity Surveyors _Registration_ Act. Cap 269.pdf')}">
            <img class='info-hub-spot' src="${resource(dir: 'img', file: 'Architects_Concept2.png')}"
                 class="img-responsive"/>
        </a>
    </div>

    <div class="item">
        <a href="${resource(dir: 'downloads', file: 'The Building Societies Act, Cap 87_0.pdf')}">
            <img class='info-hub-spot' src="${resource(dir: 'img', file: 'Building.png')}"
                 class="img-responsive"/>
        </a>
    </div>

    <div class="item">
        <a href="${resource(dir: 'downloads', file: 'The Mortgage Financing (Special Provisions) Act No. 17 of 2008.pdf')}">
            <img class='info-hub-spot' src="${resource(dir: 'img', file: 'Mortgage_Concept2.png')}"
                 class="img-responsive"/>
        </a>
    </div>

    <div class="item">
        <a href="${resource(dir: 'downloads', file: 'The Unit Titles Act No. 16 of 2008.pdf')}">
            <img class='info-hub-spot' src="${resource(dir: 'img', file: 'Unit-Titles_concept-2.png')}"
                 class="img-responsive"/>
        </a>
    </div>

    <div class="item">
        <a href="${resource(dir: 'downloads', file: 'The National Housing Corporation Act. Cap 295.pdf')}">
            <img class='info-hub-spot' src="${resource(dir: 'img', file: 'NHC.png')}"
                 class="img-responsive"/>
        </a>
    </div>

    <div class="item">
        <a href="reportAndStatistics">
            <img class='info-hub-spot'
                 src="${resource(dir: 'img', file: 'Reports,Statistics & Trends.png')}"
                 class="img-responsive"/>
        </a>
    </div>

    <div class="item">
        <a href="A Bill For The Estate Agency Act,2015">
            <img class='info-hub-spot'
                 src="${resource(dir: 'img', file: 'A Bill For The Estate Agency Act,2015.jpg')}"
                 class="img-responsive"/>
        </a>
    </div>

</DIV>