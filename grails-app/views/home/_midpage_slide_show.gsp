
        <div class="rslides" style="border-radius:0px;">

            <g:each var="advert" in="${footerAdverts}">

                    <div class="item">
                        <a href="${advert.url}" title="${advert.caption}" target="_blank">
                            <img class="img-polaroid" style="width:100%;"
                                 src="<g:createLink controller='image' action='renderAdvert'
                                                    id='${advert.id}'/>"
                                 alt="${advert.caption}" class="img-responsive"/>
                        </a>
                    </div>

            </g:each>
        </div>
