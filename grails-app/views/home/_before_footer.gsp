<div class="container" style=" border-radius:5px; padding:5px;" align="center">
    <!-- PHONE VIEW -->
    <div class="row hidden-lg hidden-md">
        <div class="col-lg-12" align="center">
            <a href="http://expo.darproperty.co.tz" class="hoverlink" target="_blank">
                <img src="${resource(dir: "images", file: "expo_logo.png")}" alt="expo.darproperty.co.tz"
                     class="img-responsive">
            </a>
        </div>
    </div>

    <div class="row hidden-lg hidden-md">
        <div class="col-lg-12" align="center">
            <g:link controller="insurance" class="hoverlink" target="_blank">
                <img src="${resource(dir: "images", file: "insurance_logo.png")}" alt="darproperty.co.tz/insurance"
                     class="img-responsive">
            </g:link>
        </div>
    </div>
</div>

<div class="row hidden-lg hidden-md">
    <div class="col-lg-12" align="center">
        <g:link controller="mortgage" class="hoverlink" target="_blank">
            <img src="${resource(dir: "images", file: "mortgage_logo.jpg")}" alt="darproperty.co.tz/mortgage"
                 class="img-responsive">
        </g:link>
    </div>
</div>

<div class="row hidden-lg hidden-md">
    <div class="col-sm-12 " align="center">
        <a href="https://play.google.com/store/apps/details?id=tz.co.darproperty.magazine"
           title="The Darproperty App." class="hoverlink" target="_blank">
            <img src="${resource(dir: 'bootstrap/imgs/', file: 'playstore.png')}"
                 alt="The Darproperty App"/>
        </a>
    </div>
</div>


<div class="margine2"></div>

<!-- Products Logo -->
<div class="container" align="center">

    <div class="row hidden-sm hidden-xs">

        %{--<div class="col-sm-3" align="left">--}%
        %{--<p class="others hidden-sm hidden-xs">OUR OTHER PRODUCTS    <span class="glyphicon glyphicon-hand-right"></span></p>--}%
        %{--</div>--}%

        <div class="col-lg-3">
            <g:link controller="mortgage" target="_blank" class="hoverlink">
                <img src="${resource(dir: "images", file: "mortgage_logo.jpg")}" alt="darproperty.co.tz/mortgage"
                     class="img-responsive">
            </g:link>
        </div>

        <div class="col-lg-3">
            <a href="http://expo.darproperty.co.tz" target="_blank" class="hoverlink">
                <img src="${resource(dir: "images", file: "expo_logo.png")}" alt="expo.darproperty.co.tz"
                     class="img-responsive">
            </a>
        </div>

        <div class="col-lg-3">
            <g:link controller="insurance" target="_blank" class="hoverlink">
                <img src="${resource(dir: "images", file: "insurance_logo.png")}" alt="darproperty.co.tz/insurance"
                     class="img-responsive">
            </g:link>
        </div>

        <div class="col-lg-3 ">
            <a href="https://play.google.com/store/apps/details?id=tz.co.darproperty.magazine" class="hoverlink"
               title="The Darproperty App." target="_blank">
                <img src="${resource(dir: 'bootstrap/imgs/', file: 'playstore.png')}"
                     alt="The Darproperty App"/>
            </a>
        </div>

    </div>
</div>

<div class="container">

    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title mod_txt" align="center">ADDING ADVERT FORM</h4>
                </div>

                <div class="modal-body">

                    <fieldset>

                        <g:form controller="receipts" action='receiptsRegister' method='post'>
                            <div class="form-group">
                                <label class="control-label col-sm-3 ">Reference No:</label>

                                <div class="col-sm-6">
                                    <g:textField name="refno" placeholder="Enter Your Ref.no Here" required=""
                                                 class="form-control" autocomplete="off"/>
                                </div>
                                <br><br>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-3 offset"></div>

                                <div class="col-sm-6">
                                    <input type="submit" value="Proceed >>" name="step0" class='btn btn-primary'>
                                </div>
                            </div>

                        </g:form>

                    </fieldset>

                </div>

                <div class="modal-footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-6">
                                <p class="hd4"
                                   align="left">NB: If you do not have the reference number or you didn't pay for an advert <a
                                        href="">click here</a>.</p>
                            </div>

                            <div class="col-sm-6">
                                <button type="button" class="btn btn-default " data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>

<script>
    $(document).ready(function () {
        $("#myBtn").click(function () {
            $("#myModal").modal();
        });
    });
</script><br>