%{--<div  class="rslides" data-ride="carousel">--}%

%{--<!-- Wrapper for slides -->--}%

%{--<g:each status='index' var="advert" in="${mainAdverts}">--}%
%{--<g:if test="${index == 0}">--}%
%{--<div class="item active">--}%
%{--<g:render template="home_slide_show" model="${[advert: advert]}"></g:render>--}%
%{--</div>--}%
%{--</g:if>--}%
%{--<g:else>--}%
%{--<div class="item">--}%
%{--<g:render template="home_slide_show" model="${[advert: advert]}"></g:render>--}%
%{--</div>--}%
%{--</g:else>--}%
%{--</g:each>--}%


%{--</div>--}%


<div class="block-slider block-slider4">
    <ul class="" id="bxslider-home4">
        <g:each status='index' var="advert" in="${mainAdverts}">

            <li>
                <img src="<g:createLink controller='image' action='renderAdvert' id='${advert.id}'/>"
                     alt="${advert.caption}"/>
                %{--<img src="../newlook/img/h4-slide.png" alt="Slide">--}%

                <div class="caption-group">
                    %{--<h4 class="caption title">--}%
                         %{--<span class="primary">${advert.caption} <i class="fa fa-arrow-down"></i> <strong></strong>--}%
                    %{--</span>--}%
                    %{--</h4>--}%

                    <a href="${advert.url}"
                       class="caption button-radius"
                       title="${advert.caption}" target="_blank"><span class="icon"></span> <g:message code="know.more"></g:message> </a>

                </div>
            </li>
        </g:each>
    </ul>
</div>