
<!-- LIST OF ENQUIRIES  -->
<div class="list-group table-autoflow">
    <g:if test="${enquiries.name}">
        <g:render template="../enquiries/latestEnquiries" collection="${enquiries}" var="enquiry"></g:render>



        <g:if test="${countEnquiries[0] > 3}">
        <g:link controller="search" action="user_enquiry" class="btn btn-block btn-danger">



                <i class="fa fa-arrow-circle-down"></i> << <g:message code="home.loadmore"></g:message> >> <i
                    class="fa fa-arrow-circle-down"></i>

        </g:link>
        </g:if>
    %{--<dp:paginateEnquiries total="${countEnquiries}"></dp:paginateEnquiries>--}%
    </g:if>
    <g:else>
    <p class="alert alert-info text-center"> <g:message code="enquiry.befirst"></g:message> </p>
</g:else>

</div>

<!-- LIST OF ENQUIRIES -->



