<div class="row">
<p class="pull-left"><g:message code="read"></g:message>:</p>
<br>

<p>
<ul class="list-unstyled" style="font-size:9pt;">
    <li class="col-lg-6">
        <label class="control-label col-lg-1 glyphicon glyphicon-hand-right"></label>
        <span class="col-lg-11 infolink">
            <a href="${resource(dir: 'downloads', file: 'The Architects and  Quantity Surveyors _Registration_ Act. Cap 269.pdf')}">
                The Architects and  Quantity Surveyors Registration Act. Cap 269
            </a>
        </span>
    </li>

    <li class="col-lg-6">
        <label class="control-label col-lg-1 glyphicon glyphicon-hand-right"></label>
        <span class="col-lg-11 infolink">
            <a href="${resource(dir: 'downloads', file: 'The Building Societies Act, Cap 87_0.pdf')}">
                The Building Societies Act, Cap 87 0
            </a></span>
    </li>

    <li class="col-lg-6">
        <label class="control-label col-lg-1 glyphicon glyphicon-hand-right"></label>
        <span class="col-lg-11 infolink">
            <a href="${resource(dir: 'downloads', file: 'The Mortgage Financing (Special Provisions) Act No. 17 of 2008.pdf')}">
                The Mortgage Financing (Special Provisions) Act No. 17 of 2008
            </a>
        </span>
    </li>

    <li class="col-lg-6">
        <label class="control-label col-lg-1 glyphicon glyphicon-hand-right"></label>
        <span class="col-lg-11 infolink">
            <a href="${resource(dir: 'downloads', file: 'The Unit Titles Act No. 16 of 2008.pdf')}">
                The Unit Titles Act No. 16 of 2008
            </a>
        </span>
    </li>

    <li class="col-lg-6">
        <label class="control-label col-lg-1 glyphicon glyphicon-hand-right"></label>
        <span class="col-lg-11 infolink">
            <a href="${resource(dir: 'downloads', file: 'The National Housing Corporation Act. Cap 295.pdf')}">
                The National Housing Corporation Act. Cap 295
            </a>
        </span>
    </li>

    <li class="col-lg-6">
        <label class="control-label col-lg-1 glyphicon glyphicon-hand-right"></label>
        <span class="col-lg-11 infolink">
            <a href="reportAndStatistics">
                Reports,Statistics & Trends
            </a>
        </span>
    </li>

    <li class="col-lg-6">
        <label class="control-label col-lg-1 glyphicon glyphicon-hand-right"></label>
        <span class="col-lg-11 infolink">
            <a href="A Bill For The Estate Agency Act,2015">
                A Bill For The Estate Agency Act, 2015
            </a>
        </span>
    </li>

</ul>
</p>
</div>