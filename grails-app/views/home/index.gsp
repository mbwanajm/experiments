<!DOCTYPE html>
<html lang="en">
<head>
    <title><g:layoutTitle default="DarProperty: East Africa's Ultimate Property Guide"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="${resource(dir: 'css', file: 'main.css')}"/>

    <link rel="shortcut icon" href="${resource(dir: 'images', file: 'shortcutIcon.png')}" type="image/x-icon"/>
    <link rel="stylesheet" href="${resource(dir: 'bootstrap/css', file: 'bootstrap.min.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'bootstrap/css', file: 'custom.css')}"/>
    <g:javascript src="lib/jquery-1.8.3.js"/>
    <g:javascript src="lib/bootstrap.js"/>
    <g:javascript src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"/>
    <g:javascript src="bootstrap/js/bootstrap.min.js"/>
    <g:javascript type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-33712962-1']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        })();

    </g:javascript>

    <g:layoutHead/>

</head>

<body>
<!--

-->
<div class="container">

    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-header navbar-center">
            <g:link controller="home" action="home" class="navbar-brand">
                <img alt="Logo" src="${resource(dir: 'bootstrap/imgs/', file: 'logo.png')}"/>
            </g:link>
        </div>

        <div class="navbar-header">

            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#main-collapse" aria-expanded="false">
                <span class="sr-only">Toogle</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

        </div>
        <!--  collapse  -->
        <div class="collapse navbar-collapse" id="main-collapse">
            <ul class="nav navbar-nav navbar-left">
                <li><g:link controller="home" action="home">
                    <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                </g:link>
                </li>
                <li><g:link controller="aboutUS" action="aboutUS">
                    <g:message code="menu.aboutUs"/>
                </g:link></li>
                <li><g:link controller="MonthlyIssue" action="showAllMagazines">
                    <g:message code="menu.magazine"/>
                </g:link></li>
                <li><a href="http://international.darproperty.co.tz">
                    <g:message code="menu.international"/>
                </a></li>
                <li><a href="#">Forums <span class="sr-only">(current)</span></a></li>
                <li><g:link controller="agent" action="showAllAgents">
                    <g:message code="menu.agents"/>
                </g:link></li>


                <li class="dropdown">
                    <g:link controller="home" action="infoHub" class="dropdown-toggle" data-toggle="dropdown"
                            role="button" aria-haspopup="true" aria-expanded="false">
                        The Hub<span class="caret"></span></g:link>
                    <ul class="dropdown-menu">
                        <li><g:link controller="home" action="infoHub">
                            <g:message code="menu.infoHub"/>
                        </g:link></li>
                        <li><g:link controller="article" action="readArticle" id="45">
                            Library
                        </g:link></li>

                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        <g:message code="menu.advertise"/> <span class="caret"></span></a>
                    <ul class="dropdown-menu">

                        <li>
                            <g:link controller="home" action='whyAdvertise' tabindex="-1">
                                <g:message code="menu.advertise.whyAdvertise"/>
                            </g:link>
                        </li>
                        <li>
                            <g:link controller='home' action='advertisingPolicy' tabindex="-1">
                                <g:message code="menu.advertise.advertisingPolicy"/>
                            </g:link>
                        </li>
                        <li role="separator" class="divider"></li>
                        <li>
                            <a href="${resource(dir: 'img', file: 'rates.pdf')}">
                                <g:message code="menu.advertise.rateCard"/>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        <g:message code="menu.tools"/><span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <g:link controller="tool" action="mortgageCalculator" tabindex="-1">
                                <g:message code="menu.tools.mortgageCalculator"/>
                            </g:link>
                            <g:link controller="tool" action="valueCalculator"
                                    tabindex="-1"><g:message code="menu.tools.valueEstimator"/>
                            </g:link>
                        </li>

                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        Other Services<span class="caret"></span></a>
                    <ul class="dropdown-menu">

                        <li><a href="">Mortgage</a></li>
                        <li role="separator" class="divider"></li>

                        <li><a href="">Exhibition</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="">Insurance</a></li>

                    </ul>
                </li>
            </ul>


            <ul class="nav navbar-nav navbar-right">
                <sec:ifLoggedIn>
                    <li>
                        <g:link controller="agent" action="profile" id="${sec.loggedInUserInfo(field: 'id')}">
                            My Account
                        </g:link>
                    </li>
                    <li>
                        <g:link controller="logout">
                            <g:message code="menu.logout"/>
                        </g:link>
                    </li>
                </sec:ifLoggedIn>
                <sec:ifNotLoggedIn>
                    <li>
                        <g:link controller="agent" action="authenticate">
                            <g:message code="menu.login"/>
                        </g:link>
                    </li>
                </sec:ifNotLoggedIn>


                <span class="lang"><g:link controller='home' action="home" params="[lang: 'sw']"><img
                        src="${resource(dir: 'bootstrap/imgs/', file: 'tz.png')}" title="Kiswahili"/></g:link>
                    | <g:link controller='home' action="home" params="[lang: 'en']"><img
                        src="${resource(dir: 'bootstrap/imgs/', file: 'gb.png')}" title="English"/></g:link></span>

            </ul>
        </div>
    </nav>
</div>


<br> <br> <br> <br>

<!-- fb -->
<div id="fb-root"></div>
<script>(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5&appId=1670558763159469";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- /fb -->

<div class="container">
    <div class="row">
        <div class="col-lg-9">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <g:each status='index' var="advert" in="${mainAdverts}">
                        <g:if test="${index == 0}">
                            <div class="item active">
                                <g:render template="home_slide_show" model="${[advert: advert]}"></g:render>
                            </div>
                        </g:if>
                        <g:else>
                            <div class="item">
                                <g:render template="home_slide_show" model="${[advert: advert]}"></g:render>
                            </div>
                        </g:else>
                    </g:each>
                </div>
                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>

        <div class="col-lg-3" align="center">
            <div class="issue">
                <a href="#" class="thumbnail">
                    <h5><g:message code="home.latestIssue"/> (${magazine.title})</h5>
                    <img class='latestissue img-polaroid'
                         src="${g.createLink(controller: 'image', action: 'renderMonthlyIssuePhoto', id: magazine.id)}"
                         alt=" Latest Issue"/>
                </a>

                <g:link controller="monthlyIssue" action="showAllMagazines" class="btn btn-default" role="button">
                    <span>See all issues:</span> <span class="glyphicon glyphicon-folder-open"></span>
                </g:link>
            </div>

        </div>
    </div>
</div>


<div class="margine2"></div>

<div class="container">
    <div class="well">
        <div class="row">
            <div class="col-sm-8">
                <div class="container">

                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" href="#home">House</a></li>
                        <li><a data-toggle="tab" href="#menu1">Apartment</a></li>
                        <li><a data-toggle="tab" href="#menu2">Office</a></li>
                        <li><a data-toggle="tab" href="#menu3">Plot</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="row">
                            <div class="col-xs-1">
                                <div id="home" class="tab-pane fade in active">
                                    <div class="btn-group-vertical" role="group" aria-label="...">
                                        <br>
                                        <a data-toggle="tab" href="#rent" class="btn btn-info active">RENT</a>
                                        <a data-toggle="tab" href="#buy" class="btn btn-info ">BUY</a>

                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-11">
                                <div class="tab-content">
                                    <div id="rent" class="tab-pane fade">
                                        <h3>RENT</h3>

                                        <p>Rent the house</p>
                                    </div>

                                    <div id="buy" class="tab-pane fade ">
                                        <h3>BUY</h3>

                                        <p>Buy the house</p>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div id="menu1" class="tab-pane fade">
                            <h3>Appartment</h3>

                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        </div>

                        <div id="menu2" class="tab-pane fade">
                            <h3>Office</h3>

                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                        </div>

                        <div id="menu3" class="tab-pane fade">
                            <h3>Plot</h3>

                            <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                        </div>
                    </div>

                </div>

            </div>

            <div class="col-sm-2 offset"></div>

            <div class="col-sm-2">
                <button type="button" class="btn btn-info glyphicon glyphicon-arrow-up" data-toggle="tooltip"
                        title="SELL NOW !">
                    <br>POST YOUR LISTING</button>
            </div>
        </div>
    </div>

</div>

<!-- 
<div class='row home-slide-show-container'>
    <div class='span9'>
        <ul class='rslides'>
            <g:each status='index' var="advert" in="${mainAdverts}">
    <li>
    <g:if test="${index == 0}">
        <div class="item active">
        <g:render template="home_slide_show" model="${[advert: advert]}"></g:render>
        </div>
    </g:if>
    <g:else>
        <div class="item">
        <g:render template="home_slide_show" model="${[advert: advert]}"></g:render>
        </div>
    </g:else>
    </li>
</g:each>
        </ul>

    </div>
    <div class='span3'>
        <g:render template="/search/advanced_search"/>
    </div>
</div>

 -->
<div class="container">
    <div class="row">
        <p class="hd1">News & <span class="hd2">Articles</span>
        <hr/></p>
        <div class="col-sm-6 col-md-1">

            <img alt="Others" title="others" height="150" src="${resource(dir: 'bootstrap/imgs/', file: 'prev.png')}"/>

        </div>

        <g:render template="/article/article_teaser" collection="${latestArticles}" var="article"/>
        <div class="col-sm-6 col-md-1">

            <img alt="Others" title="more" height="150" src="${resource(dir: 'bootstrap/imgs/', file: 'next.png')}"/>

        </div>

    </div>

    <div class="row">
        <p align="right" class="issue">
            <g:link controller="article" action="readArticle" id="1" class="btn btn-default" role="button">
                <span class="glyphicon glyphicon-folder-open">Open library..</span></g:link>
        </p>
    </div>

</div>

<div class="container">
    <div class="row">
        <p class="hd1">Latest <span class="hd2">Properties</span>
        <hr/></p>
        <div class="col-sm-6 col-md-1">

            <img alt="Others" title="others" height="150" src="${resource(dir: 'bootstrap/imgs/', file: 'prev.png')}"/>

        </div>
        <g:render template="/property/property_advert" collection="${latestProperty}" var="property"/>
        <div class="col-sm-6 col-md-1">

            <img alt="Others" title="more" height="150" src="${resource(dir: 'bootstrap/imgs/', file: 'next.png')}"/>

        </div>
    </div>

    <div class="row">
        <p align="right" class="issue">
            <g:link controller="property" action="showAllProperties" class="btn btn-default">
                <span class="glyphicon glyphicon-folder-open">Show all..</span></g:link>
        </p>
    </div>
</div>


<div class="container">
    <div class="row">
        <p class="hd1">Tools <span class="hd2">Kit</span>
        <hr/>

        <div class="col-sm-4" align="left">
            <a class="btn btn-primary" role="button" data-toggle="collapse" href="#value" aria-expanded="false"
               aria-controls="value">
                VALUE ESTIMATOR
            </a>

            <div class="collapse" id="value">
                <div class="well">
                    ...
                </div>
            </div>
        </div>

        <div class="col-sm-4" align="center">
            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#mortgage"
                    aria-expanded="false" aria-controls="mortgage">
                MORTGAGE CALCULATOR
            </button>

            <div class="collapse" id="mortgage">
                <div class="well">
                    <div class='row visible-desktop'>
                        <div class='span4'>

                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-sm-4" align="right">
            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#prop"
                    aria-expanded="false" aria-controls="prop">
                PROPERTY ALERT
            </button>

            <div class="collapse" id="prop">
                <div class="well">
                    ...
                </div>
            </div>
        </div>
    </div>
</p>

</div>

<div class="margine2"></div>
<!--  -->

<div class="container">
    <div class="row">
        <div class="col-xs-1 offset"></div>

        <div class="col-xs-10">
            <div class="rslides">
                <g:each var="advert" in="${footerAdverts}">
                    <li>
                        <div class="item">
                            <a href="${advert.url}" title="${advert.caption}" target="_blank">
                                <img class="img-polaroid"
                                     src="<g:createLink controller='image' action='renderAdvert' id='${advert.id}'/>"
                                     alt="${advert.caption}">
                            </a>
                        </div>
                    </li>
                </g:each>
            </div>
        </div>

        <div class="col-xs-1 offset"></div>
    </div>
</div>


<br>
</body>
</html>
