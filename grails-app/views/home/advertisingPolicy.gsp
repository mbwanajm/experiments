<html>
<head>
    <title>Dar Property: Advertising Policy</title>
    <meta name="layout" content="main"/>
</head>

<body>
<div class="container">

    <g:render template="../pageTitle" model="[title: g.message(code: 'menu.advertise.advertisingPolicy')]"></g:render>
    <div class="row small-font margin-top-20px">
        <div class="col-sm-12">

            <g:message code="advertisingPolicy"/>

        </div>

    </div>
</div>
</body>

</html>
