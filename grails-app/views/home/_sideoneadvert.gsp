<div class="col-lg-6">
    %{--SIDE 1 ADVERT--}%
    <div class="rslides">

        <g:each var="advert" in="${side1}">

                <div class="item">
                    <a href="${advert.url}" title="${advert.caption}" target="_blank">
                        <img
                                src="<g:createLink controller='image' action='renderAdvert' id='${advert.id}'/>"
                                alt="${advert.caption}" class="img-responsive" style=""/>
                    </a>
                </div>

        </g:each>

    </div>
    <p class="hidden-lg hidden-md">
<br>

    </p>
    %{--/SIDE 1 ADVERT--}%
</div>