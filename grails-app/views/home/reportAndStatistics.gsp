<html>
<head>
    <title>Dar Property>>Info Hub</title>
    <meta name="layout" content="main"/>
</head>

<body>
<g:render template="../pageTitle" model="[title: g.message(code: 'menu.reportStatitisticsAndTrends')]"></g:render>
<br>

<div class="row">
    <div class="span12" align="center">
        <br>
        <br>
        <ul>
            <li>
                <a href="${resource(dir: 'downloads', file: 'TANZANIA MORTGAGE MARKET UPDATE 2014.pdf')}">
                    <h4>Mortgage Market Update 2014</h4>
                </a>
            </li>
        </ul>
        <ul>
            <li>
                <a href="${resource(dir: 'downloads', file: 'Presentation on Real Estate Meeting.pdf')}">
                    <h4>Avic Town Presentation On Real Estate Meeting</h4>
                </a>
            </li>
        </ul>
        <br>
        <br>
    </div>
</div>

<br>

</body>

</html>


