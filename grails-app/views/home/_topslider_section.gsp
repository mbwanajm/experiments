<div class="slider-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <!-- Slider -->
                <g:render template="toppage_slide_show" model="[mainAdverts: mainAdverts]"></g:render>
                <!-- ./Slider -->
            </div>

            <div class="col-lg-3" align="center">

                <h5 class="redhead text-center">
                    <g:message code="home.latestIssue"/> (${magazine.title})</h5>
                <g:link controller='monthlyIssue' action='viewLatestMagazine'>
                    <img class='latestissue img-responsive'
                         src="${g.createLink(controller: 'image', action: 'renderMonthlyIssuePhoto', id: magazine.id)}"
                         alt=" Latest Issue"/>
                </g:link>
                <span class="visible-xs "><br></span><br>
                <g:link controller="monthlyIssue" action="showAllMagazines" class="btn btn-default" role="button">
                    <small><i class="fa fa-list-alt"></i> <g:message code="home.allissue"/></small>
                </g:link>
            </div>
        </div>
    </div>
</div>