
<%@ page import="com.mbwanajm.darproperty.Advert_prices" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'advert_prices.label', default: 'Advert_prices')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-advert_prices" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-advert_prices" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list advert_prices">
			
				<g:if test="${advert_pricesInstance?.advert_type}">
				<li class="fieldcontain">
					<span id="advert_type-label" class="property-label"><g:message code="advert_prices.advert_type.label" default="Adverttype" /></span>
					
						<span class="property-value" aria-labelledby="advert_type-label"><g:fieldValue bean="${advert_pricesInstance}" field="advert_type"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${advert_pricesInstance?.price_per_day}">
				<li class="fieldcontain">
					<span id="price_per_day-label" class="property-label"><g:message code="advert_prices.price_per_day.label" default="Priceperday" /></span>
					
						<span class="property-value" aria-labelledby="price_per_day-label"><g:fieldValue bean="${advert_pricesInstance}" field="price_per_day"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${advert_pricesInstance?.link_to_table}">
				<li class="fieldcontain">
					<span id="link_to_table-label" class="property-label"><g:message code="advert_prices.link_to_table.label" default="Linktotable" /></span>
					
						<span class="property-value" aria-labelledby="link_to_table-label"><g:fieldValue bean="${advert_pricesInstance}" field="link_to_table"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${advert_pricesInstance?.id}" />
					<g:link class="edit" action="edit" id="${advert_pricesInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
