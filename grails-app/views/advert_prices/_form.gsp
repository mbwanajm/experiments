<%@ page import="com.mbwanajm.darproperty.Advert_prices" %>



<div class="fieldcontain ${hasErrors(bean: advert_pricesInstance, field: 'advert_type', 'error')} required">
	<label for="advert_type">
		<g:message code="advert_prices.advert_type.label" default="Adverttype" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="advert_type" required="" value="${advert_pricesInstance?.advert_type}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: advert_pricesInstance, field: 'price_per_day', 'error')} required">
	<label for="price_per_day">
		<g:message code="advert_prices.price_per_day.label" default="Priceperday" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="price_per_day" value="${fieldValue(bean: advert_pricesInstance, field: 'price_per_day')}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: advert_pricesInstance, field: 'link_to_table', 'error')} ">
	<label for="link_to_table">
		<g:message code="advert_prices.link_to_table.label" default="Linktotable" />
		
	</label>
	<g:textField name="link_to_table" value="${advert_pricesInstance?.link_to_table}"/>
</div>

