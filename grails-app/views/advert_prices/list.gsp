
<%@ page import="com.mbwanajm.darproperty.Advert_prices" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'advert_prices.label', default: 'Advert_prices')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-advert_prices" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-advert_prices" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="advert_type" title="${message(code: 'advert_prices.advert_type.label', default: 'Adverttype')}" />
					
						<g:sortableColumn property="price_per_day" title="${message(code: 'advert_prices.price_per_day.label', default: 'Priceperday')}" />
					
						<g:sortableColumn property="link_to_table" title="${message(code: 'advert_prices.link_to_table.label', default: 'Linktotable')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${advert_pricesInstanceList}" status="i" var="advert_pricesInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${advert_pricesInstance.id}">${fieldValue(bean: advert_pricesInstance, field: "advert_type")}</g:link></td>
					
						<td>${fieldValue(bean: advert_pricesInstance, field: "price_per_day")}</td>
					
						<td>${fieldValue(bean: advert_pricesInstance, field: "link_to_table")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${advert_pricesInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
