<%@ page import="com.mbwanajm.darproperty.Subaminities" %>



<div class="fieldcontain ${hasErrors(bean: subaminitiesInstance, field: 'subject', 'error')} required">
	<label for="subject">
		<g:message code="subaminities.subject.label" default="Subject" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="subject" maxlength="200" required="" value="${subaminitiesInstance?.subject}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: subaminitiesInstance, field: 'point', 'error')} required">
	<label for="point">
		<g:message code="subaminities.point.label" default="Point" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="point" value="${fieldValue(bean: subaminitiesInstance, field: 'point')}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: subaminitiesInstance, field: 'visible', 'error')} required">
	<label for="visible">
		<g:message code="subaminities.visible.label" default="Visible" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="visible" type="number" value="${subaminitiesInstance.visible}" required=""/>
</div>

