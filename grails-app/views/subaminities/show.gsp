
<%@ page import="com.mbwanajm.darproperty.Subaminities" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'subaminities.label', default: 'Subaminities')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-subaminities" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-subaminities" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list subaminities">
			
				<g:if test="${subaminitiesInstance?.subject}">
				<li class="fieldcontain">
					<span id="subject-label" class="property-label"><g:message code="subaminities.subject.label" default="Subject" /></span>
					
						<span class="property-value" aria-labelledby="subject-label"><g:fieldValue bean="${subaminitiesInstance}" field="subject"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${subaminitiesInstance?.point}">
				<li class="fieldcontain">
					<span id="point-label" class="property-label"><g:message code="subaminities.point.label" default="Point" /></span>
					
						<span class="property-value" aria-labelledby="point-label"><g:fieldValue bean="${subaminitiesInstance}" field="point"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${subaminitiesInstance?.visible}">
				<li class="fieldcontain">
					<span id="visible-label" class="property-label"><g:message code="subaminities.visible.label" default="Visible" /></span>
					
						<span class="property-value" aria-labelledby="visible-label"><g:fieldValue bean="${subaminitiesInstance}" field="visible"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${subaminitiesInstance?.id}" />
					<g:link class="edit" action="edit" id="${subaminitiesInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
