
<%@ page import="com.mbwanajm.darproperty.Subaminities" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'subaminities.label', default: 'Subaminities')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-subaminities" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-subaminities" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="subject" title="${message(code: 'subaminities.subject.label', default: 'Subject')}" />
					
						<g:sortableColumn property="point" title="${message(code: 'subaminities.point.label', default: 'Point')}" />
					
						<g:sortableColumn property="visible" title="${message(code: 'subaminities.visible.label', default: 'Visible')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${subaminitiesInstanceList}" status="i" var="subaminitiesInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${subaminitiesInstance.id}">${fieldValue(bean: subaminitiesInstance, field: "subject")}</g:link></td>
					
						<td>${fieldValue(bean: subaminitiesInstance, field: "point")}</td>
					
						<td>${fieldValue(bean: subaminitiesInstance, field: "visible")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${subaminitiesInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
