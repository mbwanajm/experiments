<div class="container">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h2 class="section-heading">Other Coverage</h2>

            <h3 class="section-subheading text-muted">Other covers provided by us.</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <ul class="timeline">
                <li>
                    <div class="timeline-image">
                        <img class="img-circle img-responsive" src="img/about/marine_cargo.jpg" alt="">
                    </div>

                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4>Marine Cargo Insurance</h4>
                            <h4 class="subheading">Export and Import</h4>
                        </div>

                        <div class="timeline-body">
                            <p class="text-muted">Insurance for Imports/Exports comes standard with many commonly requested extensions, such as cover for delayed unpacking (up to 90 days), extended transit and goods transported to the wrong destination.</p>
                        </div>
                    </div>
                </li>
                <li class="timeline-inverted">
                    <div class="timeline-image">
                        <img class="img-circle img-responsive" src="img/about/marine.jpg" alt="">
                    </div>

                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4>Marine Insurance</h4>
                            <h4 class="subheading">Marine (Ship) Hull</h4>
                        </div>

                        <div class="timeline-body">
                            <p class="text-muted">Covers the loss or damage of ships, cargo, terminals, and any transport or cargo by which property is transferred, acquired, or held between the points of origin and final destination.</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="timeline-image">
                        <img class="img-circle img-responsive" src="img/about/aircraft.jpg" alt="">
                    </div>

                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4>Aviation Insurance</h4>
                            <h4 class="subheading">Aircraft Insurance</h4>
                        </div>

                        <div class="timeline-body">
                            <p class="text-muted">This is insurance coverage geared specifically to the operation of aircraft and the risks involved in aviation.</p>
                        </div>
                    </div>
                </li>
                <li class="timeline-inverted">
                    <div class="timeline-image">
                        <img class="img-circle img-responsive" src="img/about/professional.jpg" alt="">
                    </div>

                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4>Professional Indemnity Insurance</h4>
                            <h4 class="subheading">Professional liability insurance</h4>
                        </div>

                        <div class="timeline-body">
                            <p class="text-muted">It protects your business against legal costs and claims by third parties for damages arising from acts, omissions or breaches of professional duty in the course of your business.</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="timeline-image">
                        <img class="img-circle img-responsive" src="img/about/erection.jpg" alt="">
                    </div>

                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4>Erection All Risk Insurance</h4>
                            <h4 class="subheading">Erection All Risk policy</h4>
                        </div>

                        <div class="timeline-body">
                            <p class="text-muted">This is specially designed to cover loss or damage to projects that involves erection/installation of plant, machinery and equipment ranging from erection of a single machine to a large power plant.</p>
                        </div>
                    </div>
                </li>
                <li class="timeline-inverted">
                    <div class="timeline-image">
                        <img class="img-circle img-responsive" src="img/about/contractors_plant.jpg" alt="">
                    </div>

                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4>Contractor Plant and Machinery Insurance</h4>
                            <h4 class="subheading">CPM insurance</h4>
                        </div>

                        <div class="timeline-body">
                            <p class="text-muted">Protects your business from damage or loss of expensive machinery. For a small construction contracting business because these machines represent such a large investment.</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="timeline-image">
                        <img class="img-circle img-responsive" src="img/about/ternant.jpg" alt="">
                    </div>

                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4>Tenant Liability Insurance</h4>
                            <h4 class="subheading">Liability-general and tenants</h4>
                        </div>

                        <div class="timeline-body">
                            <p class="text-muted">Sometimes accidents happen. If you damage your landlord’s property – including the furniture, fittings and fixtures – they could ask you to pay for the repair. This is known as tenant’s liability, and it might be a mandatory part of your tenancy agreement.</p>
                        </div>
                    </div>
                </li>
                <li class="timeline-inverted">
                    <div class="timeline-image">
                        <h4><a class="page-scroll" href="#contact" style="color:#fff;">
                            Contact
                            <br>Us !
                        </a></h4>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>