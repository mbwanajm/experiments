<div class="container">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h2 class="section-heading">Contact Us</h2>

            <h3 class="section-subheading text-muted">Have any questions or queries? Give us a call or drop us a message and we will get back to you as soon as we can.</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <g:hasErrors bean="${flash.errorBean}">
                <div class="alert alert-block alert-error">
                    <g:renderErrors bean="${flash.errorBean}" as="list"/>
                </div>
            </g:hasErrors>

            <g:if test="${flash.successMessage}">
                <div class='alert alert-success'>${flash.successMessage}</div>
            </g:if>
            <g:if test="${flash.errorMessage}">
                <div class='alert alert-error'>${flash.errorMessage}</div>
            </g:if>

            <g:form controller="contacts" action="save" name="sentMessage" id="contactForm"
                    method="post">
                <div class="row">
                    <input type="hidden" value="insurance" name="page">

                    <div class="col-md-6">

                        <div class="form-group">
                            <input type="text" name="name" class="form-control" placeholder="Your Name *" id="name"
                                   required
                                   data-validation-required-message="Please enter your name.">

                            <p class="help-block text-danger"></p>
                        </div>

                        <div class="form-group">
                            <input type="email" name="email" class="form-control" placeholder="Your Email *"
                                   id="email" required
                                   data-validation-required-message="Please enter your email address." autocomplete="off">

                            <p class="help-block text-danger"></p>
                        </div>

                        <div class="form-group">
                            <input type="tel" name="phone" class="form-control" placeholder="Your Phone *"
                                   id="phone" required
                                   data-validation-required-message="Please enter your phone number." autocomplete="off">

                            <p class="help-block text-danger"></p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <textarea class="form-control" name="message" placeholder="Your Message *" id="message"
                                      required
                                      data-validation-required-message="Please enter a message."></textarea>

                            <p class="help-block text-danger"></p>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-lg-12 text-center">
                        <div id="success"></div>
                        <button type="submit" class="btn btn-xl">Send Message</button>
                    </div>
                </div>
            </g:form>
        </div>
    </div>
</div>