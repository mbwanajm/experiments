<html>
<head>
    <meta name="layout" content="insurance"/>
</head>

<body>

<!-- Profile Section -->
<section id="profile">
    <g:render template="profile"></g:render>
</section>

<!-- Portfolio Grid Section -->
<section id="portfolio" class="bg-light-gray">
    <g:render template="products"></g:render>
</section>

<!-- About Section -->
<section id="other">
    <g:render template="other_coverage"></g:render>
</section>

<!-- contact into Section -->
<section id="info" class="bg-light-gray">
    <g:render template="contacts_info"></g:render>
</section>

<!-- Principles Aside -->
<g:render template="principles"></g:render>

<!-- Contact Section -->
<section id="contact">
    <g:render template="contactus"></g:render>
</section>


<g:render template="productView"></g:render>

</body>
</html>