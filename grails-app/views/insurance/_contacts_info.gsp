<div class="container">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h2 class="section-heading">Contacts Info </h2>

            <h3 class="section-subheading text-muted">Our location and contacts information..</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <div id="map"></div>
        </div>
        <div class="col-lg-6">
            <address>
                <strong>Darproperty</strong><br>
                Mikocheni B - Msuya St.<br>
                P.O BOX 105499<br>
                Dar-es-salaam<br>
                Tanzania<br>
                <strong>Email</strong>: insurance@darproperty.net<br>
                <strong>Call Us</strong>: +255 654 058869 /+255 22 2780517 /+255 713751868  <br>
            </address>
        </div>
    </div>

</div>