<aside class="clients">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center ">
                <b> Our Partners</b>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <g:link controller="home" action="home" target="_blank">
                    <img src="img/logos/envato.jpg" class="img-responsive img-centered"  alt="">
                </g:link>
            </div>



            <div class="col-md-6 col-sm-6">
                <a href="http://www.maxinsure-tz.com" target="_blank">
                    <img src="img/logos/creative-market.jpg" class="img-responsive img-centered" alt="">
                </a>
            </div>
        </div>
    </div>
</aside>