
<!-- Portfolio Modals -->
<!-- Use the modals below to showcase details about your portfolio projects! -->

<!-- Portfolio Modal 1 -->
<div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">
                        <!-- Project Details Go Here -->
                        <h2>Home Safe</h2>

                        <p class="item-intro text-muted">Insure your home (building and household contents) have
                        a single policy unique to insure.</p>
                        <img class="img-responsive img-centered" src="${resource(dir: 'insurance/img/portfolio', file: 'roundicons-free.png')}" alt="">

                        <h4>What Covered ?<br></h4>

                        <p>
                            <span class="text-red pull-left">The policy is tailor - made to cover against:</span><br>
                        <ul class="modal-list">
                            <li>Fire ,Lightning and explosion</li>
                            <li>Perils of nature or natural calamities ( Storms, foods, Earthquakes, Landslides Tsunami
                            and ect)</li>
                            <li>Impact damage by vehicles/animals aircraft</li>
                            <li>Riots, strikes and malicious acts</li>
                            <li>Burglary and House Breaking</li>
                            <li>All Risk for your valuable and expensive electronic items</li>
                            <li>Breakdown of domestic electrical appliances</li>
                            <li>Your legal liability to domestic help as well as third parties</li>
                            <li>A unique first in the world – the policy has a section to insure against cancellations of
                            festivities, such as weddings.</li>
                        </ul>
                    </p>
                        <h4>What is not Covered?<br></h4>

                        <p>
                        <ul class="modal-list">
                            <li>Willful destruction of property</li>
                            <li>Loss or damage cause d by depreciation or wear and tear</li>
                            <li>Loss or damage caused by nuclear weapons material and /or ionizing radiations</li>
                            <li>Loss or damage arising from any consequences of war, invasion, act of foreign enemy
                            etc</li>
                            <li>Terrorism</li>
                        </ul>
                    </p>

                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i
                                class="fa fa-times"></i> Close the Product</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Portfolio Modal 2 -->
<div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">
                        <h2>Motor Insurance</h2>

                        <p class="item-intro text-muted">Free from stress</p>
                        <img class="img-responsive img-centered" src="${resource(dir: 'insurance/img/portfolio', file: 'startup-framework-preview.png')}"
                             alt="">

                        <h4>What Covered ?<br></h4>

                        <p>
                            <span class="text-red pull-left">Loss or damage to vehicle caused by:</span><br>
                        <ul class="modal-list">
                            <li>Accident</li>
                            <li>Burglary or theft</li>
                            <li>Fire , explosion, self-ignition , Lighting</li>
                            <li>Riots, Strikes and Malicious Acts (Optional)</li>
                            <li>Earthquake, Flood Storm, Landslide or rock slide (Optional)</li>
                            <li>Liability to Third parties due to accident</li>
                        </ul>
                    </p>
                        <h4>What is not Covered?<br></h4>

                        <p>
                        <ul class="modal-list">
                            <li>General ageing, wear and tear</li>
                            <li>Loss caused by person driving without valid driving license</li>
                            <li>Mechanical or electrical breakdown failure</li>
                            <li>Loss caused by person driving under intoxication</li>
                            <li>Consequential loss</li>
                            <li>Loss/ damage attributable to war/ mutiny / Nuclear risks/ Terrorism</li>
                            <li>Damage to tyres / tubes unless damaged in an accident</li>
                        </ul>
                    </p>

                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i
                                class="fa fa-times"></i> Close the Product</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Portfolio Modal 3 -->
<div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">
                        <!-- Project Details Go Here -->
                        <h2>Indutrial All Risks</h2>

                        <p class="item-intro text-muted">Cover your industrial risks.</p>
                        <img class="img-responsive img-centered" src="${resource(dir: 'insurance/img/portfolio', file: 'treehouse-preview.png')}" alt="">

                        <h4>What Covered ?<br></h4>

                        <p>
                            <span class="text-red pull-left">The cover in its widest form, referred to as all risk includes the following perils/covers.</span><br>
                        <ul class="list-group modal-list">
                            <li>Section I (material damage)</li>
                            <ul class="list-group-item">
                                <li>Fire and allied perils</li>
                                <li>Burglar</li>
                                <li>Machinery breakdown/boiler explosion/electronic equipment insurance</li>
                            </ul>
                            <li>Section II (business interruption))</li>
                            <ul class="list-group-item">
                                <li>Fire loss of profit</li>
                                <li>Machinery loss of profit</li>
                            </ul>
                        </ul>
                    </p>

                        <h4>What is not Covered?<br></h4>

                        <p>
                        <ul class="modal-list">
                            <li>Inherent vice, normal wear and tear</li>
                            <li>Collapse or cracking of building</li>
                            <li>Faulty or defective design, material or workmanship</li>
                            <li>Pollution, contamination</li>
                            <li>Inventory losses</li>
                            <li>Fraud, larceny</li>
                            <li>Interruption of the water supply, gas, electricity, or fuel system or failure of
                            the effluent disposal system etc.</li>
                            <li>War/warlike perils</li>
                        </ul>
                    </p>
                        <p>
                            <span class="text-red pull-left">Property include</span><br>

                        <ul class="modal-list">
                            <li>Money, cheque, stamps, bonds, credit card,
                            bullion, precious stones, works of art and etc.</li>
                            <li>Vehicle licensed for road use</li>
                            <li>Livestock, growing crops or trees</li>
                        </ul>
                    </p>

                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i
                                class="fa fa-times"></i> Close the Product</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Portfolio Modal 4 -->
<div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">
                        <!-- Project Details Go Here -->
                        <h2>Personal Accident Insurance</h2>

                        <p class="item-intro text-muted">A worldwide cover against accident.</p>
                        <img class="img-responsive img-centered" src="${resource(dir: 'insurance/img/portfolio', file: 'golden-preview.png')}" alt="">
                        <h4>What Covered ?<br></h4>

                        <p>
                        <ul class="modal-list">
                            <li>Accidental Death</li>
                            <li>Permanent , partial or total Disablement</li>
                            <li>Temporary total disablement pay a weekly benefit</li>
                            <li>Medical expenses- pays a pre- agreed amount for medical expenses</li>

                        </ul>
                    </p>
                        <h4>What is not Covered?<br></h4>

                        <p>
                        <ul class="modal-list">
                            <li>War perils</li>
                            <li>Nuclear risks</li>
                            <li>Self- inflicted wounds</li>

                        </ul>
                    </p>

                        <h4>Who are not Covered?<br></h4>

                        <p>
                        <ul class="modal-list">
                            <li>Armed Forces (Army/ navy/ Air Force)</li>
                            <li>Para – military force</li>
                            <li>Professional Sports person</li>
                            <li>Hazardous posts (Bungee jumping - Jet skiing , Para – gliding, etc)</li>
                            <li>Merchant navy personnel/ airline Pilots</li>
                            <li>Off-shore workers (oil rigs/ Drilling platforms)</li>
                        </ul>
                    </p>

                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i
                                class="fa fa-times"></i> Close the Project</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Portfolio Modal 5 -->
<div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">
                        <!-- Project Details Go Here -->
                        <h2>Goods in Transit Insurance</h2>

                        <p class="item-intro text-muted">Covers risks during transit by rail/road.</p>
                        <img class="img-responsive img-centered" src="${resource(dir: 'insurance/img/portfolio', file: 'escape-preview.png')}" alt="">
                        <h4>What Covered ?<br></h4>

                        <p>
                        <ul class="modal-list">
                            <li>All risk basis subject to specific exclusions</li>
                            <li>Specified peril policy in which the perils are specifically mentioned like fire, and over
                            turning collision etc.</li>
                        </ul>
                    </p>
                        <h4>What is not Covered?<br></h4>

                        <p>
                        <ul class="modal-list">
                            <li>Loss or damage due to wilful misconduct, ordinary leakage, improper packing, delay inherent
                            vice, war strike, riot and civil and commotion.</li>

                        </ul>
                    </p>

                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i
                                class="fa fa-times"></i> Close the Product</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Portfolio Modal 6 -->
<div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">
                        <!-- Project Details Go Here -->
                        <h2>Shop Insurance</h2>

                        <p class="item-intro text-muted">Give al round protection to your shop and its contents thereof by means of a comprehensive
                        package policy designed for the Tanzanian Shopkeeper.</p>
                        <img class="img-responsive img-centered" src="${resource(dir: 'insurance/img/portfolio', file: 'dreams-preview.png')}" alt="">
                        <h4>What Covered ?<br></h4>

                        <p>
                        <ul class="modal-list">
                            <li>Fire insurance for building/ contents.</li>
                            <li>Theft</li>
                            <li>Money insurance, while money is in transit or in counter or in safe after business hours.</li>
                            <li>Plate Glass fixed to the windows/ display area front / of the shop</li>
                            <li>Workmen’s compensation insurance for the staff members while on official duty</li>
                            <li>Liability to Third Parties</li>
                            <li>Fidelity Guarantee Insurance, for losses caused by shop employees</li>
                        </ul>
                    </p>
                        <h4>What is not Covered?<br></h4>

                        <p>
                        <ul class="modal-list">
                            <li>Wear and tear, depreciation</li>
                            <li>Excess applicable for each section</li>
                            <li>War, Nuclear Perils etc.</li>
                            <li>As applicable to the respective sections</li>

                        </ul>
                    </p>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i
                                class="fa fa-times"></i> Close the Product</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Portfolio Modal 7 -->
<div class="portfolio-modal modal fade" id="portfolioModal7" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">
                        <!-- Project Details Go Here -->
                        <h2>Travel Insurance</h2>

                        <p class="item-intro text-muted">We cover your travel risks.</p>
                        <img class="img-responsive img-centered" src="${resource(dir: 'insurance/img/portfolio', file: 'travel-insurance.png')}" alt="">
                        <h4>What Covered ?<br></h4>

                        <p>
                        <ul class="modal-list">
                            <li>Medical Expenses and Hospitalization Abroad.</li>
                            <li>Emergency Dental Care</li>
                            <li>Legal Defence.</li>
                            <li>Loss of Passport</li>
                            <li>Delivery of Medicines</li>
                            <li>Emergency Return Home</li>
                        </ul>
                    </p>
                        <h4>What is not Covered?<br></h4>

                        <p>
                        <ul class="modal-list">
                            <li>No cover is provided for trips inside the Country of residence</li>
                            <li>War</li>
                            <li>Motor Sports Racing</li>
                            <li>Big Game Hunting, Underwater Diving</li>
                            <li>Competition Sports</li>

                        </ul>
                    </p>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i
                                class="fa fa-times"></i> Close the Product</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Portfolio Modal 8 -->
<div class="portfolio-modal modal fade" id="portfolioModal8" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">
                        <!-- Project Details Go Here -->
                        <h2>Fidelity Guarantee</h2>

                        <p class="item-intro text-muted">We ensures that your organization are protected from the
                        few bad apples.</p>
                        <img class="img-responsive img-centered" src="${resource(dir: 'insurance/img/portfolio', file: 'fedelity-preview.png')}" alt="">
                        <h4>What Covered ?<br></h4>

                        <p>
                        <ul class="modal-list">
                            <li>The policy indemnifies the insured against any direct monetary loss sustained by any act of
                            fraud/ dishonesty committed by an employee during the policy period.</li>
                        </ul>
                    </p>
                        <h4>What is not Covered?<br></h4>

                        <p>
                        <ul class="modal-list">
                            <li>Losses not sustained within a retroactive period</li>
                            <li>Loss sustained prior to the inception of the original policy</li>

                        </ul>
                    </p>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i
                                class="fa fa-times"></i> Close the Product</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Portfolio Modal 9 -->
<div class="portfolio-modal modal fade" id="portfolioModal9" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">
                        <!-- Project Details Go Here -->
                        <h2>Electronic Equipments Insurance</h2>

                        <p class="item-intro text-muted">Free from loss or damage.</p>
                        <img class="img-responsive img-centered" src="${resource(dir: 'insurance/img/portfolio', file: 'electronics-preview.png')}" alt="">
                        <h4>What Covered ?<br></h4>

                        <p>
                        <ul class="modal-list">
                            <li>Physical loss or damage to electronic equipment and data media as well as the increased
                            cost of working resulting therefrom.</li>
                        </ul>
                    </p>
                        <h4>What is not Covered?<br></h4>

                        <p>
                        <ul class="modal-list">
                            <li>Losses or damage due to wear and tear, war willful acts or willful negligence, aesthetic
                            defects and consequential loss</li>

                        </ul>
                    </p>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i
                                class="fa fa-times"></i> Close the Product</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Portfolio Modal 10 -->
<div class="portfolio-modal modal fade" id="portfolioModal10" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">
                        <!-- Project Details Go Here -->
                        <h2>Burglary Insurance</h2>

                        <p class="item-intro text-muted">We insure strong security.</p>
                        <img class="img-responsive img-centered" src="${resource(dir: 'insurance/img/portfolio', file: 'burglary-preview.png')}" alt="">
                        <h4>What Covered ?<br></h4>

                        <p>
                        <ul class="modal-list">
                            <li>Loss or damage due to burglary or house- breaking.</li>
                            <li>Damage caused to the premises resulting from burglary.</li>
                        </ul>
                    </p>
                        <h4>What is not Covered?<br></h4>

                        <p>
                        <ul class="modal-list">
                            <li>Goods lying in open unless specifically insured</li>
                            <li>Infidelity on inmate or member of the insurer’s household or his business</li>
                            <li>Riot and strike , civil commotion, terrorist activities.</li>
                            <li>Earthquake, flood, storm, volcanic eruption etc.</li>
                            <li>War, warlike perils</li>
                            <li>Loss by use of the key to the safe of any duplicate thereof.</li>
                            <li>Loss or damage attributable to willful/gross neglige.</li>
                        </ul>
                    </p>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i
                                class="fa fa-times"></i> Close the Product</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Portfolio Modal 11 -->
<div class="portfolio-modal modal fade" id="portfolioModal11" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">
                        <!-- Project Details Go Here -->
                        <h2>Money Insurance</h2>

                        <p class="item-intro text-muted">We handle money risks.</p>
                        <img class="img-responsive img-centered" src="${resource(dir: 'insurance/img/portfolio', file: 'money-preview.png')}" alt="">
                        <h4>What Covered ?<br></h4>

                        <p>
                        <ul class="modal-list">
                            <li>Loss of Money relating to business or profession due to accident or misfortune whilst in
                            transit or when money is held in safe at the insured’s premises</li>
                            <li>Replacement or repair of the insured’s safe or strong room damage by thieves or burglars</li>
                        </ul>
                    </p>
                        <h4>What is not Covered?<br></h4>

                        <p>
                        <ul class="modal-list">
                            <li>Shortage due to error or mission</li>
                            <li>Loss of money entrusted to any person other than insured/ his authorized employee</li>
                            <li>Loss occurring after business hours, unless the money is in a locked safe or strong room.</li>
                            <li>Loss occasioned by Riot, Strike and Terrorist Activity</li>
                            <li>Money carried under contact of affreightment</li>
                            <li>Theft of money from unattended vehicle/Loss of Damage to money in transit by post</li>
                            <li>Loss of money collected by authorized employee (s) of the insured whilst in transit to the
                            insured premises or bank in their personal custody for more than 48 hours</li>
                            <li>Loss of money from safe/ strong room using key/ duplicate key to the safe/ strong room</li>
                            <li>War and warlike perils, lonising radiation or contamination by radioactivity and etc.</li>
                            <li>Consequential loss or legal liability of any kind</li>
                        </ul>
                    </p>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i
                                class="fa fa-times"></i> Close the Product</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Portfolio Modal 12 -->
<div class="portfolio-modal modal fade" id="portfolioModal12" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">
                        <!-- Project Details Go Here -->
                        <h2>Fire and Allied Perils Insurance</h2>

                        <p class="item-intro text-muted">This policy is ideal for SMEs and cooperates alike, those who need coverage for their business
                        against fire and natural perils that can devastate property and business.</p>
                        <img class="img-responsive img-centered" src="${resource(dir: 'insurance/img/portfolio', file: 'fire-preview.png')}" alt="">
                        <h4>What Covered ?<br></h4>

                        <p>
                        <ul class="modal-list">
                            <li>Fire</li>
                            <li>Lighting</li>
                            <li>Explosion</li>
                            <li>Perils of natural calamities (storms, floods, earthquakes, tsunami etc.)</li>
                            <li>Riots, strikes, and malicious acts</li>
                            <li>Bursting of pipes/water tanks, sprinkler, leakage and bush fires.</li>
                        </ul>
                    </p>
                        <h4>What is not Covered?<br></h4>

                        <p>
                        <ul class="modal-list">
                            <li>Willful acts or gross negligence</li>
                            <li>Destruction/damage by own fermentation, natural heating or spontaneous combustion</li>
                            <li>Explosion/Implosion damage to boilers, damage caused by centrifugal forces.</li>
                            <li>War and nuclear group of perils.</li>
                            <li>Unspecified precious stones. Chaques, currency, documents etc.</li>
                            <li>Consequential losses</li>
                            <li>Theft during/ after operation of peril</li>
                            <li>Terrorism</li>
                        </ul>
                    </p>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i
                                class="fa fa-times"></i> Close the Product</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

