<div class="container">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h2 class="section-heading">Profile</h2>

            <h3 class="section-subheading text-muted">We take care of your risks.</h3>
        </div>
    </div>

    <div class="row text-center">
        <div class="col-md-4">
            <span class="fa-stack fa-4x">
                <i class="fa fa-circle fa-stack-2x text-primary"></i>
                <i class="fa fa-binoculars fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">Goal</h4>

            <p class="text-muted">
                Provide insurance products and services that address the risks concerns
                of customer with value pricing.
            </p>
        </div>

        <div class="col-md-4">
            <span class="fa-stack fa-4x">
                <i class="fa fa-circle fa-stack-2x text-primary"></i>
                <i class="fa fa-star fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">Core Values</h4>

            <p class="text-muted">
                Professional approach to every business - underwriting, sales and initial claim processing.
                <br><br>
                Conduct business in an upfront and open manner, being fair and ethical.
                Dedication in delivery of quality service.
            </p></div>

        <div class="col-md-4">
            <span class="fa-stack fa-4x">
                <i class="fa fa-circle fa-stack-2x text-primary"></i>
                <i class="fa fa-wrench fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">Services We Offer</h4>

            <p class="text-muted">
                All general insurance products, such as industrial risk,
                fire and allied perils, motor insurance, home insurance..etc
            </p>
        </div>
    </div>
</div>