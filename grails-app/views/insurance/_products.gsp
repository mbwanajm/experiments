<div class="container">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h2 class="section-heading">Main Coverage</h2>

            <h3 class="section-subheading text-muted">We provide all general insurance products.</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 col-sm-6 portfolio-item">
            <a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">
                <div class="portfolio-hover">
                    <div class="portfolio-hover-content">
                        <i class="fa fa-plus fa-3x"></i>
                    </div>
                </div>
                <img src="${resource(dir: 'insurance/img/portfolio', file: 'roundicons.png')}" class="img-responsive" alt="http://isure.spaceracedigital.netdna-cdn.com/wp-content/uploads/2016/03/Home-Insurance.png">
            </a>

            <div class="portfolio-caption">
                <h4>Home Insurance</h4>

                <p class="text-muted">
                    Insure your home safety
                </p>
            </div>
        </div>

        <div class="col-md-4 col-sm-6 portfolio-item">
            <a href="#portfolioModal2" class="portfolio-link" data-toggle="modal">
                <div class="portfolio-hover">
                    <div class="portfolio-hover-content">
                        <i class="fa fa-plus fa-3x"></i>
                    </div>
                </div>
                <img src="${resource(dir: 'insurance/img/portfolio', file: 'startup-framework.png')}" class="img-responsive" alt="www.rediff.com%2Fbusiness%2Fslide-show%2Fslide-show-1-perfin-how-to-lower-your-motor-insurance-premium%2F20110317.htm&psig=AFQjCNH067RMKDvlRihRuI3LxHj816Nmag&ust=1459836238741229">
            </a>

            <div class="portfolio-caption">
                <h4>Motor Insurance</h4>

                <p class="text-muted">Free from stress</p>
            </div>
        </div>

        <div class="col-md-4 col-sm-6 portfolio-item">
            <a href="#portfolioModal3" class="portfolio-link" data-toggle="modal">
                <div class="portfolio-hover">
                    <div class="portfolio-hover-content">
                        <i class="fa fa-plus fa-3x"></i>
                    </div>
                </div>
                <img src="${resource(dir: 'insurance/img/portfolio', file: 'treehouse.png')}" class="img-responsive" alt="http://www.smartbusinessinsurance.com.au/blog/wp-content/uploads/2013/07/business-insurance-lake-county-illinois.jpeg.">
            </a>

            <div class="portfolio-caption">
                <h4>Indutrial All Risks</h4>

                <p class="text-muted">Cover your industrial risks</p>
            </div>
        </div>

        <div class="col-md-4 col-sm-6 portfolio-item">
            <a href="#portfolioModal4" class="portfolio-link" data-toggle="modal">
                <div class="portfolio-hover">
                    <div class="portfolio-hover-content">
                        <i class="fa fa-plus fa-3x"></i>
                    </div>
                </div>
                <img src="${resource(dir: 'insurance/img/portfolio', file: 'golden.png')}" class="img-responsive" alt="http://www.summitbank.com.pk/wp-content/uploads/2011/06/personal-accidental1.jpg">
            </a>

            <div class="portfolio-caption">
                <h4>Personal Accident Insurance</h4>

                <p class="text-muted">We cover against accident</p>
            </div>
        </div>

        <div class="col-md-4 col-sm-6 portfolio-item">
            <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                <div class="portfolio-hover">
                    <div class="portfolio-hover-content">
                        <i class="fa fa-plus fa-3x"></i>
                    </div>
                </div>
                <img src="${resource(dir: 'insurance/img/portfolio', file: 'escape.png')}" class="img-responsive" alt="http://www.airseaworldwide.com/aswweb/images/asw/p_s/HHG/transit-insurance.jpg">
            </a>

            <div class="portfolio-caption">
                <h4>Goods in Transit Insurance</h4>

                <p class="text-muted">Cover transit risks</p>
            </div>
        </div>

        <div class="col-md-4 col-sm-6 portfolio-item">
            <a href="#portfolioModal6" class="portfolio-link" data-toggle="modal">
                <div class="portfolio-hover">
                    <div class="portfolio-hover-content">
                        <i class="fa fa-plus fa-3x"></i>
                    </div>
                </div>
                <img src="${resource(dir: 'insurance/img/portfolio', file: 'dreams.png')}" class="img-responsive" alt="http://transformsa.co.za/wp-content/uploads/2014/07/women-sme.jpg">
            </a>

            <div class="portfolio-caption">
                <h4>Shop Insurance</h4>

                <p class="text-muted">Give all around shop protection</p>
            </div>
        </div>

        <div class="col-md-4 col-sm-6 portfolio-item">
            <a href="#portfolioModal7" class="portfolio-link" data-toggle="modal">
                <div class="portfolio-hover">
                    <div class="portfolio-hover-content">
                        <i class="fa fa-plus fa-3x"></i>
                    </div>
                </div>
                <img src="${resource(dir: 'insurance/img/portfolio', file: 'travel-insurance-preview.png')}" class="img-responsive" alt="http://majzaytours.com/images/travelinsurance.jpg">
            </a>

            <div class="portfolio-caption">
                <h4>Travel Insurance</h4>

                <p class="text-muted">We cover your travel risks</p>
            </div>
        </div>

        <div class="col-md-4 col-sm-6 portfolio-item">
            <a href="#portfolioModal8" class="portfolio-link" data-toggle="modal">
                <div class="portfolio-hover">
                    <div class="portfolio-hover-content">
                        <i class="fa fa-plus fa-3x"></i>
                    </div>
                </div>
                <img src="${resource(dir: 'insurance/img/portfolio', file: 'fedelity.png')}" class="img-responsive" alt="http://www.saicinsurance.net/wp-content/uploads/2014/11/fidelity-new.jpg">
            </a>

            <div class="portfolio-caption">
                <h4>Fidelity Guarantee</h4>

                <p class="text-muted">Be protected by few bad apples</p>
            </div>
        </div>

        <div class="col-md-4 col-sm-6 portfolio-item">
            <a href="#portfolioModal9" class="portfolio-link" data-toggle="modal">
                <div class="portfolio-hover">
                    <div class="portfolio-hover-content">
                        <i class="fa fa-plus fa-3x"></i>
                    </div>
                </div>
                <img src="img/portfolio/electronics.png" class="img-responsive" alt="http://bigelectronics.net/images/art/laptop-insurance.png">
            </a>

            <div class="portfolio-caption">
                <h4>Electronic Equipments Insurance</h4>

                <p class="text-muted">Free from loss or damage</p>
            </div>
        </div>

        <div class="col-md-4 col-sm-6 portfolio-item">
            <a href="#portfolioModal10" class="portfolio-link" data-toggle="modal">
                <div class="portfolio-hover">
                    <div class="portfolio-hover-content">
                        <i class="fa fa-plus fa-3x"></i>
                    </div>
                </div>
                <img src="${resource(dir: 'insurance/img/portfolio', file: 'burglary.png')}" class="img-responsive" alt="http://www.cornettlawoffice.com/wp-content/uploads/2014/11/burglary.jpg.">
            </a>

            <div class="portfolio-caption">
                <h4>Burglary Insurance</h4>

                <p class="text-muted">We insure strong security</p>
            </div>
        </div>

        <div class="col-md-4 col-sm-6 portfolio-item">
            <a href="#portfolioModal11" class="portfolio-link" data-toggle="modal">
                <div class="portfolio-hover">
                    <div class="portfolio-hover-content">
                        <i class="fa fa-plus fa-3x"></i>
                    </div>
                </div>
                <img src="${resource(dir: 'insurance/img/portfolio', file: 'money.png')}" class="img-responsive" alt="http://www.naboodainsurance.com/wp-content/uploads/2014/07/Money-Gold-113.jpg">
            </a>

            <div class="portfolio-caption">
                <h4>Money Insurance</h4>

                <p class="text-muted">We handle money risks</p>
            </div>
        </div>

        <div class="col-md-4 col-sm-6 portfolio-item">
            <a href="#portfolioModal12" class="portfolio-link" data-toggle="modal">
                <div class="portfolio-hover">
                    <div class="portfolio-hover-content">
                        <i class="fa fa-plus fa-3x"></i>
                    </div>
                </div>
                <img src="${resource(dir: 'insurance/img/portfolio', file: 'fire.png')}" class="img-responsive" alt="http://www.tokiomarine.com/content/sites/sg/en/business/products/property/fire-insurance/_jcr_content.renderimage.479.365.jpg">
            </a>

            <div class="portfolio-caption">
                <h4>Fire & Allied Perils Insurance</h4>

                <p class="text-muted">We are fire shield & strong than perils</p>
            </div>
        </div>
    </div>
</div>