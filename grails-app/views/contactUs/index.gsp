<html>
<head>
    <title>Dar Property Contact Us</title>
    <meta name="layout" content="main"/>

</head>

<body>
<div class="container">
    <g:render template="../pageTitle" model="[title: g.message(code: 'menu.contactUs')]"></g:render>
    <div class="row">
        <div class='col-sm-12'>
            <div id='map'/>
        </div>
    </div>
    <div class="margine2"></div>

    <div class="row">
        <div class="col-sm-8">
            <div class='' style="border:0px solid #fff;">
                <!--Feel free to contact us for any enquries, questions, comments and feedback,
            we promise to get back to you as soon as possible</p> -->

<h2 align="center" style="margin-top:0px;"><g:message code="property.messageUs"></g:message></h2>
                <g:render template="../sendMessage"
                          model="[controller: 'tool', action: 'sendEmailToDarproperty', id: 0, msg:params.msg]"/>
            </div>
        </div>

        <div class="col-sm-4">
            <h4><g:message code='contactDetails'/></h4>

            <address>
                <strong>DarProperty</strong><br>
                Mikocheni<br>
                P.O BOX 105499<br>
                Dar-es-salaam<br>
                Tanzania<br>
            </address>

            <p><br>

            <p><strong>Email</strong>: info@darproperty.net<br>
                <strong>Call Us</strong>:<br> +255 784238962 / <br>+255 713751868 <br>
        </div>

    </div>
</div>

</body>

</html>




