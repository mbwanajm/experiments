<div class="container" align="left">
    <g:render template="../pageTitle" model="[title: 'Direct Actions']"></g:render>
    <div class="row">
        <div class="col-sm-4">
            <a href="#addBank"><span class="glyphicon glyphicon-bookmark"></span> Add Bank</a>
        </div>

        <div class="col-sm-4">
            <a href="#addPart"><span class="glyphicon glyphicon-bookmark"></span> Add Partner</a>
        </div>

        <div class="col-sm-4">
            <a href="#rmvPart"><span class="glyphicon glyphicon-bookmark"></span> Add Partner</a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <a href="#addRate"><span class="glyphicon glyphicon-bookmark"></span> Add Interest Rates</a>
        </div>

        <div class="col-sm-4">
            <a href="#updateRate"><span class="glyphicon glyphicon-bookmark"></span> Update Rate</a>
        </div>

        <div class="col-sm-4">
            <a href="#exportClassfied"><span class="glyphicon glyphicon-bookmark"></span> Export Classifieds</a>
        </div>
    </div>
</div>
