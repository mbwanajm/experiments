<br>
<div class="container" id="exportClassfied">
    <g:render template="../pageTitle" model="[title:'Export Classfieds Contacts']"></g:render>
    <div class="row">
        <a href=""><span class="glyphicon glyphicon-refresh"></span> Refresh</a>

        <%
            def currenyyear = new GregorianCalendar().get(Calendar.YEAR)
            String page = "";
            int startyear = 2012
            int endyear = 0
            int i = startyear;
            endyear = endyear == 0 ? currenyyear : endyear;
            while (i <= endyear) {
                page += "<option value='" + i + "'> " + i + " </option>";
                i++;
            }

        %>
        <g:form controller="classfieds" action="export_contacts" method="get" class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-sm-6">
                    Start Year:
                </label>

                <div class="col-sm-5">

                    <select name="startyear" class="form-control">

                        ${page}
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-6">
                    End year:
                </label>

                <div class="col-sm-5">
                    <select name="endyear" class="form-control">
                        ${page}
                    </select>
                </div>
            </div>

            <div class="form-group">

                <div class="col-sm-offset-6 col-sm-5">
                    <input type="submit" name="export" class="btn btn-primary" value="EXPORT">
                </div>
            </div>
        </g:form>
    </div>
</div>