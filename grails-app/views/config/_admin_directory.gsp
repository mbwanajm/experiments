<div class="container">
    <div class="row">
        <g:render template="../pageTitle" model="[title: 'Admin Directory']"></g:render>
    </div>

    <div class="panel panel-default">

        <div class="heading">

        </div>

        <div class="body" style="padding:5px; background-color:#f0f0f0; box-shadow:0px 2px 5px #888;" align="left">

            <div class="container" align="center">
                <div class="row">
                    <div class="col-xs-3">
                        <g:link controller="property" action="list" class="btn btn-default form-control"><span
                                class="glyphicon glyphicon-wrench"></span> Properties</g:link>
                    </div>

                    <div class="col-xs-3">
                        <g:link controller="amenities" action="list" class="btn btn-default form-control"><span
                                class="glyphicon glyphicon-wrench"></span> Amenities</g:link>
                    </div>

                    <div class="col-xs-3">
                        <g:link controller="tag" action="list" class="btn btn-default form-control"><span
                                class="glyphicon glyphicon-wrench"></span> Tags</g:link>
                    </div>

                    <div class="col-xs-3">
                        <g:link controller="region" action="list" class="btn btn-default form-control"><span
                                class="glyphicon glyphicon-wrench"></span> Regions</g:link>
                    </div>
                </div>
                <br>

                <div class="row">
                    <div class="col-xs-3">
                        <g:link controller="region_areas" action="list" class="btn btn-default form-control"><span
                                class="glyphicon glyphicon-wrench"></span> Areas Points</g:link>
                    </div>
                    <div class="col-xs-3">
                        <g:link controller="advertType" action="list" class="btn btn-default form-control"><span
                                class="glyphicon glyphicon-wrench"></span> Adverts Types</g:link>
                    </div>
                    <div class="col-xs-3">
                        <g:link controller="advert" action="list" class="btn btn-default form-control"><span
                                class="glyphicon glyphicon-wrench"></span> Adverts</g:link>
                    </div>

                    <div class="col-xs-3">
                        <g:link controller="article" action="list" class="btn btn-default form-control"><span
                                class="glyphicon glyphicon-wrench"></span> Articles</g:link>
                    </div>


                </div>
                <br>

                <div class="row">
                    <div class="col-xs-3">
                        <g:link controller="faq" action="list" class="btn btn-default form-control"><span
                                class="glyphicon glyphicon-wrench"></span> FaQ</g:link>
                    </div>

                    <div class="col-xs-3">
                        <g:link controller="enquiries" action="list" class="btn btn-default form-control"><span
                                class="glyphicon glyphicon-wrench"></span> Enquiries</g:link>
                    </div>

                    <div class="col-xs-3">
                        <g:link controller="contacts" action="list" class="btn btn-default form-control"><span
                                class="glyphicon glyphicon-wrench"></span> Mails</g:link>
                    </div>

                    <div class="col-xs-3">
                        <g:link controller="subscriber" action="list" class="btn btn-default form-control"><span
                                class="glyphicon glyphicon-wrench"></span> Mail Subscribers</g:link>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-xs-3">
                        <g:link controller="banks" action="list" class="btn btn-default form-control"><span
                                class="glyphicon glyphicon-wrench"></span> Banks</g:link>
                    </div>

                    <div class="col-xs-3">
                        <g:link controller="interestrates" action="list" class="btn btn-default form-control"><span
                                class="glyphicon glyphicon-wrench"></span> Interest Rates</g:link>
                    </div>

                    <div class="col-xs-3">
                        <g:link controller="parteners_banks" action="list" class="btn btn-default form-control"><span
                                class="glyphicon glyphicon-wrench"></span> Our Partners</g:link>
                    </div>

                    <div class="col-xs-3">
                        <g:link controller="country" action="list" class="btn btn-default form-control"><span
                                class="glyphicon glyphicon-wrench"></span> Countries </g:link>
                    </div>
                </div>
                <br>
                <div class="row">

                    <div class="col-xs-3">
                        <g:link controller="currency" action="list" class="btn btn-default form-control"><span
                                class="glyphicon glyphicon-wrench"></span> Currencies </g:link>
                    </div>
                    <div class="col-xs-3">
                        <g:link controller="profile" action="list" class="btn btn-default form-control"><span
                                class="glyphicon glyphicon-wrench"></span> Profiles</g:link>
                    </div>
                    <div class="col-xs-3">
                        <g:link controller="agent" action="list" class="btn btn-default form-control"><span
                                class="glyphicon glyphicon-wrench"></span> Agents</g:link>
                    </div>
                    <div class="col-xs-3">
                        <g:link controller="userRole" action="list" class="btn btn-default form-control"><span
                                class="glyphicon glyphicon-wrench"></span> User Role</g:link>
                    </div>
                </div>

            </div>

        </div>

    </div>

</div>