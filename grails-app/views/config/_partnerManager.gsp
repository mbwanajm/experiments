<%@ page import="tz.co.darproperty.*" %>
<br>
<%

    if (request.getParameter("addpart") != null) {
        int bankid = Integer.parseInt(request.getParameter("bankselector"));
        String email = request.getParameter("email");

        try {
            br.addPartner(bankid, email);
            out.print(br.getMsg());
        } catch (Exception e) {
            out.print(e);
        }
    }

    if (request.getParameter("rmvpartner") != null) {
        String bank = request.getParameter("bankselector");
        int bankid = br.bankId(bank);


        try {
            br.rmvPartner(bankid);
            out.print(br.getMsg());
        } catch (Exception e) {
            out.print(e);
        }
    }

    if (request.getParameter("rmvpartner") != null) {
        String bank = request.getParameter("bankselector");
        int bankid = br.bankId(bank);


        try {
            br.rmvPartner(bankid);
            out.print(br.getMsg());
        } catch (Exception e) {
            out.print(e);
        }
    }

    if (request.getParameter("updaterate") != null) {

        int bankid = Integer.parseInt(request.getParameter("bankselector"));
        double rate = Double.parseDouble(request.getParameter("rate"));
        String country = request.getParameter("countryselector");
        try {

            br.setCountry(country);
            br.setRate(rate);
            br.updateRate(bankid);
            out.print(br.getMsg());
        } catch (Exception e) {
            out.print(e);
        }
    }

    if (request.getParameter("addrate") != null) {
        int bank = Integer.parseInt(request.getParameter("bankselector"));

        double rate = Double.parseDouble(request.getParameter("rate"));
        String country = request.getParameter("countryselector");
        try {
            br.setBankid(bank);
            br.setCountry(country);
            br.setRate(rate);
            br.addRate();
            out.print(br.getMsg());
        } catch (Exception e) {
            out.print(e);
        }
    }

    if (request.getParameter("addbank") != null) {
        String bank = request.getParameter("bankname");

        try {

            br.setBankname(bank);
            br.addBank();
            out.print(br.getMsg());
        } catch (Exception e) {
            out.print(e);
        }
    }
%>
<br>

<div class="container" id="addBank">
    <div class="row">
        <g:render template="../pageTitle" model="[title: 'Add Bank']"></g:render>
    </div>

    <div class="row">
        <a href=""><span class="glyphicon glyphicon-refresh"></span> Refresh</a>

        <form action="" method="post" name="form3" role="form" class="form-horizontal">

            <div class="form-group">
                <label class="control-label col-xs-6">
                    Bank name:
                </label>

                <div class="col-xs-6">
                    <input type="text" name="bankname" size="50" placeholder="Bank Name" class="form-control"
                           required="required"/>
                </div>
            </div>

            <div class="form-group">
                <div class="col-xs-6 offset"></div>

                <div class="col-xs-6">
                    <input type="submit" name="addbank" value="Add Bank" class="btn btn-primary"/>
                </div>
            </div>

        </form>
    </div>
</div>
<br>

<div class="container" id="addPart">
    <div class="row">
        <g:render template="../pageTitle" model="[title: 'Add Partner']"></g:render>
    </div>

    <div class="row">
        <a href=""><span class="glyphicon glyphicon-refresh"></span> Refresh</a>

        <form action="" method="post" name="form3" role="form" class="form-horizontal">
            <div class="form-group">

                <label class="control-label col-xs-6">
                    Banks:
                </label>

                <div class="col-xs-6">
                    <select name="bankselector" class="form-control">
                        <%=br.allBanks()%>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-xs-6">
                    Email:
                </label>

                <div class="col-xs-6">
                    <input type="text" name="email" size="50" placeholder="Email Address" class="form-control"
                           required="required"/>
                </div>
            </div>

            <div class="form-group">
                <div class="col-xs-6 offset"></div>

                <div class="col-xs-6">
                    <input type="submit" name="addpart" value="Add Partner" class="btn btn-primary"/>
                </div>
            </div>

        </form>
    </div>
</div>
<br>

<div class="container" id="rmvPart">
    <div class="row">
        <g:render template="../pageTitle" model="[title: 'Remove Partner']"></g:render>
    </div>

    <div class="row">
        <a href=""><span class="glyphicon glyphicon-refresh"></span> Refresh</a>

        <form action="" method="post" name="form3" role="form" class="form-horizontal">
            <div class="form-group">

                <label class="control-label col-xs-6">
                    Our Bank Partners:
                </label>

                <div class="col-xs-6">
                    <select name="bankselector" class="form-control">
                        <%=br.our_bank_parteners_contacts()%>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <div class="col-xs-6 offset"></div>

                <div class="col-xs-6">
                    <input type="submit" name="rmvpartner" value="Remove Partner" class="btn btn-danger"/>
                </div>
            </div>

        </form>
    </div>
</div>
<br>

<div class="container" id="addRate">
    <div class="row">
        <g:render template="../pageTitle" model="[title: 'Add Rates']"></g:render>
    </div>

    <div class="row">
        <a href=""><span class="glyphicon glyphicon-refresh"></span> Refresh</a>

        <form action="" method="post" name="form3" role="form" class="form-horizontal">
            <div class="form-group">

                <label class="control-label col-xs-6">
                    Bank:
                </label>

                <div class="col-xs-6">
                    <select name="bankselector" class="form-control">
                        <%=br.allBanks()%>
                    </select>
                </div>
            </div>

            <div class="form-group">

                <label class="control-label col-xs-6">
                    Country:
                </label>

                <div class="col-xs-6">
                    <select name="countryselector" class="form-control">
                        <%=br.countrySelector()%>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-xs-6">
                    Rate:
                </label>

                <div class="col-xs-6">
                    <input type="text" maxlength="20" name="rate" size="50" placeholder="Interest rate"
                           class="form-control" required="required"/>
                </div>
            </div>

            <div class="form-group">
                <div class="col-xs-6 offset"></div>

                <div class="col-xs-6">
                    <input type="submit" name="addrate" value="Add Rate" class="btn btn-primary"/>
                </div>
            </div>

        </form>
    </div>
</div>
<br>

<div class="container" id="updateRate">
    <div class="row">
        <g:render template="../pageTitle" model="[title: 'Update Rates']"></g:render>
    </div>

    <div class="row">
        <a href=""><span class="glyphicon glyphicon-refresh"></span> Refresh</a>

        <form action="" method="post" name="form3" role="form" class="form-horizontal">
            <div class="form-group">

                <label class="control-label col-xs-6">
                    Browse Rates:
                </label>

                <div class="col-xs-6">
                    <select name="bankselector" class="form-control">
                        <%=br.allRates()%>
                    </select>
                </div>
            </div>

            <div class="form-group">

                <label class="control-label col-xs-6">
                    Country:
                </label>

                <div class="col-xs-6">
                    <select name="countryselector" class="form-control">
                        <%=br.countrySelector()%>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-xs-6">
                    Rate:
                </label>

                <div class="col-xs-6">
                    <input type="text" maxlength="20" name="rate" size="50" placeholder="Interest rate"
                           class="form-control" required="required"/>
                </div>
            </div>

            <div class="form-group">
                <div class="col-xs-6 offset"></div>

                <div class="col-xs-6">
                    <input type="submit" name="updaterate" value="Update Rate" class="btn btn-success"/>
                </div>
            </div>

        </form>
    </div>
</div>
<br>
