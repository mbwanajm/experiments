<%@ page import="com.mbwanajm.darproperty.Parteners_banks" %>



<div class="fieldcontain ${hasErrors(bean: parteners_banksInstance, field: 'bank_email', 'error')} required">
	<label for="bank_email">
		<g:message code="parteners_banks.bank_email.label" default="Bankemail" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="bank_email" required="" value="${parteners_banksInstance?.bank_email}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: parteners_banksInstance, field: 'bank', 'error')} required">
	<label for="bank">
		<g:message code="parteners_banks.bank.label" default="Bank" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="bank" name="bank.id" from="${com.mbwanajm.darproperty.Banks.list()}" optionKey="id" required="" value="${parteners_banksInstance?.bank?.id}" class="many-to-one"/>
</div>

