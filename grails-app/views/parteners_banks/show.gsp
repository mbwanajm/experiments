
<%@ page import="com.mbwanajm.darproperty.Parteners_banks" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'parteners_banks.label', default: 'Parteners_banks')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-parteners_banks" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="create" controller="config" action="index"> <span class="glyphicon glyphicon-wrench"></span></g:link></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-parteners_banks" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list parteners_banks">
			
				<g:if test="${parteners_banksInstance?.bank_email}">
				<li class="fieldcontain">
					<span id="bank_email-label" class="property-label"><g:message code="parteners_banks.bank_email.label" default="Bankemail" /></span>
					
						<span class="property-value" aria-labelledby="bank_email-label"><g:fieldValue bean="${parteners_banksInstance}" field="bank_email"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${parteners_banksInstance?.bank}">
				<li class="fieldcontain">
					<span id="bank-label" class="property-label"><g:message code="parteners_banks.bank.label" default="Bank" /></span>
					
						<span class="property-value" aria-labelledby="bank-label"><g:link controller="banks" action="show" id="${parteners_banksInstance?.bank?.id}">${parteners_banksInstance?.bank?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${parteners_banksInstance?.id}" />
					<g:link class="edit" action="edit" id="${parteners_banksInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
