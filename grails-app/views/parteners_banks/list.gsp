
<%@ page import="com.mbwanajm.darproperty.Parteners_banks" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'parteners_banks.label', default: 'Parteners_banks')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-parteners_banks" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="create" controller="config" action="index"> <span class="glyphicon glyphicon-wrench"></span></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-parteners_banks" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table class="table table-responsive">
				<thead>
					<tr class="hd2">
					
						<g:sortableColumn property="bank_email" title="${message(code: 'parteners_banks.bank_email.label', default: 'Bankemail')}" />
					
						<th><g:message code="parteners_banks.bank.label" default="Bank" /></th>
					
					</tr>
				</thead>
				<tbody class="hd2">
				<g:each in="${parteners_banksInstanceList}" status="i" var="parteners_banksInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${parteners_banksInstance.id}">${fieldValue(bean: parteners_banksInstance, field: "bank_email")}</g:link></td>
					
						<td>${fieldValue(bean: parteners_banksInstance, field: "bank")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${parteners_banksInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
