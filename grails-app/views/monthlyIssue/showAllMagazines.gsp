<html>
<head>
    <title>DarProperty>>The Magazine</title>
    <meta name="layout" content="main"/>
</head>

<body>
<div class="container">
    <g:render template="../pageTitle" model="[title: 'All Our Magazines']"></g:render>
    <br>

    <div class='row'>

        <g:each in="${magazines}" var="magazine">
            <div class="col-lg-3">
                <br>
                <b class="wid-view-more ">${magazine.title}</b>
                <hr class="style-one">
                <br>
                <g:link controller='monthlyIssue' action='viewMagazine' id='${magazine.id}'>
                    <img src="<g:createLink controller='image' action='renderMonthlyIssuePhoto'
                                            id='${magazine.id}'/>" alt="read this"/>
                </g:link>

                <br>

                <div >
                    <p class="pull-left">

                        <small class="hd2">  <g:message code="home.share"></g:message> <i class="fa fa-share"></i> </small>

                        <span class="" style="color:#4a6ea9; font-size:30pt;">
                            <!-- FACEBOOK -->
                            <a href="http://www.facebook.com/dialog/feed?app_id=1670558763159469&redirect_uri=http%3A%2F%2Fwww.darproperty.co.tz%2FmonthlyIssue%2FviewMagazine%2F${magazine.id}&link=http%3A%2F%2Fwww.darproperty.co.tz%2FmonthlyIssue%2FviewMagazine%2F${magazine.id}"
                               target="">
                                <span class="fa fa-facebook-square hidden-sm hidden-xs"></span>
                                <span class="fa fa-facebook hidden-lg"></span>
                                %{-- <img
                                         src="${resource(dir: 'bootstrap/imgs/', file: 'facebook.png')}"
                                         alt="Facebook"
                                         class=""
                                         title="Share on Facebook" />--}%
                            </a>
                        </span>

                        <span class=" hidden-xs hidden-sm"
                              style="background-color:#EEE; border-color:transparent; border-width: 0px;">
                        </span>

                        <span class=" hidden-lg" style="color:#2ab200;font-size:30pt;">
                            <!-- WHATSAPP -->
                            <a
                                    href="whatsapp://send?text= Check  ${magazine.title}  at  Darproperty  READ ONLINE NOW:-> http://www.darproperty.co.tz/monthlyIssue/viewMagazine/${magazine.id}"
                                    data-action="share/whatsapp/share"
                                    target="_blank"
                                    title="Whatsapp share">
                                <img
                                        src="${resource(dir: 'bootstrap/imgs/', file: 'whatsappgreen.png')}"
                                        alt="Whatsapp"
                                        width="40"
                                        title="Share on Whatsapp"/>
                            </a>
                        </span>

                        <span style="color:#659fcb; font-size:30pt;" class="text-info" >
                            <!-- TWITTER -->

                            <!--class="twitter-share-button"{count}  -->
                            <a href="https://twitter.com/intent/tweet?text=Read%20now%20online%20${magazine.title}%20issue%20at%20DarProperty%20,%20VIA%20@thedarproperty%20in&amp;url=http%3A%2F%2Fwww.darproperty.co.tz%2FmonthlyIssue%2FviewMagazine%2F${magazine.id}"
                               class="shortenUrl"
                               data-social-url="https://twitter.com/intent/tweet?text=Read+now+${magazine.title}+issue+VIA+@thedarproperty+in&amp;url="
                               data-target-url="http://www.darproperty.co.tz/monthlyIssue/viewMagazine/${magazine.id}"
                               target="_blank">
                                <span class="fa fa-twitter-square hidden-sm hidden-xs"></span>
                                <span class="fa fa-twitter hidden-lg"></span>
                                %{--<img
                                        src="${resource(dir: 'bootstrap/imgs/', file: 'twitter.png')}"
                                        alt="Twitter"
                                        class=""
                                        title="Share on Twitter"/>--}%
                            </a>
                        </span>
                    </p>
                    <div class="hidden-md hidden-lg">
                        <br><br><br><br>
                    </div>
                </div>
            </div>
        </g:each>


    </div>
</div>

</body>
</html>
