<%@ page import="com.mbwanajm.darproperty.MonthlyIssue" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'monthlyIssue.label', default: 'MonthlyIssue')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<a href="#show-monthlyIssue" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                                   default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="show-monthlyIssue" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <ol class="property-list monthlyIssue">

        <g:if test="${monthlyIssueInstance?.title}">
            <li class="fieldcontain">
                <span id="title-label" class="property-label"><g:message code="monthlyIssue.title.label"
                                                                         default="Title"/></span>

                <span class="property-value" aria-labelledby="title-label"><g:fieldValue bean="${monthlyIssueInstance}"
                                                                                         field="title"/></span>

            </li>
        </g:if>

        <g:if test="${monthlyIssueInstance?.embedCode}">
            <li class="fieldcontain">
                <span id="embedCode-label" class="property-label"><g:message code="monthlyIssue.embedCode.label"
                                                                             default="Embed Code"/></span>

                <span class="property-value" aria-labelledby="embedCode-label"><g:fieldValue
                        bean="${monthlyIssueInstance}" field="embedCode"/></span>

            </li>
        </g:if>

        <g:if test="${monthlyIssueInstance?.image}">
            <li class="fieldcontain">
                <span id="image-label" class="property-label"><g:message code="monthlyIssue.image.label"
                                                                         default="Image"/></span>

            </li>
        </g:if>

    </ol>
    <g:form>
        <fieldset class="buttons">
            <g:hiddenField name="id" value="${monthlyIssueInstance?.id}"/>
            <g:link class="edit" action="edit" id="${monthlyIssueInstance?.id}"><g:message
                    code="default.button.edit.label" default="Edit"/></g:link>
            <g:actionSubmit class="delete" action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
        </fieldset>
    </g:form>
</div>
</body>
</html>
