<html>
<head>
    <title>The Magazine(${magazine.title})</title>
    <meta name="layout" content="main"/>
</head>

<body>

<% def title = "Reading $magazine.title issue" %>
<g:render template="../pageTitle" model="[title: title]"></g:render>
<div class="row">
    <div class="span12" align="center">
        ${magazine.embedCode}
    </div>
</div>
<script>
    $(document).ready(function () {
        function issuu() {
            var issue = $(".issuuembed");
            var style =
                    "width: " + issue.parent().innerWidth() +
                    "px; height: "  + issue.parent().innerWidth() * .7625 + "px;";

            issue.attr({"style": style });
        }

        $(function () {
            issuu();
            $(window).resize(function (e) {
                issuu();
            });
        });
    });
</script>
</body>

</html>
