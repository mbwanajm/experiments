<%@ page import="com.mbwanajm.darproperty.MonthlyIssue" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'monthlyIssue.label', default: 'MonthlyIssue')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<a href="#list-monthlyIssue" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                                   default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="list-monthlyIssue" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <table>
        <thead>
        <tr>

            <g:sortableColumn property="title" title="${message(code: 'monthlyIssue.title.label', default: 'Title')}"/>

            <g:sortableColumn property="embedCode"
                              title="${message(code: 'monthlyIssue.embedCode.label', default: 'Embed Code')}"/>


        </tr>
        </thead>
        <tbody>
        <g:each in="${monthlyIssueInstanceList}" status="i" var="monthlyIssueInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                <td><g:link action="show"
                            id="${monthlyIssueInstance.id}">${fieldValue(bean: monthlyIssueInstance, field: "title")}</g:link></td>

                <td>${fieldValue(bean: monthlyIssueInstance, field: "embedCode")}</td>

            </tr>
        </g:each>
        </tbody>
    </table>

    <div class="pagination">
        <g:paginate total="${monthlyIssueInstanceTotal}"/>
    </div>
</div>
</body>
</html>
