<%@ page import="com.mbwanajm.darproperty.MonthlyIssue" %>



<div class="fieldcontain ${hasErrors(bean: monthlyIssueInstance, field: 'title', 'error')} ">
    <label for="title">
        <g:message code="monthlyIssue.title.label" default="Title"/>

    </label>
    <g:textField name="title" value="${monthlyIssueInstance?.title}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: monthlyIssueInstance, field: 'embedCode', 'error')} ">
    <label for="embedCode">
        <g:message code="monthlyIssue.embedCode.label" default="Embed Code"/>

    </label>
    <g:textArea name="embedCode" cols="40" rows="5" maxlength="1000" value="${monthlyIssueInstance?.embedCode}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: monthlyIssueInstance, field: 'image', 'error')} ">
    <label for="image">
        <g:message code="monthlyIssue.image.label" default="Image"/>

    </label>
    <input type="file" id="image" name="image"/>
</div>

