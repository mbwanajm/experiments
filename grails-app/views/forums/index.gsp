
<html>
<head>

    <meta name="layout" content="main"/>
    <title>DarProperty>>Discussion Forums</title>
</head>

<body>

<div class="container">
    <div class="row in-title">
        <div class="col-sm-12">
            <h3 align="center"><g:message code="Forums.title"/></h3>
        </div>
    </div>

    <div class="margine2"></div>

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default" style="border:0px solid #fff;">


                <div class="panel-body">
                    <!-- START -->
                    <div id="disqus_thread"></div>
                    <script>
                        /**
                         * RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                         * LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
                         */
                        /*
                         var disqus_config = function () {
                         this.page.url = PAGE_URL; // Replace PAGE_URL with your page's canonical URL variable
                         this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                         };
                         */
                        (function () { // DON'T EDIT BELOW THIS LINE
                            var d = document, s = d.createElement('script');

                            s.src = '//darpropertyforum.disqus.com/embed.js';

                            s.setAttribute('data-timestamp', +new Date());
                            (d.head || d.body).appendChild(s);
                        })();
                    </script>
                    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript"
                                                                      rel="nofollow">comments powered by Disqus.</a>
                    </noscript>
                    <!-- END -->

                </div>

                <div class="panel-footer">
                    <small>Copyright © <%=new GregorianCalendar().get(Calendar.YEAR) %>. Powered by <a
                            href="https://darpropertyforum.disqus.com" class="hd2">DarProperty Disqus Channel</a>
                    </small>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>