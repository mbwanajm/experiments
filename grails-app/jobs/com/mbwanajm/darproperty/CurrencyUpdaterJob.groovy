package com.mbwanajm.darproperty

/**
 * This job updates the currency rates
 */
class CurrencyUpdaterJob {
    CurrencyService currencyService

    static triggers = {
      simple repeatInterval: 1000 * 60 * 60 * 12 // execute job once every 12 hrs
    }

    def execute() {
       currencyService.updateRates()
       log.info("updated currency rates")
    }
}
