package com.mbwanajm.darproperty

import grails.plugin.mail.MailService
import grails.plugins.springsecurity.Secured
import org.springframework.dao.DataIntegrityViolationException


class ContactsController {
    MailService mailService
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    @Secured(['ROLE_ADMIN'])
    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [contactsInstanceList: Contacts.list(params), contactsInstanceTotal: Contacts.count()]
    }

    def create() {
        [contactsInstance: new Contacts(params)]
    }

    def save() {

        def contactsInstance = new Contacts(params)
        if (!contactsInstance.save(flush: true)) {

            render(view: "create", model: [contactsInstance: contactsInstance])
            return

        }

        def paging = params.page

        if (paging == "insurance") {
//            SendEmail mail = new SendEmail();
//            mail.setSender("info@darproperty.net");
//            mail.setPassword("mikocheni120");
//            mail.setSubject("INSURANCE WEB-PAGE MESSAGE")
//            mail.setBody("'" + params.message + "' <br>From<br> (Name:" + params.name + ", Email:" + params.email + ", Phone: " + params.phone + ")");
//            if (mail.sendEmailForInsurance()) {
//                flash.message = message(code: 'default.created.message', args: [message(code: 'contacts.label', default: 'Thank you, we have received your mail'), contactsInstance.id])
//            } else {
//                flash.message = message(code: 'default.created.message', args: [message(code: 'contacts.label', default: 'Sorry, We have not receive your mail'), contactsInstance.id])
//
//            }

            try {
                mailService.sendMail {
                    to "insurance@darproperty.net", "firstajep@gmail.com", "michael@darproperty.net"
                    from "info@darproperty.net"
                    subject "INSURANCE WEB-PAGE MESSAGE"
                    body "'$params.message' \n\nFrom \n(Name: $params.name, Email: $params.email, Phone: $params.phone) \n © Darproperty Insurance"
                }

                flash.successMessage = 'Your message has been sent succesfully'

            } catch (Exception ex) {
                flash.errorMessage = 'Sorry something went wrong please try to send your message again' + ex
            }
        } else if (paging == "mortgage") {
//            SendEmail mail = new SendEmail();
//            mail.setSender("info@darproperty.net");
//            mail.setPassword("mikocheni120");
//            mail.setSubject("MORTGAGE WEB-PAGE MESSAGE")
//            mail.setBody("'" + params.message + "' <br>From<br> (Name:" + params.name + ", Email:" + params.email + ", Phone: " + params.phone + ")");
//            if (mail.sendEmailForMortgage()) {
//                flash.message = message(code: 'default.created.message', args: [message(code: 'contacts.label', default: 'Thank you, we have received your mail'), contactsInstance.id])
//            } else {
//                flash.message = message(code: 'default.created.message', args: [message(code: 'contacts.label', default: 'Sorry, We have not receive your mail'), contactsInstance.id])
//
//            }
//        } else {
//            flash.message = message(code: 'default.created.message', args: [message(code: 'contacts.label', default: 'Thank you, we have received your mail'), contactsInstance.id])
//        }


            try {
                mailService.sendMail {
                    to "mortgage@darproperty.net", "firstajep@gmail.com", "michael@darproperty.net"
                    from "info@darproperty.net"
                    subject "MORTGAGE WEB-PAGE MESSAGE"
                    body "'$params.message' \n\nFrom \n(Name: $params.name, Email: $params.email, Phone: $params.phone) \n © Darproperty Mortgage"
                }

                flash.successMessage = 'Your message has been sent succesfully'

            } catch (Exception ex) {
                flash.errorMessage = 'Sorry something went wrong please try to send your message again' + ex
            }
        }

        redirect(action: "show", id: contactsInstance.id, paging:paging)
    }

    def show(Long id) {
        def contactsInstance = Contacts.get(id)
        if (!contactsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'contacts.label', default: 'Contacts'), id])
            redirect(action: "list")
            return
        }

        [contactsInstance: contactsInstance]
    }

    @Secured(['ROLE_ADMIN'])
    def edit(Long id) {
        def contactsInstance = Contacts.get(id)
        if (!contactsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'contacts.label', default: 'Contacts'), id])
            redirect(action: "list")
            return
        }

        [contactsInstance: contactsInstance]
    }

    @Secured(['ROLE_ADMIN'])
    def update(Long id, Long version) {
        def contactsInstance = Contacts.get(id)
        if (!contactsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'contacts.label', default: 'Contacts'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (contactsInstance.version > version) {
                contactsInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'contacts.label', default: 'Contacts')] as Object[],
                        "Another user has updated this Contacts while you were editing")
                render(view: "edit", model: [contactsInstance: contactsInstance])
                return
            }
        }

        contactsInstance.properties = params

        if (!contactsInstance.save(flush: true)) {
            render(view: "edit", model: [contactsInstance: contactsInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'contacts.label', default: 'Contacts'), contactsInstance.id])
        redirect(action: "show", id: contactsInstance.id)
    }

    @Secured(['ROLE_ADMIN'])
    def delete(Long id) {
        def contactsInstance = Contacts.get(id)
        if (!contactsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'contacts.label', default: 'Contacts'), id])
            redirect(action: "list")
            return
        }

        try {
            contactsInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'contacts.label', default: 'Contacts'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'contacts.label', default: 'Contacts'), id])
            redirect(action: "show", id: id)
        }
    }
}
