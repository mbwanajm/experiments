package com.mbwanajm.darproperty

import org.springframework.dao.DataIntegrityViolationException
import grails.plugins.springsecurity.Secured
import grails.converters.JSON
class Region_areasController {
	def scaffold = true
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	@Secured(['ROLE_ADMIN'])
    def index() {
        redirect(action: "list", params: params)
    }
	
	def getRegionsForAreas() {
		def country = Country.findByName(params.name)
		render country.regions.collect{it.name} as JSON
	}
	@Secured(['ROLE_ADMIN'])
    def list(Integer max) {
        params.max = Math.min(max ?: 20, 100)
        [region_areasInstanceList: Region_areas.list(params), region_areasInstanceTotal: Region_areas.count()]
    }
	@Secured(['ROLE_ADMIN'])
    def create() {
        [region_areasInstance: new Region_areas(params)]
    }
	@Secured(['ROLE_ADMIN'])
    def save() {
        def region_areasInstance = new Region_areas(params)
        if (!region_areasInstance.save(flush: true)) {
            render(view: "create", model: [region_areasInstance: region_areasInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'region_areas.label', default: 'Region_areas'), region_areasInstance.id])
        redirect(action: "show", id: region_areasInstance.id)
    }
	@Secured(['ROLE_ADMIN'])
    def show(Long id) {
        def region_areasInstance = Region_areas.get(id)
        if (!region_areasInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'region_areas.label', default: 'Region_areas'), id])
            redirect(action: "list")
            return
        }

        [region_areasInstance: region_areasInstance]
    }
	@Secured(['ROLE_ADMIN'])
    def edit(Long id) {
        def region_areasInstance = Region_areas.get(id)
        if (!region_areasInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'region_areas.label', default: 'Region_areas'), id])
            redirect(action: "list")
            return
        }

        [region_areasInstance: region_areasInstance]
    }
	@Secured(['ROLE_ADMIN'])
    def update(Long id, Long version) {
        def region_areasInstance = Region_areas.get(id)
        if (!region_areasInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'region_areas.label', default: 'Region_areas'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (region_areasInstance.version > version) {
                region_areasInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'region_areas.label', default: 'Region_areas')] as Object[],
                          "Another user has updated this Region_areas while you were editing")
                render(view: "edit", model: [region_areasInstance: region_areasInstance])
                return
            }
        }

        region_areasInstance.properties = params

        if (!region_areasInstance.save(flush: true)) {
            render(view: "edit", model: [region_areasInstance: region_areasInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'region_areas.label', default: 'Region_areas'), region_areasInstance.id])
        redirect(action: "show", id: region_areasInstance.id)
    }
	@Secured(['ROLE_ADMIN'])
    def delete(Long id) {
        def region_areasInstance = Region_areas.get(id)
        if (!region_areasInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'region_areas.label', default: 'Region_areas'), id])
            redirect(action: "list")
            return
        }

        try {
            region_areasInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'region_areas.label', default: 'Region_areas'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'region_areas.label', default: 'Region_areas'), id])
            redirect(action: "show", id: id)
        }
    }
}
