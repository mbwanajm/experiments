package com.mbwanajm.darproperty

import grails.plugins.springsecurity.Secured
import org.springframework.dao.DataIntegrityViolationException

class Parteners_banksController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }
    @Secured(['ROLE_ADMIN'])
    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [parteners_banksInstanceList: Parteners_banks.list(params), parteners_banksInstanceTotal: Parteners_banks.count()]
    }
    @Secured(['ROLE_ADMIN'])
    def create() {
        [parteners_banksInstance: new Parteners_banks(params)]
    }
    @Secured(['ROLE_ADMIN'])
    def save() {
        def parteners_banksInstance = new Parteners_banks(params)
        if (!parteners_banksInstance.save(flush: true)) {
            render(view: "create", model: [parteners_banksInstance: parteners_banksInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'parteners_banks.label', default: 'Parteners_banks'), parteners_banksInstance.id])
        redirect(action: "show", id: parteners_banksInstance.id)
    }
    @Secured(['ROLE_ADMIN'])
    def show(Long id) {
        def parteners_banksInstance = Parteners_banks.get(id)
        if (!parteners_banksInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'parteners_banks.label', default: 'Parteners_banks'), id])
            redirect(action: "list")
            return
        }

        [parteners_banksInstance: parteners_banksInstance]
    }
    @Secured(['ROLE_ADMIN'])
    def edit(Long id) {
        def parteners_banksInstance = Parteners_banks.get(id)
        if (!parteners_banksInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'parteners_banks.label', default: 'Parteners_banks'), id])
            redirect(action: "list")
            return
        }

        [parteners_banksInstance: parteners_banksInstance]
    }
    @Secured(['ROLE_ADMIN'])
    def update(Long id, Long version) {
        def parteners_banksInstance = Parteners_banks.get(id)
        if (!parteners_banksInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'parteners_banks.label', default: 'Parteners_banks'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (parteners_banksInstance.version > version) {
                parteners_banksInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'parteners_banks.label', default: 'Parteners_banks')] as Object[],
                          "Another user has updated this Parteners_banks while you were editing")
                render(view: "edit", model: [parteners_banksInstance: parteners_banksInstance])
                return
            }
        }

        parteners_banksInstance.properties = params

        if (!parteners_banksInstance.save(flush: true)) {
            render(view: "edit", model: [parteners_banksInstance: parteners_banksInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'parteners_banks.label', default: 'Parteners_banks'), parteners_banksInstance.id])
        redirect(action: "show", id: parteners_banksInstance.id)
    }
    @Secured(['ROLE_ADMIN'])
    def delete(Long id) {
        def parteners_banksInstance = Parteners_banks.get(id)
        if (!parteners_banksInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'parteners_banks.label', default: 'Parteners_banks'), id])
            redirect(action: "list")
            return
        }

        try {
            parteners_banksInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'parteners_banks.label', default: 'Parteners_banks'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'parteners_banks.label', default: 'Parteners_banks'), id])
            redirect(action: "show", id: id)
        }
    }
}
