package com.mbwanajm.darproperty

import grails.plugins.springsecurity.Secured
import org.springframework.dao.DataIntegrityViolationException

class BanksController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }
    @Secured(['ROLE_ADMIN'])
    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [banksInstanceList: Banks.list(params), banksInstanceTotal: Banks.count()]
    }
    @Secured(['ROLE_ADMIN'])
    def create() {
        [banksInstance: new Banks(params)]
    }
    @Secured(['ROLE_ADMIN'])
    def save() {
        def banksInstance = new Banks(params)
        if (!banksInstance.save(flush: true)) {
            render(view: "create", model: [banksInstance: banksInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'banks.label', default: 'Banks'), banksInstance.id])
        redirect(action: "show", id: banksInstance.id)
    }
    @Secured(['ROLE_ADMIN'])
    def show(Long id) {
        def banksInstance = Banks.get(id)
        if (!banksInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'banks.label', default: 'Banks'), id])
            redirect(action: "list")
            return
        }

        [banksInstance: banksInstance]
    }
    @Secured(['ROLE_ADMIN'])
    def edit(Long id) {
        def banksInstance = Banks.get(id)
        if (!banksInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'banks.label', default: 'Banks'), id])
            redirect(action: "list")
            return
        }

        [banksInstance: banksInstance]
    }
    @Secured(['ROLE_ADMIN'])
    def update(Long id, Long version) {
        def banksInstance = Banks.get(id)
        if (!banksInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'banks.label', default: 'Banks'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (banksInstance.version > version) {
                banksInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'banks.label', default: 'Banks')] as Object[],
                          "Another user has updated this Banks while you were editing")
                render(view: "edit", model: [banksInstance: banksInstance])
                return
            }
        }

        banksInstance.properties = params

        if (!banksInstance.save(flush: true)) {
            render(view: "edit", model: [banksInstance: banksInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'banks.label', default: 'Banks'), banksInstance.id])
        redirect(action: "show", id: banksInstance.id)
    }

    def delete(Long id) {
        def banksInstance = Banks.get(id)
        if (!banksInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'banks.label', default: 'Banks'), id])
            redirect(action: "list")
            return
        }

        try {
            banksInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'banks.label', default: 'Banks'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'banks.label', default: 'Banks'), id])
            redirect(action: "show", id: id)
        }
    }
}
