package com.mbwanajm.darproperty

import grails.plugins.springsecurity.Secured
import org.springframework.scheduling.annotation.Async

class AgentController {
    def scaffold = true
    AgentService agentService
    def springSecurityService

    def login = {

    }

    def profile() {


        def agent = Agent.findById(params.id)
//        if (params.rating ) {
//
//            String ip = params.ip
//            int rate = Integer.parseInt(params.rate)
//            def ids = params.id
//            agent = Agent.findById(params.id)
//            try {
//                def ag = new Agent_rates(agent: agent, ip: ip, rate: rate)
//                if (ag.save(flush: true)) {
//////                    [agent: agent, agentService: agentService]
//                   // redirect(controller: "agent", action: "profile", params: [id: agent.id,agent: agent, agentService: agentService] )
//                    redirect( action: "showAllAgents" )
//
//                } else {
//                    ag.errors.allErrors.each {
//                        flash.errorMessage = "Failed to rate " + it;
////                       [agent: agent, agentService: agentService]
//                     //   redirect(controller: "agent", action: "profile", params: [id: agent.id,agent: agent, agentService: agentService])
//                        redirect( action: "showAllAgents" )
//                    }
//                }
//            } catch (Exception ex) {
//             //   flash.errorMessage = "" + ex;
//
//            }
//
//            redirect( action: "showAllAgents" )
//        } else {

        [agent: agent, agentService: agentService]
//        }


    }

    @Async
    def rateAgent() {

//        String ip = params.ip
//        int rate = Integer.parseInt(params.rate)
//        def ids = params.id
//        def agent = Agent.findById(params.id)
//        def ag = new Agent_rates(agent: agent, ip: ip, rate: rate)
//        ag.save()
//            try {
//                def ag = new Agent_rates(agent: agent, ip: ip, rate: rate)
//                if (ag.save(flush: true)) {
//////                    [agent: agent, agentService: agentService]
//                    // redirect(controller: "agent", action: "profile", params: [id: agent.id,agent: agent, agentService: agentService] )
//                    redirect(action: "showAllAgents")
//
//                } else {
//                    ag.errors.allErrors.each {
////                        flash.errorMessage = "Failed to rate " + it;
//////                       [agent: agent, agentService: agentService]
////                        //   redirect(controller: "agent", action: "profile", params: [id: agent.id,agent: agent, agentService: agentService])
//                        redirect(action: "showAllAgents")
//                    }
//                }
//                redirect(action: "showAllAgents")
//            } catch (Exception ex) {
//                //   flash.errorMessage = "" + ex;
//
//            }

        redirect(action: "showAllAgents")

    }

    // trick to show login page
    @Secured(['ROLE_AGENT'])
    def authenticate() {

        redirect(controller: 'agent', action: "profile", id: springSecurityService.getPrincipal().id)
    }

    @Secured(['ROLE_AGENT'])
    def editProfile() {
        def agent
        if (flash.agent) {
            agent = agentService.createAgentFromApc(flash.agent)

        } else {
            agent = Agent.findById(params.id)
        }

        [agent: agent, apc: flash.agent]
    }

    def agentSignup() {
        if (params.create) {
            String username = params.email;
            def fname = params.firstName;
            def mname = '';
            def lname = params.lastName;
            def company = params.company;
            def phone = params.phone;
            def email = params.email;
            def pword = params.password;
            def pconfirm = params.confirm;
            def mailing = params.mailing;
            def enabled = false;
            def expired = false;
            def passwordExpired = false;

            def usernameExist = Agent.findByUsername(username.toString()) ? true : false;
            def emailExists = Profile.findByEmail(email) ? true : false;
            def phoneExists = Profile.findByPhone(phone) ? true : false;
            def validPassword = (pword == pconfirm ? (pword.size() > 5) ? true : "your password should have more than 6 characters!" : "your passwords do'nt match");
            def userRole = Role.findByAuthority('ROLE_AGENT')

            //check if username exists


            if (usernameExist) {
                flash.error = username + " already taken!"
            } else {
                if (emailExists) {
                    flash.error = email + " already taken"
                } else {
                    if (phoneExists) {
                        flash.error = phone + " already taken"
                    } else {
                        if (validPassword) {
                            def signupProfile = new Profile(firstName: fname,middleName: '', lastName: lname, company: company, email: email, phone: phone)
                            if (signupProfile.save()) {
                                def prof = Profile.findByEmail(username)

                                def agent = new Agent(username: username, password: pword, accountExpired: false, accountLocked: false, enabled: true, profile: prof)
                                if (agent.save()) {

                                    if (UserRole.create(agent, userRole)) {


                                        flash.message = "User created"
                                        flash.error = ""


                                    } else {
                                        def user = User.findByUsername(username)
                                        user.delete()
                                        prof = Profile.findByEmail(username)
                                        prof.delete()
                                        flash.error = "user role can't be added"
                                    }

                                } else {
                                    prof = Profile.findByEmail(username)
                                    prof.delete()
                                    flash.error = "User can't be created"
                                }
                            } else {
                                flash.error = "Error: Can't create profile. Try again later!"
                            }

                        } else {
                            flash.error = validPassword
                        }
                    }
                }
            }
        }
    }

    @Secured(['ROLE_AGENT'])
    def saveProfile(AgentProfileCommand apc) {
        if (apc.validate()) {
            Agent agent = agentService.createAgentFromApc(apc)
            agent.save()
            redirect(action: "profile", params: [id: agent.id])
            flash.message = "Profile saved succesfully"
        } else {
            flash.message = "User has errors"
            flash.agent = apc
            redirect(action: "editProfile")
            //return [agent: apc]
        }

    }

    @Secured(['ROLE_AGENT'])
    def changePassword() {
        Agent agent = springSecurityService.currentUser
        [agent: agent]

    }

    @Secured(['ROLE_AGENT'])
    def savePassword() {
        if (params.password.isEmpty()) {
            flash.message = "Can't change password, empty passwords not allowed"
            redirect(action: "changePassword")
        } else if (params.password != params.passwordRepeat) {
            flash.message = "Can't change password, the two passwords don't match"
            redirect(action: "changePassword")
        } else {
            Agent agent = springSecurityService.currentUser
            agent.password = params.password
            agent.save()
            redirect(action: "profile", params: [id: agent.id])
            flash.message = "Password changed succesfully"

        }

    }

    def viewProperty() {
        params << ['max': 10, sort: 'dateAdded', order: 'desc']
        def agent = Agent.findById(params.id)
        def propertys = Property.findAllByAgentAndDeleted(agent, false, params)
        def propertyCount = Property.findAllByAgentAndDeleted(agent, false).size()

        [agent: agent, propertys: propertys, propertyCount: propertyCount]
    }

    def messages = {

    }

    def statistics = {

    }

    @Secured(['ROLE_ADMIN'])
    def zxcv() {

    }

    @Secured(['ROLE_ADMIN'])
    def giveAgentRole() {
        def agent = Agent.findById(params.id)
        if (agent) {
            def agentRole = Role.findByAuthority('ROLE_AGENT') ?: new Role(authority: 'ROLE_AGENT').save(failOnError: true)

            if (!agent.authorities.contains(agentRole)) {
                UserRole.create agent, agentRole
                redirect(controller: 'home')
            }
        }
    }

    def showAllAgents() {
        def agents = Agent.list(sort: "profile.company", company: "desc", max: 12)

        [agents: agents, agentsCount: agents.count {}]
    }

    def showProfileFromName() {

        def agent = Agent.findByUsername(params.id)
        if (agent) {
            render(view: 'profile', model: [agent: agent])
        } else {
            redirect(action: 'showAllAgents')
        }

    }

    def showMessages() {
        Agent agent = springSecurityService.currentUser
        def propertyWithMessages = []
        def messageCount = 0
        agent.property.each { Property property ->
            if (property.messages) {
                propertyWithMessages << property
                messageCount += property.messages.size()
            }
        }
        [messageCount: messageCount, propertyWithMessages: propertyWithMessages, agent: agent]
    }

    def deleteMessage() {
        Message message = Message.findById(Long.parseLong(params.id))
        Property owningProperty = Property.findById(Long.parseLong(params.propertyId))
        owningProperty.removeFromMessages(message)

        Agent agent = springSecurityService.currentUser
        def propertyWithMessages = []
        def messageCount = 0
        agent.property.each { Property property ->
            if (property.messages) {
                propertyWithMessages << property
                messageCount += property.messages.size()
            }
        }
        flash.messageDeletionSuccess = "Message from ${message.email} has been deleted succesfully"
        render view: 'showMessages', model: [messageCount: messageCount, propertyWithMessages: propertyWithMessages, agent: agent]

    }

    def createAgent() {


    }
}


class AgentProfileCommand {
    def id
    String email
    String firstName
    String middleName
    String lastName

    byte[] photo

    String phone
    String company


    static constraints = {
        email(email: true)
        firstName(blank: false)
        middleName(blank: true)
        lastName(blank: false)
        photo(nullable: true, minSize: 0, maxSize: 10485760)// 10MB
    }
}
