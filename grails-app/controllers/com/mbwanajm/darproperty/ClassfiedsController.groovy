package com.mbwanajm.darproperty

import grails.plugins.springsecurity.Secured

class ClassfiedsController {
    EmailService emailService
    def index = {}

    @Secured(['ROLE_ADMIN'])
    def export_contacts() {
        if (params.export) {
            int start = Integer.parseInt(params.startyear)
            int end = Integer.parseInt(params.endyear)
            if (start > end) {
                int hold = start
                start = end
                end = hold
            }
            def startyear = "" + start
            def endyear = "" + end
            [emailService: emailService, startyear: startyear, endyear: endyear]
        } else {
            redirect(controller: "config", action: "index")
        }


    }

    @Secured(['ROLE_ADMIN'])
    def extract(String description) {
        def contacts = description.substring(description.indexOf('{{') + 2, description.indexOf('}}'))
        def details = contacts.split(',')
        def name = details[0];
        def phone = details[1];
        def email = details[2];
        return [name: name, phone: phone, email: email]
    }
}
