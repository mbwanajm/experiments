package com.mbwanajm.darproperty

class TaskController {
    ImageService imageService

    def resizeAllPropertyPhotos() {
        def photos = Photo.getAll()
        photos.each { photo ->
            log.info('converting ${photo.name}')
            photo.image = imageService.resize(photo.image, 368, 276)
            photo.save()
        }
    }
}
