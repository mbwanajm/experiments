package com.mbwanajm.darproperty

import org.springframework.dao.DataIntegrityViolationException
import grails.plugins.springsecurity.Secured
class AmenitiesController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	@Secured(['ROLE_ADMIN'])
    def index() {
        redirect(action: "list", params: params)
    }
	@Secured(['ROLE_ADMIN'])
    def list(Integer max) {
        params.max = Math.min(max ?: 5, 100)
        [amenitiesInstanceList: Amenities.list(params), amenitiesInstanceTotal: Amenities.count()]
    }
	@Secured(['ROLE_ADMIN'])
    def create() {
        [amenitiesInstance: new Amenities(params)]
    }
	@Secured(['ROLE_ADMIN'])
    def save() {
        def amenitiesInstance = new Amenities(params)
        if (!amenitiesInstance.save(flush: true)) {
            render(view: "create", model: [amenitiesInstance: amenitiesInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'amenities.label', default: 'Amenities'), amenitiesInstance.id])
        redirect(action: "show", id: amenitiesInstance.id)
    }
	@Secured(['ROLE_ADMIN'])
    def show(Long id) {
        def amenitiesInstance = Amenities.get(id)
        if (!amenitiesInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'amenities.label', default: 'Amenities'), id])
            redirect(action: "list")
            return
        }

        [amenitiesInstance: amenitiesInstance]
    }
	@Secured(['ROLE_ADMIN'])
    def edit(Long id) {
        def amenitiesInstance = Amenities.get(id)
        if (!amenitiesInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'amenities.label', default: 'Amenities'), id])
            redirect(action: "list")
            return
        }

        [amenitiesInstance: amenitiesInstance]
    }
	@Secured(['ROLE_ADMIN'])
    def update(Long id, Long version) {
        def amenitiesInstance = Amenities.get(id)
        if (!amenitiesInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'amenities.label', default: 'Amenities'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (amenitiesInstance.version > version) {
                amenitiesInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'amenities.label', default: 'Amenities')] as Object[],
                          "Another user has updated this Amenities while you were editing")
                render(view: "edit", model: [amenitiesInstance: amenitiesInstance])
                return
            }
        }

        amenitiesInstance.properties = params

        if (!amenitiesInstance.save(flush: true)) {
            render(view: "edit", model: [amenitiesInstance: amenitiesInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'amenities.label', default: 'Amenities'), amenitiesInstance.id])
        redirect(action: "show", id: amenitiesInstance.id)
    }
	@Secured(['ROLE_ADMIN'])
    def delete(Long id) {
        def amenitiesInstance = Amenities.get(id)
        if (!amenitiesInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'amenities.label', default: 'Amenities'), id])
            redirect(action: "list")
            return
        }

        try {
            amenitiesInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'amenities.label', default: 'Amenities'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'amenities.label', default: 'Amenities'), id])
            redirect(action: "show", id: id)
        }
    }
}
