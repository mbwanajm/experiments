package com.mbwanajm.darproperty
import org.springframework.dao.DataIntegrityViolationException
import grails.plugins.springsecurity.Secured


class SubscriberController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }
    @Secured(['ROLE_ADMIN'])
    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [subscriberInstanceList: Subscriber.list(params), subscriberInstanceTotal: Subscriber.count()]
    }

    def create() {
        [subscriberInstance: new Subscriber(params)]
    }

    def save() {
        def subscriberInstance = new Subscriber(params)
        if (!subscriberInstance.save(flush: true)) {
            render(view: "create", model: [subscriberInstance: subscriberInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'subscriber.label', default: 'Subscriber'), subscriberInstance.id])
        redirect(action: "show", id: subscriberInstance.id)
    }

    def show(Long id) {
        def subscriberInstance = Subscriber.get(id)
        if (!subscriberInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'subscriber.label', default: 'Subscriber'), id])
            redirect(action: "list")
            return
        }

        [subscriberInstance: subscriberInstance]
    }

    def edit(Long id) {
        def subscriberInstance = Subscriber.get(id)
        if (!subscriberInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'subscriber.label', default: 'Subscriber'), id])
            redirect(action: "list")
            return
        }

        [subscriberInstance: subscriberInstance]
    }

    def update(Long id, Long version) {
        def subscriberInstance = Subscriber.get(id)
        if (!subscriberInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'subscriber.label', default: 'Subscriber'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (subscriberInstance.version > version) {
                subscriberInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'subscriber.label', default: 'Subscriber')] as Object[],
                          "Another user has updated this Subscriber while you were editing")
                render(view: "edit", model: [subscriberInstance: subscriberInstance])
                return
            }
        }

        subscriberInstance.properties = params

        if (!subscriberInstance.save(flush: true)) {
            render(view: "edit", model: [subscriberInstance: subscriberInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'subscriber.label', default: 'Subscriber'), subscriberInstance.id])
        redirect(action: "show", id: subscriberInstance.id)
    }

    def delete(Long id) {
        def subscriberInstance = Subscriber.get(id)
        if (!subscriberInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'subscriber.label', default: 'Subscriber'), id])
            redirect(action: "create")
            return
        }

        try {
            subscriberInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'subscriber.label', default: 'Subscriber'), id])
            redirect(action: "create")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'subscriber.label', default: 'Subscriber'), id])
            redirect(action: "show", id: id)
        }
    }

    @Secured(['ROLE_ADMIN'])
    def email_broadcasting(){



        def receiver = params.receiver;
        def message = params.message
        def subject = params.subject
//        if(message){
//            SendEmail mail = new SendEmail();
//            mail.setSender("info@darproperty.net");
//            mail.setPassword("mikocheni120");
//            mail.setSubject(""+subject+"");
//            mail.setBody("<h4 style='color:#ff0000;' align = 'center'>DARPROPERTY MESSAGE</h4><hr> <p align = 'center'>"+message+"</p><hr><p style='color:#ff0000;' align = 'center'> ---- Darproperty © "+year+" All rights reserved ---- </p>");
//            if(mail.broadcastEmail(receiver)){
//                flash.message = mail.getMsg()
//            }
//        }
        redirect(action: "list", params: params)
    }
}
