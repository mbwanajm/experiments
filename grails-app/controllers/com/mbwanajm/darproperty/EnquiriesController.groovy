package com.mbwanajm.darproperty

import grails.plugins.springsecurity.Secured
import grails.plugin.mail.MailService
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.scheduling.annotation.Async


import javax.mail.internet.InternetAddress
import java.text.DateFormat
import java.text.SimpleDateFormat

class EnquiriesController {
    def scaffold = true
    AgentService agentService
    MailService mailService
    def simpleCaptchaService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    @Secured(['ROLE_ADMIN'])
    def index() {
        redirect(action: "list", params: params)
    }

    @Secured(['ROLE_ADMIN'])
    def list(Integer max) {
        params.max = Math.min(max ?: 6, 100)
        [enquiriesInstanceList: Enquiries.list(params), enquiriesInstanceTotal: Enquiries.count()]
    }

    @Secured(['ROLE_ADMIN'])
    def create() {
        [enquiriesInstance: new Enquiries(params)]
    }

    @Secured(['ROLE_ADMIN'])
    def save() {
        def enquiriesInstance = new Enquiries(params)
        if (!enquiriesInstance.save(flush: true)) {
            render(view: "create", model: [enquiriesInstance: enquiriesInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'enquiries.label', default: 'Enquiries'), enquiriesInstance.id])
        redirect(action: "show", id: enquiriesInstance.id)
    }

    def show(Long id) {
        def enquiriesInstance = Enquiries.get(id)
        if (!enquiriesInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'enquiries.label', default: 'Enquiries'), id])
            redirect(action: "list")
            return
        }

        [enquiriesInstance: enquiriesInstance]
    }

    @Secured(['ROLE_ADMIN'])
    def edit(Long id) {
        def enquiriesInstance = Enquiries.get(id)
        if (!enquiriesInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'enquiries.label', default: 'Enquiries'), id])
            redirect(action: "list")
            return
        }

        [enquiriesInstance: enquiriesInstance]
    }

    @Secured(['ROLE_ADMIN'])
    def update(Long id, Long version) {
        def enquiriesInstance = Enquiries.get(id)
        if (!enquiriesInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'enquiries.label', default: 'Enquiries'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (enquiriesInstance.version > version) {
                enquiriesInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'enquiries.label', default: 'Enquiries')] as Object[],
                        "Another user has updated this Enquiries while you were editing")
                render(view: "edit", model: [enquiriesInstance: enquiriesInstance])
                return
            }
        }

        enquiriesInstance.properties = params

        if (!enquiriesInstance.save(flush: true)) {
            render(view: "edit", model: [enquiriesInstance: enquiriesInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'enquiries.label', default: 'Enquiries'), enquiriesInstance.id])
        redirect(action: "show", id: enquiriesInstance.id)
    }

    @Secured(['ROLE_ADMIN'])
    def delete(Long id) {
        def enquiriesInstance = Enquiries.get(id)
        if (!enquiriesInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'enquiries.label', default: 'Enquiries'), id])
            redirect(action: "list")
            return
        }

        try {
            enquiriesInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'enquiries.label', default: 'Enquiries'), id])
            redirect(action: "list")
            return
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'enquiries.label', default: 'Enquiries'), id])
            redirect(action: "list")
            return
        }

        redirect(action: "list")
    }

    @Async
    def sendEnquiries() {

        def nms = params.names
        def phn = params.phone
        def eml = params.email

        def typ = params.type
        def styp = params.saleType
        def cnty = params.country
        def rgn = params.region
        def are = params.area
        def crr = params.currency
        def due = params.duedate
        def rooms = params.rooms
        if (params.desc) {
            try {


                double max = Double.parseDouble(params.maxPrice)
                double min = Double.parseDouble(params.minPrice)
                def desc = params.desc
                String subject = "";
                String body = "";
                if (styp != "any" && typ != "any") {
                    boolean captchaValid = simpleCaptchaService.validateCaptcha(params.captcha)
                    if (!captchaValid) {
                        flash.errorMessage = "You made a mistake while entering the Captcha!<br>You may try again"
                    } else {
                        if (max < min) {
                            def hold = min;
                            min = max;
                            max = hold;
                        }

                        subject = "YOUR POST ON DARPROPERTY WEBSITE";
                        String pgr = "<div align='center'><h1 style='color:#ed1c24'><b>PROPERTY LEAD</b></h1><p> Dear Agent, <b>" + nms + "</b> looking for " + typ + " " + styp + " " + (rooms == 0 ? "" : "with " + rooms + " rooms") + ".<br> If its possible found in <b>" + (cnty == "any" ? "any country" : cnty) + ", " + (rgn == "any" ? "any region" : rgn) + " at " + are + " </b>area.<br>The price should be between <b> " + min + " </b> to <b>" + max + " (" + crr + ")</b> <br> " + (desc != null ? "Also: <i> " + desc + " </i>" : "") + "<hr>If you find such a property, please; <br> Call: " + phn + ", Or Email: " + eml + " <br> Thank you!</p> <hr> <p style = 'background-color:#909090; color:#fff;' > Please respond on this before " + due + ".<br> Copyright Darproperty &copy; 2016 </p> </div > ";

                        body = "<div align='center'><h1 style='color:#ed1c24'><b>PROPERTY ENQUIRY POST</b></h1><p>Dear visitor you have been posted that <br> <i>You are looking for " + typ + " " + styp + " at " + are + " - " + rgn + ", " + cnty + "</i><br> If you did not post this please: <br> Call: +255 713751868 <br>OR Email: info@darproperty.net</p> <p style='background-color:#909090; color:#fff;'>Copyright Darproperty &copy; 2016</p> </div>";

//                        SendEmail smail = new SendEmail();
//                        smail.setSender("info@darproperty.net");
//                        smail.setPassword("mikocheni120");
                        String ml = eml
//                        smail.setRecipient(ml);
//                        smail.setSubject(subject);
//                        smail.setBody(body);


                        Calendar c = Calendar.getInstance();
                        c.setTime(new Date()); // Now use today date.
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");//set date format
                        String output = sdf.format(c.getTime());//set display as formatted
                        sdf = new SimpleDateFormat("hh:mm:ss");
                        String timmer = sdf.format(c.getTime());

//                        if (smail.sendThroughDarProperty()) {

//                        mailService.sendMail {
//                            to "$ml"
//                            from "info@darproperty.net"
//                            subject "$subject"
//                            body "PROPERTY ENQUIRY POST \n\n Dear visitor you have been posted that \n You are looking for  $typ $styp  at  are  - $rgn , $cnty \n\n If you did not post this please:  Call: +255 713751868  \n Email: info@darproperty.net \n\nCopyright Darproperty  "
//                        }
//                        mailService.wait(2000)
                        String datestr = output;
                        String dateend = due
                        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        Date startDate = (Date) formatter.parse(datestr);
                        Date endDate = (Date) formatter.parse(dateend + " " + timmer);


                        def e = new Enquiries(name: nms, phone: phn, email: eml, max_price: max, min_price: min, sale_type: styp, type: typ, description: desc,
                                currency: crr, country: cnty, region: rgn, area: are, rooms: rooms, dateAdded: startDate, version: 0, dueDate: endDate)

                        if (e.save(flush: true)) {
                            flash.successMessage = "Your Enquiry posted"
//                                smail.setSubject("PROPERTY ENQUIRY FROM DARPROPERTY");
//                                smail.setBody(pgr);
                            def agents = Agent.list()
                            String[] lis = agents.profile.email
                            String mails = ""
                            for (int i = 0; i < lis.length; i++) {

                                if (lis[i] && lis[i] != "info@darproperty.net")  {
                                    mails += lis[i]
                                    mails += ","

                                }
                            }
//                            mails += "firstajep@gmail.com"
//
                            String [] recipientList = mails.split(",")
                            InternetAddress[] recipientAddress = new InternetAddress[recipientList.length];
                            //loop and trim each address
                            int counter = 0;
                            for (String recipient : recipientList) {
                                recipientAddress[counter] = new InternetAddress(recipient.trim());
                                counter++;

                            }
                            def room = (rooms == 0 ? "" : "with " + rooms + " rooms")
                            cnty = (cnty == "any" ? "any country" : cnty)
                            rgn = (rgn == "any" ? "any region" : rgn)
                            desc =  (desc != null ? "Also: <i> " + desc + " </i>" : "")
                            def year = new GregorianCalendar().get(Calendar.YEAR)
                           // recipientAddress
//                            for(int i = 0; i < recipientAddress.length; i++) {
                                mailService.sendMail {
                                    to recipientList.each {it+",\"aloycejpm@gmail.com\""}
                                    from "$eml"
                                    subject "PROPERTY ENQUIRY FROM DARPROPERTY"
                                    body "PROPERTY LEAD \n\n\n  Dear Agent, \n $nms looking for $typ $styp $room.\n If its possible found in $cnty , $rgn at $are  area.\nThe price should be between  $min to $max ($crr)\n $desc \n\n If you find such a property, please; \n Call: $phn, Or Email: $eml Thank you! \n\n  Please respond on this before $due.\n Copyright Darproperty $year "
                                }
                                flash.successMessage = "Your Enquiry posted & been sent to all agents"
//                            }
//                                }
                        } else {
                            flash.errorMessage = "Problem to save your enquiry "
                            e.errors.allErrors.each {
                                flash.errorMessage += " " + it;


                            }

                        }

//                        } else {
//
//                            flash.errorMessage = (" Sorry There is a problem with your email address. Please Try Again !");
//
//                        }

                    }
                } else {
                  //  flash.errorMessage = "Please select property type and sale type respectively.";
                }
            } catch (Exception ex) {
               // flash.errorMessage = "" + ex;

            }

        } else {
            flash.errorMessage = "Please Add your description.";

        }
        redirect(controller: "search", action: "user_enquiry", allEnquiry: Enquiries.list(params), numberofenquiries: Enquiries.count())


    }

    @Async
    def mailingEnquiries() {
        boolean captchaValid = simpleCaptchaService.validateCaptcha(params.captcha)
        def year = new GregorianCalendar().get(Calendar.YEAR)

        if (captchaValid) {
            def email = params.email
            def name = params.name
            def msg = params.message
            def receiver = params.receiver
            def phone = params.phone
            try {
                mailService.sendMail {
                    to "$receiver"
                    from "$params.email"
                    subject "ENQUIRY RESPONSE YOU HAVE POSTED IN DARPROPERTY WEBSITE"
                    body "Hello there,\n$name, respond on your property enquiry. \nMessage:  \n$msg \n\n Contacts with them by \n\n email:$email \nphone:$phone \nIn Regards; www.darproperty.co.tz/search/user_enquiry\n © $year Darproperty "
                }

                flash.successMessage = 'Your message has been sent succesfully'

            } catch (Exception ex) {
                flash.errorMessage = 'Sorry something went wrong please try to send your message again' + ex
            }
        } else {
            flash.errorMessage = "You made a mistake while entering the Captcha!<br>You may try again"
        }

        redirect(controller: "search", action: "user_enquiry", allEnquiry: Enquiries.list(params), numberofenquiries: Enquiries.count())

    }

    @Async
    def mailingAbuse() {
        boolean captchaValid = simpleCaptchaService.validateCaptcha(params.captcha)
        def year = new GregorianCalendar().get(Calendar.YEAR)

        if (captchaValid) {
            def email = params.email
            def name = params.name
            def saletype = params.saletype
            def type = params.type
            def msg = params.reason
            def others = params.other
            try {
                mailService.sendMail {
                    to "aloycejpm@gmail.com", "info@darproperty.net"
                    from "$params.email"
                    subject "ABUSE REPORT FROM DARPROPERTY"
                    body "Hello Admin,\n There is abuse by enquiry posted by $name [$email] \n Who posted that wants $type - $saletype" +
                            " \n Reasons:\n  $msg \n $others \n\n Please respond quickly \n  Here: http://www.darproperty.co.tz/search/user_enquiry \n© $year Darproperty  "
                }

                flash.successMessage = 'Your complaints have been sent successfully'

            } catch (Exception ex) {
                flash.errorMessage = 'Sorry something went wrong please try to send your message again' + ex
            }
        } else {
            flash.errorMessage = "You made a mistake while entering the Captcha!<br>You may try again"
        }

        redirect(controller: "search", action: "user_enquiry", allEnquiry: Enquiries.list(params), numberofenquiries: Enquiries.count())

    }

}