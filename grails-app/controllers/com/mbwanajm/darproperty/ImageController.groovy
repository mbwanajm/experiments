package com.mbwanajm.darproperty

class ImageController {
	
	def photoView = {
		
		Photo photo = Photo.findById(params.id)
		if (photo?.image) {
			response.setContentLength(photo.image.length)
			response.outputStream.write(photo.image)
		} else {
			response.sendError(404)
		}
	}

    def renderAdvert = {
        try{
            Advert advert = Advert.findById(params.id)
            if (advert?.photo) {
                response.setContentLength(advert.photo.length)
                response.outputStream.write(advert.photo)
                //impression
                advert.views = advert.views ? advert.views += 1 : 1
                advert.save()
            } else {
                response.sendError(404)
            }
        }catch (Exception e){

        }

    }

    def renderArticlePhoto = {
        Article article = Article.findById(params.id)
        if (article?.image) {
            response.setContentLength(article.image.length)
            response.outputStream.write(article.image)

        } else {
            response.sendError(404)
        }
    }
	
	def renderAdvertRepo = {
		Advert advert = Advert.findById(params.id)
		if (advert?.photo) {
			response.setContentLength(advert.photo.length)
			response.outputStream.write(advert.photo)
			
		} else {
			response.sendError(404)
		}
	}

    def renderAgentPhoto = {
        Agent agent = Agent.findById(params.id)
        if (agent?.profile?.photo) {
            response.setContentLength(agent.profile.photo.length)
            response.outputStream.write(agent.profile.photo)
        } else {
            response.sendError(404)
        }
    }

    def renderPropertyPhoto = {
        Photo photo = Photo.findById(params.id)
        if (photo?.image) {
            response.setContentLength(photo.image.length)
            response.outputStream.write(photo.image)
        } else {
            response.sendError(404)
        }
    }

    def renderMonthlyIssuePhoto = {
        MonthlyIssue monthlyIssue = MonthlyIssue.findById(params.id)
        if (monthlyIssue?.image) {
            response.setContentLength(monthlyIssue.image.length)
            response.outputStream.write(monthlyIssue.image)
        } else {
            response.sendError(404)
        }
    }
}
