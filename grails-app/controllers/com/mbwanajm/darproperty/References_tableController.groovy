package com.mbwanajm.darproperty

import org.springframework.dao.DataIntegrityViolationException

class References_tableController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [references_tableInstanceList: References_table.list(params), references_tableInstanceTotal: References_table.count()]
    }

    def create() {
        [references_tableInstance: new References_table(params)]
    }

    def save() {
        def references_tableInstance = new References_table(params)
        if (!references_tableInstance.save(flush: true)) {
            render(view: "create", model: [references_tableInstance: references_tableInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'references_table.label', default: 'References_table'), references_tableInstance.id])
        redirect(action: "show", id: references_tableInstance.id)
    }

    def show(Long id) {
        def references_tableInstance = References_table.get(id)
        if (!references_tableInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'references_table.label', default: 'References_table'), id])
            redirect(action: "list")
            return
        }

        [references_tableInstance: references_tableInstance]
    }

    def edit(Long id) {
        def references_tableInstance = References_table.get(id)
        if (!references_tableInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'references_table.label', default: 'References_table'), id])
            redirect(action: "list")
            return
        }

        [references_tableInstance: references_tableInstance]
    }

    def update(Long id, Long version) {
        def references_tableInstance = References_table.get(id)
        if (!references_tableInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'references_table.label', default: 'References_table'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (references_tableInstance.version > version) {
                references_tableInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'references_table.label', default: 'References_table')] as Object[],
                          "Another user has updated this References_table while you were editing")
                render(view: "edit", model: [references_tableInstance: references_tableInstance])
                return
            }
        }

        references_tableInstance.properties = params

        if (!references_tableInstance.save(flush: true)) {
            render(view: "edit", model: [references_tableInstance: references_tableInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'references_table.label', default: 'References_table'), references_tableInstance.id])
        redirect(action: "show", id: references_tableInstance.id)
    }

    def delete(Long id) {
        def references_tableInstance = References_table.get(id)
        if (!references_tableInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'references_table.label', default: 'References_table'), id])
            redirect(action: "list")
            return
        }

        try {
            references_tableInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'references_table.label', default: 'References_table'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'references_table.label', default: 'References_table'), id])
            redirect(action: "show", id: id)
        }
    }
}
