package com.mbwanajm.darproperty

import org.springframework.dao.DataIntegrityViolationException

class Advert_pricesController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [advert_pricesInstanceList: Advert_prices.list(params), advert_pricesInstanceTotal: Advert_prices.count()]
    }

    def create() {
        [advert_pricesInstance: new Advert_prices(params)]
    }

    def save() {
        def advert_pricesInstance = new Advert_prices(params)
        if (!advert_pricesInstance.save(flush: true)) {
            render(view: "create", model: [advert_pricesInstance: advert_pricesInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'advert_prices.label', default: 'Advert_prices'), advert_pricesInstance.id])
        redirect(action: "show", id: advert_pricesInstance.id)
    }

    def show(Long id) {
        def advert_pricesInstance = Advert_prices.get(id)
        if (!advert_pricesInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'advert_prices.label', default: 'Advert_prices'), id])
            redirect(action: "list")
            return
        }

        [advert_pricesInstance: advert_pricesInstance]
    }

    def edit(Long id) {
        def advert_pricesInstance = Advert_prices.get(id)
        if (!advert_pricesInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'advert_prices.label', default: 'Advert_prices'), id])
            redirect(action: "list")
            return
        }

        [advert_pricesInstance: advert_pricesInstance]
    }

    def update(Long id, Long version) {
        def advert_pricesInstance = Advert_prices.get(id)
        if (!advert_pricesInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'advert_prices.label', default: 'Advert_prices'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (advert_pricesInstance.version > version) {
                advert_pricesInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'advert_prices.label', default: 'Advert_prices')] as Object[],
                          "Another user has updated this Advert_prices while you were editing")
                render(view: "edit", model: [advert_pricesInstance: advert_pricesInstance])
                return
            }
        }

        advert_pricesInstance.properties = params

        if (!advert_pricesInstance.save(flush: true)) {
            render(view: "edit", model: [advert_pricesInstance: advert_pricesInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'advert_prices.label', default: 'Advert_prices'), advert_pricesInstance.id])
        redirect(action: "show", id: advert_pricesInstance.id)
    }

    def delete(Long id) {
        def advert_pricesInstance = Advert_prices.get(id)
        if (!advert_pricesInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'advert_prices.label', default: 'Advert_prices'), id])
            redirect(action: "list")
            return
        }

        try {
            advert_pricesInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'advert_prices.label', default: 'Advert_prices'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'advert_prices.label', default: 'Advert_prices'), id])
            redirect(action: "show", id: id)
        }
    }
}
