package com.mbwanajm.darproperty

class HomeController {
    AdvertsService advertsService
    PropertyService propertyService
    ArticleService articleService
    EnquiriesService enquiriesService

    def index() {
    redirect(action: "home")
	
    }

    def home() {
		def featuredProperty = propertyService.featuredProperty;
        def featuredPropertySmall = propertyService.featuredPropertyForSmall;
        def mainAdverts = advertsService.mainSlideShowAdverts;
        def footerAdverts = advertsService.footerSlideShowAdverts;
        def footerAdverts2 = advertsService.footerSlideShow2Adverts;
		def side1 = advertsService.side1Advert;
        def topProperty = propertyService.topProperty;
        def latestProperty = propertyService.latestProperty;
        def latestPropertySmall = propertyService.latestPropertySmall;
        def latestArticles = articleService.latestArticles;
        def latestArticlesSmall = articleService.latestArticlesSmall;
        def latestEnquiries = enquiriesService.latestEnquiries()
        params.max = 5

        def magazine = MonthlyIssue.listOrderById(order: 'desc', max: 1)[0]

        [
                mainAdverts: mainAdverts,
                footerAdverts: footerAdverts,
				side1: side1,
				featuredProperty: featuredProperty,
                footerAdverts2:footerAdverts2,
                featuredPropertySmall: featuredPropertySmall,
                topProperty: topProperty,
                latestProperty: latestProperty,
                latestPropertySmall: latestPropertySmall,
                latestArticles: latestArticles,
                latestArticlesSmall: latestArticlesSmall,
                magazine: magazine,
                latestEnquiries: latestEnquiries,
                latestEnquiriesCount: latestEnquiries.count ,
                latestEnquiriesMobi:enquiriesService.latestEnquiries()

        ]
    }

    def whyAdvertise() {
        [topProperty: propertyService.topProperty]
    }

    def advertisingPolicy() {
        [topProperty: propertyService.topProperty]
    }

    def infoHub() {
        def numberOfArticles = Article.count;
		[latestArticles: articleService.latestArticles, numberOfArticles:numberOfArticles]
    }

    def reportAndStatistics(){

    }
}