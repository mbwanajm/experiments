package com.mbwanajm.darproperty

import org.springframework.scheduling.annotation.Async

import java.awt.geom.Arc2D.Double
import java.text.SimpleDateFormat;

class SearchController {
    CurrencyService currencyService
    ArticleService articleService
    EnquiriesService enquiriesService


    def index() {
    }

    /**
     * Search for a particular item
     */
    def generalSearch() {
        params['max'] = 5
        params['useAndDefaultOperator'] = false
        // in case of empty or number search go back to same page
        // TODO fix error when a number is searched
        if (!params.searchTerm || params.searchTerm.isInteger()) redirect(uri: '/')

        //allow suggestions
        params.suggestQuery = true

        try {
            def searchResult = Property.search(params.searchTerm, params)

            // for some reason searchable doesn't load properties that are not indexed,
            // hence they dont show up in the view, this loads the results manually fixing the problem
            def gormLoadedResults = searchResult.results.collect { Property.get(it.id) }
            searchResult.results = gormLoadedResults

            render(view: 'propertyResults', model: [searchResult: searchResult])

        } catch (e) {
            render(view: 'propertyResults', model: [searchError: true])
        }

    }

    @Async
    def newSearch() {
        params['max'] = 10
        def sortfrom = 'newSearch'
        def tab1 = 'active'
        def tab2 = ''
        def tab3 = ''
        def tab4 = ''
        def cheap = ''
        def expensive = ''
        def old = ''
        def latest = ''
        def view = ''





        def searchResult = Property.createCriteria().list(params) {
            eq('deleted', false)

            if (params.type && params.type != 'any') {
                type {
                    eq('name', params.type)
                }
            }


            if (params.saleType && params.saleType != 'any') {
                saleType {
                    eq('name', params.saleType)
                }
            }

            if (params.country && params.country != 'any') {
                country {
                    eq('name', params.country)
                }
            }

            if (params.region && params.region != 'any') {
                eq('region', params.region)
            }


            if (params.area && params.area != 'any') {
                eq('area', params.area)
            }

            if (params.agent && params.agent != 'any') {
                eq(
                        'agent',
                        Agent.findByProfile(
                                Profile.findByCompany(params.agent)
                        )
                )
            }

            if (params.currency && (params.minPrice || params.maxPrice) && params.maxPrice != params.minPrice && params.currency != 'Currency') {
                def selectedCurrency = params.currency

                def currencies = ['TZS': [:], 'USD': [:], 'KES': [:], 'RWF': [:]]

                double minPrice = java.lang.Double.parseDouble(params.minPrice)
                double maxPrice = java.lang.Double.parseDouble(params.maxPrice)

                if (minPrice > maxPrice) {
                    def temporary = minPrice;
                    minPrice = maxPrice
                    maxPrice = temporary
                }

                currencies[params.currency].min = minPrice ?: 0;
                currencies[params.currency].max = maxPrice ?: 9999999999999999;

                currencies.each { currency, Map range ->
                    if (currency == selectedCurrency) return
                    range.min = currencyService.convert(minPrice, selectedCurrency, currency)
                    range.max = currencyService.convert(maxPrice, selectedCurrency, currency)
                }


                or {
                    currencies.each { currency, range ->

                        and {
                            eq('currency', currency)
                            between('price', range.min, range.max)
                        }
                    }
                }


            }

            switch (params.tab) {
                case 'forsale':
                    tab4 = ''
                    tab1 = 'active'
                    tab2 = ''
                    tab3 = ''
                    break;
                case 'forrent':
                    tab4 = ''
                    tab2 = 'active'
                    tab1 = ''
                    tab3 = ''
                    break;
                case 'advanced':
                    tab4 = ''
                    tab3 = 'active'
                    tab2 = ''
                    tab1 = ''
                    break;
                case 'name':
                    tab4 = 'active'
                    tab3 = ''
                    tab2 = ''
                    tab1 = ''
                    break;

            }


            switch (params.sortBy) {
                case 'latest':
                    order('dateAdded', 'desc')
                    latest = 'btn btn-danger hd2 disabled sort'
                    break;

                case 'oldest':
                    order('dateAdded', 'asc')
                    old = 'btn btn-danger hd2 disabled sort'
                    break;

                case 'cheapest':
                    order('price', 'asc')
                    cheap = 'btn btn-danger hd2 disabled sort'
                    break;

                case 'expensive':
                    order('price', 'desc')
                    expensive = 'btn btn-danger hd2 disabled sort'
                    break;

                case 'views':
                    order('views', 'desc')
                    view = 'btn btn-danger hd2 disabled sort'
                    break;
            }

        }

        render(view: 'homeResults', model: [searchResult: searchResult, searchTerms: params, tab1: tab1, tab2: tab2, tab3: tab3, tab4: tab4, latest: latest, old: old, cheap: cheap, expensive: expensive, view: view, sortfrom: sortfrom])

    }

    @Async
    def advancedSearch() {
        params['max'] = 10
        def sortfrom = 'newSearch'
        def tab1 = 'active'
        def tab2 = ''
        def tab3 = ''
        def tab4 = ''
        def cheap = ''
        def expensive = ''
        def old = ''
        def latest = ''
        def view = ''





        def searchResult = Property.createCriteria().list(params) {
            eq('deleted', false)

            if (params.type && params.type != 'any') {
                type {
                    eq('name', params.type)
                }
            }


            if (params.saleType && params.saleType != 'any') {
                saleType {
                    eq('name', params.saleType)
                }
            }

            if (params.country && params.country != 'any') {
                country {
                    eq('name', params.country)
                }
            }

            if (params.region && params.region != 'any') {
                eq('region', params.region)
            }


            if (params.area && params.area != 'any') {
                eq('area', params.area)
            }

            if (params.agent && params.agent != 'any') {
                eq(
                        'agent',
                        Agent.findByProfile(
                                Profile.findByCompany(params.agent)
                        )
                )
            }

            if (params.currency && (params.minPrice || params.maxPrice) && params.maxPrice != params.minPrice && params.currency != 'Currency') {
                def selectedCurrency = params.currency

                def currencies = ['TZS': [:], 'USD': [:], 'KES': [:], 'RWF': [:]]

                double minPrice = java.lang.Double.parseDouble(params.minPrice)
                double maxPrice = java.lang.Double.parseDouble(params.maxPrice)

                if (minPrice > maxPrice) {
                    def temporary = minPrice;
                    minPrice = maxPrice
                    maxPrice = temporary
                }

                currencies[params.currency].min = minPrice ?: 0;
                currencies[params.currency].max = maxPrice ?: 9999999999999999;

                currencies.each { currency, Map range ->
                    if (currency == selectedCurrency) return
                    range.min = currencyService.convert(minPrice, selectedCurrency, currency)
                    range.max = currencyService.convert(maxPrice, selectedCurrency, currency)
                }


                or {
                    currencies.each { currency, range ->

                        and {
                            eq('currency', currency)
                            between('price', range.min, range.max)
                        }
                    }
                }


            }

            switch (params.tab) {
                case 'forsale':
                    tab4 = ''
                    tab1 = 'active'
                    tab2 = ''
                    tab3 = ''
                    break;
                case 'forrent':
                    tab4 = ''
                    tab2 = 'active'
                    tab1 = ''
                    tab3 = ''
                    break;
                case 'advanced':
                    tab4 = ''
                    tab3 = 'active'
                    tab2 = ''
                    tab1 = ''
                    break;
                case 'name':
                    tab4 = 'active'
                    tab3 = ''
                    tab2 = ''
                    tab1 = ''
                    break;

            }


            switch (params.sortBy) {
                case 'latest':
                    order('dateAdded', 'desc')
                    latest = 'btn btn-danger hd2 disabled sort'
                    break;

                case 'oldest':
                    order('dateAdded', 'asc')
                    old = 'btn btn-danger hd2 disabled sort'
                    break;

                case 'cheapest':
                    order('price', 'asc')
                    cheap = 'btn btn-danger hd2 disabled sort'
                    break;

                case 'expensive':
                    order('price', 'desc')
                    expensive = 'btn btn-danger hd2 disabled sort'
                    break;

                case 'views':
                    order('views', 'desc')
                    view = 'btn btn-danger hd2 disabled sort'
                    break;
            }

        }

        render(view: 'homeResults', model: [searchResult: searchResult, searchTerms: params, tab1: tab1, tab2: tab2, tab3: tab3, tab4: tab4, latest: latest, old: old, cheap: cheap, expensive: expensive, view: view, sortfrom: sortfrom])


    }

    def handlePrice(String priceString) {
        if (!priceString.startsWith('> ')) {
            def prices = priceString.split(' - ')
            def from = Double.parseDouble(prices[0].trim())
            def to = Double.parseDouble(prices[1].trim());

            def tzsFrom = Math.ceil(currencyService.convertToTZS(from, 'USD') / 10000) * 10000
            def tzsTo = Math.ceil(currencyService.convertToTZS(to, 'USD') / 10000) * 10000

            return [from: from, to: to, tzsFrom: tzsFrom, tzsTo: tzsTo]

        } else {
            def price = Double.parseDouble(
                    priceString
                            .replace('> ', '')
                            .trim()
            )

            def tzsPrice = Math.ceil(currencyService.convertToTZS(price, 'USD') / 10000) * 10000
            return [price: price, tzsPrice: tzsPrice]
        }


    }

    def homeResults() {


    }

    @Async
    def searchbyName() {
        params['max'] = 10
        def sortfrom = 'searchbyName'
        def tab1 = ''
        def tab2 = ''
        def tab3 = ''
        def tab4 = ''
        def cheap = ''
        def expensive = ''
        def old = ''
        def latest = ''
        def view = ''
        switch (params.tab) {
            case 'forsale':
                tab4 = ''
                tab1 = 'active'
                tab2 = ''
                tab3 = ''
                break;
            case 'forrent':
                tab4 = ''
                tab2 = 'active'
                tab1 = ''
                tab3 = ''
                break;
            case 'advanced':
                tab4 = ''
                tab3 = 'active'
                tab2 = ''
                tab1 = ''
                break;
            case 'name':
                tab4 = 'active'
                tab3 = ''
                tab2 = ''
                tab1 = ''
                break;

        }
        def searchResult = Property.createCriteria().list(params) {
            eq('deleted', false)
            if (params.name) {
                def name = params.name
                like('name', name + '%')

                switch (params.sortBy) {
                    case 'latest':
                        order('dateAdded', 'desc')
                        latest = 'btn btn-danger hd2 disabled sort'
                        break;

                    case 'oldest':
                        order('dateAdded', 'asc')
                        old = 'btn btn-danger hd2 disabled sort'
                        break;

                    case 'cheapest':
                        order('price', 'asc')
                        cheap = 'btn btn-danger hd2 disabled sort'
                        break;

                    case 'expensive':
                        order('price', 'desc')
                        expensive = 'btn btn-danger hd2 disabled sort'
                        break;

                    case 'views':
                        order('views', 'desc')
                        view = 'btn btn-danger hd2 disabled sort'
                        break;
                }
            }
        }
        render(view: 'homeResults', model: [searchResult: searchResult, searchTerms: params, tab1: tab1, tab2: tab2, tab3: tab3, tab4: tab4,latest: latest, old: old, cheap: cheap, expensive: expensive, view: view, sortfrom: sortfrom])
    }

    def user_enquiry() {

        Date date = new Date()
        def sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
        def datetime = sdf.format(date)

       // params.max = 3
        def enquiry = Enquiries.findAllByDueDateGreaterThanEquals(new Date().parse("yyyy-MM-dd hh:mm:ss", datetime), [sort: 'id', order: 'desc'])
        [allEnquiry: enquiry, numberofenquiries: Enquiries.count()]

    }

    @Async
    def searchArticle() {
        def latestArticles = articleService.latestArticles;
        def numberOfArticles = Article.count;
        params.max = 4
        params.order = 'desc'
        def articlesToShow = Article.listOrderByDateCreated(params)

        params['max'] = 10

        def searchResult = Article.createCriteria().list(params) {
            if (params.bytitle) {
                like('title', '%' + params.title + '%')

            }
            if (params.date) {
                def day = params.day
                def month = params.month
                def year = params.year

                def dateval = Date.parse("yyyy-MM-dd", year + '-' + month + '-' + day)
                ge('dateCreated', dateval)

            }


        }

        render(view: '../article/articleLibrary', model: [searchResult: searchResult, searchTerms: params, articlesToShow: articlesToShow, numberOfArticles: numberOfArticles])


    }

}
