package com.mbwanajm.darproperty

import grails.plugins.springsecurity.Secured
import org.springframework.dao.DataIntegrityViolationException

class InterestratesController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }
    @Secured(['ROLE_ADMIN'])
    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [interestratesInstanceList: Interestrates.list(params), interestratesInstanceTotal: Interestrates.count()]
    }
    @Secured(['ROLE_ADMIN'])
    def create() {
        [interestratesInstance: new Interestrates(params)]
    }
    @Secured(['ROLE_ADMIN'])
    def save() {
        def bank = Banks.findById(Long.parseLong(params.bank))
        def interestratesInstance = new Interestrates(country: params.country, bank: bank,rate: params.rate,bank )
        if (!interestratesInstance.save(flush: true)) {
            render(view: "create", model: [interestratesInstance: interestratesInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'interestrates.label', default: 'Interestrates'), interestratesInstance.id])
        redirect(action: "show", id: interestratesInstance.id)
    }
    @Secured(['ROLE_ADMIN'])
    def show(Long id) {
        def interestratesInstance = Interestrates.get(id)
        if (!interestratesInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'interestrates.label', default: 'Interestrates'), id])
            redirect(action: "list")
            return
        }

        [interestratesInstance: interestratesInstance]
    }
    @Secured(['ROLE_ADMIN'])
    def edit(Long id) {
        def interestratesInstance = Interestrates.get(id)
        if (!interestratesInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'interestrates.label', default: 'Interestrates'), id])
            redirect(action: "list")
            return
        }

        [interestratesInstance: interestratesInstance]
    }
    @Secured(['ROLE_ADMIN'])
    def update(Long id, Long version) {
        def interestratesInstance = Interestrates.get(id)
        if (!interestratesInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'interestrates.label', default: 'Interestrates'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (interestratesInstance.version > version) {
                interestratesInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'interestrates.label', default: 'Interestrates')] as Object[],
                          "Another user has updated this Interestrates while you were editing")
                render(view: "edit", model: [interestratesInstance: interestratesInstance])
                return
            }
        }

        interestratesInstance.properties = params

        if (!interestratesInstance.save(flush: true)) {
            render(view: "edit", model: [interestratesInstance: interestratesInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'interestrates.label', default: 'Interestrates'), interestratesInstance.id])
        redirect(action: "show", id: interestratesInstance.id)
    }
    @Secured(['ROLE_ADMIN'])
    def delete(Long id) {
        def interestratesInstance = Interestrates.get(id)
        if (!interestratesInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'interestrates.label', default: 'Interestrates'), id])
            redirect(action: "list")
            return
        }

        try {
            interestratesInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'interestrates.label', default: 'Interestrates'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'interestrates.label', default: 'Interestrates'), id])
            redirect(action: "show", id: id)
        }
    }
}
