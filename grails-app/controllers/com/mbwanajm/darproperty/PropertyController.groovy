package com.mbwanajm.darproperty

import grails.plugin.mail.MailService
import grails.plugins.springsecurity.Secured
import org.springframework.scheduling.annotation.Async

class PropertyController {
    def scaffold = true
    def springSecurityService
    def imageService
    MailService mailService
    def simpleCaptchaService
    @Secured(['ROLE_ADMIN'])
    def edit(){}
    @Secured(['ROLE_ADMIN'])
    def list(){}

    def addProperty() {
        Agent agent = springSecurityService.currentUser
        Property property = new Property();
        [agent: agent, property: property, springSecurityService:springSecurityService]
    }

    def editProperty() {
        Agent agent = springSecurityService.currentUser
        Property property = Property.findById(params.propertyId)
        render(view: 'addProperty', model: [agent: agent, property: property])
    }
    @Async
    def createProperty() {
        Agent agent = springSecurityService.currentUser

        def pType = params.type
        def pSaleType = params.saleType
        def pCountry = params.country
        params.remove("type"); params.remove("saleType"); params.remove("country")

        // encode the owners information into the description
        if (agent.username == 'fsbo') {
            params.description = "${params.description}{{${params.fsboOwnersName},${params.fsboPhone},${params.fsboEmail}}}"
        }

        Property property = new Property(params)
        property.type = Type.findByName(pType)
        property.saleType = SaleType.findByName(pSaleType)
        property.country = Country.findByName(pCountry)
        property.dateAdded = new Date();

        if (property.validate()) {
            Photo photo = new Photo();
            photo.name = "main"
            photo.image = params.photo.getBytes()

            if (photo.validate()) {
                photo.image = imageService.resize(photo.image, 368, 276)

                property.addToPhotos(photo)
                agent.addToProperty(property)
                addPhotosAndTags(params, property)

                property.save();
                flash.property = property;
                flash.message = "Property $property.name saved succesfully"
                redirect(controller: "property", action: "editProperty", params: [propertyId: property.id])
                return
            }

            flash.photo = photo
        }

        flash.message = "property has error"
        flash.property = property

        redirect(controller: "property", action: "addProperty")
    }

    def updateProperty() {
        Agent agent = springSecurityService.currentUser

        def pType = params.type
        def pSaleType = params.saleType
        def pCountry = params.country
        params.remove("type"); params.remove("saleType"); params.remove("country")

        // encode the owners information into the description
        if (agent.username == 'fsbo') {
            params.description = "${params.description}{{${params.fsboOwnersName},${params.fsboPhone},${params.fsboEmail}}}"
        }

        Property property = Property.findById(params.id)
        property.type = Type.findByName(pType)
        property.saleType = SaleType.findByName(pSaleType)
        property.country = Country.findByName(pCountry)
        property.dateAdded = new Date();
        property.properties = params


        if (property.validate()) {
            agent.addToProperty(property)

            // add all the other photos and tags
            property.tags.clear()
            addPhotosAndTags(params, property)

            def id = property.save();
            flash.message = "Property $property.name saved succesfully"

        } else {
            flash.message = "property has errors"
        }

        flash.property = property;
        redirect(controller: "property", action: "editProperty", params: [propertyId: property.id])
    }


    def addPhotosAndTags(params, property) {
        def tagNames = Tag.findAll().collect { it.name }
        params.each { String name, value ->
            if (name.startsWith('photo') && name.size() > 5) {
                Photo otherPhoto = new Photo();
                otherPhoto.name = name

                if (value.getBytes()) {
                    otherPhoto.image = imageService.resize(value.getBytes(), 640, 480)
                    property.addToPhotos(otherPhoto)
                }

            } else {
                if (tagNames.contains(name)) {
                    if (value) property.addToTags(Tag.findByName(name))
                }
            }
        }
    }

    def viewProperty() {
        Property property = Property.findById(params.id)
        property.views = property.views ? property.views += 1 : 1
        property.save()
        [property: property]
    }

    def delete() {
        Agent agent = springSecurityService.currentUser
        Property property = Property.findById(params.propertyId)
        property.deleted = true
        property.deletedOn = new Date()
        property.save()
        // agent.removeFromProperty(property)
        // property.delete()
        redirect(controller: "agent", action: "viewProperty", id: agent.id)
    }

    def approve() {
        Agent agent = springSecurityService.currentUser
        Property property = Property.findById(params.propertyId)
        property.approved = true
        property.save()
        // agent.removeFromProperty(property)
        // property.delete()
        redirect(controller: "agent", action: "viewProperty", id: agent.id)
    }

    def disapprove() {
        Agent agent = springSecurityService.currentUser
        Property property = Property.findById(params.propertyId)
        property.approved = false
        property.save()
        // agent.removeFromProperty(property)
        // property.delete()
        redirect(controller: "agent", action: "viewProperty", id: agent.id)
    }
    def deletePhoto() {
        Photo photo = Photo.findById(params.photoId)
        Property property = Property.findById(params.propertyId)

        property.removeFromPhotos(photo)
        photo.delete()

        // in case we have deleted a main photo set any other photo as main
        if (photo.name == 'main') {
            Iterator photoIterator = property.photos.iterator()
            if (photoIterator.hasNext()) {
                Photo otherPhoto = photoIterator.next()
                otherPhoto.name = 'main'
                otherPhoto.save()
            }
        }

        flash.message = "deleted photo $photo.name succesfully"
        redirect(controller: "property", action: "editProperty", params: [propertyId: property.id])

    }

    def setAsMainPhoto() {
        Property property = Property.findById(params.propertyId)
        Photo photo = Photo.findById(params.photoId)

        property.photos.each { Photo otherPhoto ->
            if (otherPhoto.name == 'main') {
                otherPhoto.name = photo.name
            }
        }

        photo.name = 'main'
        photo.save()

        flash.message = "set as main photo succesfully"
        redirect(controller: "property", action: "editProperty", params: [propertyId: property.id])

    }


    def search() {
        def matchedProperties = [];
        def searchTerm = params.searchTerm

        render(view: "listProperties", model: [propertys: matchedProperties])
    }

    def showAllProperties() {
        params['max'] = 12
        params['sort'] = 'dateAdded'
        params['order'] = 'desc'
        def tab1= 'active'
        def tab2=''
        def tab3=''

       // def propertys = Property.listOrderByDateAdded(params)
        def propertys = Property.findAllByDeleted(false,params)
        def propertyCount = Property.count()
        render(
                view: "listProperties",
                model: [propertys: propertys, propertyCount: propertyCount,tab1: tab1, tab2: tab2, tab3: tab3]
        )
    }
    @Async
    def addMessage() {
        Property property = Property.findById(Long.parseLong(params.id))

        boolean captchaValid = simpleCaptchaService.validateCaptcha(params.captcha)
        if (!captchaValid) {
            flash.errorMessage = "You made a mistake while entering the Captcha!<br>You may try again"

        } else {
            Message message = new Message(
                    email: params.email,
                    phone: params.phone,
                    message: params.message
            )

            if (message.validate()) {
                property.addToMessages(message)

                try {
                    def receiver
                    def name

                    if (property.isFsbo()) {
                        receiver = property.fsboDetails.email
                        name = property.fsboDetails.name

                    } else {
                        receiver = property.agent.profile.email
                        name = property.agent.profile.firstName
                    }

                    def emailBody = """
Hello $name,

You have received a message for your property ${property.name} posted in DarProperty:

Message:
$params.message


sender's phone is $params.phone
sender's email is $params.email


regards,
DarProperty team.

"""
                    mailService.sendMail {
                        to receiver
                        from "info@darproperty.net"
                        subject "You have received a message for your property posted on the DarProperty MLS"
                        body emailBody
                    }

                    flash.successMessage = "Your message has been succesfully sent!"
                } catch (Exception ex) {
                    flash.errorMessage = 'Sorry something went wrong please try to send your message again'
                    ex.printStackTrace()
                }
            } else {
                flash.errorBean = message
            }
        }

       // render(view: 'viewProperty', model: [property: property])

    }



    def export() {
        response.outputStream.withWriter { Writer writer -> "Service Suspended: Please Contact DarProperty Tanzania" }
        /*
        DecimalFormat df = new DecimalFormat("#0.##")
        def types = [house: 'House', apartment: 'Apartment', plot: 'Plot', warehouse: 'Warehouse', 'office space': 'Office Space', camp: 'Camp', hotel: 'Hotel']
        response.outputStream.withWriter { Writer writer ->
            MarkupBuilder xml = new MarkupBuilder(writer)

            def propertys = Property.findAll(params)
            xml.PropertyContent {
                propertys.each { Property prop ->
                    Property {
                        PropertyId(prop.id)
                        Action('ADD')
                        PropertyType(types[prop.type.name])
                        PropertyFor(prop.saleType.name.replace('for ', ''))
                        PropertyTitle(prop.name)
                        PriceUnit('Per Unit')
                        Price(df.format(prop.price))
                        Currency(prop.currency)
                        Country('Tanzania')
                        State()
                        City(prop.region)
                        Locality(prop.area)
                        Address(prop.location)
                        NoOfBedrooms(prop.numberOfRooms)
                        NoOfBathrooms(prop.numberOfBathRooms)
                        Src_Area(df.format(prop.size))
                        AreaUnit('Sq Mtr')
                        PlotSizeBy('Sq Mtr')
                        Amenities()
                        PropertyUrl("http:www.darproperty.co.tz/property/viewProperty/$prop.id")
                        Description(prop.description)
                        PostedBy("DarProperty")
                        if (!prop.isFsbo()) {
                            Emails(prop.agent.profile.email)
                            PhoneNumbers(prop.agent.profile.phone)
                        } else {
                            Emails(prop.fsboDetails.email)
                            PhoneNumbers(prop.fsboDetails.phone)
                        }
                        PostDate(prop.dateAdded ? prop.dateAdded.format('dd/MM/yyyy') : '01/03/2013')
                        EndDate(prop.dateAdded ? (prop.dateAdded + 400).format('dd/MM/yyyy') : '01/06/2014')
                    }
                }

            }
        }
        */
    }

}