package com.mbwanajm.darproperty

import org.springframework.dao.DataIntegrityViolationException

class MonthlyIssueController {
    def scaffold = true;

    def showAllMagazines() {
        def magazines = MonthlyIssue.listOrderById(order: 'desc');
        [magazines: magazines]
    }

    def viewMagazine() {
        def magazine = MonthlyIssue.get(params.id)
        [magazine: magazine]
    }

    def viewLatestMagazine() {
        def magazine = MonthlyIssue.listOrderById(order: 'desc', max:1)[0];
        render(view: "viewMagazine", model: [magazine: magazine])
    }

}
