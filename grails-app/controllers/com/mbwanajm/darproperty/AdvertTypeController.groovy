package com.mbwanajm.darproperty

import org.springframework.dao.DataIntegrityViolationException

class AdvertTypeController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [advertTypeInstanceList: AdvertType.list(params), advertTypeInstanceTotal: AdvertType.count()]
    }

    def create() {
        [advertTypeInstance: new AdvertType(params)]
    }

    def save() {
        def advertTypeInstance = new AdvertType(params)
        if (!advertTypeInstance.save(flush: true)) {
            render(view: "create", model: [advertTypeInstance: advertTypeInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'advertType.label', default: 'AdvertType'), advertTypeInstance.id])
        redirect(action: "show", id: advertTypeInstance.id)
    }

    def show(Long id) {
        def advertTypeInstance = AdvertType.get(id)
        if (!advertTypeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'advertType.label', default: 'AdvertType'), id])
            redirect(action: "list")
            return
        }

        [advertTypeInstance: advertTypeInstance]
    }

    def edit(Long id) {
        def advertTypeInstance = AdvertType.get(id)
        if (!advertTypeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'advertType.label', default: 'AdvertType'), id])
            redirect(action: "list")
            return
        }

        [advertTypeInstance: advertTypeInstance]
    }

    def update(Long id, Long version) {
        def advertTypeInstance = AdvertType.get(id)
        if (!advertTypeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'advertType.label', default: 'AdvertType'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (advertTypeInstance.version > version) {
                advertTypeInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'advertType.label', default: 'AdvertType')] as Object[],
                          "Another user has updated this AdvertType while you were editing")
                render(view: "edit", model: [advertTypeInstance: advertTypeInstance])
                return
            }
        }

        advertTypeInstance.properties = params

        if (!advertTypeInstance.save(flush: true)) {
            render(view: "edit", model: [advertTypeInstance: advertTypeInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'advertType.label', default: 'AdvertType'), advertTypeInstance.id])
        redirect(action: "show", id: advertTypeInstance.id)
    }

    def delete(Long id) {
        def advertTypeInstance = AdvertType.get(id)
        if (!advertTypeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'advertType.label', default: 'AdvertType'), id])
            redirect(action: "list")
            return
        }

        try {
            advertTypeInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'advertType.label', default: 'AdvertType'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'advertType.label', default: 'AdvertType'), id])
            redirect(action: "show", id: id)
        }
    }
}
