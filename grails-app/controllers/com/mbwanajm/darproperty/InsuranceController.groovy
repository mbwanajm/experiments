package com.mbwanajm.darproperty

class InsuranceController {

    def index() {
        redirect(action: "home")
    }

    def home(){

    }
    def sendEmailForInsurance() {


        try {
            mailService.sendMail {
                to "info@darproperty.net"
                from "$params.email"
                subject "Message On Insurance Contact Page"
                body "$params.name\n\n$params.message\n\nmy email:$params.email\nmy phone:$params.phone"
            }

            flash.successMessage = 'Your message has been sent succesfully'
            render(view: 'home')

        } catch (Exception ex) {
            flash.errorMessage = 'Sorry something went wrong please try to send your message again'
            render(view: 'home')
        }
    }

}
