package com.mbwanajm.darproperty

import org.springframework.dao.DataIntegrityViolationException

class ReceiptsController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [receiptsInstanceList: Receipts.list(params), receiptsInstanceTotal: Receipts.count()]
    }

    def create() {
        [receiptsInstance: new Receipts(params)]
    }

    def save() {
        def receiptsInstance = new Receipts(params)
        if (!receiptsInstance.save(flush: true)) {
            render(view: "create", model: [receiptsInstance: receiptsInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'receipts.label', default: 'Receipts'), receiptsInstance.id])
        redirect(action: "show", id: receiptsInstance.id)
    }

    def show(Long id) {
        def receiptsInstance = Receipts.get(id)
        if (!receiptsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'receipts.label', default: 'Receipts'), id])
            redirect(action: "list")
            return
        }

        [receiptsInstance: receiptsInstance]
    }

    def edit(Long id) {
        def receiptsInstance = Receipts.get(id)
        if (!receiptsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'receipts.label', default: 'Receipts'), id])
            redirect(action: "list")
            return
        }

        [receiptsInstance: receiptsInstance]
    }

    def update(Long id, Long version) {
        def receiptsInstance = Receipts.get(id)
        if (!receiptsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'receipts.label', default: 'Receipts'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (receiptsInstance.version > version) {
                receiptsInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'receipts.label', default: 'Receipts')] as Object[],
                          "Another user has updated this Receipts while you were editing")
                render(view: "edit", model: [receiptsInstance: receiptsInstance])
                return
            }
        }

        receiptsInstance.properties = params

        if (!receiptsInstance.save(flush: true)) {
            render(view: "edit", model: [receiptsInstance: receiptsInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'receipts.label', default: 'Receipts'), receiptsInstance.id])
        redirect(action: "show", id: receiptsInstance.id)
    }

    def delete(Long id) {
        def receiptsInstance = Receipts.get(id)
        if (!receiptsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'receipts.label', default: 'Receipts'), id])
            redirect(action: "list")
            return
        }

        try {
            receiptsInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'receipts.label', default: 'Receipts'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'receipts.label', default: 'Receipts'), id])
            redirect(action: "show", id: id)
        }
    }
	def receiptsRegister(){}
}
