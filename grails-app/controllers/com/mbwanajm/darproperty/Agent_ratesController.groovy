package com.mbwanajm.darproperty

import grails.plugins.springsecurity.Secured
import org.hibernate.NonUniqueObjectException
import org.springframework.dao.DataIntegrityViolationException

class Agent_ratesController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [agent_ratesInstanceList: Agent_rates.list(params), agent_ratesInstanceTotal: Agent_rates.count()]
    }

    @Secured(['ROLE_ADMIN'])
    def create() {
        [agent_ratesInstance: new Agent_rates(params)]
    }

    @Secured(['ROLE_ADMIN'])
    def save() {
        def agent_ratesInstance = new Agent_rates(params)
        if (!agent_ratesInstance.save(flush: true)) {
            render(view: "create", model: [agent_ratesInstance: agent_ratesInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'agent_rates.label', default: 'Agent_rates'), agent_ratesInstance.id])
        redirect(action: "show", id: agent_ratesInstance.id)
    }

    def show(Long id) {
        def agent_ratesInstance = Agent_rates.get(id)
        if (!agent_ratesInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'agent_rates.label', default: 'Agent_rates'), id])
            redirect(action: "list")
            return
        }

        [agent_ratesInstance: agent_ratesInstance]
    }

    @Secured(['ROLE_ADMIN'])
    def edit(Long id) {
        def agent_ratesInstance = Agent_rates.get(id)
        if (!agent_ratesInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'agent_rates.label', default: 'Agent_rates'), id])
            redirect(action: "list")
            return
        }

        [agent_ratesInstance: agent_ratesInstance]
    }

    @Secured(['ROLE_ADMIN'])
    def update(Long id, Long version) {
        def agent_ratesInstance = Agent_rates.get(id)
        if (!agent_ratesInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'agent_rates.label', default: 'Agent_rates'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (agent_ratesInstance.version > version) {
                agent_ratesInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'agent_rates.label', default: 'Agent_rates')] as Object[],
                        "Another user has updated this Agent_rates while you were editing")
                render(view: "edit", model: [agent_ratesInstance: agent_ratesInstance])
                return
            }
        }

        agent_ratesInstance.properties = params

        if (!agent_ratesInstance.save(flush: true)) {
            render(view: "edit", model: [agent_ratesInstance: agent_ratesInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'agent_rates.label', default: 'Agent_rates'), agent_ratesInstance.id])
        redirect(action: "show", id: agent_ratesInstance.id)
    }

    @Secured(['ROLE_ADMIN'])
    def delete(Long id) {
        def agent_ratesInstance = Agent_rates.get(id)
        if (!agent_ratesInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'agent_rates.label', default: 'Agent_rates'), id])
            redirect(action: "list")
            return
        }

        try {
            agent_ratesInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'agent_rates.label', default: 'Agent_rates'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'agent_rates.label', default: 'Agent_rates'), id])
            redirect(action: "show", id: id)
        }
    }

    def rateAgent() {

            if (params.rate) {
                Agent_rates agentrates = new Agent_rates(params)

                if (!agentrates.save(flash: true)) {
                    agentrates.errors.allErrors.each {
                        flash.message = it
                    }

                }
            }

//        return
        redirect(controller: "agent", action: "showAllAgents")


    }
}
