package com.mbwanajm.darproperty

import org.springframework.dao.DataIntegrityViolationException

class SubaminitiesController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [subaminitiesInstanceList: Subaminities.list(params), subaminitiesInstanceTotal: Subaminities.count()]
    }

    def create() {
        [subaminitiesInstance: new Subaminities(params)]
    }

    def save() {
        def subaminitiesInstance = new Subaminities(params)
        if (!subaminitiesInstance.save(flush: true)) {
            render(view: "create", model: [subaminitiesInstance: subaminitiesInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'subaminities.label', default: 'Subaminities'), subaminitiesInstance.id])
        redirect(action: "show", id: subaminitiesInstance.id)
    }

    def show(Long id) {
        def subaminitiesInstance = Subaminities.get(id)
        if (!subaminitiesInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'subaminities.label', default: 'Subaminities'), id])
            redirect(action: "list")
            return
        }

        [subaminitiesInstance: subaminitiesInstance]
    }

    def edit(Long id) {
        def subaminitiesInstance = Subaminities.get(id)
        if (!subaminitiesInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'subaminities.label', default: 'Subaminities'), id])
            redirect(action: "list")
            return
        }

        [subaminitiesInstance: subaminitiesInstance]
    }

    def update(Long id, Long version) {
        def subaminitiesInstance = Subaminities.get(id)
        if (!subaminitiesInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'subaminities.label', default: 'Subaminities'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (subaminitiesInstance.version > version) {
                subaminitiesInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'subaminities.label', default: 'Subaminities')] as Object[],
                          "Another user has updated this Subaminities while you were editing")
                render(view: "edit", model: [subaminitiesInstance: subaminitiesInstance])
                return
            }
        }

        subaminitiesInstance.properties = params

        if (!subaminitiesInstance.save(flush: true)) {
            render(view: "edit", model: [subaminitiesInstance: subaminitiesInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'subaminities.label', default: 'Subaminities'), subaminitiesInstance.id])
        redirect(action: "show", id: subaminitiesInstance.id)
    }

    def delete(Long id) {
        def subaminitiesInstance = Subaminities.get(id)
        if (!subaminitiesInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'subaminities.label', default: 'Subaminities'), id])
            redirect(action: "list")
            return
        }

        try {
            subaminitiesInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'subaminities.label', default: 'Subaminities'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'subaminities.label', default: 'Subaminities'), id])
            redirect(action: "show", id: id)
        }
    }
}
