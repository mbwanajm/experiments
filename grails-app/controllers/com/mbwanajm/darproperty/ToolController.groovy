package com.mbwanajm.darproperty

import grails.plugin.mail.MailService
import org.springframework.scheduling.annotation.Async

class ToolController {
    MailService mailService
    AdvertsService advertsService
    def simpleCaptchaService

    @Async
    def mortgageCalculator() {

        [calculatorAdverts: advertsService.calculatorSlideShowAdverts, footerAdverts: advertsService.footerSlideShowAdverts]
    }

    def calculateMortgage() {

        if (params.mort) {
            def price = params.prce
            def currency = params.cont
            def country = params.cnp
            def tax = 0.1

            def cal = new GregorianCalendar()
            def mon = cal.get(Calendar.MONTH)
            def year = cal.get(Calendar.YEAR)

            def a = "";
            switch (mon + 1) {
                case 1:
                    a = "Jan";
                    break;
                case 2:
                    a = "Feb";
                    break;
                case 3:
                    a = "Mar";
                    break;
                case 4:
                    a = "Apr";
                    break;
                case 5:
                    a = "May";
                    break;
                case 6:
                    a = "Jun";
                    break;
                case 7:
                    a = "Jul";
                    break;
                case 8:
                    a = "Aug";
                    break;
                case 9:
                    a = "Sept";
                    break;
                case 10:
                    a = "Oct";
                    break;

                case 11:
                    a = "Nov";
                    break;

                case 12:
                    a = "Dec";
                    break;


            }

            [price: price, currency: currency, country: country, tax: tax, a: a, mon: mon, year: year, calculatorAdverts: advertsService.calculatorSlideShowAdverts, footerAdverts: advertsService.footerSlideShowAdverts]

        } else {
            redirect(action: "mortgageCalculator")
        }
    }

    @Async
    def applyMortgage() {
        if (params.sbmt2) {
            def price = params.prc
            def currency = params.crr
            def country = params.cnt
            [price: price, currency: currency, country: country, calculatorAdverts: advertsService.calculatorSlideShowAdverts, footerAdverts: advertsService.footerSlideShowAdverts]
        } else if (params.sbmt3) {
            def names = params.names
            def phone = params.phone
            def email = params.email
            def bank = params.bank
            def price = params.prce
            def dp = params.dp
            def terms = params.terms
            def currency = params.crr
            def country = params.cnt
            boolean captchaValid = simpleCaptchaService.validateCaptcha(params.captcha)
            Banks ban = Banks.findById(bank)
            def bankEmail = Parteners_banks.findAllByBank(ban)
            String to_address = bankEmail.bank_email[0]
            if (captchaValid) {


                try {
                    mailService.sendMail {
//                            to "info@darproperty.net"
                        to "$to_address"
                        from "$params.email"
                        subject "MORTGAGE APPLICANT FROM DARPROPERTY"
                        body "Dear manager a lead has been created from Darproperty,\n The information of a lead are as follow \n\n  Name: $names \n Loan amount($currency): $price \n Downpayment: $dp %, in $terms years. \n\n Contacts; \n PnoneNo. $phone \n Email Address.$email\n\n All rights reserved © 2016 Darproperty "
                    }
                    flash.successMessage = 'Your message has been sent succesfully'
//                    String body = "<p style='color:#000099;'>Dear manager a lead has been created from Darproperty,  <br> The information of a lead are as follow</p> <hr> <table ><tr><td style='color:#ed1c24;'> Name: </td><td > $names </td></tr> <tr><td style='color:#ed1c24;'> Loan amount($currency): </td> <td >$price </td></tr> <tr><td style='color:#ed1c24;'>Downpayment:</td><td >$dp %, in $terms years.</td></tr><tr><td style='color:#909090;'>Contacts; </td><td></td></tr><tr><td style='color:#ed1c24;'> PnoneNo. </td><td> $phone </td></tr><tr><td style='color:#ed1c24;'> Email Address.</td><td> $email </td></tr></table> <hr> <small style='color:#999;' ><b> All rights reserved © 2016 Darproperty </b></small> ";
//                    String subject = " MORTGAGE APPLICANT FROM DARPROPERTY";
//                    SendEmail smail = new SendEmail();
//                    smail.setSender("info@darproperty.net");
//                    smail.setPassword("mikocheni120");
//                    smail.setRecipient(to_address);
//                    smail.setSubject(subject);
//                    smail.setBody(body);
//                    if (smail.sendThroughDarProperty()) {
//                        flash.successMessage = 'Your message has been sent succesfully'
//                    } else {
//                        flash.errorMessage = ' Sorry Your application failed to to be sent. Please try again'
//                    }

                } catch (Exception ex) {
                    flash.errorMessage = 'Sorry something went wrong please try to send your message again' + ex
                }

                //  flash.successMessage = 'Your message has been sent succesfully'
                [params: params, price: price, currency: currency, country: country, calculatorAdverts: advertsService.calculatorSlideShowAdverts, footerAdverts: advertsService.footerSlideShowAdverts]
            } else {
                flash.errorMessage = "You made a mistake while entereing the Captcha!<br>You may try again"
                [params: params, price: price, currency: currency, country: country, calculatorAdverts: advertsService.calculatorSlideShowAdverts, footerAdverts: advertsService.footerSlideShowAdverts]
            }


        } else {
            redirect(action: "mortgageCalculator")
        }

    }

    @Async
    def valueCalculator() {
        //redirect(controller: "home", action: "home")
        [calculatorAdverts: advertsService.calculatorSlideShowAdverts]
    }

    @Async
    def calculateValue() {
        if (params.location && params.country
                && params.region && params.region != "any"
                && params.area && params.area != "any") {
            def country = params.country
            def region = params.region
            def area = params.area

            def amenities = Amenities.findAllByVisible(1, [sort: "holder", order: "desc"])
            [amenities: amenities, country: country, region: region, area: area, calculatorAdverts: advertsService.calculatorSlideShowAdverts]

        } else {
            redirect(action: "valueCalculator")
        }
    }

    @Async
    def estimateResults() {
        if (params.calculate) {
            def err = ""
            double item = 0;
            int builtArea = 0
            int plotArea = 0
            def areaLocated = ""
            def regionLocated = ""
            def areaPoint = ""
            double areaPoints = 0.0

            int countAmenities = 0



            double identical_sum = 0.0;
            double inadentical_sum = 0.0;
            double sum = 0.0;
            double selected = 0.0;
            double sum_selected = 0.0;
            double point_amenities = 0.0;
            double gsp = 0.0;
            double net_selling_price = 0.0;
            double selling_price = 0.0;
            double rental_price = 0.0;

            try {
                builtArea = Integer.parseInt((params.builtup ? params.builtup : "0"))
                plotArea = Integer.parseInt((params.plotarea ? params.plotarea : "0"))
                areaLocated = params.areaLocated
                regionLocated = Region.findByName(params.regionLocated)
                areaPoint = Region_areas.findByRegion_idAndAreas_string(regionLocated.id, areaLocated)
                areaPoints = areaPoint.point
                def allAmenities = Amenities.findAllByVisible(1)
                countAmenities = Amenities.countByVisible(1)

                if (plotArea != 0 && plotArea < builtArea) {
//           swap
                    int hold = plotArea
                    plotArea = builtArea
                    builtArea = hold
                }

                for (int i = 0; i < countAmenities; i++) {

                    item += Double.parseDouble(params.getProperty("" + allAmenities.id[i]))

                }

//            calculate

                if (plotArea == 0) {
                    gsp = ((builtArea) * 1100);
                    point_amenities = ((gsp * (item / 100.0)) * 100.0) / 100.0;
                    net_selling_price = gsp + point_amenities;
                    selling_price = ((net_selling_price * (areaPoints / 100.0)) * 100.0) / 100.0;
                    rental_price = ((selling_price / 96) * 100.0) / 100.0;
                } else {
                    gsp = ((plotArea - builtArea) * 500 + builtArea * 1100);
                    point_amenities = ((gsp * (item / 100.0)) * 100.0) / 100.0;
                    net_selling_price = gsp + point_amenities;
                    selling_price = ((net_selling_price * (areaPoints / 100.0)) * 100.0) / 100.0;
                    rental_price = ((selling_price / 96) * 100.0) / 100.0;
                }

            } catch (Exception e) {
                // err = "" + e
            }

            [item         : item, err: err, areaPoints: areaPoints, areaLocated: areaLocated, plotArea: plotArea, builtArea: builtArea,
             regionLocated: regionLocated,
             gsp          : gsp, net_selling_price: net_selling_price, point_amenities: point_amenities, selling_price: selling_price,
             rental_price : rental_price]

        } else {
            redirect(action: "valueCalculator")
        }

    }

    @Async
    def sendEmailToDarproperty() {
        boolean captchaValid = simpleCaptchaService.validateCaptcha(params.captcha)

        if (captchaValid) {
            try {
                mailService.sendMail {
                    to "info@darproperty.net"
                    from "$params.email"
                    subject "Message On DarProperty Contact Page"
                    body "$params.message\n\nmy email:$params.email\nmy phone:$params.phone"
                }

                flash.successMessage = 'Your message has been sent succesfully'

            } catch (Exception ex) {
                flash.errorMessage = 'Sorry something went wrong please try to send your message again'
            }
        } else {
            flash.errorMessage = "You made a mistake while entering the Captcha!<br>You may try again"
        }

        render(view: '../contactUs/index')
    }

    @Async
    def sendEmailForMortgage() {
        boolean captchaValid = simpleCaptchaService.validateCaptcha(params.captcha)

        if (captchaValid) {
            try {
                mailService.sendMail {
                    to "info@darproperty.net"
                    from "$params.email"
                    subject "Message On Mortgage Contact Page"
                    body "$params.message\n\nmy email:$params.email\nmy phone:$params.phone"
                }

                flash.successMessage = 'Your message has been sent succesfully'

            } catch (Exception ex) {
                flash.errorMessage = 'Sorry something went wrong please try to send your message again'
            }
        } else {
            flash.errorMessage = "You made a mistake while entereing the Captcha!<br>You may try again"
        }

        render(view: '../contactUs/index')
    }


}
