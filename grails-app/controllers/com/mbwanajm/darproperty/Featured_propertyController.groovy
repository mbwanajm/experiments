package com.mbwanajm.darproperty

import org.springframework.dao.DataIntegrityViolationException

class Featured_propertyController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [featured_propertyInstanceList: Featured_property.list(params), featured_propertyInstanceTotal: Featured_property.count()]
    }

    def create() {
        [featured_propertyInstance: new Featured_property(params)]
    }

    def save() {
        def featured_propertyInstance = new Featured_property(params)
        if (!featured_propertyInstance.save(flush: true)) {
            render(view: "create", model: [featured_propertyInstance: featured_propertyInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'featured_property.label', default: 'Featured_property'), featured_propertyInstance.id])
        redirect(action: "show", id: featured_propertyInstance.id)
    }

    def show(Long id) {
        def featured_propertyInstance = Featured_property.get(id)
        if (!featured_propertyInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'featured_property.label', default: 'Featured_property'), id])
            redirect(action: "list")
            return
        }

        [featured_propertyInstance: featured_propertyInstance]
    }

    def edit(Long id) {
        def featured_propertyInstance = Featured_property.get(id)
        if (!featured_propertyInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'featured_property.label', default: 'Featured_property'), id])
            redirect(action: "list")
            return
        }

        [featured_propertyInstance: featured_propertyInstance]
    }

    def update(Long id, Long version) {
        def featured_propertyInstance = Featured_property.get(id)
        if (!featured_propertyInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'featured_property.label', default: 'Featured_property'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (featured_propertyInstance.version > version) {
                featured_propertyInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'featured_property.label', default: 'Featured_property')] as Object[],
                          "Another user has updated this Featured_property while you were editing")
                render(view: "edit", model: [featured_propertyInstance: featured_propertyInstance])
                return
            }
        }

        featured_propertyInstance.properties = params

        if (!featured_propertyInstance.save(flush: true)) {
            render(view: "edit", model: [featured_propertyInstance: featured_propertyInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'featured_property.label', default: 'Featured_property'), featured_propertyInstance.id])
        redirect(action: "show", id: featured_propertyInstance.id)
    }

    def delete(Long id) {
        def featured_propertyInstance = Featured_property.get(id)
        if (!featured_propertyInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'featured_property.label', default: 'Featured_property'), id])
            redirect(action: "list")
            return
        }

        try {
            featured_propertyInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'featured_property.label', default: 'Featured_property'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'featured_property.label', default: 'Featured_property'), id])
            redirect(action: "show", id: id)
        }
    }
	def addFeaturedProperties(){}
	def addFeaturedPhotos(){}
	def saveProperty(){}
}
