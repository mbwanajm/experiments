package com.mbwanajm.darproperty

class SecurityTagLib {
    static namespace = 'dp'
    def springSecurityService

    def ifCurrentlyLoggedIn = { attrs, body ->
        Agent agent = springSecurityService.currentUser
        if (agent) {
            if (agent.id == attrs.id) {
                out << body()
            }
        }
    }
}
