package com.mbwanajm.darproperty


import java.text.DecimalFormat
import java.text.SimpleDateFormat

class DateTagLib {
    static final long second = 1000
    static final long minute = second * 60
    static final long hour = minute * 60
    static final long day = hour * 24

    def dateFromNow = { attrs ->
        def date = attrs.date
        def niceDate = getNiceDate(date)

        out << niceDate
    }

    def dateFormatting = { attrs ->
        def date = attrs.date
        def niceDate = getFormattedDate(date)

        out << niceDate
    }

    static String getNiceDate(Date date) {

        def result = "";
        try {


            Calendar c = Calendar.getInstance();
            c.setTime(new Date()); // Now use today date.
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");//set date format
            String ds = sdf.format(c.getTime());//set display as formatted
            String format = "yyyy-MM-dd hh:mm:ss.S";

            sdf = new SimpleDateFormat(format);
            String date2 = ds;//+ " 00:00:00.0";
            Date dateObj1 = date;
            Date dateObj2 = sdf.parse(date2);

            DecimalFormat crunchifyFormatter = new DecimalFormat("###,###");

//            System.out.println("Difference between years: "+getDiffYears(dateObj1, dateObj2));
            long diff = (dateObj2.getTime() - dateObj1.getTime()) ;

            long diffDays =  (diff / (24 * 60 * 60 * 1000));
//            System.out.println("difference between days: " + diffDays);

            long diffhours = ((diff / (60 * 60 * 1000)));
//            System.out.println("difference between hours: " + crunchifyFormatter.format(diffhours));

            long diffmin =  ((diff / (60 * 1000)));
//            System.out.println("difference between minutes: " + crunchifyFormatter.format(diffmin));

            long diffsec = ((diff / (1000)));
//            System.out.println("difference between seconds: " + crunchifyFormatter.format(diffsec));
//
//            System.out.println("difference between milliseconds: " + crunchifyFormatter.format(diff));

            if (getDiffYears(dateObj1, dateObj2) > 0) {
                result = getDiffYears(dateObj1, dateObj2) + " years ago.";
            } else {
                if (diffDays > 0) {
                    result = diffDays + " days ago.";
                }else{
                    if (diffhours > 0) {
                        result = crunchifyFormatter.format(diffhours) + " hours ago.";
                    } else {
                        if (diffmin > 0) {
                            result = crunchifyFormatter.format(diffmin) + " minutes ago";
                        } else if((diffsec > 0))  {
                            result = crunchifyFormatter.format(diffsec)+" seconds ago. ";
                        }else{
                            result = "minutes ago."
                        }
                    }
                }
            }

        } catch (Exception e) {
            //e.printStackTrace();
            result = "" + e
        }
        return result
    }
static getDiffYears(Date first, Date last){
    Calendar a = getCalendar(first);
    Calendar b = getCalendar(last);
    int diff = b.get(Calendar.YEAR) - a.get(Calendar.YEAR);
    if (a.get(Calendar.MONTH) > b.get(Calendar.MONTH) ||
            (a.get(Calendar.MONTH) == b.get(Calendar.MONTH) && a.get(Calendar.DATE) > b.get(Calendar.DATE))) {
        diff--;
    }
    return diff;
}

    static Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance(Locale.US);
        cal.setTime(date);
        return cal;
    }
    static String getFormattedDate(Date date) {

        def result = "";
        try {


            result = new SimpleDateFormat("dd-M-yyyy").format(date)

        } catch (Exception e) {
            //e.printStackTrace();
            result = "" + e
        }
        return result
    }




}
