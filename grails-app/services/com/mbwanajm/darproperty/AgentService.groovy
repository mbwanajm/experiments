package com.mbwanajm.darproperty

import com.mbwanajm.darproperty.Agent
import grails.plugins.springsecurity.SpringSecurityService
import org.hibernate.validator.constraints.Email

class AgentService {
    SpringSecurityService springSecurityService

    def createAgentFromApc(apc) {
        Agent agent = springSecurityService.currentUser //Agent.findById(apc.id)
        agent.profile.firstName = apc.firstName
        agent.profile.middleName = apc.middleName
        agent.profile.lastName = apc.lastName
        agent.profile.company = apc.company
        agent.profile.phone = apc.phone
        agent.profile.email = apc.email

        if (apc.photo) {
            agent.profile.photo = apc.photo
        }

        return agent
    }
    def createNewAgent(params) {
        Agent agent = new Agent()
        agent.username = params.username
    }

    def listAgentEmails() {
        //def profile = Profile.findAllByEmailIsNotNull()
         Agent.list()

    }
}
