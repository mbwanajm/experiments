package com.mbwanajm.darproperty

import java.text.SimpleDateFormat

class EnquiriesService {
    def latestEnquiries() {
        Date date = new Date()
        def sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
        def datetime = sdf.format(date)
        Enquiries.findAllByDueDateGreaterThanEquals(new Date().parse("yyyy-MM-dd hh:mm:ss", datetime), [sort:'id',order: 'desc', max: 3])

    }

}
