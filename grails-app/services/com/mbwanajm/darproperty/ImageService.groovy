package com.mbwanajm.darproperty

import net.coobird.thumbnailator.Thumbnailator

class ImageService {

    byte[] resize(byte[] imageData, int xSize, int ySize) {
        def inputStream = new ByteArrayInputStream(imageData)
        def outputStream = new ByteArrayOutputStream();
        Thumbnailator.createThumbnail(inputStream, outputStream, xSize, ySize)
        outputStream.toByteArray()
    }

}
