package com.mbwanajm.darproperty

import groovy.json.JsonSlurper
import org.springframework.scheduling.annotation.Async

/**
 * This service can be used to convert between different currencies
 */
class CurrencyService {

    static def rates = [USD: 2000, KES:20, RWF:18, TZS:1] // contains conversion rates against the TZS

   /**
    * Convert a certain amount of money from sourceCurrency to destinationCurrency
    *
    * @param amount
    * @param sourceCurrency
    * @param destCurrency
    * @return
    */
    @Async
    def convert(amount, sourceCurrency, destCurrency) {
        if (sourceCurrency == destCurrency) return amount
        if (!rates) updateRates() // there are no rates update them

        return (amount * rates[sourceCurrency]) / rates[destCurrency]
    }

    /**
    * Useful to convert to Tanzanian Shilling
    * @param amount
    * @param sourceCurrency
    * @return
    */
    def convertToTZS(amount, sourceCurrency) {
        if (!rates) updateRates()
        return amount * rates[sourceCurrency]
    }

    /**
    * update the exchange rates
    * @return
    */
    def updateRates() {
        def result = "http://openexchangerates.org/api/latest.json?app_id=3bb8afadff5e442792905a78361cb033".toURL().text

        if (result) {
            def json = new JsonSlurper().parseText(result)

            // make tanzanian shilling the base currency
            json.rates.each { currency, rate ->
                rates[currency] = json.rates.TZS / rate
            }
        }else{
            rates = [USD: 2000, KES:20, RWF:18, TZS:1]
        }
    }
}
