package com.mbwanajm.darproperty

class PropertyService {

    def getTopProperty() {
        int offset = (Math.random() *  Property.count) - 2
        Agent fsbo = Agent.findById(2)
        def topProperty = Property.findAllByAgentAndDeleted(fsbo,false, [sort: 'views', offset: offset, max: 2, order: 'desc'])
        //  if(topProperty.size() < 5) generateRandomProperty()
        return topProperty;

    }

    def getLatestProperty() {
        def latestProperty = Property.findAllByDeleted(false, [max:9, sort: 'dateAdded', order: 'desc'])

        return latestProperty
    }


    def getLatestPropertySmall() {
        def latestProperty = Property.findAllByDeleted(false, [max:2, sort: 'dateAdded', order: 'desc'])

        return latestProperty
    }

    def getFsboPosts(){
        Agent fsbo = Agent.findById(2)
        def fsbos = Property.findAllByAgentAndDeleted(fsbo, false)
        return fsbos
    }
	
	def getFeaturedProperty() {
        //int offset = (Math.random() *  Property.count) - 6
//		Agent fsbo = Agent.findById(2)
//		def featured = Property.findAllByAgentAndDeleted(fsbo, false,[max:9, sort: 'dateAdded', order: 'desc'])
//
//
//				return featured
        int offset = (Math.random() *  5)
        Agent fsbo = Agent.findById(2)
        def topProperty = Property.findAllByAgentAndDeleted(fsbo,false, [sort: 'dateAdded', offset: offset, max: 9, order: 'desc'])
        //  if(topProperty.size() < 5) generateRandomProperty()
        return topProperty;
	}

    def getFeaturedPropertyForSmall() {
//
//        Agent fsbo = Agent.findById(2)
//        def featured = Property.findAllByAgentAndDeleted(fsbo, false,[max:4, sort: 'dateAdded', order: 'desc'])
//
//
//        return featured
        int offset = (Math.random() *  Property.count) - 2
        Agent fsbo = Agent.findById(2)
        def topProperty = Property.findAllByAgentAndDeleted(fsbo,false, [sort: 'dateAdded', offset: offset, max: 4, order: 'desc'])
        //  if(topProperty.size() < 5) generateRandomProperty()
        return topProperty;
    }

    private def generateRandomProperty(){
        int offset = (Math.random() *  Property.count) - 5
        def topProperty = Property.findAllByDeleted(false, [sort: 'views', offset: offset, max: 5, order: 'desc'])
      //  if(topProperty.size() < 5) generateRandomProperty()
        return topProperty;
    }
}
