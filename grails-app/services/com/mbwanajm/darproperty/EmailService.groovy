package com.mbwanajm.darproperty

import org.springframework.scheduling.annotation.Async

import javax.mail.MessagingException
import javax.mail.PasswordAuthentication
import javax.mail.Session
import javax.mail.Transport
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage

class EmailService {

    def serviceMethod() {

    }
    @Async
    boolean  sendThroughDarProperty(String sender,String password, String receiver, String subject,String body) {
        boolean sent = false;
        Properties props = new Properties();
        props.put("mail.smtp.host", "mail.darproperty.net");  // smtp.mail.yahoo.com //smtp.gmail.com//  smtp.live.com
        props.put("mail.smtp.socketFactory.port", "465");  //gmail 465 live 587 yahoo
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(sender, password);//change accordingly
                    }
                });
        //compose message
        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(sender));//change accordingly
            message.addRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(receiver));
            message.setSubject(subject);

            message.setContent(body(), "text/html");

            Transport.send(message);


            sent = true;





        } catch (MessagingException e) {

           e.printStackTrace("Email Transfer Failed " + e);

        }
        return sent;
    }

    String checkEmail(String email) {
        String done = "";

        if (email.indexOf("/") > -1) {
            email = email.substring(0, email.indexOf("/"));
        }

        int firstAt = email.indexOf("@");
        if (firstAt != -1) {
            int length = email.length();
            String first = email.substring(0, firstAt);
            String last = email.substring(firstAt, length);
            int l = last.length();
            int lat = last.indexOf(".");
            if (lat != -1) {

                int firstLastDot = last.indexOf(".");
                String afterAt = last.substring(0, firstLastDot);
                int ln = afterAt.length();
                if (ln > 2) {
                    String afterDot = last.substring(firstLastDot, l);
                    int ln2 = afterDot.length();
                    if (ln2 > 2) {

                        done = email;
                    } else {
                    }
                } else {

                }

                ///
            } else {
            }

        } else {

        }
        //true means its valid
        return done;
    }

    String srink_email(String filename) {
        String mail = filename.replaceAll("\\s+", "");
        return mail;
    }
}
