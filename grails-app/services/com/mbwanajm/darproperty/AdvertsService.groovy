package com.mbwanajm.darproperty

class AdvertsService {

    def getMainSlideShowAdverts() {

        AdvertType advertType = AdvertType.findByName("main slideshow")
        def adverts = Advert.findAllByType(advertType)
        return adverts

    }

    def getFooterSlideShowAdverts() {

        AdvertType advertType = AdvertType.findByName("footer slideshow")
        def adverts = Advert.findAllByType(advertType)
        return adverts

    }

    def getFooterSlideShow2Adverts() {
        try{
            AdvertType advertType = AdvertType.findByName("footer slideshow2")
            def adverts = Advert.findAllByType(advertType)
            return adverts
        }catch(Exception e){

        }

    }
	
	def getCalculatorSlideShowAdverts() {
        try{
		AdvertType advertType = AdvertType.findByName("calculator page")
		def adverts = Advert.findAllByType(advertType)
		return adverts
    }catch(Exception e){

    }
	}
	def getSide1Advert() {

		AdvertType advertType = AdvertType.findByName("side one")
		def adverts = Advert.findAllByType(advertType)
		return adverts

	}
}
