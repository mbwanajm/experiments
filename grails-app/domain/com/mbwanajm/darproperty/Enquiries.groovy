package com.mbwanajm.darproperty

class Enquiries {
    private static final Date NULL_DATE = new Date(0)
    String name
    String phone
    String email
    String country
    String region
    String area
    String type
    String sale_type
    Date dateAdded
    Date dueDate
    String currency
    String description
    int rooms = 0

    double max_price
    double min_price
    static constraints = {
        dateAdded (nullable: true)
        dueDate(nullable: true)

        email(email: true)
        description(blank: false, size: 0..500)
    }
}
