package com.mbwanajm.darproperty

class Currency {
double usd_value
    String country
    String tag


    static constraints = {
        usd_value(blank:false)
        country(blank:false)
        tag(unique: true)
    }
}
