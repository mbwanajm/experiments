package com.mbwanajm.darproperty

class Parteners_banks {
    Banks bank
    String bank_email
    static constraints = {
        bank_email(blank:  false, unique: true)
        bank(blank: false)
    }
}
