package com.mbwanajm.darproperty

class Receipts {
	static searchable = {
		references_table(component: true)
	}
    String firstName
    String middleName
    String lastName
    String email
	String phone
	String tel
	Date dateReceived
	String company
    static constraints = {
		company(blank:true, nullable:true)
		phone(blank:false)
		email(email: true)
		firstName(blank: false)
		middleName(nullable:true, blank:true)
		lastName(blank: false)
		dateReceived(blank:false)
    }
	References_table references_table
	static hasMany = [featured_property: Featured_property]
}
