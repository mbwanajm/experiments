package com.mbwanajm.darproperty

class Property {
    static searchable = {
        tags(component: true)
        type(component: true)
        saleType(component: true)
        agent(component: true)
        spellCheck "include"

    }
    Agent agent
    String name
    String description
    String descriptionSW
    Country country
    String region
    String area
    String location

    Double price
    String currency
    Double size
    Integer numberOfRooms = 0
    Integer numberOfBathRooms = 0

    Integer views
    Date dateAdded
    Boolean deleted = false;
    Boolean approved = false
    Date deletedOn

    static constraints = {
        name(blank: false)
        description(blank: false, size: 0..500)
        country(blank:false)
        region(blank: false)
        area(blank: false)
        agent(nullable: true)
        views(nullable: true)
        dateAdded(nullable: true)
        approved(nullable: true)
        location(nullable: true)
        deleted(nullable: true)
        deletedOn(nullable: true)
        price(blank:true, nullable:true)
    }

    Type type
    SaleType saleType
    static belongsTo = [agent: Agent]
    static hasMany = [tags: Tag, photos: Photo, messages: Message]

    String toString() {
        name
    }
    def getFsboDetails() {
        if (description) {
            def detailsContent = description.substring(description.indexOf('{{') + 2, description.indexOf('}}'))
            def details = detailsContent.split(',')


            return [name: details[0], phone: details[1], email: details[2], description: description.replace("{{$detailsContent}}", '')]
        } else {
            return [:]
        }
    }

    boolean isFsbo() {
        return agent?.username == 'fsbo'
    }

}
