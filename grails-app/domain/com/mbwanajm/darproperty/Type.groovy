package com.mbwanajm.darproperty

class Type {
    static searchable = true
    String name

    static constraints = {
        name(blank: false)
    }

    public String toString() {
        return name
    }
}
