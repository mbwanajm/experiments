package com.mbwanajm.darproperty

class References_table {
	static searchable = true

String refNo
Date date
double amount 
    static constraints = {
		refNo(unique: true,blank:false )
		date(blank: false)
		amount(blank: false)
    }
	String toString() { refNo }
	static belongsTo = Receipts
}
