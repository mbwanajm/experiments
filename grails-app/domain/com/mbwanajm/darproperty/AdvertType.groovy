package com.mbwanajm.darproperty

class AdvertType {
    String name

    static constraints = {
        name(blank: false, nullable: false)
    }

    String toString() {
        return name
    }

}
