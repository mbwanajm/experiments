package com.mbwanajm.darproperty



class Featured_property {
	static searchable = {
		tags(component: true)
		type(component: true)
		saleType(component: true)
		receipts(component: true)
		spellCheck "include"

	}

	String name
	String description
	String descriptionSW
	Country country
	String region
	String area
	String location

	Double price
	String currency
	Double size
	Date dateAdded
	Date expireDate
    static constraints = {
		receipts(blank:false) 
		name(blank: false)
		description(blank: false, size: 0..500)
		country(blank:false)
		region(blank: false)
		area(blank: false)
		dateAdded(nullable: true)
		location(nullable: true)
		expireDate(nullable: true)
		price(blank:false)
		currency(blank:false) 
    }
	Type type
	SaleType saleType
	static belongsTo = [receipts: Receipts]
	static hasMany = [tags: Tag, photos: Photo]
}
