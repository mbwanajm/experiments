package com.mbwanajm.darproperty

class Message {
    String email
    String phone
    String message
    Date dateCreated

    static belongsTo = [Property]

    static constraints = {
        email(nullable: false, email: true)
        phone(nullable: false, minSize: 9)
        message(nullable: false, maxSize: 1200)
    }
}
