package com.mbwanajm.darproperty

class Photo {

    String name
    String description
    byte[] image

    static constraints = {
        name(blank: false)
        description(nullable: true, maxSize: 300)
        image(nullable: false, minSize: 1, maxSize: 10485760)  // 10MB
    }

    
}
