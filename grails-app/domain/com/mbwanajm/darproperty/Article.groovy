package com.mbwanajm.darproperty

class Article {
    String title
    Date dateCreated
    String source
    String content
	String tag
    byte[] image= null

    static constraints = {
        title(nullable: false)
        content(nullable: false, maxSize: 20000)
        image(nullable: true, minSize: 1, maxSize: 10485760)  // 10MB
    }
}
