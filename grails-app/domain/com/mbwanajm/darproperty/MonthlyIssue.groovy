package com.mbwanajm.darproperty

/**
 * Contains information about dar property monthly issues
 * Created with IntelliJ IDEA.
 * User: mbwana
 * Date: 5/3/12
 * Time: 8:10 PM
 * To change this template use File | Settings | File Templates.
 */
class MonthlyIssue {
    String title;
    String embedCode;
    byte[] image;

    static constraints = {
        title(nullable: false)
        embedCode(nullable: false, maxSize: 1000)
        image(nullable: true, minSize: 1, maxSize: 10485760)  // 10MB
    }
}
