package com.mbwanajm.darproperty

class SaleType {
    static searchable = true
    String name

    static constraints = {
        name(nullable: false)
    }

    public String toString() {
        name
    }
}
