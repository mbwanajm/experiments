package com.mbwanajm.darproperty

import java.text.SimpleDateFormat;

import com.sun.xml.internal.ws.api.streaming.XMLStreamReaderFactory.Default;

class Advert {
	
    String url
    String caption
    String description
    AdvertType type
    byte[] photo
    long views = 0
	Date date  //= ""+(new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat().getCalendar().getTime()));//new Date()//""+sdf.format(sdf.getCalendar().getTime());
    static constraints = {
        url(url: true)
        photo(minSize: 1, maxSize: 20485760)// 10MB
		date(nullable:true)
    }

}
