package com.mbwanajm.darproperty

class Banks {
    String bankname
    double rates
    static constraints = {
        bankname(unique: true, nullable: false)
    }
    static hasMany = [Interestrates, Parteners_banks]
    public String toString(){
        bankname
    }
}
