package com.mbwanajm.darproperty

class Amenities {
int visible = 1
String title
String holder
double point 
    static constraints = {
		title(blank: false, size: 0..200)
		holder(nullable:true)
    }
	static hasMany = [Amenities_tag]
}
