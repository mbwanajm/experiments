package com.mbwanajm.darproperty

class Agent extends User {
    static searchable = {
        profile(component: true)
    }

    static constraints = {

    }

    Profile profile
    static hasMany = [property: Property, agent_rate:Agent_rates]



}
