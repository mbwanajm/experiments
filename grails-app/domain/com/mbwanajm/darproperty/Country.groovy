package com.mbwanajm.darproperty

class Country {
    String name
    static hasMany = [regions:Region]
    static constraints = {
    }

    public String toString(){
        name
    }
}
