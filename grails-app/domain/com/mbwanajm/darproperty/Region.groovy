package com.mbwanajm.darproperty



class Region {

	
    String name

    static constraints = {
        name(nullable: false)
    }

    static hasMany = [ areas: String , region_areas:Region_areas]

    String toString() {
        name
		
		
    }
	

}
