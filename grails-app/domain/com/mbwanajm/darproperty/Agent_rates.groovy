package com.mbwanajm.darproperty

class Agent_rates {
//    static searchable = {
//        agent(component: true)
//
//    }
    Agent agent
    String ip
    int rate = 0
    static constraints = {
        agent(nullable: true)
        ip(blank: false)
        rate(blank:false)
    }
    static belongsTo = [agent: Agent]

    String toString() {
       "$rate"
    }
}
