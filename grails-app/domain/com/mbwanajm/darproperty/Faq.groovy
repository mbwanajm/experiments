package com.mbwanajm.darproperty

class Faq {
String question
String answer
    static constraints = {
		question(nullable: false)
		answer(nullable: false, maxSize: 1000)
    }
}
