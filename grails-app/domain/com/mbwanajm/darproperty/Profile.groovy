package com.mbwanajm.darproperty

class Profile {
    static searchable = true
    String firstName
    String middleName
    String lastName
    String email
    int mailing = 1

    byte[] photo

    String phone
    String company

    static constraints = {
        email(email: true)
        firstName(blank: false)
        middleName(blank: true)
        lastName(blank: false)
        photo(nullable: true, maxSize: 10485760)// 10MB
    }

    static belongsTo = Agent

    String toString() {
        "$firstName $middleName $lastName"
    }

}
