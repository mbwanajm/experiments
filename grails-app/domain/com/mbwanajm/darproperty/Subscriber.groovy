package com.mbwanajm.darproperty

class Subscriber {
    String firstname
    String lastname
    String email
    static constraints = {
        firstname(nullable: false)
        lastname(nullable: false)
        email(unique: true, blank: false)
    }
}
