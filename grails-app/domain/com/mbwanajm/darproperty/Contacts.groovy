package com.mbwanajm.darproperty

class Contacts {
String name
    String email
    String phone
    String message
    String page
    static constraints = {
        name(blank: false)
        email(email: true,blank: false)
        phone(blank: false,minSize: 9)
        message(nullable: false, maxSize: 1200)
    }
}
