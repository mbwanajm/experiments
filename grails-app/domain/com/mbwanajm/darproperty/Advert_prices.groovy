package com.mbwanajm.darproperty

class Advert_prices {
String advert_type
double price_per_day
String link_to_table
    static constraints = {
		advert_type(blank:false,unique: true )
		price_per_day(blank:false)
		link_to_table(blank:true,nullable:true)
		}
}
