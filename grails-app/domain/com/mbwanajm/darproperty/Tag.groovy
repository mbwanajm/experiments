package com.mbwanajm.darproperty

class Tag {
    static searchable = true
    String name

    static constraints = {
        name(blank: false)
    }

    String toString() { name }

}
