package com.mbwanajm.darproperty

class Interestrates {

    String country
    double rate = 0
    Banks bank
    static constraints = {
        country(blank: false)
        bank(blank: false)
    }

}
