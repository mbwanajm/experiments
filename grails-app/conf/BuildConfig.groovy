grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.source.level = 1.6

//grails.project.war.file = "target/${appName}-${appVersion}.war"
grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // uncomment to disable ehcache
        // excludes 'ehcache'
    }
    log "warn" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    repositories {
        inherits true

        grailsPlugins()
        grailsHome()
        grailsCentral()

        // uncomment the below to enable remote dependency resolution
        // from public Maven repositories
        mavenLocal()
        mavenCentral()
        mavenRepo "http://snapshots.repository.codehaus.org"
        mavenRepo "http://repository.codehaus.org"
        mavenRepo "http://download.java.net/maven/2/"
        mavenRepo "http://repository.jboss.com/maven2/"
        mavenRepo "https://repo.grails.org/grails/plugins"
    }
    dependencies {
        // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes eg.
        runtime 'net.coobird:thumbnailator:0.4.2'
        runtime 'mysql:mysql-connector-java:5.1.20'
    }

    plugins {
        compile ':disqus:0.1'
        //compile ":catharsis-widgets:2.7"
        compile ':mail:1.0.1'
        //':quartz:1.0-RC4'
		//compile ":spring-social-core:0.1.31"
		//social plugin
		//compile ":spring-social-facebook:0.1.32"
		//facebook plugin

        test(":spock:0.7") {
            exclude "spock-grails-support"
        }

        runtime ":hibernate:$grailsVersion"
        runtime ":jquery:1.8.3"
        //runtime ":resources:1.1.6"

        // Uncomment these (or add new ones) to enable additional resources capabilities
        //runtime ":zipped-resources:1.0"
        //runtime ":cached-resources:1.0"
        //runtime ":yui-minify-resources:0.1.4"

        build ":tomcat:$grailsVersion"
        runtime ":database-migration:1.2.1"
        compile ':cache:1.0.1'
        compile ":spring-security-core:1.2.7.3"
        compile "org.grails.plugins:executor:0.3"


        compile ":searchable:0.6.4"
        compile ":simple-captcha:0.9.9"
        //compile ":google-analytics:2.0"
    }
}
