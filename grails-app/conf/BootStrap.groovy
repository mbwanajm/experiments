import com.mbwanajm.darproperty.*

class BootStrap {
    def springSecurityService

    def init = { servletContext ->

        // Set default roles
        def userRole = Role.findByAuthority('ROLE_AGENT') ?: new Role(authority: 'ROLE_AGENT').save(failOnError: true)
        def adminRole = Role.findByAuthority('ROLE_ADMIN') ?: new Role(authority: 'ROLE_ADMIN').save(failOnError: true)

        // add admin user
        def adminUser = User.findByUsername('admin') ?: new User(
                username: 'admin',
                password: 'nimda',
                enabled: true).save(failOnError: true)

        if (!adminUser.authorities.contains(adminRole)) {
            UserRole.create adminUser, adminRole
        }

        // AdvertTypes

        // install countries
        installCountries()


    }

    def installCountries() {
        Country tanzania = Country.findByName("Tanzania")
        if (tanzania) return

        tanzania = new Country(name: "Tanzania")
        Region.findAll().each { Region region ->
            tanzania.addToRegions(region)
        }

        tanzania.save()

        Property.findAll().each { Property property ->
            property.country = tanzania
            property.save()
        }



        Country kenya = new Country(name: "Kenya")


        Region nairobi = new Region(name: "Nairobi")
        '''Bahati
Benhard Estate
Bernhard Estate
Chiromo
City Estate
Dagoretti
Dagoretti Corner
Dandora
Doonholm
Eastleigh
Embakasi
Empakasi
Gachie
Gathanga
Gatharaini
Harambee
Highridge
Jamhuri Park
Kabete
Kahawa
Kaloleni
Kariobangi
Kariobangi South
Karura
Karura Farm
Kassarani
Kerichwa Kubwa
Kibera
Kibera Special Settlement Area
Kileleshwa
Kileleshwan
Kilimani Estate
Kwangwari
Makadara
Mbagathi
Mbari Ya Njiru
Mbotela
Mitchell Park
Moi
Muchatha
Mutei
Muthaiga
Mutuini
Nairoba
Nairobi
Nairobi Hill
Nairobi Industrial Area
Nairobi South
Nairobi West
Ofafa
Otiende
Outer Ring Road
Pangani
Parklands
Pumuani
Pumwani
Riruta
Ruaka
Ruiru
Shauri Moyo
Spring Valley
Thomson Estate
Uhuru
Upper Hill
Upper Parklands Estate
Uthiru
Villa Franca Dairy Farm
Waithaka
Wattle Blossom Farm
Westlands
Woodley Estate'''.eachLine { String area ->
            nairobi.addToAreas(area.trim())
        }

        nairobi.save()

        Region mombasa = new Region(name: 'Mombasa')
        '''Old Mombasa
Mvita
Old Town
Kisaoni
Kisauni
Nyali
Shimanzi
Shamanzi
Junda
Mtongwe
Kipevu
Likoni
Kindunguni
Frere Town
Changamwe
Mkunguni
Mto Panga
Mirarani
Bamburi
Kwa Jomvu
Jumvu
Jomvu
Mwakirunge
Mwachirun'''.eachLine { String area ->
            mombasa.addToAreas(area.trim())
        }
        mombasa.save()

        kenya.addToRegions(nairobi)
        kenya.addToRegions(mombasa)
        kenya.save()

    }

    def destroy = {

    }
}
