-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 07, 2016 at 10:26 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `darproperty`
--

-- --------------------------------------------------------

--
-- Table structure for table `advert`
--

CREATE TABLE IF NOT EXISTS `advert` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `photo` mediumblob NOT NULL,
  `type_id` bigint(20) NOT NULL,
  `url` varchar(255) NOT NULL,
  `date` datetime DEFAULT NULL,
  `views` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKAB3E6FD4FA8E55E` (`type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

-- --------------------------------------------------------

--
-- Table structure for table `advert_prices`
--

CREATE TABLE IF NOT EXISTS `advert_prices` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `advert_type` varchar(255) NOT NULL,
  `link_to_table` varchar(255) DEFAULT NULL,
  `price_per_day` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `advert_type` (`advert_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `advert_type`
--

CREATE TABLE IF NOT EXISTS `advert_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `agent_rates`
--

CREATE TABLE IF NOT EXISTS `agent_rates` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `agent_id` bigint(20) DEFAULT NULL,
  `ip` varchar(255) NOT NULL,
  `rate` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK76D8DDD9ABA385AA` (`agent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amenities`
--

CREATE TABLE IF NOT EXISTS `amenities` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL DEFAULT '15',
  `holder` varchar(255) DEFAULT NULL,
  `point` double NOT NULL,
  `title` varchar(200) NOT NULL,
  `visible` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `amenities_tag`
--

CREATE TABLE IF NOT EXISTS `amenities_tag` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL DEFAULT '15',
  `amenities_id` bigint(20) NOT NULL,
  `tag_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `amenities_tag_view`
--
CREATE TABLE IF NOT EXISTS `amenities_tag_view` (
`id` bigint(20)
,`Ame.Title` varchar(200)
,`Tg.Name` varchar(255)
,`point` double
);
-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `content` varchar(20000) NOT NULL,
  `date_created` datetime NOT NULL,
  `source` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` mediumblob,
  `tag` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=91 ;

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE IF NOT EXISTS `banks` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL DEFAULT '0',
  `bankname` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `bankname` (`bankname`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `message` varchar(1200) NOT NULL,
  `name` varchar(255) NOT NULL,
  `page` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `country_region`
--

CREATE TABLE IF NOT EXISTS `country_region` (
  `country_regions_id` bigint(20) DEFAULT NULL,
  `region_id` bigint(20) DEFAULT NULL,
  KEY `FK7CF137BDBDC98B4A` (`country_regions_id`),
  KEY `FK7CF137BD4F81B12A` (`region_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE IF NOT EXISTS `currency` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL DEFAULT '0',
  `country` varchar(255) NOT NULL,
  `tag` varchar(255) NOT NULL,
  `usd_value` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tag` (`tag`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `enquiries`
--

CREATE TABLE IF NOT EXISTS `enquiries` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `area` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `description` varchar(500) NOT NULL,
  `email` varchar(255) NOT NULL,
  `max_price` double NOT NULL,
  `min_price` double NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `region` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE IF NOT EXISTS `faq` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `answer` varchar(1000) NOT NULL,
  `question` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `featured_property`
--

CREATE TABLE IF NOT EXISTS `featured_property` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `area` varchar(255) NOT NULL,
  `country_id` bigint(20) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `date_added` datetime DEFAULT NULL,
  `description` varchar(500) NOT NULL,
  `descriptionsw` varchar(255) NOT NULL,
  `expire_date` datetime DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `price` double NOT NULL,
  `receipts_id` bigint(20) NOT NULL,
  `region` varchar(255) NOT NULL,
  `sale_type_id` bigint(20) NOT NULL,
  `size` double NOT NULL,
  `type_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKEAF8FEC636E33059` (`sale_type_id`),
  KEY `FKEAF8FEC64565FBCA` (`receipts_id`),
  KEY `FKEAF8FEC6D309AC0A` (`country_id`),
  KEY `FKEAF8FEC62ABCBF6A` (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `featured_property_photo`
--

CREATE TABLE IF NOT EXISTS `featured_property_photo` (
  `featured_property_photos_id` bigint(20) DEFAULT NULL,
  `photo_id` bigint(20) DEFAULT NULL,
  KEY `FKD24BF379CD185C36` (`featured_property_photos_id`),
  KEY `FKD24BF3798F21A8A` (`photo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `featured_property_tag`
--

CREATE TABLE IF NOT EXISTS `featured_property_tag` (
  `featured_property_tags_id` bigint(20) DEFAULT NULL,
  `tag_id` bigint(20) DEFAULT NULL,
  KEY `FKB55D1AA1E771AF5E` (`featured_property_tags_id`),
  KEY `FKB55D1AA1BDFA058A` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `interestrates`
--

CREATE TABLE IF NOT EXISTS `interestrates` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL DEFAULT '0',
  `bank` bigint(20) NOT NULL,
  `country` varchar(255) NOT NULL,
  `rate` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `bank` (`bank`),
  KEY `bank_2` (`bank`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `message` varchar(1200) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1231 ;

-- --------------------------------------------------------

--
-- Table structure for table `monthly_issue`
--

CREATE TABLE IF NOT EXISTS `monthly_issue` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `embed_code` varchar(1000) NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` mediumblob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `our_bank_parteners`
--
CREATE TABLE IF NOT EXISTS `our_bank_parteners` (
`id` bigint(20)
,`bankname` varchar(255)
,`email` varchar(255)
);
-- --------------------------------------------------------

--
-- Table structure for table `parteners_banks`
--

CREATE TABLE IF NOT EXISTS `parteners_banks` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL DEFAULT '0',
  `bank_email` varchar(255) NOT NULL,
  `bank_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `photo`
--

CREATE TABLE IF NOT EXISTS `photo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `description` varchar(300) DEFAULT NULL,
  `image` mediumblob NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2047 ;

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE IF NOT EXISTS `profile` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `company` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `photo` mediumblob,
  `mailing` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2148 ;

-- --------------------------------------------------------

--
-- Table structure for table `property`
--

CREATE TABLE IF NOT EXISTS `property` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `agent_id` bigint(20) DEFAULT NULL,
  `currency` varchar(255) NOT NULL,
  `date_added` datetime DEFAULT NULL,
  `description` varchar(500) NOT NULL,
  `location` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `number_of_rooms` int(11) NOT NULL,
  `price` double DEFAULT '0',
  `sale_type_id` bigint(20) NOT NULL,
  `size` double NOT NULL,
  `type_id` bigint(20) NOT NULL,
  `views` int(11) DEFAULT NULL,
  `number_of_bath_rooms` int(11) NOT NULL,
  `area` varchar(255) NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `deleted_on` datetime DEFAULT NULL,
  `region` varchar(255) NOT NULL,
  `descriptionsw` varchar(255) NOT NULL,
  `country_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKC4CDDDD536E33059` (`sale_type_id`),
  KEY `FKC4CDDDD5ABA385AA` (`agent_id`),
  KEY `FKC4CDDDD52ABCBF6A` (`type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=613 ;

-- --------------------------------------------------------

--
-- Table structure for table `property_message`
--

CREATE TABLE IF NOT EXISTS `property_message` (
  `property_messages_id` bigint(20) DEFAULT NULL,
  `message_id` bigint(20) DEFAULT NULL,
  KEY `FKDE0BDF9DF1023749` (`property_messages_id`),
  KEY `FKDE0BDF9DAABC0A6A` (`message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `property_photo`
--

CREATE TABLE IF NOT EXISTS `property_photo` (
  `property_photos_id` bigint(20) DEFAULT NULL,
  `photo_id` bigint(20) DEFAULT NULL,
  KEY `FKA43B0B486F0D9574` (`property_photos_id`),
  KEY `FKA43B0B488F21A8A` (`photo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `property_report`
--
CREATE TABLE IF NOT EXISTS `property_report` (
`id` bigint(20)
,`photo_id` bigint(20)
,`image` mediumblob
,`photo_name` varchar(255)
,`photo_description` varchar(300)
,`version` bigint(20)
,`agent_id` bigint(20)
,`company` varchar(255)
,`currency` varchar(255)
,`price` double
,`sale_type` varchar(255)
,`date_added` datetime
,`description` varchar(500)
,`location` varchar(255)
,`property_name` varchar(255)
,`number_of_rooms` int(11)
,`size` double
,`type` varchar(255)
,`views` int(11)
,`number_of_bath_rooms` int(11)
,`area` varchar(255)
,`region` varchar(255)
,`country` varchar(255)
);
-- --------------------------------------------------------

--
-- Table structure for table `property_tag`
--

CREATE TABLE IF NOT EXISTS `property_tag` (
  `property_tags_id` bigint(20) DEFAULT NULL,
  `tag_id` bigint(20) DEFAULT NULL,
  KEY `FKCC7DDA309FEB05C` (`property_tags_id`),
  KEY `FKCC7DDA30BDFA058A` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `receipts`
--

CREATE TABLE IF NOT EXISTS `receipts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `company` varchar(255) DEFAULT NULL,
  `date_received` datetime NOT NULL,
  `email` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) NOT NULL,
  `references_table_id` bigint(20) NOT NULL,
  `tel` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKCFCBD53B57B5064A` (`references_table_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `references_table`
--

CREATE TABLE IF NOT EXISTS `references_table` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `amount` double NOT NULL,
  `date` datetime NOT NULL,
  `ref_no` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ref_no` (`ref_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `region`
--

CREATE TABLE IF NOT EXISTS `region` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `regional_area`
--
CREATE TABLE IF NOT EXISTS `regional_area` (
`id` bigint(20)
,`Region` varchar(255)
,`Area` varchar(255)
,`Point %` double
);
-- --------------------------------------------------------

--
-- Table structure for table `region_areas`
--

CREATE TABLE IF NOT EXISTS `region_areas` (
  `region_id` bigint(20) DEFAULT NULL,
  `areas_string` varchar(255) DEFAULT NULL,
  `id` bigint(20) NOT NULL,
  `version` bigint(20) NOT NULL,
  `point` double NOT NULL DEFAULT '15',
  KEY `FKB22CC47B4F81B12A` (`region_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `region_region_areas`
--

CREATE TABLE IF NOT EXISTS `region_region_areas` (
  `region_region_areas_id` bigint(20) DEFAULT NULL,
  `region_areas_id` bigint(20) DEFAULT NULL,
  KEY `FK54D5F3E6B8FE66F8` (`region_region_areas_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `authority` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `authority` (`authority`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `sale_type`
--

CREATE TABLE IF NOT EXISTS `sale_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `subaminities`
--

CREATE TABLE IF NOT EXISTS `subaminities` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `point` double NOT NULL,
  `subject` varchar(200) NOT NULL,
  `visible` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `subscriber`
--

CREATE TABLE IF NOT EXISTS `subscriber` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE IF NOT EXISTS `tag` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

CREATE TABLE IF NOT EXISTS `type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `account_expired` bit(1) NOT NULL,
  `account_locked` bit(1) NOT NULL,
  `enabled` bit(1) NOT NULL,
  `password` varchar(255) NOT NULL,
  `password_expired` bit(1) NOT NULL,
  `username` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `profile_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `FK36EBCB826D0F2A` (`profile_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE IF NOT EXISTS `user_role` (
  `role_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`role_id`,`user_id`),
  KEY `FK143BF46AAFA665EA` (`role_id`),
  KEY `FK143BF46A54D129CA` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_property_image`
--
CREATE TABLE IF NOT EXISTS `view_property_image` (
`property_photos_id` bigint(20)
,`name` varchar(255)
,`photo_id` bigint(20)
,`image` mediumblob
,`description` varchar(300)
,`photo_name` varchar(255)
);
-- --------------------------------------------------------

--
-- Structure for view `amenities_tag_view`
--
DROP TABLE IF EXISTS `amenities_tag_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `amenities_tag_view` AS select `amt`.`id` AS `id`,`am`.`title` AS `Ame.Title`,`ta`.`name` AS `Tg.Name`,`am`.`point` AS `point` from ((`amenities_tag` `amt` join `amenities` `am`) join `tag` `ta`) where ((`amt`.`amenities_id` = `am`.`id`) and (`amt`.`tag_id` = `ta`.`id`));

-- --------------------------------------------------------

--
-- Structure for view `our_bank_parteners`
--
DROP TABLE IF EXISTS `our_bank_parteners`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `our_bank_parteners` AS select `p`.`id` AS `id`,`b`.`bankname` AS `bankname`,`p`.`bank_email` AS `email` from (`banks` `b` join `parteners_banks` `p`) where (`p`.`bank_id` = `b`.`id`);

-- --------------------------------------------------------

--
-- Structure for view `property_report`
--
DROP TABLE IF EXISTS `property_report`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `property_report` AS select `p`.`id` AS `id`,`pp`.`photo_id` AS `photo_id`,`ph`.`image` AS `image`,`ph`.`name` AS `photo_name`,`ph`.`description` AS `photo_description`,`p`.`version` AS `version`,`pr`.`id` AS `agent_id`,`pr`.`company` AS `company`,`p`.`currency` AS `currency`,`p`.`price` AS `price`,`s`.`name` AS `sale_type`,`p`.`date_added` AS `date_added`,`p`.`description` AS `description`,`p`.`location` AS `location`,`p`.`name` AS `property_name`,`p`.`number_of_rooms` AS `number_of_rooms`,`p`.`size` AS `size`,`t`.`name` AS `type`,`p`.`views` AS `views`,`p`.`number_of_bath_rooms` AS `number_of_bath_rooms`,`p`.`area` AS `area`,`p`.`region` AS `region`,`c`.`name` AS `country` from ((((((`property` `p` join `property_photo` `pp`) join `photo` `ph`) join `profile` `pr`) join `sale_type` `s`) join `country` `c`) join `type` `t`) where ((`p`.`id` = `pp`.`property_photos_id`) and (`pp`.`photo_id` = `ph`.`id`) and (`p`.`agent_id` = `pr`.`id`) and (`p`.`sale_type_id` = `s`.`id`) and (`p`.`type_id` = `t`.`id`) and (`p`.`country_id` = `c`.`id`));

-- --------------------------------------------------------

--
-- Structure for view `regional_area`
--
DROP TABLE IF EXISTS `regional_area`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `regional_area` AS select `ra`.`id` AS `id`,`r`.`name` AS `Region`,`ra`.`areas_string` AS `Area`,`ra`.`point` AS `Point %` from (`region_areas` `ra` join `region` `r`) where (`ra`.`region_id` = `r`.`id`);

-- --------------------------------------------------------

--
-- Structure for view `view_property_image`
--
DROP TABLE IF EXISTS `view_property_image`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_property_image` AS select `pp`.`property_photos_id` AS `property_photos_id`,`p`.`name` AS `name`,`pp`.`photo_id` AS `photo_id`,`ph`.`image` AS `image`,`ph`.`description` AS `description`,`ph`.`name` AS `photo_name` from ((`property_photo` `pp` join `property` `p`) join `photo` `ph`) where ((`ph`.`id` = `pp`.`photo_id`) and (`pp`.`property_photos_id` = `p`.`id`));

--
-- Constraints for dumped tables
--

--
-- Constraints for table `advert`
--
ALTER TABLE `advert`
  ADD CONSTRAINT `FKAB3E6FD4FA8E55E` FOREIGN KEY (`type_id`) REFERENCES `advert_type` (`id`);

--
-- Constraints for table `agent_rates`
--
ALTER TABLE `agent_rates`
  ADD CONSTRAINT `FK76D8DDD9ABA385AA` FOREIGN KEY (`agent_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `country_region`
--
ALTER TABLE `country_region`
  ADD CONSTRAINT `FK7CF137BD4F81B12A` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`),
  ADD CONSTRAINT `FK7CF137BDBDC98B4A` FOREIGN KEY (`country_regions_id`) REFERENCES `country` (`id`);

--
-- Constraints for table `featured_property`
--
ALTER TABLE `featured_property`
  ADD CONSTRAINT `FKEAF8FEC62ABCBF6A` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`),
  ADD CONSTRAINT `FKEAF8FEC636E33059` FOREIGN KEY (`sale_type_id`) REFERENCES `sale_type` (`id`),
  ADD CONSTRAINT `FKEAF8FEC64565FBCA` FOREIGN KEY (`receipts_id`) REFERENCES `receipts` (`id`),
  ADD CONSTRAINT `FKEAF8FEC6D309AC0A` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`);

--
-- Constraints for table `featured_property_photo`
--
ALTER TABLE `featured_property_photo`
  ADD CONSTRAINT `FKD24BF3798F21A8A` FOREIGN KEY (`photo_id`) REFERENCES `photo` (`id`),
  ADD CONSTRAINT `FKD24BF379CD185C36` FOREIGN KEY (`featured_property_photos_id`) REFERENCES `featured_property` (`id`);

--
-- Constraints for table `featured_property_tag`
--
ALTER TABLE `featured_property_tag`
  ADD CONSTRAINT `FKB55D1AA1BDFA058A` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`),
  ADD CONSTRAINT `FKB55D1AA1E771AF5E` FOREIGN KEY (`featured_property_tags_id`) REFERENCES `featured_property` (`id`);

--
-- Constraints for table `interestrates`
--
ALTER TABLE `interestrates`
  ADD CONSTRAINT `bank-id` FOREIGN KEY (`bank`) REFERENCES `darproperty_current`.`banks` (`id`);

--
-- Constraints for table `property`
--
ALTER TABLE `property`
  ADD CONSTRAINT `FKC4CDDDD52ABCBF6A` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`),
  ADD CONSTRAINT `FKC4CDDDD536E33059` FOREIGN KEY (`sale_type_id`) REFERENCES `sale_type` (`id`),
  ADD CONSTRAINT `FKC4CDDDD5ABA385AA` FOREIGN KEY (`agent_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `property_message`
--
ALTER TABLE `property_message`
  ADD CONSTRAINT `FKDE0BDF9DAABC0A6A` FOREIGN KEY (`message_id`) REFERENCES `message` (`id`),
  ADD CONSTRAINT `FKDE0BDF9DF1023749` FOREIGN KEY (`property_messages_id`) REFERENCES `property` (`id`);

--
-- Constraints for table `property_photo`
--
ALTER TABLE `property_photo`
  ADD CONSTRAINT `FKA43B0B486F0D9574` FOREIGN KEY (`property_photos_id`) REFERENCES `property` (`id`),
  ADD CONSTRAINT `FKA43B0B488F21A8A` FOREIGN KEY (`photo_id`) REFERENCES `photo` (`id`);

--
-- Constraints for table `property_tag`
--
ALTER TABLE `property_tag`
  ADD CONSTRAINT `FKCC7DDA309FEB05C` FOREIGN KEY (`property_tags_id`) REFERENCES `property` (`id`),
  ADD CONSTRAINT `FKCC7DDA30BDFA058A` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`);

--
-- Constraints for table `receipts`
--
ALTER TABLE `receipts`
  ADD CONSTRAINT `FKCFCBD53B57B5064A` FOREIGN KEY (`references_table_id`) REFERENCES `references_table` (`id`);

--
-- Constraints for table `region_areas`
--
ALTER TABLE `region_areas`
  ADD CONSTRAINT `FKB22CC47B4F81B12A` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`);

--
-- Constraints for table `region_region_areas`
--
ALTER TABLE `region_region_areas`
  ADD CONSTRAINT `FK54D5F3E6B8FE66F8` FOREIGN KEY (`region_region_areas_id`) REFERENCES `region` (`id`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FK36EBCB826D0F2A` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`id`);

--
-- Constraints for table `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `FK143BF46A54D129CA` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK143BF46AAFA665EA` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
